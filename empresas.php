 <!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <!-- SEO Meta Tags -->
    <meta name="description" content="IDentiKIT.app para empresas - ¡Incorpora el mejor talento joven de LATAM!">
    <meta name="author" content="IDentiKIT.app ">

    <!-- OG Meta Tags to improve the way the post looks when you share the page on LinkedIn, Facebook, Google+ -->
    <meta property="og:site_name" content="IDentiKIT - Tu primera experiencia laboral" /> <!-- website name -->
    <meta property="og:site" content="https://identikit.app/" /> <!-- website link -->
    <meta property="og:title" content="IDentiKIT.app para empresas - ¡Incorpora el mejor talento joven de LATAM!"/> <!-- title shown in the actual shared post -->
    <meta property="og:description" content="Somos la primera academia diseñada para resaltar la empleabilidad joven. Aplica ahora y mejora tu futuro." />

     <!-- description shown in the actual shared post -->
    
    <meta property="og:url" content="https://identikit.app/" /> <!-- where do you want your post to link to -->
    <meta property="og:type" content="article" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   
    <meta property="og:image" content="http://identikit.app/landingv1/v3/images/logo-og.png" />
    <meta property="og:image:secure_url" content="https://identikit.app/landingv1/v3/images/logo-og.png" />
    <meta property="og:image:type" content="image/png" />
    <meta property="og:image:width" content="300" />
    <meta property="og:image:height" content="300" />



    <!-- Website Title -->
    <title>IDentiKIT.app para empresas - ¡Incorpora el mejor talento joven de LATAM!</title>
    
    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700&display=swap&subset=latin-ext" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/fontawesome-all.css" rel="stylesheet">
    <link href="css/swiper.css" rel="stylesheet">
    <link href="css/magnific-popup.css" rel="stylesheet">
    <link href="css/styles.css" rel="stylesheet">
    
    <!-- Favicon  -->
    <link rel="icon" href="images/logo.png">

      <!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-K2NHRZT4R7"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'G-K2NHRZT4R7');
	</script>

	<!-- Google Tag Manager -->
	<script>
		(function(w, d, s, l, i) {
			w[l] = w[l] || [];
			w[l].push({
				'gtm.start': new Date().getTime(),
				event: 'gtm.js'
			});
			var f = d.getElementsByTagName(s)[0],
				j = d.createElement(s),
				dl = l != 'dataLayer' ? '&l=' + l : '';
			j.async = true;
			j.src =
				'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
			f.parentNode.insertBefore(j, f);
		})(window, document, 'script', 'dataLayer', 'GTM-5QGSH5J');
	</script>
	<!-- End Google Tag Manager -->

</head>
<body data-spy="scroll" data-target=".fixed-top">

     <!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QGSH5J" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
    
    <?php require_once "wsp2.php";?>
    <!-- Preloader -->
    <div class="spinner-wrapper">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>
    <!-- end of preloader -->
    

    <!-- Navigation -->
   <?php include_once "nav.php";?>


    <!-- Header -->
    <header id="header" class="header">
        <div class="header-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-xl-5">
                        <div class="text-container">
                            <h1>¿Buscas incorporar talento joven a tu equipo?</h1>
                            <p class="p-large">En IDentiKIT encontrarás al mejor talento joven de LATAM </p>

                            <a class="btn-solid-lg page-scroll" href="#features">Descubre más</a>
                            


                        </div> <!-- end of text-container -->
                    </div> <!-- end of col -->
                    <div class="col-lg-6 col-xl-7">
                        <div class="image-container">
                            <div class="img-wrapper">
                                <img class="img-fluid" src="images/empresa.png" alt="alternative">
                            </div> <!-- end of img-wrapper -->
                        </div> <!-- end of image-container -->
                    </div> <!-- end of col -->
                </div> <!-- end of row -->
            </div> <!-- end of container -->
        </div> <!-- end of header-content -->
    </header> <!-- end of header -->
    <svg class="header-frame" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 1920 310"><defs><style>.cls-1{fill:#0070ff;}</style></defs><title>header-frame</title><path class="cls-1" d="M0,283.054c22.75,12.98,53.1,15.2,70.635,14.808,92.115-2.077,238.3-79.9,354.895-79.938,59.97-.019,106.17,18.059,141.58,34,47.778,21.511,47.778,21.511,90,38.938,28.418,11.731,85.344,26.169,152.992,17.971,68.127-8.255,115.933-34.963,166.492-67.393,37.467-24.032,148.6-112.008,171.753-127.963,27.951-19.26,87.771-81.155,180.71-89.341,72.016-6.343,105.479,12.388,157.434,35.467,69.73,30.976,168.93,92.28,256.514,89.405,100.992-3.315,140.276-41.7,177-64.9V0.24H0V283.054Z"/></svg>
    <!-- end of header -->
    <!-- Customers -->
    <div class="slider-1">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    
                    <!-- Image Slider -->
                    <div class="slider-container">
                        <div class="swiper-container image-slider">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                        <img class="img-fluid" src="images/customer-logo-5.png" alt="alternative">
                                </div>
                                <div class="swiper-slide">
                                        <img class="img-fluid" src="images/customer-logo-2.png" alt="alternative">
                                </div>
                                <div class="swiper-slide">
                                        <img class="img-fluid" src="images/customer-logo-3.png" alt="alternative">
                                </div>
                                <div class="swiper-slide">
                                        <img class="img-fluid" src="images/customer-logo-4.png" alt="alternative">
                                </div>
                                <div class="swiper-slide">
                                        <img class="img-fluid" src="images/customer-logo-1.png" alt="alternative">
                                </div>
                                <div class="swiper-slide">
                                        <img class="img-fluid" src="images/customer-logo-6.png" alt="alternative">
                                </div>
                            </div> <!-- end of swiper-wrapper -->
                        </div> <!-- end of swiper container -->
                    </div> <!-- end of slider-container -->
                    <!-- end of image slider -->

                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of slider-1 -->
    <!-- end of customers -->

    <div class="cards-1">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="h2-heading" style="color:#0070ff; margin-bottom: 20px !important;">¿Que somos?</h2>
                    <p class="p-heading">Una academia para ayudar a los jovenes a conseguir su primer empleo. Nuestros expertos los guiarán, acompañarán y capacitarán de manera personalizada en todo el proceso hasta que consigan su primer trabajo.</p>
                </div> 
            </div> 
            <div class="row">
                <div class="col-lg-12">

                    <div class="card">
                        <div class="card-image">
                            <img class="img-fluid" src="images/empresa1.png" alt="alternative">
                        </div>
                        <div class="card-body">
                            <h4 class="card-title" style="color:#f9de2a;">Inclusion social y laboral</h4>
                            <p>Tod@s los jovenes tiene derecho a su primer trabajo</p>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-image">
                            <img class="img-fluid" src="images/empresa2.png" alt="alternative">
                        </div>
                        <div class="card-body">
                            <h4 class="card-title" style="color:#f9de2a;">Creemos los jovenes</h4>
                            <p>Sabemos perfectamente el potencial que tienen</p>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-image">
                            <img class="img-fluid" src="images/empresa3.png" alt="alternative">
                        </div>
                        <div class="card-body">
                            <h4 class="card-title" style="color:#f9de2a;">Habilidad</h4>
                            <p>Los preparamos con una habilidad del futuro para mejorar su empleabilidad</p>    
                        </div>
                    </div>

                </div> 
            </div> 
        </div>
      
    </div>
    <!-- Features -->
    <div id="features" class="tabs">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="above-heading">Bootcamp</div>
                   <h3 class="h2-heading">¡No existe una clase que enseñe a buscar trabajo!</h3>
                    <p class="p-heading">Diseñamos una academia junto a los mejores profesionales de RRHH para ayudar a los jovenes a ingresar en el mundo laboral. Un bootcamp  en el que van a poder acceder a contenido educativo para conseguir su primer empleo y además aprender una habilidad del futuro.</p>
                </div> <!-- end of col -->  
            </div> <!-- end of row -->
            <div class="row">
                <div class="col-lg-12">

                    <!-- Tabs Links -->
                    <ul class="nav nav-tabs" id="argoTabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="nav-tab-1" data-toggle="tab" href="#tab-1" role="tab" aria-controls="tab-1" aria-selected="true"><img src="images/users.png" width="30"> Contratar talento joven</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="nav-tab-2" data-toggle="tab" href="#tab-2" role="tab" aria-controls="tab-2" aria-selected="false"><img src="images/man-teacher.png" width="30"> Capacitar jóvenes de mi empresa</a>
                        </li>
                        
                    </ul>
                    <!-- end of tabs links -->

                    <!-- Tabs Content -->
                    <div class="tab-content" id="argoTabsContent">

                        <!-- Tab -->
                        <div class="tab-pane fade show active" id="tab-1" role="tabpanel" aria-labelledby="tab-1">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="image-container">
                                        <img class="img-fluid" src="images/features-1.png" alt="alternative">
                                    </div> <!-- end of image-container -->
                                </div> <!-- end of col -->
                                <div class="col-lg-6">
                                    <div class="text-container">
                                        <h3>¡Incorpora el mejor talento joven de LATAM!</h3>
                                        <p>En IDentiKIT puedes contratar talento joven de una forma muy fácil, rápida y segura.</p>
                                        <ul class="list-unstyled li-space-lg">
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Registra tu empresa</div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Completa tu perfil</div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Carga tus vacantes</div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">¡Carga 3 vacantes y recibe hasta 15 candidatos gratis!</div>
                                            </li>
                                        </ul>
                                        <a class="btn-solid-reg " href="app/views/auth/registro2.php" style="color: white;">Contratar</a>
                                    </div> <!-- end of text-container -->
                                </div> <!-- end of col -->
                            </div> <!-- end of row -->
                        </div> <!-- end of tab-pane -->
                        <!-- end of tab -->

                        <!-- Tab -->
                        <div class="tab-pane fade" id="tab-2" role="tabpanel" aria-labelledby="tab-2">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="image-container">
                                        <img class="img-fluid" src="images/features-3.png" alt="alternative">
                                    </div> <!-- end of image-container -->
                                </div> <!-- end of col -->
                                <div class="col-lg-6">
                                    <div class="text-container">
                                        <h3>¡Capacitamos a tu talento joven!</h3>
                                        <p>Te ofrecemos un pack de 3 módulos en los cuales capacitamos a tus talentos jóvenes con habilidades blandas. Ademas, tenes la opcion de prepararlos para que aprendan una habilidad del futuro (Programación fullstack, marketing digital, diseño UX/UI) </p>
                                        <ul class="list-unstyled li-space-lg">
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Modulo "¡Aprendamos juntos!"</div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Modulo "¡A practicar!"</div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Modulo "IDentiKIT, Linkedin & social"</div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Modulo "Nos importa tu futuro"</div>
                                            </li>
                                        </ul>
                                        <a class="btn-solid-reg" href="https://web.whatsapp.com/send?phone=5493584366602&amp;text=¡Hola, quiero capacitar a los jovenes de mi empresa con IDentiKIT.app para empresas! " style="color: white;">Capacitar</a>
                                    </div> <!-- end of text-container -->
                                </div> <!-- end of col -->
                            </div> <!-- end of row -->
                        </div> <!-- end of tab-pane -->
                        <!-- end of tab -->

                        
                        
                    </div> <!-- end of tab content -->
                    <!-- end of tabs content -->

                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of tabs -->
    <!-- end of features -->
    
    
    <!-- Video -->
    <div id="video" class="basic-2">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">

                    <!-- Video Preview -->
                    <div class="image-container">
                        <div class="video-wrapper">
                            <a class="popup-youtube" href="images/video-empresas.mp4" data-effect="fadeIn" loop>
                                <img class="img-fluid" src="images/video-image.png" alt="Video empresas IDentIKIT.app">
                                <span class="video-play-button">
                                    <span></span>
                                </span>
                            </a>
                        </div> <!-- end of video-wrapper -->
                    </div> <!-- end of image-container -->
                    <!-- end of video preview -->

                    <div class="p-heading">IDentiKIT es una plataforma que restalta la empleabilidad joven de una forma fácil, rápida y segura. </div>        
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of basic-2 -->
    <!-- end of video -->



    <!-- Testimonials -->
    <div class="slider-2">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">      
                    
                    <!-- Text Slider -->
                    <div class="slider-container">
                        <div class="swiper-container text-slider">
                            <div class="swiper-wrapper">
                                
                                <!-- Slide -->
                                <div class="swiper-slide">
                                    <div class="image-wrapper">
                                        <img class="img-fluid" src="images/testimonial-1.jpg" alt="alternative">
                                    </div> <!-- end of image-wrapper -->
                                    <div class="text-wrapper">
                                        <div class="testimonial-text">“Más que sólo lograr un objetivo fue más un crecimiento personal”</div>
                                        <div class="testimonial-author">Abril - Estudiante ultimo año secundaria</div>
                                    </div> <!-- end of text-wrapper -->
                                </div> <!-- end of swiper-slide -->
                                <!-- end of slide -->

                                <!-- Slide -->
                                <div class="swiper-slide">
                                    <div class="image-wrapper">
                                        <img class="img-fluid" src="images/testimonial-2.jpg" alt="alternative">
                                    </div> <!-- end of image-wrapper -->
                                    <div class="text-wrapper">
                                        <div class="testimonial-text">“IDentiKIT es una inversión a largo plazo porque lo que aprendes lo puedes aplicar en cualquier trabajo que busques”</div>
                                        <div class="testimonial-author">Julian - Estudiante diseño grafico</div>
                                    </div> <!-- end of text-wrapper -->
                                </div> <!-- end of swiper-slide -->
                                <!-- end of slide -->

                                <!-- Slide -->
                                <div class="swiper-slide">
                                    <div class="image-wrapper">
                                        <img class="img-fluid" src="images/testimonial-3.jpg" alt="alternative">
                                    </div> <!-- end of image-wrapper -->
                                    <div class="text-wrapper">
                                        <div class="testimonial-text">“Definitivamente IDentiKIT funciona genial, y más que todo es un aprendizaje para la vida”</div>
                                        <div class="testimonial-author">Marcos - programador autodidacta</div>
                                    </div> <!-- end of text-wrapper -->
                                </div> <!-- end of swiper-slide -->
                                <!-- end of slide -->

                                <!-- Slide -->
                                <div class="swiper-slide">
                                    <div class="image-wrapper">
                                        <img class="img-fluid" src="images/testimonial-4.jpg" alt="alternative">
                                    </div> <!-- end of image-wrapper -->
                                    <div class="text-wrapper">
                                        <div class="testimonial-text">“Se nota el amor con el que hacen las cosas y se ve en los resultados ”</div>
                                        <div class="testimonial-author">Leila - Viaja por el mundo</div>
                                    </div> <!-- end of text-wrapper -->
                                </div> <!-- end of swiper-slide -->
                                <!-- end of slide -->

                            </div> <!-- end of swiper-wrapper -->
                            
                            <!-- Add Arrows -->
                            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div>
                            <!-- end of add arrows -->

                        </div> <!-- end of swiper-container -->
                    </div> <!-- end of slider-container -->
                    <!-- end of text slider -->

                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of slider-2 -->
    <!-- end of testimonials -->




   <div class="footer">
       <div class="container">
           <div class="row">
               <div class="col-md-4">
                   <div class="footer-col first">
                       <h4>About IDentiKIT.app</h4>
                       <p class="p-small">IDentiKIT es una plataforma que busca resaltar la empleabilidad joven de una forma fácil, rápida y segura.</p>
                   </div>
               </div> <!-- end of col -->
               <div class="col-md-4">
                   <div class="footer-col middle">
                       <h4>Links</h4>
                       <ul class="list-unstyled li-space-lg p-small">
                           <li class="media">
                               <i class="fas fa-square"></i>
                               <div class="media-body"><a class="white" href="empresas.php">Empresas</a></div>
                           </li>
                           <li class="media">
                               <i class="fas fa-square"></i>
                               <div class="media-body"><a class="white" href="vacantes.php">Vacantes</a></div>
                           </li>
                            <li class="media">
                               <i class="fas fa-square"></i>
                               <div class="media-body"><a class="white" href="terms-conditions.php">Términos y Condiciones</a></div>
                           </li>
                            <li class="media">
                               <i class="fas fa-square"></i>
                               <div class="media-body"><a class="white" href="privacy-policy.php">Políticas de privacidad</a></div>
                           </li>
                       </ul>
                   </div>
               </div> <!-- end of col -->
               <div class="col-md-4">
                   <div class="footer-col last">
                       <h4>Contact</h4>
                       <ul class="list-unstyled li-space-lg p-small">
                           <li class="media">
                               <i class="fas fa-map-marker-alt"></i>
                               <div class="media-body">Argentina</div>
                           </li>
                           <li class="media">
                               <i class="fas fa-envelope"></i>
                               <div class="media-body"><a class="white" href="mailto:contacto@IDentiKIT.app">contacto@IDentiKIT.app</a> <i class="fas fa-globe"></i><a class="white" href="#home">IDentiKIT.app</a></div>
                           </li>
                       </ul>
                   </div> 
               </div> <!-- end of col -->
           </div> <!-- end of row -->
       </div> <!-- end of container -->
   </div> <!-- end of footer -->  
   <!-- end of footer -->
    
        
    <!-- Scripts -->
    <script src="js/jquery.min.js"></script> <!-- jQuery for Bootstrap's JavaScript plugins -->
    <script src="js/popper.min.js"></script> <!-- Popper tooltip library for Bootstrap -->
    <script src="js/bootstrap.min.js"></script> <!-- Bootstrap framework -->
    <script src="js/jquery.easing.min.js"></script> <!-- jQuery Easing for smooth scrolling between anchors -->
    <script src="js/swiper.min.js"></script> <!-- Swiper for image and text sliders -->
    <script src="js/jquery.magnific-popup.js"></script> <!-- Magnific Popup for lightboxes -->
    <script src="js/validator.min.js"></script> <!-- Validator.js - Bootstrap plugin that validates forms -->
    <script src="js/scripts.js"></script> <!-- Custom scripts -->
    <script src="js_whatsapp.js" type="text/javascript"></script>

</body>
</html>