<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <!-- SEO Meta Tags -->
    <meta name="description" content="Somos la primera academia diseñada para resaltar la empleabilidad joven. Aplica ahora y mejora tu futuro.">
    <meta name="author" content="IDentiKIT.app ">

    <!-- OG Meta Tags to improve the way the post looks when you share the page on LinkedIn, Facebook, Google+ -->
    <meta property="og:site_name" content="IDentiKIT - Tu primera experiencia laboral" /> <!-- website name -->
    <meta property="og:site" content="https://identikit.app/" /> <!-- website link -->
    <meta property="og:title" content="¡Consigue tu primera experiencia laboral! - IDentiKIT.app"/> <!-- title shown in the actual shared post -->
    <meta property="og:description" content="Somos la primera academia diseñada para resaltar la empleabilidad joven. Aplica ahora y mejora tu futuro." />

     <!-- description shown in the actual shared post -->
    
    <meta property="og:url" content="https://identikit.app/" /> <!-- where do you want your post to link to -->
    <meta property="og:type" content="article" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   
    <meta property="og:image" content="http://identikit.app/landingv1/v3/images/logo-og.png" />
    <meta property="og:image:secure_url" content="https://identikit.app/landingv1/v3/images/logo-og.png" />
    <meta property="og:image:type" content="image/png" />
    <meta property="og:image:width" content="300" />
    <meta property="og:image:height" content="300" />

    <!-- Website Title -->
    <title>Políticas de privacidad - IDentiKIT.app</title>
    
    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700&display=swap&subset=latin-ext" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/fontawesome-all.css" rel="stylesheet">
    <link href="css/swiper.css" rel="stylesheet">
	<link href="css/magnific-popup.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">
	
	<!-- Favicon  -->
    <link rel="icon" href="images/logo.png">

      <!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-K2NHRZT4R7"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'G-K2NHRZT4R7');
	</script>

	<!-- Google Tag Manager -->
	<script>
		(function(w, d, s, l, i) {
			w[l] = w[l] || [];
			w[l].push({
				'gtm.start': new Date().getTime(),
				event: 'gtm.js'
			});
			var f = d.getElementsByTagName(s)[0],
				j = d.createElement(s),
				dl = l != 'dataLayer' ? '&l=' + l : '';
			j.async = true;
			j.src =
				'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
			f.parentNode.insertBefore(j, f);
		})(window, document, 'script', 'dataLayer', 'GTM-5QGSH5J');
	</script>
	<!-- End Google Tag Manager -->


</head>
<body data-spy="scroll" data-target=".fixed-top">

  <!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QGSH5J" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

     <?php require_once "wsp.php";?>
     <!-- Preloader -->
     <div class="spinner-wrapper">
         <div class="spinner">
             <div class="bounce1"></div>
             <div class="bounce2"></div>
             <div class="bounce3"></div>
         </div>
     </div>
     <!-- end of preloader -->
     

     <!-- Navigation -->
    <?php include_once "nav.php";?>

    <!-- Header -->
    <header id="header" class="ex-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Políticas de privacidad</h1>
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </header> <!-- end of ex-header -->
    <!-- end of header -->


    <!-- Breadcrumbs -->
    <div class="ex-basic-1">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumbs">
                        <a href="index.php">Home</a><i class="fa fa-angle-double-right"></i><span>Políticas de privacidad</span>
                    </div> <!-- end of breadcrumbs -->
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of ex-basic-1 -->
    <!-- end of breadcrumbs -->


    <!-- Privacy Content -->
    <div class="ex-basic-2">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-container">
                        <p>Dando cumplimiento a los términos señalados por la Ley 1581 de 2012, IDentiKIT.app. se permite informar:</p>
                        <h3>SECCIÓN 1 - ¿QUÉ HACEMOS CON TU INFORMACIÓN? Datos privados que recibimos y recopilamos</h3>
                        <p>IDentiKIT.app recopila y recibe automáticamente cierta información de su computadora o dispositivo móvil, incluidas las actividades que realiza en nuestro sitio web, las plataformas y las aplicaciones, el tipo de hardware y software que está utilizando (por ejemplo, su sistema operativo o navegador) e información obtenida de las cookies. Por ejemplo, cada vez que visita el sitio web o utiliza los Servicios, recopilamos automáticamente su dirección IP, navegador y tipo de dispositivo, tiempos de acceso, la página web de la que vino, las regiones desde las que navega por la página web y el página(s) web a las que accede (según corresponda). Hay más en esta sección y queremos mantenerlo informado al respecto.
                        Email marketing: Al registrarte, podremos enviarte correos electrónicos acerca de nuestro servicio y otras actualizaciones.</p>
                        <p>Cuando se registra por primera vez en una cuenta de IDentiKIT.app y cuando utiliza los servicios recopilamos cierta<a class="blue" href="#your-link"> información personal sobre usted</a> como:</p>
                        <div class="row">
                            <div class="col-md-6">
                                <ul class="list-unstyled li-space-lg indent">
                                    <li class="media">
                                        <i class="fas fa-square"></i>
                                        <div class="media-body">El área geográfica donde usa su computadora y dispositivos móviles.</div>
                                    </li>
                                    <li class="media">
                                        <i class="fas fa-square"></i>
                                        <div class="media-body">Su nombre completo, nombre de usuario y dirección de correo electrónico y otros detalles de contacto deben proporcionarse en los formularios de contacto.</div>
                                    </li>
                                    <li class="media">
                                        <i class="fas fa-square"></i>
                                        <div class="media-body">Una identificación de usuario única de IDentiKIT.app (una cadena alfanumérica) que se le asigna al registrarse.</div>
                                    </li>
                                    <li class="media">
                                        <i class="fas fa-square"></i>
                                        <div class="media-body">Cada sistema se respalda regularmente y no fallará.</div>
                                    </li>
                                    <li class="media">
                                        <i class="fas fa-square"></i>
                                        <div class="media-body">Su dirección IP y, cuando corresponda, la marca de tiempo relacionada con su consentimiento y confirmación de consentimiento.</div>
                                    </li>
                                    <li class="media">
                                        <i class="fas fa-square"></i>
                                        <div class="media-body">Otra información enviada por usted o los representantes de su organización a través de varios métodos y técnicas practicadas.</div>
                                    </li>
                                </ul>
                            </div> <!-- end of col -->

                            <div class="col-md-6">
                                <ul class="list-unstyled li-space-lg indent">
                                    <li class="media">
                                        <i class="fas fa-square"></i>
                                        <div class="media-body">Su dirección de facturación y cualquier otra información necesaria para completar cualquier transacción financiera, y al realizar compras a través de los Servicios, también podemos recopilar su tarjeta de crédito o información de MercadoPago o cualquier otro dato confidencial que considere.</div>
                                    </li>
                                    <li class="media">
                                        <i class="fas fa-square"></i>
                                        <div class="media-body">Contenido generado por el usuario (como mensajes, publicaciones, comentarios, páginas, perfiles, imágenes, fuentes o comunicaciones intercambiadas en las plataformas compatibles que se pueden usar).</div>
                                    </li>
                                    <li class="media">
                                        <i class="fas fa-square"></i>
                                        <div class="media-body">Imágenes u otros archivos que puede publicar a través de nuestros Servicios.</div>
                                    </li>
                                    <li class="media">
                                        <i class="fas fa-square"></i>
                                        <div class="media-body">Información (como mensajes, publicaciones, comentarios, páginas, perfiles, imágenes) que podemos recibir en relación con las comunicaciones que nos envía, como consultas o comentarios.</div>
                                    </li>
                                </ul>
                            </div> <!-- end of col -->
                        </div> <!-- end of row -->
                    </div> <!-- end of text-container-->

                    <div class="text-container">
                        <h3>SECCIÓN 2 - CONSENTIMIENTO</h3>
                        <p>¿Cómo obtienen mi consentimiento?

                            Cuando nos provees tu información personal para completar un formulario, implicamos que aceptas la recolección y uso por esa razón específica solamente.
                        Si te pedimos tu información personal por una razón secundaria, como marketing, te pediremos directamente tu expreso consentimiento, o te daremos la oportunidad de negarte.</p>

                        <p>¿Cómo puedo anular mi consentimiento?

                        Si luego de haber aceptado cambias de opinión, puedes anular tu consentimiento para que te contactemos, por la recolección, uso o divulgación de tu información, en cualquier momento, contactandonos a info@identikit.app</p>
                    </div> <!-- end of text container -->

                    <div class="text-container">
                        <h3>SECCIÓN 3 - DIVULGACIÓN</h3>
                        <p>Podemos divulgar tu información personal si se nos requiere por ley o si violas nuestros Términos de Servicio.</p>

                    </div>                


                    <div class="text-container">
                        <h3>SECCIÓN 4 - SEGURIDAD</h3>
                        <p>Para proteger tu información personal, tomamos precauciones razonables y seguimos las mejores prácticas de la industria para asegurarnos de que no haya pérdida de manera inapropiada, mal uso, acceso, divulgación, alteración o destrucción de la misma.</p>

                    </div>

                    <div class="text-container">
                        <h3>SECCIÓN 5 - EDAD DE CONSENTIMIENTO</h3>
                        <p>Al utilizar este sitio, declaras que tienes al menos la mayoría de edad en tu estado o provincia de residencia.</p>

                    </div>


                    <div class="text-container">
                        <h3>SECCIÓN 6 - CAMBIOS A ESTA POLÍTICA DE PRIVACIDAD</h3>
                        <p>Nos reservamos el derecho de modificar esta política de privacidad en cualquier momento, así que por favor revísala frecuentemente.  Cambios y aclaraciones entrarán en vigencia inmediatamente después de su publicación en el sitio web.  Si hacemos cambios materiales a esta política, notificaremos aquí que ha sido actualizada, por lo cual estás enterado de qué información recopilamos, cómo y bajo qué circunstancias, si las hubiere, la utilizamos y/o divulgamos.</p>

                    </div>


                    <div class="text-container">
                        <h3>Cómo usamos los datos de IDentiKIT.app</h3>
                        <p>IDentiKIT.app utiliza los datos de los visitantes para los siguientes propósitos generales y para otros específicos que son importantes:</p>
                        <ol class="li-space-lg">
                            <li>Para identificarlo cuando inicia sesión en su cuenta para que podamos iniciar un proceso de seguridad del usuario durante toda la sesión y la duración</li>

                            <li>Para permitirnos operar los Servicios y brindárselos sin temor a perder valiosa información confidencial de sus usuarios.</li>

                            <li>Para verificar sus transacciones y para la confirmación de compras, facturación, seguridad y autenticación (incluidos los tokens de seguridad para la comunicación con los instalados). Siempre tome medidas de seguridad como no guardar contraseñas en su navegador o escribirlas</li>

                            <li>Para analizar el sitio web o los otros Servicios y la información sobre nuestros visitantes y usuarios, incluida la investigación de la demografía y el comportamiento de nuestros usuarios para mejorar nuestro contenido y Servicios.</li>

                            <li>Para comunicarnos con usted acerca de su cuenta y brindarle asistencia de servicio al cliente, incluida la respuesta a sus comentarios y preguntas.</li>

                            <li>Para compartir estadísticas agregadas (no identificables) sobre los usuarios de los Servicios con posibles anunciantes y socios.</li>

                            <li>Para mantenerlo informado sobre los Servicios, características, encuestas, boletines, ofertas, encuestas, boletines, ofertas, concursos y eventos que creemos que pueden resultarle útiles o que nos ha solicitado.</li>

                            <li>Para vender o comercializar productos y servicios de IDentiKIT.app para usted o en otras partes del mundo donde la legislación es menos restrictiva</li>

                            <li>Para comprender mejor sus necesidades y las necesidades de los usuarios en conjunto, diagnosticar problemas, analizar tendencias, mejorar las características y la facilidad de uso de los Servicios, y comprender y comercializar mejor a nuestros clientes y usuarios.</li>

                            <li>Para mantener los Servicios seguros y protegidos para todos los que usan la aplicación, desde los administradores hasta los usuarios habituales con derechos limitados.</li>

                            <li>También utilizamos información no identificable recopilada con fines estadísticos para realizar un seguimiento del número de visitas a los Servicios con el fin de introducir mejoras y mejorar la usabilidad de los Servicios. Podemos compartir este tipo de datos estadísticos para que nuestros socios también comprendan con qué frecuencia las personas usan los Servicios, para que ellos también puedan brindarle una experiencia óptima.</li>

                        </ol>
                    </div> <!-- end of text-container -->

                    <div class="text-container">
                        <h3>Consentimiento para usar la página de IDentiKIT.app</h3>
					    
                        <p>Al usar cualquiera de los Servicios, o enviar o recopilar cualquier Información personal a través de los Servicios, usted acepta la recopilación, transferencia, almacenamiento, divulgación y uso de su Información personal de la manera establecida en esta Política de privacidad. Si no da su consentimiento para el uso de su Información personal de estas maneras, deje de usar los Servicios, debe ser seguro y fácil para garantizar una excelente experiencia de usuario.</p>
                    </div> <!-- end of text-container -->
                                       
                    <div class="row">
                        <div class="col-md-6">
                            <div class="text-container last">
                                <h3>Consulta qué datos tenemos</h3>
                                <p>IDentiKIT.app utiliza tecnología de seguimiento en la página de destino, en las Aplicaciones y en las Plataformas, incluidos los identificadores de aplicaciones móviles y una ID de usuario única de IDentiKIT.app para ayudarnos. Reconocerlo a través de diferentes Servicios, para monitorear el uso y el enrutamiento del tráfico web para los Servicios, y para personalizar y mejorar los Servicios.</p>
                               
                                <p>Al visitar IDentiKIT.app o usar los Servicios, usted acepta el uso de cookies en su navegador y correos electrónicos basados en HTML. Usted visita un sitio web, al usar cualquiera de los Servicios, o al enviar o recopilar cualquier Información personal a través de los Servicios, usted acepta y usa su Información personal.</p>
                            </div> <!-- end of text container -->
                        </div> <!-- end of col-->
                        <div class="col-md-6">

                            <!-- Privacy Form -->
                            <div class="form-container">
                                <form id="privacyForm" data-toggle="validator" data-focus="false">
                                    <div class="form-group">
                                        <input type="text" class="form-control-input" id="pname" required>
                                        <label class="label-control" for="pname">Name</label>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group">
                                        <input type="email" class="form-control-input" id="pemail" required>
                                        <label class="label-control" for="pemail">Email</label>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group">
                                        <select class="form-control-select" id="pselect" required>
                                            <option class="select-option" value="" disabled selected>Select Option</option>
                                            <option class="select-option" value="Delete data">Delete my data</option>
                                            <option class="select-option" value="Show me data">Show me my data</option>
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group checkbox">
                                        <input type="checkbox" id="pterms" value="Agreed-to-Terms" required>I have read and agree to IDentiKIT's <a href="privacy-policy.html">Privacy Policy</a> and <a href="terms-conditions.html">Terms Conditions</a>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="form-control-submit-button">SUBMIT</button>
                                    </div>
                                    <div class="form-message">
                                        <div id="pmsgSubmit" class="h3 text-center hidden"></div>
                                    </div>
                                </form>
                            </div> <!-- end of form container -->
                            <!-- end of privacy form -->

                        </div> <!-- end of col--> 
                    </div> <!-- end of row -->
                    <a class="btn-outline-reg" href="index.php">BACK</a>
                </div> <!-- end of col-->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of ex-basic-2 -->
    <!-- end of privacy content -->


     <?php include_once "footer.php";?>

    	
    <!-- Scripts -->
    <script src="js/jquery.min.js"></script> <!-- jQuery for Bootstrap's JavaScript plugins -->
    <script src="js/popper.min.js"></script> <!-- Popper tooltip library for Bootstrap -->
    <script src="js/bootstrap.min.js"></script> <!-- Bootstrap framework -->
    <script src="js/jquery.easing.min.js"></script> <!-- jQuery Easing for smooth scrolling between anchors -->
    <script src="js/swiper.min.js"></script> <!-- Swiper for image and text sliders -->
    <script src="js/jquery.magnific-popup.js"></script> <!-- Magnific Popup for lightboxes -->
    <script src="js/validator.min.js"></script> <!-- Validator.js - Bootstrap plugin that validates forms -->
    <script src="js/scripts.js"></script> <!-- Custom scripts -->
</body>
</html>