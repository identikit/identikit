<!DOCTYPE html>
<html lang="es">
<head>
   <!-- SEO Meta Tags -->
       <meta name="description" content="Somos la primera academia diseñada para resaltar la empleabilidad joven. Aplica ahora y mejora tu futuro.">
       <meta name="author" content="IDentiKIT.app ">

       <!-- OG Meta Tags to improve the way the post looks when you share the page on LinkedIn, Facebook, Google+ -->
       <meta property="og:site_name" content="IDentiKIT - Tu primera experiencia laboral" /> <!-- website name -->
       <meta property="og:site" content="https://identikit.app/" /> <!-- website link -->
       <meta property="og:title" content="¡Consigue tu primera experiencia laboral! - IDentiKIT.app"/> <!-- title shown in the actual shared post -->
       <meta property="og:description" content="Somos la primera academia diseñada para resaltar la empleabilidad joven. Aplica ahora y mejora tu futuro." />

        <!-- description shown in the actual shared post -->
       
       <meta property="og:url" content="https://identikit.app/" /> <!-- where do you want your post to link to -->
       <meta property="og:type" content="article" />
       <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
      
       <meta property="og:image" content="http://identikit.app/landingv1/v3/images/logo-og.png" />
       <meta property="og:image:secure_url" content="https://identikit.app/landingv1/v3/images/logo-og.png" />
       <meta property="og:image:type" content="image/png" />
       <meta property="og:image:width" content="300" />
       <meta property="og:image:height" content="300" />



       <!-- Website Title -->
       <title>Iniciar sesión - IDentiKIT.app</title>
       
    
    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700&display=swap&subset=latin-ext" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/fontawesome-all.css" rel="stylesheet">
    <link href="css/swiper.css" rel="stylesheet">
	<link href="css/magnific-popup.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">
	
	<!-- Favicon  -->
    <link rel="icon" href="images/logo.png">

       <!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-K2NHRZT4R7"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'G-K2NHRZT4R7');
	</script>

	<!-- Google Tag Manager -->
	<script>
		(function(w, d, s, l, i) {
			w[l] = w[l] || [];
			w[l].push({
				'gtm.start': new Date().getTime(),
				event: 'gtm.js'
			});
			var f = d.getElementsByTagName(s)[0],
				j = d.createElement(s),
				dl = l != 'dataLayer' ? '&l=' + l : '';
			j.async = true;
			j.src =
				'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
			f.parentNode.insertBefore(j, f);
		})(window, document, 'script', 'dataLayer', 'GTM-5QGSH5J');
	</script>
	<!-- End Google Tag Manager -->
</head>
<body data-spy="scroll" data-target=".fixed-top">
            
            <!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QGSH5J" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
        
   <?php require_once "wsp.php";?>
       <!-- Preloader -->
       <div class="spinner-wrapper">
           <div class="spinner">
               <div class="bounce1"></div>
               <div class="bounce2"></div>
               <div class="bounce3"></div>
           </div>
       </div>
       <!-- end of preloader -->
       

       <!-- Navigation -->
      <?php include_once "nav.php";?>


    <!-- Header -->
    <header id="header" class="ex-2-header">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1>Iniciar sesión</h1>
                   <p>Bienvenid@ a IDentiKIT.app</p> 
                    <!-- Sign Up Form -->
                    <div class="form-container">
                        <form id="logInForm" data-toggle="validator" data-focus="false">
                            <div class="form-group">
                                <input type="email" class="form-control-input" required>
                                <label class="label-control" for="lemail">Email</label>
                                <div class="help-block with-errors"></div>
                            </div>
                            
                            <div class="form-group">
                                <input type="password" class="form-control-input" id="lpassword" required>
                                <label class="label-control" for="lpassword">Contraseña</label>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="form-control-submit-button">Iniciar Sesión</button>
                            </div>

                            <div class="form-message">
                                <div id="lmsgSubmit" class="h3 text-center hidden"></div>
                            </div>
                        </form>
                    </div> <!-- end of form container -->
                    <!-- end of sign up form -->

                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </header> <!-- end of ex-header -->
    <!-- end of header -->


    <!-- Scripts -->
    <script src="js/jquery.min.js"></script> <!-- jQuery for Bootstrap's JavaScript plugins -->
    <script src="js/popper.min.js"></script> <!-- Popper tooltip library for Bootstrap -->
    <script src="js/bootstrap.min.js"></script> <!-- Bootstrap framework -->
    <script src="js/jquery.easing.min.js"></script> <!-- jQuery Easing for smooth scrolling between anchors -->
    <script src="js/swiper.min.js"></script> <!-- Swiper for image and text sliders -->
    <script src="js/jquery.magnific-popup.js"></script> <!-- Magnific Popup for lightboxes -->
    <script src="js/validator.min.js"></script> <!-- Validator.js - Bootstrap plugin that validates forms -->
    <script src="js/scripts.js"></script> <!-- Custom scripts -->
    <script src="js_whatsapp.js" type="text/javascript"></script>

</body>
</html>