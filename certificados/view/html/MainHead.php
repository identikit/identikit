<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


<link href="../../public/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="../../public/lib/Ionicons/css/ionicons.css" rel="stylesheet">
<link href="../../public/lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
<link href="../../public/lib/jquery-switchbutton/jquery.switchButton.css" rel="stylesheet">


<link href="../../public/lib/datatables/jquery.dataTables.css" rel="stylesheet">
<link href="../../public/datatables/buttons.dataTables.min.css" rel="stylesheet"/>

<link href="../../public/lib/select2/css/select2.min.css" rel="stylesheet">

<link href="../../public/css/bracket.css" rel="stylesheet">