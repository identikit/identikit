
        <?php
            $path_cvs    = $db->query("select path_video_cv,member_no from tbl_users_data");

            foreach ($path_cvs as $path_cv){
                $getInfoUser = $db->query("SELECT first_name, last_name, member_no, about from `tbl_users` 
                where member_no=:member_no",array("member_no"=>$path_cv["member_no"]));
                $fullname    = $getInfoUser[0]["first_name"] . " " . $getInfoUser[0]["last_name"]; 
                      
                    if ($user_online == true) {
                        $member_no = $getInfoUser[0]["member_no"];
                        
                        echo ' 
                          <!-- video starts -->
                          <div class="video" id="video'.$member_no.'">
                          <video class="video__player" src="../../../'.$path_cv['path_video_cv'].'"></video>';

                        $isCheckedHeart = $db->query("select id from tbl_likes_videocv where like_user_id=:like_user_id and like_member_no=:like_member_no", 
                        array("like_user_id" => $_SESSION["myid"], "like_member_no" => $member_no));

                        $background  = '';
                        $color       = '';
                        
                        if (!empty($isCheckedHeart)) {
                            $background = "#ff0047";
                            $color      = "white";
                        }
                        
                        echo '<!-- footer -->
                              <div class="videoFooter">
                                <div class="videoFooter__text">
                                  <a class="heart_mark float-right" value=' . $member_no . ' style="background:' . $background . '; color:' . $color . '; cursor:pointer">
                                  <i class="ti-heart" id="heart"></i></a>';
                    } 

                    echo '
                          <h3>'.$fullname.'</h3>
                          <p class="videoFooter__description">'.$getInfoUser[0]["about"].'.</p>            
                          </div>
                      </div>
                    </div>';
            }
        ?>