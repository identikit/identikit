<?php
    require '../../../db/db.php';
    $db = new DbPDO();

	// Content Type JSON
	header("Content-type: application/json");

	$res = array('error' => false);

	

	if (isset($_GET['action'])) {
		$action = $_GET['action'];
	}


    if ($action == 'VideoCvs') {
		if (!isset($_GET['memberNo'])){
			$user_id = ""; 
		} else {
			$user_id = $_GET['memberNo'];
		}

		$path_cvs    = $db->query("select A.first_name, A.last_name, A.About,A.member_no, B.Path_video_cv 
			from `tbl_users` AS A 
			inner join `tbl_users_data` AS B 
			on A.member_no = B.member_no");
		
		$result = [];

		foreach($path_cvs as $path_cv){

			//$CheckedHeartCv = array("heart",null);
			$CheckedHeartCv =  array('heart' => false);

			$CheckedHeart = $db->query("select id from tbl_likes_videocv 
			where like_user_id=:like_user_id and like_member_no=:like_member_no", 
			array("like_user_id" => $user_id, "like_member_no" => $path_cv["member_no"]));
			
			if(!empty($CheckedHeart)){
				$CheckedHeartCv =  array('heart' => true);
			}

			//array_push($path_cv,$CheckedHeartCv);
			$joinHeartVideoCv = $path_cv + $CheckedHeartCv;
			array_push($result,$joinHeartVideoCv);
		}

		$res['VideoCvs'] = $result;
	}

	if ($action == 'UpdateHeart'){
		if (!isset($_POST['currentSession'])){
			$user_id = ""; 
		} else {
			$user_id       = $_POST['currentSession'];
			$owner_videocv = $_POST['Owner_videoCv'];     
		}
		
		$checklike = $db->query("select id from `tbl_likes_videocv` where like_user_id = :like_user_id and like_member_no = :like_member_no",
		array(
			"like_user_id"   =>  $user_id,
			"like_member_no" =>  $owner_videocv
		));

		if ($checklike == null){
			$sql       = "INSERT INTO `tbl_likes_videocv` (like_user_id,like_member_no) VALUES(:like_user_id, :like_member_no)";
			$db->query($sql, array(
				"like_user_id"   => $user_id,
				"like_member_no" => $owner_videocv
			));	
		} else {
			$db->query("DELETE from `tbl_likes_videocv` where like_user_id=:like_user_id and like_member_no=:like_member_no",
			array(
				"like_user_id"   => $user_id,
				"like_member_no" => $owner_videocv));
		}

	}

	echo json_encode($res);
	

?>