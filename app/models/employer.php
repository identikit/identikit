<?php
    require_once("../../db/db.php");

    date_default_timezone_set("America/Argentina/Buenos_Aires");

    class employer {

        function __construct(){
            include '../views/employer/constants/check-login.php';
            $this->myid     = $_SESSION['myid'];
            $this->db        = new DbPDO(); 
        }

        public function agregarconversacion(){
            $data = array();
        
            $mensaje                      = $_POST['mensaje'];
            $receiverId                   = $_POST['receiverId'];
            $senderId                     = $_POST['senderId'];
            $message_creation_date        = date("Y/m/d");
            $message_creation_hour        = date("h:i:sa");
        
            try {
                $this->db->query("insert into tbl_messages(message_body,message_sender_id,message_receiver_id,message_creation_hour,message_creation_date,message_status) 
                VALUES (:message_body,:message_sender_id,:message_receiver_id,:message_creation_hour,:message_creation_date,:message_status)",
                array(
                    "message_body"           => $mensaje,
                    "message_sender_id"      => $senderId,
                    "message_receiver_id"    => $receiverId,
                    "message_creation_hour"  => $message_creation_hour,
                    "message_creation_date"  => $message_creation_date,
                    "message_status"         => 1));
            } catch(PDOException $e) {
                echo 'Excepción capturada: ',  $e->getMessage(), "\n";
            }
            finally {
                $data['status']='correcto';
            }
            return $data;
        }

        public function DropDp(){
            require '../../constants/db_config.php';
            require '../constants/check-login.php';
            $this->db->query("UPDATE tbl_users SET avatar='' WHERE member_no='$myid'");
            $GetTblUsers    = $db->query("SELECT * FROM tbl_users WHERE member_no='$myid'");
            
            foreach($GetTblUsers as $user)
            {
                $_SESSION['avatar'] = $user['avatar'];
                header("location:../");
            } 
        }

        public function DropJob($job_id) {            
            
            try {
                $this->db->query("DELETE FROM tbl_jobs WHERE job_id= ".$job_id." AND company = '$this->myid'", 
                array('company' => $_SESSION['myid']));
                $this->db->query("DELETE FROM tbl_job_applications WHERE job_id= ".$job_id."");
                header("location:../views/employer/index.php?r=0173&jobid=$job_id");                                
            }catch(PDOException $e){
                
            }
        }

        public function UpdateImage(){
                        
            $servername = "localhost";
            $username   = "u352312733_idkitdb";
            $password   = "Fucksociety150*0";
            $dbname     = "u352312733_idkitdb";

            $image = addslashes(file_get_contents($_FILES['image']['tmp_name']));

            $myid = $this->myid;

            if ($_FILES["image"]["size"] > 923000000) {
                header("location:../?r=3478");
            }else{
                try {
                    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
                    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                    
                    $stmt = $conn->prepare("UPDATE tbl_users SET avatar='$image' WHERE member_no='$myid'");
                    $stmt->execute();
                    
                    
                    $stmt = $conn->prepare("SELECT * FROM tbl_users WHERE member_no='$myid'");
                    $stmt->execute();
                    $result = $stmt->fetchAll();

                    foreach($result as $row){
                        $_SESSION['avatar'] = $row['avatar'];
                        header("location:../views/employer/index.php");
                    }                               
                }catch(PDOException $e)
                {

                }
            }
            
        }

        public function postJob(){
            
            $postdate      = date('F d, Y');
            $title         = $_POST['title'];
            $city          = $_POST['city'];
            $country       = $_POST['country'];
            $category      = $_POST['category'];
            $type          = $_POST['jobtype'];
            $exp           = $_POST['experience'];
            $desc          = $_POST['description'];
            $tech          = $_POST['tech'];
            $company       = $_SESSION['myid'];
            $deadline      = $_POST['deadline'];


            try {
                $this->db->query("INSERT INTO tbl_jobs (title, city, country, category, type, experience, 
                description, tech, company, date_posted, closing_date)
                VALUES (:title, :city, :country, :category, :type, :experience, 
                :description, :technology, :company, :dateposted, :closingdate)",
                array(
                    "title"       => $title,
                    "city"        => $city,
                    "country"     => $country,
                    "category"    => $category,
                    "type"        => $type,
                    "experience"  => $exp, 
                    "description" => $desc,
                    "technology"  => $tech,
                    "company"     => $company,
                    "dateposted"  => $postdate,
                    "closingdate" => $deadline,
                ));
                header("location:../views/employer/index.php?r=9843");		  
                } catch (PDOException $e){
                    echo "Connection failed: " . $e->getMessage();
            }
        }

        public function UpdateJob(){
            // require '../../constants/db_config.php';
            // require '../constants/check-login.php';
            

            $job_id         = $_POST['jobid'];
            $title          = ucwords($_POST['title']);
            $city           = ucwords($_POST['city']);
            $country        = $_POST['country'];
            $category       = $_POST['category'];
            $type           = $_POST['jobtype'];
            $exp            = $_POST['experience'];
            $desc           = $_POST['description'];
            $rec            = $_POST['requirements'];
            $res            = $_POST['responsiblities'];
            $tech           = $_POST['tech'];
            $deadline       = $_POST['deadline'];
            $myid           = $_SESSION['myid'];

            try {                
                $this->db->query("UPDATE tbl_jobs SET title = :title, city = :city, country = :country, category = :category, type = :type, 
                experience = :experience, description = :description, tech = :technology, responsibility = :responsibility, requirements = :requirements, 
                closing_date = :deadline WHERE job_id = :jobid AND company = :myid", 
                array(
                    "title"             => $title,
                    "city"              => $city,
                    "country"           => $country,
                    "category"          => $category,
                    "type"              => $type,
                    "experience"        => $exp,
                    "description"       => $desc,
                    "responsibility"    => $res,
                    "requirements"      => $rec,
                    "jobid"             => $job_id,
                    "deadline"          => $deadline,
                    "technology"        => $tech,
                    "myid"              => $myid
                ));

                header("location:../views/employer/index.php?r=0369&jobid=$job_id");
                                
            }catch(PDOException $e)
            {

            }
        }

        public function cambiarStatus(){
           
            $jobid  = $_POST['jobid'];
            $status = $_POST['status'];
            $member = $_POST['member'];
            $email  = $_POST['email'];
            $name   = $_POST['name'];
            
            try {
                $this->db->query("UPDATE tbl_job_applications SET EVacante=:estado WHERE member_no = :member and job_id = :jobid",
                    array(
                        "member" => $member,
                        "jobid"  => $jobid, 
                        "estado" => $status
                    )
                );
            
                $usuarios = $this->db->query("SELECT A.first_name, B.title FROM `tbl_users` as A inner join `tbl_jobs`as B 
                                              where A.member_no = B.company and B.job_id=:jobid",
                    array("jobid" => $jobid)
                );

                //$empresa = $usuarios[0][0];
                $empresa = "Empresa";
                $vacante = "Programador";
                //$vacante = $usuarios[0][1];

                $img  = "";
                $text = "";

                switch($status){
                    case 2:
                        $img  = "IRevision.png";
                        break;
                    case 3:
                        $img  = "IEntrevista.png";
                        break;
                    case 4:
                        $img  = "IAceptado.png";
                        break;
                    case 5:
                        $img  = "IRechazado.png";
                        break;
                }
    

                include('helper.php');

                $helper = new helper();
                $helper->SendEmailChangeStatus($email, $img, $name, $empresa, $vacante, $status);
               
                header('Location: ' . $_SERVER['HTTP_REFERER']);
                          
                }catch(PDOException $e)
                {
                    echo 'Excepción capturada: ',  $e->getMessage(), "\n";
                }
        }

    public function UpdateProfile(){
            $companame     = ucwords($_POST['company']);
            $esta          = $_POST['year'];
            $type          = ucwords($_POST['type']);
            $people        = $_POST['people'];
            $web           = $_POST['web'];
            $city          = ucwords($_POST['city']);
            $street        = ucwords($_POST['street']);
            $zip           = ucwords($_POST['zip']);
            $phone         = $_POST['phone'];
            $country       = $_POST['country'];
            $about         = $_POST['background'];
            $service       = $_POST['services'];
            $myemail       = $_POST['email'];
            $cuit          = $_POST['cuit'];
            $myid          = $_SESSION['myid'];

            try {
                $sql   = "SELECT * FROM tbl_users WHERE email = '".$myemail."' AND member_no != '$myid'";
                $users = $this->db->query("SELECT * FROM tbl_users WHERE email = '".$myemail."' AND member_no != '$myid'");
                $rec   = count($users);
                
                if ($rec == "0") {
                    $this->db->query("UPDATE tbl_users SET first_name = :compname, byear = :esta, title = :type, city = :city, street = :street, 
                    zip = :zip, country = :country, phone = :phone, about = :about, :services = :service, people = :people, 
                    website = :website, dni = :cuit WHERE member_no=:myid", 
                    array(
                        "compname"             => $companame,
                        "esta"                 => $esta,
                        "type"                 => $type,
                        "city"                 => $city,
                        "street"               => $street,
                        "zip"                  => $zip,
                        "country"              => $country,
                        "phone"                => $phone,
                        "about"                => $about,
                        "service"              => $service,
                        "people"               => $people,
                        "website"              => $web,
                        "cuit"                 => $cuit,
                        "myid"                 => $myid
                    ));
                
                    $_SESSION['compname']    = $companame;
                    $_SESSION['established'] = $esta;
                    $_SESSION['myemail']     = $myemail;
                    $_SESSION['myphone']     = $phone;
                    $_SESSION['comptype']    = $type;
                    $_SESSION['mycity']      = $city;
                    $_SESSION['mystreet']    = $street;
                    $_SESSION['myzip']       = $zip;
                    $_SESSION['mycountry']   = $country;
                    $_SESSION['mydesc']      = $about;
                    $_SESSION['myserv']      = $service;
                    $_SESSION['website']     = $web;
                    $_SESSION['people']      = $people;
                    $_SESSION['cuit']        = $cuit;

                    header("location:../views/employer/index.php?r=9837");	

                }else{
                    header("location:../views/employer/index.php?r=0927");
                }
                                
            }catch(PDOException $e){
                echo "Connection failed: " . $e->getMessage();
            }
        }

        public function logout(){
            session_start();
            $_SESSION['logged'] = false;
            session_unset();
            session_destroy();
            header("location:../../");
        }

        private function executeFunction($sql){
            try{
                return $sql;
            }catch(PDOException $e) {
                echo 'Excepción capturada: ',  $e->getMessage(), "\n";
            }
        }
    }

?>