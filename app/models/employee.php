<?php
    require_once("../../db/db.php");

    date_default_timezone_set("America/Argentina/Buenos_Aires");

    class employee {

        function __construct(){
            include '../views/employee/constants/check-newlogin.php';
            $this->db       = new DbPDO();
            $this->myid     = $_SESSION['myid']; 
        }
        
        public function agregarconversacion(){

            $data = array();
            $mensaje                      = $_POST['mensaje'];
            $receiverId                   = $_POST['receiverId'];
            $senderId                     = $_POST['senderId'];
            $message_creation_date        = date("Y/m/d");
            $message_creation_hour        = date("h:i:sa");
        
            try {
                $this->db->query("insert into tbl_messages(message_body,message_sender_id,message_receiver_id,message_creation_hour,message_creation_date,message_status) 
                VALUES (:message_body,:message_sender_id,:message_receiver_id,:message_creation_hour,:message_creation_date,:message_status)",
                array(
                    "message_body"           => $mensaje,
                    "message_sender_id"      => $senderId,
                    "message_receiver_id"    => $receiverId,
                    "message_creation_hour"  => $message_creation_hour,
                    "message_creation_date"  => $message_creation_date,
                    "message_status"         => 1
                ));
        
            } catch(PDOException $e) {
                echo 'Excepción capturada: ',  $e->getMessage(), "\n";
            }
            finally {
                $data['status']='correcto';
            }
        
            echo json_encode($data);
        }
        public function AddEducation(){
            $title          = $_POST['educationtitle'];
            $country        = $_POST['ecountry'];
            $type           = $_POST['typeed'];
            // $area           = $_POST['earea'];
            $descripcion    = $_POST['description-edu'];
            $institucion    = $_POST['einstitution'];
            $status         = $_POST['estatus'];
            $startdate_m    = $_POST['startdate_m'];
            $startdate_y    = $_POST['startdate_y'];
            $enddate_m      = $_POST['enddate_m'];
            $enddate_y      = $_POST['enddate_y'];

            if(!empty($title)){
                try{
                    $sql = "insert into tbl_education(member_no,title,descripcion,country,type,
                    area,institution,status,start_date_m,end_date_m,start_date_y,
                    end_date_y) 
                    VALUES ('".$this->myid."','".$title."','".$descripcion."','".$country."','".$type."','Tecnologia','".$institucion."',
                    '".$status."','".$startdate_m."','".$enddate_m."','".$startdate_y."','".$enddate_y."')";
                    
                    $this->db->query($sql);
                    // $this->db->query("insert into tbl_education(member_no,title,country,type,
                    // area,institution,status,start_date_m,end_date_m,start_date_y,
                    // end_date_y) 
                    // VALUES (:member_no,':title',':country',':type','Tecnologia',':institution',
                    // ':status',':start_date_m',':end_date_m',':start_date_y',':end_date_y')",
                    // array(
                    //     "member_no"    => $this->myid,
                    //     "title"        => $title,
                    //     "country"      => $country,
                    //     "type"         => $type,
                    //     "area"         => $area,
                    //     "institution"  => $institucion,
                    //     "status"       => $status,
                    //     "start_date_m" => $startdate_m,
                    //     "end_date_m"   => $enddate_m,
                    //     "start_date_y" => $startdate_y,
                    //     "end_date_y"   => $enddate_y
                    // ));
                } catch(PDOException $e) {
                    echo 'Excepción capturada: ',  $e->getMessage(), "\n";
                }
            }
            header("location:../views/employee/profile.php");
        }
        public function AddLang(){
            $getLang    =  $_POST["lang"];
            $getL_write =  $_POST["l_write"];
            $getL_read  =  $_POST["l_read"];

            if(!empty($getLang)){
                $this->db->query("insert into tbl_lang(member_no,lang,l_write,l_read) VALUES (:member_no,:lang,:l_write,:l_read)",
                array(
                    "member_no"  => $this->myid,
                    "lang"       => $getLang,
                    "l_write"    => $getL_write,
                    "l_read"     => $getL_read,
                ));
            }
            header("location:../views/employee/profile.php");
        }
        public function UpdateTags(){ 
            $checkUserAdd = $this->db->query("SELECT * FROM tbl_usuarios_data WHERE iduser = :iduser" , array("iduser" => $this->myid));
            
            if(!empty($checkUserAdd)){
                $this->db->query("UPDATE tbl_usuarios_data SET tags = :tags WHERE iduser = :iduser", array("tags" => $_POST['ability'], "iduser" => $this->myid));
            } else { 
                $this->db->query("INSERT INTO tbl_usuarios_data (iduser,tags) VALUES (:iduser,:tags)", array("iduser" => $this->myid, "tags" => $_POST['ability']));
            }
            header("location:../views/employee/profile.php");					  
        }

        public function getAll(){
            $idvideo = $_GET['action'];
            $mensajes = $this->db->query("SELECT A.*, B.path FROM `tbl_preguntas` as A inner join tbl_usuarios as B ON A.userid = B.id where A.videoID=:idvideo",
                        array("idvideo" => $idvideo)); 
            return $mensajes;

        }

        public function UpdateProfile(){
           
            $fname = ucwords($_POST['fname']);
            $lname = ucwords($_POST['lname']);
            $myemail = $_POST['email'];
            $street = ucwords($_POST['street']);
            $phone = $_POST['phone'];
            $country = $_POST['country'];
            $mydni = $_POST['dni'];
            $ability = $_POST['category'];
            
            try {                
                $usuarios = $this->db->query("SELECT * FROM tbl_usuarios WHERE email = :email AND id = :myid", array("email" => $myemail, "myid" => $this->myid));
                $rec = count($usuarios);
                if ($rec == "1") {

                    $sql = "UPDATE tbl_usuarios SET name = '".$fname."', surname = '".$lname."', email = '".$myemail."', 
                    province = '".$street."', country = '".$country."', cellphone = '".$phone."', 
                    identification = '".$mydni."' WHERE id=".$this->myid."";

                    $sqlability = "update tbl_usuarios_data set ability = '".$ability."' WHERE iduser=".$this->myid."";

                    $this->db->query($sql);
                    $this->db->query($sqlability);
                
                    $_SESSION['name'] = $fname;
                    $_SESSION['surname'] = $lname;
                    $_SESSION['myemail'] = $myemail;
                    $_SESSION['mycity'] = $city;
                    $_SESSION['street'] = $street;
                    $_SESSION['mycountry'] = $country;
                    $_SESSION['identification'] = $mydni;                    
                    header("location:../views/employee/profile.php?r=9837");
                    
                }else{
                    // header("location:../?r=0927");
                }
                        
                }catch(PDOException $e){

                }
            }

        public function AddExperience(){
               
                //require '../constants/check-login.php';

                $jobtitle = ucwords($_POST['jobtitle']);

                if (isset($_POST['duties'])) {
                    $duties = $_POST['duties'];	
                }else{
                    $duties = "";	
                }

                $techno = ($_POST['techno']);
                $startdate = $_POST['startdate'];
                $enddate = $_POST['enddate'];
                $link = $_POST['link'];

                if (isset($_POST['view'])) {
                    $view = $_POST['view'];	    
                }else{
                    $view = "";	
                }

                try {                        
                    $this->db->query("INSERT INTO tbl_experience (member_no, title, link, duties, techno, start_date, end_date, view) 
                    VALUES (:myid, :title, :link, :duties, :techno, :startdate, :enddate, :view)",
                    array(
                        "title"     => $jobtitle,
                        "link"      => $link, 
                        "duties"    => $duties, 
                        "techno"    => $techno, 
                        "startdate" => $startdate, 
                        "enddate"   => $enddate, 
                        "view"      => $view,
                        "myid"      => $this->myid
                    )); 

                    header("location:../views/employee/miperfil.php?r=9210");					  
                }catch(PDOException $e){
                    echo "Connection failed: " . $e->getMessage();
                }
        }

        public function NewDp(){ 
                
                $myid = $this->myid;
                $objetivo = $_POST["objetivo"];

                $Namefile     = basename($_FILES["image"]["name"]);
                $TmpName      = $_FILES['image']['tmp_name'];
                $beforepath   = "../../";

                $path        = 'data/employee/' . $myid . '/' . $Namefile;
                $base_path   = 'data/employee/' . $myid . '/';
                $realpath    = $beforepath . $base_path;
         
                
                if (!file_exists($realpath)) {
                    mkdir($realpath, 0777, true);
                }   

                if(isset($Namefile)){
                    if(empty($Namefile)){
                        header("location:../views/employee/profile.php?r=9200");
                    } else if (move_uploaded_file($TmpName, $realpath . '/' . $Namefile )) {{
                        $this->db->query("UPDATE tbl_usuarios set path=:path where id=:myid",
                            array(
                                "myid"    => $myid,
                                "path"   =>  $path
                            ));   
                        session_start();
                        $_SESSION['myavatar'] = $path;
                        header("location:../views/employee/profile.php?r=9837");		
                    }
                }

               
            }
        }


        public function AddQuestion(){
            $pregunta   = $_GET['pregunta'];
            $videoId    = $_GET['idvideo'];
            $status     = 1;
            $myid       = $this->myid;
            $date       = date('Y-m-d H:i:s');

            $this->db->query("INSERT INTO tbl_preguntas (userid, pregunta, videoID, status, created_at) VALUES (:userid, :pregunta, :videoId, :status, :apply_date)",
                array(
                    "userid"     => $myid,
                    "pregunta"   => $pregunta,
                    "videoId"    => $videoId,
                    "status"     => $status,
                    "apply_date" => $date
                )); 

            // header("location:../views/employee/perfilv2.php?r=9837");	
        }

        public function UpdatePerfilv2(){
            $myid = $this->myid;
            $objetivo = $_POST["objetivo"];

            $this->db->query("UPDATE tbl_usuarios_data SET objetivo=:objetivo WHERE iduser=:myid",
            array(
                "myid"       => $myid,
                "objetivo"   =>  $objetivo
            ));

            header("location:../views/employee/profile.php?r=9837");		

        }

        public function UpdateExperience(){
            $jobtitle = ucwords($_POST['jobtitle']);

            if (isset($_POST['duties'])) {
                $duties = $_POST['duties'];	
            }else{
                $duties = "";	
            }

            $techno    = ($_POST['techno']);
            $startdate = $_POST['startdate'];
            $enddate   = $_POST['enddate'];
            $link      = $_POST['link'];
            $exp_id    = $_POST['expid'];

            if (isset($_POST['view'])) {
                $view = $_POST['view'];	
            }else{
                $view = "";	
            }

            try {
                $this->db->query("UPDATE tbl_experience SET title=:title, link=:link, duties = :duties, techno = :techno, 
                start_date =  :startdate, end_date = :enddate, view = :view WHERE id=:expid AND member_no = :myid",
                array(
                    "title"     => $jobtitle,
                    "link"      => $link, 
                    "duties"    => $duties, 
                    "techno"    => $techno, 
                    "startdate" => $startdate, 
                    "enddate"   => $enddate, 
                    "view"      => $view,
                    "myid"      => $this->myid,
                    "expid"     => $exp_id
                ));

                header("location:../views/employee/miperfil.php?r=9215");					  
            }catch(PDOException $e){

            }
        }

        public function DropExperience($exp_id){
            try {    
                $this->db->query("DELETE FROM tbl_experience WHERE id=:expid AND member_no = :myid", 
                array(
                    "expid" => $exp_id,
                    "myid"  => $this->myid
                ));
                
                header("location:../views/employee/miperfil.php?r=0593");				  
            }catch(PDOException $e){

            }
        }

        public function logout(){
            session_start();
            $_SESSION['logged'] = false;
            session_unset();
            session_destroy();
            header("location:../../");                
        }

        public function ApplyJob(){
                        
            $apply_date = date('m/d/Y');
        
            if (isset($_SESSION['logged']) && $_SESSION['logged'] == true) {
                $myid   = $_SESSION['myid'];	
                $myrole = $_SESSION['role'];
                $status = $_SESSION['status'];
                $opt    = $_GET['opt'];
                $opt1   = $_GET['opt1'];
            
                if ($status == 1){            
                    try {
                        $job_aplicattions = $this->db->query("SELECT * FROM tbl_job_applications WHERE iduser = ".$myid." AND job_id = :jobid",
                        array(
                            "jobid"=> $opt
                        ));
                        $rec = count($job_aplicattions);
            
                        $usuarios = $this->db->query("select tcuenta,id from tbl_usuarios as A inner join tbl_jobs as B where B.company = A.id and B.job_id=:jobid",
                        array(
                            "jobid"=> $opt
                        ));
            
                        $TCuenta = $usuarios[0]['tcuenta'];
                        $Company = $usuarios[0]['id'];
                        $num = 0;
                        $conteo = 0;
            
                        $job_aplicattions = $this->db->query("SELECT COUNT(*) as number_job FROM tbl_job_applications WHERE company = :company",
                        array(
                            ":company",$Company
                        ));
                        
                        if ($job_aplicattions != null){
                            $conteo = $job_aplicattions[0]['number_job'];
                        } 
            
                        switch($TCuenta){
                            case 1:
                                $num = 6;
                                break;
                            case 2:
                                $num = 30;
                                break;
                            case 3:
                                $num = 99999;
                                break;
                        }
                
                        if ($rec == 0) {
                        
                            try {
                                $checkExperience = $this->db->query("select B.tags from tbl_usuarios as A inner join tbl_usuarios_data as B ON A.id = B.iduser where A.id=".$myid."");
                                $checkExperienceStatus = 0; 
                
                                if ($checkExperience[0]["tags"] != ""){
                                    $checkExperienceStatus = 1; 
                                }
                
                                if ($checkExperienceStatus){
                                    if($conteo < $num){
                                        $this->db->query("INSERT INTO tbl_job_applications (iduser, company, job_id, application_date)
                                        VALUES (:memberno, :company, :jobid, :appdate)",array("memberno"=>$myid,"company"=>$Company,"jobid"=>$opt,"appdate"=>$apply_date));
                
                                        // $jobs = $this->db->query("SELECT email FROM `tbl_jobs` as A inner join tbl_usuarios as B where A.company = B.id and A.job_id=:jobid",
                                        // array("jobid"=>$opt));
                        
                                        // $email = "";
                                        // foreach($jobs as $job) {
                                        //     $email = $job['email'];
                                        // }

                                        $email = $_SESSION['myemail'];
                                
                                        $user = $_SESSION['name'] . " " .$_SESSION['surname'];
                                        $link = "https://identikit.app/employer/postulados.php?jobid=" . $opt;
    
                                        require('helper.php');

                                        $helper = new helper();
                                        $helper->SendEmailApplyJob($user,$email,$opt1,$link);

                                        print '<br>
                                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                                    <strong>Felicidades!</strong> Aplicaste con exito.
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>';
                
                                    } else {
                                        print '<br>
                                                <div class="alert alert-info alert-dismissible fade show" role="alert">
                                                    La empresa no puede recibir mas candidatos
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>'; 
                                    }
                                } else {
                                    print '<br> 
                                            <div class="alert alert-info alert-dismissible fade show" role="alert">
                                                Favor de completar tu perfil antes de postularte.
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>'; 
                                }
                    }catch(PDOException $e){
            
                }
            
                }else{
                            print '<br>
                            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                <strong>Hey!</strong> Ya aplicaste a este empleo!
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                            </div>';
            
                    }}catch(PDOException $e){
            
                    }
                }
            }
          }
        
        
        
        
        
        public function uploadVideoCv(){    
            
            $myid        = $_POST['hiddenId'];
            $Namefile    = basename($_FILES["fileToUpload"]["name"]);
            $TmpName     = $_FILES['fileToUpload']['tmp_name'];
            $beforepath  = '../../';

            $path        = 'data/employee/' . $myid . '/' . $Namefile;
            $base_path   = 'data/employee/' . $myid . '/';
            $realpath    = $beforepath . $base_path;

            if (!file_exists($realpath)) {
                mkdir($realpath, 0777, true);
            }
            
            $position      = strpos($Namefile, ".");
            $fileextension = substr($Namefile, $position + 1);
            $fileextension = strtolower($fileextension);
            
            if (isset($Namefile)) {
                if (empty($Namefile)){
                    //echo "Please choose a file";
                    header("location:../views/employee/miperfil.php?r=9215");					  
                }
                else if (!empty($Namefile)){
                    if (($fileextension !== "mp4") && ($fileextension !== "ogg") && ($fileextension !== "webm")){
                        echo "The file extension must be .mp4, .ogg, or .webm in order to be uploaded";
                    }else if (($fileextension == "mp4") || ($fileextension == "ogg") || ($fileextension == "webm")){
                        if (move_uploaded_file($TmpName, $realpath . '/' . $Namefile )) {
                            $existvideo = $this->db->query("select id from tbl_users_data where member_no=:member_no",array("member_no"=>$myid));
                            if (!$existvideo){
                                $this->db->query("insert into tbl_users_data(member_no,path_video_cv) 
                                VALUES (:member_no,:path_video_cv)",
                                array(
                                    "member_no"             => $myid,
                                    "path_video_cv"         => $path
                                ));    
                            } else { 
                                $this->db->query("update tbl_users_data set path_video_cv=:path_video_cv where member_no=:member_no",
                                array(
                                    "member_no"             => $myid,
                                    "path_video_cv"         => $path
                                ));   
                            }
                            header("location:../views/employee/miperfil.php?r=9215");					  
                        }
                    } 
                }
            }
        }   
    }
?>
