<?php
    require_once("../../db/db.php");


    class Auth {
        function __construct(){
            $this->db = new DbPDO(); 
        }
        public function Auth(){
            date_default_timezone_set('America/Argentina/Buenos_Aires');
            $last_login = date('d-m-Y h:m A [T P]');
            
            $myemail = $_POST['email'];
            $mypass = md5($_POST['password']);

            try {
            
                $usuarios = $this->db->query("SELECT * FROM tbl_users WHERE email = :myemail AND login = :mypassword", array("myemail"=>$myemail,"mypassword"=>$mypass));
                
                $rec = count($usuarios);

                if ($rec == "0") {
                    header("location:../views/auth/login.php?r=0346");
                }else{
                foreach($usuarios as $usuario){
                    $role = $usuario['role'];
                    if ($role == "employee") {
                        session_start();
                        $_SESSION['logged']  = true;
                        $_SESSION['myid']    = $usuario['id'];
                        $_SESSION['myfname'] = $usuario['first_name'];
                        $_SESSION['mylname'] = $usuario['last_name'];
                        $_SESSION['myemail'] = $usuario['email'];
                        $_SESSION['mydate']  = $usuario['bdate'];
                        $_SESSION['mymonth'] = $usuario['bmonth'];
                        $_SESSION['myyear'] = $usuario['byear'];
                        $_SESSION['myphone'] = $usuario['phone'];
                        $_SESSION['myedu'] = $usuario['education'];
                        $_SESSION['mytitle'] = $usuario['title'];
                        $_SESSION['mycity'] = $usuario['city'];
                        $_SESSION['street'] = $usuario['street'];
                        $_SESSION['myzip'] = $usuario['zip'];
                        $_SESSION['mycountry'] = $usuario['country'];
                        $_SESSION['mydesc'] = $usuario['about'];
                        $_SESSION['dni'] = $usuario['dni'];
                        $_SESSION['ability'] = $usuario['ability'];
                        $_SESSION['github'] = $usuario['github'];
                        $_SESSION['twitter'] = $usuario['twitter'];
                        $_SESSION['instagram'] = $usuario['instagram'];
                        $_SESSION['empujar'] = $usuario['empujar'];

                        $_SESSION['avatar'] = $usuario['avatar'];
                        $_SESSION['lastlogin'] = $usuario['last_login'];
                        $_SESSION['avatar'] = $usuario['avatar'];
                        $_SESSION['gender'] = $usuario['avatar'];
                        $_SESSION['role'] = $role;
                }
                if ($role == "employer"){
                    session_start();
                    $_SESSION['logged'] = true;	
                    $_SESSION['myid'] = $usuario['id'];
                    $_SESSION['compname'] = $usuario['first_name'];
                    $_SESSION['established'] = $usuario['byear'];
                    $_SESSION['myemail'] = $usuario['email'];
                    $_SESSION['myphone'] = $usuario['phone'];
                    $_SESSION['comptype'] = $usuario['title'];
                    $_SESSION['mycity'] = $usuario['city'];
                    $_SESSION['mystreet'] = $usuario['street'];
                    $_SESSION['myzip'] = $usuario['zip'];
                    $_SESSION['mycountry'] = $usuario['country'];
                    $_SESSION['mydesc'] = $usuario['about'];
                    $_SESSION['avatar'] = $usuario['avatar'];
                    $_SESSION['myserv'] = $usuario['services'];
                    $_SESSION['myexp'] = $usuario['expertise'];
                    $_SESSION['lastlogin'] = $usuario['last_login'];
                    $_SESSION['website'] = $usuario['website'];
                    $_SESSION['people'] = $usuario['people'];
                    $_SESSION['role'] = $role;
                    $_SESSION['TCuenta'] = $usuario['TCuenta'];
                }

                try {

                    $this->db->query("UPDATE tbl_users SET last_login = :lastlogin WHERE email= :email", array("lastlogin"=>$last_login,"email"=>$myemail));
                
                    //Add register to the DB

                    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                        $ip = $_SERVER['HTTP_CLIENT_IP'];
                    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                    } else {
                        $ip = $_SERVER['REMOTE_ADDR'];
                    }
                    
                    $query = @unserialize(file_get_contents('http://ip-api.com/php/'.$ip));
        
                    $city    = '';
                    $region  = '';
                    $country = '';
                

                    if($query && $query['status'] == 'success'){
                        $city         = $query['city'];
                        $region       = $query['region'];
                        $country      = $query['country'];
                    } 

                    $statistics_type_member   = $role;
                    $statistics_ip            = $ip;
                    $statistics_city          = $city;
                    $statistics_region        = $region;
                    $statistics_country       = $country;
                    $statistics_member_no     = $_SESSION['myid'];
                    $statistics_date          = date("Y/m/d");
                    $statistics_hour          = date("h:i:sa");

                    $this->db->query("INSERT into tbl_statistics (statistics_member_no,statistics_type_member, statistics_ip, statistics_country, statistics_city,statistics_region,statistics_date, statistics_hour)
                    VALUES('".$statistics_member_no."', '".$statistics_type_member."', '".$statistics_ip."', '".$statistics_country."','".$statistics_city."','".$statistics_region."','".$statistics_date."','".$statistics_hour."')");

                    header("location:../views/$role");

            }catch(PDOException $e)
            {

            }

            }
            
            }

                            
            }catch(PDOException $e)
            {

            }
        }

        public function Newauth(){
            date_default_timezone_set('America/Argentina/Buenos_Aires');
            $last_login = date('d-m-Y h:m A [T P]');
            
            $myemail = $_POST['email'];
            $mypass  = md5($_POST['password']);
            
            $usuarios = $this->db->query("SELECT A.*, B.ability FROM tbl_usuarios as A left join tbl_usuarios_data as B ON A.id = B.iduser WHERE A.email = :myemail AND pass = :mypassword", 
            array("myemail"=>$myemail,"mypassword"=>$mypass));

            $rec = count($usuarios);

            if ($rec == "0") {
                header("location:../views/auth/login-new.php?r=0346");
            } else {
                session_start();
                $_SESSION['myid']            = $usuarios[0]['id'];
                $_SESSION['myavatar']        = $usuarios[0]['path'];
                $_SESSION['name']            = $usuarios[0]['name'];
                $_SESSION['surname']         = $usuarios[0]['surname'];
                $_SESSION['myphone']         = $usuarios[0]['cellphone'];
                $_SESSION['country']         = $usuarios[0]['country'];
                $_SESSION['ability']         = $usuarios[0]['ability'];
                $_SESSION['street']          = $usuarios[0]['province'];
                $_SESSION['myemail']         = $usuarios[0]['email'];
                $_SESSION['identification']  = $usuarios[0]['identification'];
                $_SESSION['idgrupo']         = $usuarios[0]['idgrupo'];
                $_SESSION['status']          = $usuarios[0]['status'];
                $_SESSION['TCuenta']         = $usuarios[0]['tcuenta'];
                $_SESSION['logged']          = true;
                $_SESSION['role']            = 'employee';
                
                if($_SESSION['status'] == '1'){
                    header("location:../views/employee/index-nuevo.php");
                } 

                if($_SESSION['status'] == '2'){
                    header("location:../views/employer/index.php");
                }
            }

            // $role = $usuarios[0]['role'];
            // $usuario = $usuarios[0];
            
            // if($role == 'employee'){
            //     session_start();
            //     $_SESSION['logged']  = true;

            //     // $_SESSION['myid']    = $usuario['member_no'];
            //     // $_SESSION['myfname'] = $usuario['first_name'];
            //     // $_SESSION['mylname'] = $usuario['last_name'];
            //     // $_SESSION['myemail'] = $usuario['email'];
            //     // $_SESSION['mydate']  = $usuario['bdate'];
            //     // $_SESSION['mymonth'] = $usuario['bmonth'];
            //     // $_SESSION['myyear'] = $usuario['byear'];
            //     // $_SESSION['myphone'] = $usuario['phone'];
            //     // $_SESSION['myedu'] = $usuario['education'];
            //     // $_SESSION['mytitle'] = $usuario['title'];
            //     // $_SESSION['mycity'] = $usuario['city'];
            //     // $_SESSION['street'] = $usuario['street'];
            //     // $_SESSION['myzip'] = $usuario['zip'];
            //     // $_SESSION['mycountry'] = $usuario['country'];
            //     // $_SESSION['mydesc'] = $usuario['about'];
            //     // $_SESSION['dni'] = $usuario['dni'];
            //     // $_SESSION['ability'] = $usuario['ability'];
            //     // $_SESSION['github'] = $usuario['github'];
            //     // $_SESSION['twitter'] = $usuario['twitter'];
            //     // $_SESSION['instagram'] = $usuario['instagram'];
            //     // $_SESSION['empujar'] = $usuario['empujar'];

            //     // $_SESSION['avatar'] = $usuario['avatar'];
            //     // $_SESSION['lastlogin'] = $usuario['last_login'];
            //     // $_SESSION['avatar'] = $usuario['avatar'];
            //     // $_SESSION['gender'] = $usuario['avatar'];
            //     // $_SESSION['role'] = $role;
            // }


        }
        
        public function ChangePass(){
            session_start();
            $usermail     = $_SESSION['resetmail'];
            $new_password = md5($_POST['password']);
    
            try {
                
                $this->db->query("UPDATE tbl_users SET login = :newlogin WHERE email= :email",
                array(
                    "newlogin" => $new_password,
                    "email"    => $usermail
                ));
                $this->db->query("DELETE FROM tbl_tokens WHERE email = :email",
                array(
                    "email"    => $usermail
                ));
                $_SESSION['resetmail'] = "";
                header("location:../views/auth/login.php?r=3091");
                                
            }catch(PDOException $e)
            {
        
            }
        
        }

        public function changeNewPassLogin(){
            session_start();
            $usermail     = $_SESSION['resetmail'];
            $new_password = md5($_POST['password']);
    
            try {
                $this->db->query("UPDATE tbl_usuarios SET pass = :newlogin WHERE email= :email",
                array(
                    "newlogin" => $new_password,
                    "email"    => $usermail
                ));
                $this->db->query("DELETE FROM tbl_tokens WHERE email = :email",
                array(
                    "email"    => $usermail
                ));
                $_SESSION['resetmail'] = "";
                header("location:../views/auth/login-new.php?r=3091");
            }catch(PDOException $e)
            {
        
            }
        }

        public function ChangeNewPass(){
            session_start();
            $usermail     = $_SESSION['resetmail'];
            $new_password = md5($_POST['password']);
    
            try {
                
                $this->db->query("UPDATE tbl_users SET login = :newlogin WHERE email= :email",
                array(
                    "newlogin" => $new_password,
                    "email"    => $usermail
                ));
                $this->db->query("DELETE FROM tbl_tokens WHERE email = :email",
                array(
                    "email"    => $usermail
                ));
                $_SESSION['resetmail'] = "";
                header("location:../views/auth/login.php?r=3091");
                                
            }catch(PDOException $e)
            {
        
            }
        
        }
        
        public function ResetPw($opt){    
            require '../../config/uniques.php';

            $email = $_GET['email'];
        
            try {
                $usuarios = $this->db->query("SELECT * FROM tbl_usuarios WHERE email = :email",array("email"=>$email));
                $rec = count($usuarios);
                
                    if ($rec == "0") {
                        print '
                            <div class="alert alert-warning">
                                No hay cuentas asocidas a ese email <strong>'.$email.'</strong>
                            </div>';
                    } else{
                        
                            try{
                                $myfname   = $usuarios[0]['name'];
                                $mylname   = $usuarios[0]['surname'];
                                $email     = $usuarios[0]['email'];
                                $full_name = "$myfname $mylname";
        
                                $idt       = 'token'.get_rand_numbers(17).'';
                                $token     = md5($idt);
                                $def_link  = 'https://'.$_SERVER['HTTP_HOST'].'/app/views/auth/new-reset.php?token='.$token.'';
        
                                $this->db->query("DELETE FROM tbl_tokens WHERE email = :email",array("email"=>$email));
                                
                                $this->db->query("INSERT INTO tbl_tokens (email, token) VALUES (:email, :token)",array("email"=>$email,"token"=>$token));
                                        
                                require('helper.php');

                                $helper = new helper();
                                $helper->SendMessageResetPw($full_name,$email,$def_link);
                                header("location:../views/auth/recuperar.php?r=3091");

                            }
                            catch (PDOException $e) {
                                echo 'Excepción capturada: ',  $e->getMessage(), "\n";
                            }
                        
                    }
                    
                }  catch (PDOException $e) {
                    echo 'Excepción capturada: ',  $e->getMessage(), "\n";
                }
        }

        private function CreateAccountEmployee(){
            try {
                require '../../config/uniques.php';
                $role = 'employee';
                $account_type = $_POST['acctype'];
                $empujar = $_POST['empujar'];
                $last_login = date('d-m-Y h:m A [T P]');
                $member_no = 'EM'.get_rand_numbers(9).'';
                $fname = ucwords($_POST['fname']);
                $lname = ucwords($_POST['lname']);
                $email = $_POST['email'];
                $login = md5($_POST['password']);
                
                $this->db->query("INSERT INTO tbl_users (first_name, last_name, email, last_login, login, role, member_no, empujar) 
                VALUES (:fname, :lname, :email, :lastlogin, :login, :role, :memberno, :empujar)",
                array(
                    "fname"     => $fname,
                    "lname"     => $lname, 
                    "email"     => $email, 
                    "lastlogin" => $last_login,
                    "login"    => $login,
                    "role"      => $role, 
                    "memberno"  => $member_no,
                    "empujar"   => $empujar
                ));
               
                $user = $fname . " " . $lname;

                include('helper.php');

                $helper = new helper();
                $helper->CreateAccountMessage($user, $email);
            
                header("location:../views/auth/login.php?p=identi&r=1123");				  
            }catch(PDOException $e){
                header("location:../views/auth/login.php?p=identi&r=4568");
            }	            
        }

        private function CreateAccountEmployer(){
            try {
                require '../../config/uniques.php';
                $role = 'employer';
                $account_type = $_POST['acctype'];
                $last_login = date('d-m-Y h:m A [T P]');
                $comp_no = 'CM'.get_rand_numbers(9).'';
                $cname = ucwords($_POST['company']);
                $ctype = ucwords($_POST['type']);
                $email = $_POST['email'];
                $login = md5($_POST['password']);
            
                $this->db->query("INSERT INTO tbl_users (first_name, title, email, last_login, login, role, member_no) 
                VALUES (:fname, :title, :email, :lastlogin, :login, :role, :memberno)",
                array(
                    "fname"     => $cname,
                    "title"     => $ctype,
                    "email"     => $email,
                    "lastlogin" => $last_login,
                    "login"     => $login,
                    "role"      => $role,
                    "memberno"  => $comp_no
                ));
                header("location:../views/auth/login.php?p=empresa&r=1123");				  
            }catch(PDOException $e){
                header("location:../views/auth/login.php?p=empresa&r=4568");
            }	
            
        }

        public function checkEmail(){
            try {
                $email = $_POST['email'];
                $account_type = $_POST['acctype'];
                
                $usuarios = $this->db->query("SELECT * FROM tbl_users WHERE email = :email",array("email"=>$email));
                $records = count($usuarios);
                
                if ($account_type == "101") {
                    $role = "Employee";	
                }else{
                    $role = "Employer";	
                }
                
                if ($records > 0) {
                    header("location:../views/auth/login.php?p=$role&r=0927");	
                }else{
                    if ($account_type == "101") {
                        $this->CreateAccountEmployee();
                    }else{
                        $this->CreateAccountEmployer();
                    }    
                }             
            }catch(PDOException $e){
                header("location:../views/auth/login.php?p=$role&r=4568");	
            }
        }

        public function logout(){
            session_start();
            $_SESSION['logged'] = false;
            session_unset();
            session_destroy();
            header("location:../../");
        }
    }
?>
