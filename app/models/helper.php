<?php
    require_once("../../db/db.php");

    use PHPMailer\PHPMailer\PHPMailer; 

    require_once "../../src/PHPMailer/PHPMailer.php"; 
    require_once "../../src/PHPMailer/SMTP.php"; 
    require_once "../../src/PHPMailer/Exception.php"; 

    class helper {
        function __construct(){
            // include '../views/employer/constants/check-newlogin.php';
            $this->db   = new DbPDO(); 
            $this->mail = new PHPMailer(); 
        }

        public function AddLike(){
            $job = $_GET["job_id"];
        
            $getEmployerJob =  $this->db->query("select company from tbl_jobs where job_id=:job_id",array("job_id"=>$job));
            $checklike =  $this->db->query("select like_id from tbl_likes where like_user_id = :like_user_id and like_post_job = :like_post_job",
            array(
                "like_user_id"=>$_SESSION["myid"],
                "like_post_job"=>$job)
            );
        
            if ($checklike == null){
                $this->db->query("INSERT into tbl_likes (like_user_id, like_post_job,like_member_no) 
                VALUES(:like_user_id,:like_post_job,:like_member_no)",
                array(
                    "like_user_id"=>$_SESSION["myid"],
                    "like_post_job"=>$job,
                    "like_member_no"=>$getEmployerJob[0]["company"]
                ));
            } else {
                $this->db->query("DELETE from tbl_likes where like_user_id=:like_user_id and like_post_job=:like_post_job",array("like_user_id"=>$_SESSION["myid"],"like_post_job"=>$job));
            } 
        }

        public function AddLikeCv($member_no){ 
            $member_no = $_GET["AddLikeCv"];

            $checkVideoCvlike =  $this->db->query("select id from tbl_likes_videocv 
            where like_user_id = '".$_SESSION["myid"]."' and like_member_no = '".$member_no."'",
            array(
                "like_user_id"  => $_SESSION["myid"],
                "like_member_no"=> $member_no));
        
            if ($checkVideoCvlike == null){
                $this->db->query("INSERT into tbl_likes_videocv (like_user_id, like_member_no) 
                VALUES(:like_user_id,:like_member_no)",
                array(
                    "like_user_id"  => $_SESSION["myid"],
                    "like_member_no"=> $member_no
                ));
            } else {
                $this->db->query("DELETE from tbl_likes_videocv where like_user_id=:like_user_id and like_member_no=:like_member_no",
                array("like_user_id"=>$_SESSION["myid"],"like_member_no"=>$member_no));
            } 

        }

        private function startPhpMailer(){
            $this->mail->isSMTP(); 
            $this->mail->Host         = 'smtp.hostinger.com';
            $this->mail->SMTPAuth     = true; 

            $this->mail->Username     = "hola@identikit.app";  
            $this->mail->Password     = "bB*TX27gR#Fs"; 
            $this->mail->Port         = 465; 
            $this->mail->SMTPSecure   = "ssl"; 
            $this->mail->CharSet      = 'UTF-8';
            $this->mail->isHTML(true); 
            $this->mail->setFrom("hola@identikit.app", "Pablo de IDentiKIT.app"); 
        }

        private function endPhpMailer($email){
      
            if (preg_match('/(.*)@(hotmail)\.(.*)/', $email) != false) { 
                $this->mail->addCustomHeader('Mime-Version','1.0');
                $this->mail->addCustomHeader('Content-Type: text/html; charset=ISO-8859-1');
            }   
            
            if(!$this->mail->Send()) {
                $error = 'Mail error: '.$this->mail->ErrorInfo; 
                return false;
            } else {
                return;
            }
        }

        function CreateAccountMessage($user, $email){
            
            $this->startPhpMailer();
           
            $this->mail->addAddress($email); 
            $this->mail->Subject      = ('Bienvenid@ a IDentiKIT'); 
            $this->mail->addEmbeddedImage('../../public/img/correo/Bienvenida.png', 'IBienvenida', 'attachment', 'base64', 'image/png');
            $this->mail->addEmbeddedImage('../../public/img/correo/ILogo.png', 'ILogo', 'attachment', 'base64', 'image/png');

            $this->mail->Body = "    
                <center>
                    <img src='cid:ILogo' class='img-logo' width='100' height='100' />
                    <h1 style='padding:0 0 10px 0;width:100%;text-align:center;font-family:Arial;font-size:22px;font-weight:bold;line-height:28px;'>¡Bienvenido/a, " .$user. " !</h1>
                    <br>
                    <br>
                    <img src='cid:IBienvenida' class='img-logo' />
                    <p style='padding:0 0 25px 0;width:100%;text-align:center;font-family:Arial;font-size:16px;line-height:23px;'>
                        ¡Te damos la bienvenida a IDentiKIT, ahora puedes postularte en las mejores empresas tecnologicas y obtener tu primera experiencia laboral.¡Recuerda completar tu perfil para que nuestra plataforma pueda ayudarte!.
                    </p>
                    <a href='https://identikit.app/login.php' style='padding:0.6em 2em;border-radius: 40px;color:#fff;font-size:1.1em; border:0;cursor:pointer;margin:1em; background-color:#01c0fe; text-decoration:none'>Iniciar sesión</a>
                    
                </center>
            ";

            $this->endPhpMailer($email);          
        }
            
        function SendEmailChangeStatus($email, $img, $name, $empresa, $vacante,$status){
    
            $this->startPhpMailer();

            $this->mail->addAddress($email); 
            $this->mail->Subject = ("Actualizacion del estado"); 

            $this->mail->AddEmbeddedImage('../../public/img/correo/'.$img.'', 'ILogo', 'attachment', 'base64', 'image/png');

            switch($status){
                case 2:
                    $this->mail->Body = "    
                    <center>
                        Hola ".$name." tu solicitud para ".$vacante." en ".$empresa." esta en REVISION
                        <img src='cid:ILogo' class='img-logo'/> <br>
                    </center>";                
                    break;
                case 3:
                    $this->mail->Body = "    
                    <center>
                        Hola ".$name." la empresa ".$empresa." quiere organizar una entrevista contigo porque le parecio interesante tu perfil. Ingresa a la plataforma para continuar el proceso.
                        <br> <img src='cid:ILogo' class='img-logo'/> <br>
                    </center>";    
                    break;
                case 4:
                    $this->mail->Body = "    
                    <center>
                        Felicidades ".$name." fuiste aceptado para el puesto de ".$vacante." en ".$empresa." desde IDentiKIT queremos felicitarte y darte las gracias por utilizar nuestra plataforma, exito en esta nueva etapa! <br>
                        <img src='cid:ILogo' class='img-logo'/> <br>
                    </center>";    
                    break;
                case 5:
                    $this->mail->Body = "    
                    <center>
                        Hola nombre usuario lamentablemente no fuiste seleccionado para nombre trabajo en nombre empresa. Pero... Animos! Tenemos muchas mas ofertas de trabajos IDeales para tu puesto, ingresa a la IDentiKIT y continua la busqueda! Te dejamos este link para que mejores tus habilidades a la hora de buscar trabajo https://idkit.com.ar/academy.php
                    </center>";    
                    break;
            }

            $this->endPhpMailer($email);   
        }

        function SendEmailApplyJob($user,$email,$opt1,$link){
            $this->startPhpMailer();

            $this->mail->addAddress($email);
            $this->mail->Subject      = ('IDentiKIT'); 

            $this->mail->EmbeddedImage = "'../../public/img/correo/IFelicidades.png', 'IFelicidades', 'attachment', 'base64', 'image/png'";

            $this->mail->Body = "    
            <center>
                <img src='cid:IFelicidades' class='img-logo' /> <br>
                ¡Hola! estamos muy felices de 
                contarte que " . $user ." se postulo para la vacante: \"".$opt1."\"  
                con el link: \" ". $link ." \"
            </center>";
            
            $this->endPhpMailer($email);
        }

        function SendMessageResetPw($full_name,$email,$def_link){
            $this->startPhpMailer();
            
            $this->mail->addAddress($email);
            $this->mail->Subject      = ($full_name.' crea tu contraseña de IDentiKIT.app '); 
            
            $this->mail->Body = "Hola11! ". $full_name. ", en el siguiente enlace puedes crear la contraseña para tu cuenta <a href='".$def_link."'>". 
                                $def_link ."</a> de IDentiKIT.app ";   

            if (preg_match('/(.*)@(hotmail)\.(.*)/', $email) != false) { 
                $this->mail->addCustomHeader('Mime-Version','1.0');
                $this->mail->addCustomHeader('Content-Type: text/html; charset=ISO-8859-1');
            }   
            
            if(!$this->mail->Send()) {
                $error = 'Mail error: '.$this->mail->ErrorInfo; 
                print '
                <div class="alert alert-danger">
                    Ocurrio un error .
                </div>';
                return false;
            } else {
                print '
                <div class="alert alert-info">
                    Enviamos un correo a '.$email.' con instrucciones para crear la cuenta. (Si no encuentras el email por favor revisa la carpeta de spam)
                </div>';
                return;
            }
        }
    }

?>