<?php
    $opt = $_GET['opt'];
    require '../constants/settings.php';
    require '../constants/db_config.php';
    require '../constants/uniques.php';

    use PHPMailer\PHPMailer\PHPMailer;
    require_once "../PHPMailer/PHPMailer.php"; 
    require_once "../PHPMailer/SMTP.php"; 
    require_once "../PHPMailer/Exception.php"; 

    try {
        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $conn->prepare("SELECT * FROM tbl_users WHERE email = :email");
        $stmt->bindParam(':email', $opt);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $rec = count($result);
        
            if ($rec == "0") {
                print '
                    <div class="alert alert-warning">
                    No hay cuentas asocidas a ese email <strong>'.$opt.'</strong>
                    </div>';
            }else{
                foreach($result as $row){
                    try{
                        $myfname   = $row['first_name'];
                        $mylname   = $row['last_name'];
                        $email    = $row['email'];
                        $full_name = "$myfname $mylname";

                        $idt       = 'token'.get_rand_numbers(17).'';
                        $token     = md5($idt);
                        $def_link  = 'https://'.$_SERVER['HTTP_HOST'].'/reset.php?token='.$token.'';

                        $stmt      = $conn->prepare("DELETE FROM tbl_tokens WHERE email = :email");
                        $stmt->bindParam(':email', $opt);
                        $stmt->execute();

                        $stmt = $conn->prepare("INSERT INTO tbl_tokens (email, token) VALUES (:email, :token)");
                        $stmt->bindParam(':email', $opt);
                        $stmt->bindParam(':token', $token);
                        $stmt->execute();	

                        $message = "Hola! ". $full_name. ", en el siguiente enlace puedes crear la contraseña para tu cuenta <a href='".$def_link."'>". $def_link ."</a> de IDentiKIT.app";   

                        SendMail($email, $message);
                   
                    }
                    catch (PDOException $e) {
                        echo 'Excepción capturada: ',  $e->getMessage(), "\n";
                    }
                }
            }
        }  catch (PDOException $e) {
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        }

        function SendMail($email, $message){

            $mail = new PHPMailer(); 
        
            $mail->isSMTP(); 
            $mail->Host = 'smtp.hostinger.com';
            $mail->SMTPAuth = true; 
            $mail->Username = "hola@identikit.app"; 
            $mail->Password = "bB*TX27gR#Fs"; 
            $mail->Port = 465; 
            $mail->SMTPSecure = "ssl"; 
            $mail->CharSet = 'UTF-8';
            
            $mail->isHTML(true); 
            $mail->setFrom("hola@identikit.app","<no reply>");
            $mail->addAddress($email); 
            $mail->Subject = ("Actualizacion del estado"); 
            $mail->Body    = $message;
    
            if (preg_match('/(.*)@(hotmail)\.(.*)/', $email) != false) { 
                $mail->addCustomHeader('Mime-Version','1.0');
                $mail->addCustomHeader('Content-Type: text/html; charset=ISO-8859-1');
            }   
        
             
            if(!$mail->send()) {
                print '
                    <div class="alert alert-danger">
                        Ocurrio un error .
                    </div>';
            } else {
                print '
                <div class="alert alert-info">
                    Enviamos un correo a '.$email.' con instrucciones para restablecer la cuenta. (Revisar spam si no te llego)
                </div>';
            }

            
        }
?>
