<?php 
	include '../../../config/settings.php'; 
	include 'constants/check-login.php';
	
	require_once("../../../db/db.php");

	$db = new DbPDO();

	if ($user_online == "true") {
		if ($myrole == "employer") {
			} else {
				header("location:../");		
	}}else{
		header("location:../");	
	}

	$empresaId              = $_SESSION['myid'];

	if (isset($_GET['jobid'])) {
		//require '../../../config/db_config.php';
		$job_id = $_GET['jobid'];

	try {
		$trabajos = $db->query("SELECT * FROM tbl_jobs WHERE job_id = :jobid AND company = :myid",array("jobid"=>$job_id,"myid"=>$myid));

		$rec = count($trabajos);

		if ($rec == "0") {
			header("location:../");		
		}else{
			foreach($trabajos as $trabajo){
				$job_title = $trabajo['title'];
			}}}catch(PDOException $e){
		}
	}
?>

  <!-- js -->

<!DOCTYPE html>
<html>

<head>
	<!-- Basic Page Info -->
	<meta charset="utf-8">
	<title>IDentiKIT - Mis postulados</title>

	
	<link rel="apple-touch-icon" sizes="180x180" href="logov3.png">
	<link rel="icon" type="image/png" sizes="32x32" href="logov3.png">
	<link rel="icon" type="image/png" sizes="16x16" href="logov3.png">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/datatables/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/style.css">

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-PHBQLSG');</script>
	<!-- End Google Tag Manager -->


</head>
<body>

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PHBQLSG"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<?php include 'layouts/Header.php';?>
	<?php include 'layouts/Sidebar-menu.php';?>

	<div class="mobile-menu-overlay"></div>

    <div class="main-container">
        <div class="pd-ltr-20 xs-pd-20">
            <div class="min-height-200px">
				<div class="card-box mb-30">			
					<div class="pd-20">
						<h4 class="h4">Postulados en <?php echo "$job_title"; ?></h4>
					</div>
					<div class="pb-20">
						<table class="data-table table stripe hover nowrap">
							<thead>
								<tr>
									<th class="table-plus datatable-nosort">Nombre y apellido</th>
									<th>Ciudad</th>
									<th>Edad</th>
									<th>Estado</th>
									<th class="datatable-nosort">Acciones</th>
								</tr>
							</thead>
							<tbody>
							<?php				

								$TrabajosAplicados = $db->query("SELECT * FROM tbl_job_applications WHERE job_id = ".$job_id." ORDER BY id desc");

								$contador =  0;
								foreach($TrabajosAplicados as $TrabajoAplicado)
								{
									$post_date = date_format(date_create_from_format('m/d/Y', $TrabajoAplicado['application_date']), 'd');
									$post_month = date_format(date_create_from_format('m/d/Y', $TrabajoAplicado['application_date']), 'F');
									$post_year = date_format(date_create_from_format('m/d/Y', $TrabajoAplicado['application_date']), 'Y');
									$emp_id = $TrabajoAplicado['member_no'];
									$EVacante = $TrabajoAplicado['EVacante'];
									$usuarios = $db->query("SELECT * FROM tbl_users WHERE role = 'employee' AND member_no = :EmpId",array('EmpId'=>$emp_id));
									
									foreach ($usuarios as $usuario){
										$empavatar = $usuario['avatar'];
										$bdate = $usuario['bdate'];
										$bmonth = $usuario['bmonth'];
										$byear = $usuario['byear'];
										$email = $usuario['email'];
										$name = $usuario['first_name'];
										$current_year = date('Y');
										
										if ($byear != '-'){
											$myage = $current_year - $byear;
										} else {
											$myage = '';
										}
										
								?>
								<tr>
									<td class="table-plus"><?php echo $usuario['first_name'] ?> <?php echo $usuario['last_name'] ?></td>
									<td><?php echo $usuario['city'] ?></td>
									<td><?php echo $myage?> </td>
									<td>
										<?php 
											$color = "";
											$text  = "";
											switch($EVacante){
												case 1:
													$color = "secondary";
													$text  = "Entregado";
													break;
												case 2:
													$color = "warning";
													$text  = "En revision";
													break;
												case 3:
													$color = "info";
													$text  = "Entrevista";
													break;
												case 4:
													$color = "success";
													$text  = "aceptado";
													break;
												case 5:
													$color = "danger";
													$text  = "Rechazado";
													break;
											}
											echo '<button class="btn btn-'.$color.'" style="opacity:1.0">'.$text.'</button>';
										?> 
									</td>

									<td>
										<div class="dropdown">
											 <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
												<i class="dw dw-more"></i>
											</a>
											<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
												<a class="dropdown-item" href="cv.php?empid=<?=$usuario['member_no']; ?>" target="_blank"><i class="dw dw-eye"></i> Ver</a>
												<a class="dropdown-item" href="enviarmensaje.php?empid=<?=$usuario['member_no'];?>"><i class="dw dw-message"></i> Enviar mensaje</a>
												<a class="dropdown-item" id="<?= $contador ?>" onclick="status(this.id)" style="cursor:pointer"><i class="icon-copy dw dw-notification-1"></i> Actualizar estado</a>
											</div>
										</div>
									</td> 
								</tr>
								<?php
									echo '
										<div class="modal" id="myModal'.$contador.'">
										<div class="modal-dialog modal-lg">
										<div class="modal-content">

											<div class="modal-header">
												<h4 class="modal-title">Cambiar status</h4>
												<button type="button" class="close" data-dismiss="modal">&times;</button>
											</div>

											<div class="modal-body">
												<div class="status'. $contador .'">
													<button class="btn btn-warning" style="opacity:0.5" value="2" onclick="cambiarStatus(this);">En revision</button>
													...
													<button class="btn btn-info" style="opacity:0.5" value="3" onclick="cambiarStatus(this);">Entrevista</button>
													...
													<button class="btn btn-success" style="opacity:0.5" value="4" onclick="cambiarStatus(this);">Aceptado</button>
													...
													<button class="btn btn-danger" style="opacity:0.5" value="5" onclick="cambiarStatus(this);">Rechazado</button>
												</div>
													¿Estas seguro de cambiar el status?
												</div>

												<div class="modal-footer">
												<form action="../../controllers/employercontroller.php?cambiarStatus" method="POST">
													<input type="hidden" name="jobid" id="jobid" value="'.$job_id.'"/>
													<input type="hidden" name="status" id="status" value=""/>
													<input type="hidden" name="member" id="member" value="'.$emp_id.'"/>
													<input type="hidden" name="email" id="email" value="'.$email.'"/>
													<input type="hidden" name="name" id="name" value="'.$name.'"/>
													<button type="submit" class="btn btn-success">Aceptar</button>
												</form>
												<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
												</div>

											</div>
											</div>
											</div>';


											echo '
											<script>	
												function ElementOpacity(status, id){
													var doc = document.getElementsByClassName(id)[0].getElementsByTagName("button");
													
													for (var i = 0; i < status-1; i++) {
														doc[i].style="opacity:1.0";
													}
												}
												function reset(id) {
													var doc = document.getElementsByClassName(id)[0].getElementsByTagName("button");
													for (var i = 0; i <= 3; i++) {
														doc[i].style="opacity:0.5";
													}
												}

												function cambiarStatus(object){
													var parentClass = $(object).parent().attr("class");
													reset(parentClass);	
													var num = parentClass.slice(-1);

													$("#myModal" + num + " #status").val(object.value);
													
													ElementOpacity(object.value, parentClass);
												}

												ElementOpacity("'.$EVacante.'","status'.$contador.'");
											</script>';

										$contador++;	
										}
									}
							?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	function status(idModal){
		$("#myModal" + idModal).modal("show");
	}
</script>

</body>

    <script src="../../../public/complements/v1/vendors/scripts/core.js"></script>
    <script src="../../../public/complements/v1/vendors/scripts/script.min.js"></script>
    <script src="../../../public/complements/v1/vendors/scripts/process.js"></script>
    <script src="../../../public/complements/v1/vendors/scripts/layout-settings.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/jquery.dataTables.min.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/dataTables.bootstrap4.min.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/dataTables.responsive.min.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/responsive.bootstrap4.min.js"></script>
	<!-- buttons for Export datatable -->
	<script src="../../../public/complements/v1/src/plugins/datatables/js/dataTables.buttons.min.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/buttons.bootstrap4.min.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/buttons.print.min.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/buttons.html5.min.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/buttons.flash.min.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/pdfmake.min.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/vfs_fonts.js"></script>
	<!-- Datatable Setting js -->
	<script src="../../../public/complements/v1/vendors/scripts/datatable-setting.js"></script></body>
	
	<script src="../../../public/complements/v1/src/plugins/sweetalert2/sweetalert2.all.js"></script>
	<script src="../../../public/complements/v1/src/plugins/sweetalert2/sweet-alert.init.js"></script>


</html>








