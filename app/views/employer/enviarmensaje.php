<?php 
  	
    include '../../../config/settings.php'; 
    include 'constants/check-login.php';

    $postulante = $_GET["empid"];
	
    require_once("../../../db/db.php");
	$db = new DbPDO();
    
    $infoPostulante = $db->query("SELECT first_name,last_name,email from tbl_users where member_no='".$postulante."'");

    $fullname       = $infoPostulante[0]["first_name"] . " " . $infoPostulante[0]["last_name"];
    $email          = $infoPostulante[0]["email"];

    if ($user_online == "true") {
        if ($myrole == "employer") {
        } else {
            header("location:../");		
        }
	} else {
        header("location:../");	
    }
	
?>

<!DOCTYPE html>
<html> 

	<head>
		<!-- Basic Page Info -->
		<meta charset="utf-8">
		<title>IDentiKIT - Chat con <?php echo $fullname?></title>

		<link rel="apple-touch-icon" sizes="180x180" href="logov3.png">
		<link rel="icon" type="image/png" sizes="32x32" href="../logov3.png">
		<link rel="icon" type="image/png" sizes="16x16" href="../logov3.png">

		<!-- Mobile Specific Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

		<!-- Google Font -->
		<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
		<!-- CSS -->
		<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/core.css">
		<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/icon-font.min.css">
		<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
		<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/datatables/css/responsive.bootstrap4.min.css">
		<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/style.css">
		<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
		<script src="https://cdn.tiny.cloud/1/0yb7elgidejk329quamk21ozwpex0990qg48k8b1n22eyja8/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

		<!-- jQuery library -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

		<!-- Latest compiled JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-PHBQLSG');</script>
		<!-- End Google Tag Manager -->

	</head>

	<body>
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PHBQLSG"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->

		<script>
			$(document).ready(function(){														
				$('#enviarMensaje').on('click',function(){
					var mensaje     = $('#mensaje').val(); 
					var receiverId  = '<?= $_GET['empid']; ?>';
					var senderId    = '<?= $_SESSION['myid']; ?>';

					$.ajax({
						type:'POST',
						url:'../../controllers/employercontroller.php',
						dataType: "json",
						data: { 
                            'conversacion': 1,
							'mensaje':      mensaje, 
							'receiverId':   receiverId,
							'senderId':     senderId
						},
						success:function(response){
							if(response.status == "correcto"){
								$("#myModal").modal();
							}else{
								alert('ERROR');
							} 
						}
					});
				});
			});
		</script>

		<?php include 'layouts/Header.php';?>
		<?php include 'layouts/Sidebar-menu.php';?>

		<div class="mobile-menu-overlay"></div>

		<div class="main-container">
			<div class="pd-ltr-20 xs-pd-20">
				<div class="min-height-200px">
					<div class="pd-ltr-20">
					<div class="card-box pd-20 height-100-p mb-30">
						<div class="section-title-02">
							<h3 class="text-left">Enviar mensaje</h3>
						</div>
						<br>
						<div class="row gap-20">
							<div class="col-sm-2 col-md-2">
								<div class="form-group">
									<label for="Usuario"><strong>Candidato</strong></label>
									<input name="Usuario" class="form-control" value="<?=$fullname?>" disabled="disabled">
								</div>
							</div>
							<div class="clear"></div>								
							<div class="clear"></div>
							<div class="clear"></div>
							<div class="col-sm-12 col-md-12">
								<div class="form-group bootstrap3-wysihtml5-wrapper">
									<label>
										<strong>
											Mensaje
										</strong>
									</label>
									<textarea class="textarea_editor form-control border-radius-0" name="mensaje" required placeholder="Escribe tu mensaje..." id="mensaje"></textarea>												
								</div>
								<center>
									<button class="btn btn-primary col-md-12" id="enviarMensaje">Enviar</button>
								</center>
							</div>
							<div class="clear"></div>
							<div class="clear"></div>
							<div class="clear"></div>												
							<div class="clear"></div>												
							<div class="clear mb-10"></div>
							<div class="clear mb-15"></div>												
							<div class="clear"></div>
							<div class="col-sm-12 mt-30">	
								</div>
							</div>			
						</div>
						<div class="row">
							<div class="card-box height-100-p overflow-hidden col-md-12">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
	</body>

    <!-- js -->
	<script src="../../../public/complements/v1/vendors/scripts/core.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/script.min.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/process.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/layout-settings.js"></script>
	<script src="../../../public/complements/v1/src/plugins/apexcharts/apexcharts.min.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/jquery.dataTables.min.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/dataTables.bootstrap4.min.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/dataTables.responsive.min.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/responsive.bootstrap4.min.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/dashboard.js"></script>
	<script src="../../../public/complements/v1/src/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
     
    
	<script type="text/javascript">
		var useDarkMode = window.matchMedia('(prefers-color-scheme: white)').matches;

		tinymce.init({
		  selector: 'textarea#full-featured-non-premium',
		  plugins: 'print preview paste searchreplace autolink autosave save directionality visualblocks visualchars fullscreen link charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textpattern noneditable help charmap emoticons',
		  imagetools_cors_hosts: ['picsum.photos'],
		  
		  toolbar_sticky: true,
		  autosave_ask_before_unload: true,
		  autosave_interval: '30s',
		  autosave_prefix: '{path}{query}-{id}-',
		  autosave_restore_when_empty: false,
		  autosave_retention: '2m',
		  image_advtab: true,
		  link_list: [
		    { title: 'My page 1', value: 'https://www.tiny.cloud' },
		    { title: 'My page 2', value: 'http://www.moxiecode.com' }
		  ],
		  image_list: [
		    { title: 'My page 1', value: 'https://www.tiny.cloud' },
		    { title: 'My page 2', value: 'http://www.moxiecode.com' }
		  ],
		  image_class_list: [
		    { title: 'None', value: '' },
		    { title: 'Some class', value: 'class-name' }
		  ],
		  importcss_append: true,
		  file_picker_callback: function (callback, value, meta) {
		    /* Provide file and text for the link dialog */
		    if (meta.filetype === 'file') {
		      callback('https://www.google.com/logos/google.jpg', { text: 'My text' });
		    }

		    /* Provide image and alt text for the image dialog */
		    if (meta.filetype === 'image') {
		      callback('https://www.google.com/logos/google.jpg', { alt: 'My alt text' });
		    }

		    /* Provide alternative source and posted for the media dialog */
		    if (meta.filetype === 'media') {
		      callback('movie.mp4', { source2: 'alt.ogg', poster: 'https://www.google.com/logos/google.jpg' });
		    }
		  },
		  templates: [
		        { title: 'New Table', description: 'creates a new table', content: '<div class="mceTmpl"><table width="98%%"  border="0" cellspacing="0" cellpadding="0"><tr><th scope="col"> </th><th scope="col"> </th></tr><tr><td> </td><td> </td></tr></table></div>' },
		    { title: 'Starting my story', description: 'A cure for writers block', content: 'Once upon a time...' },
		    { title: 'New list with dates', description: 'New List with dates', content: '<div class="mceTmpl"><span class="cdate">cdate</span><br /><span class="mdate">mdate</span><h2>My List</h2><ul><li></li><li></li></ul></div>' }
		  ],
		  template_cdate_format: '[Date Created (CDATE): %m/%d/%Y : %H:%M:%S]',
		  template_mdate_format: '[Date Modified (MDATE): %m/%d/%Y : %H:%M:%S]',
		  height: 600,
		  image_caption: true,
		  quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
		  noneditable_noneditable_class: 'mceNonEditable',
		  toolbar_mode: 'sliding',
		  contextmenu: 'link image imagetools table',
		  skin: useDarkMode ? 'oxide-dark' : 'oxide',
		  content_css: useDarkMode ? 'dark' : 'default',
		  content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
		 });
	</script>
    <!-- The Modal -->
    <div class="modal" id="myModal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <!-- Modal body -->
                <div class="modal-body">
                    Mensaje enviado correctamente
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

</html>



