<?php 
	require '../../../config/settings.php'; 
	require 'constants/check-login.php';

	if ($user_online == "true") {
		if ($myrole == "employer") {
		}else{
		header("location:../");		
		}
		}else{
		header("location:../");	
	}

	if (isset($_GET['jobid'])) {
	$jobid = $_GET['jobid'];

	try {
		require_once("../../../db/db.php");
		$db = new DbPDO();
		
		$trabajos = $db->query("SELECT * FROM tbl_jobs WHERE job_id = :jobid AND company = '$myid'", array('jobid'=>$jobid));

		$rec = count($trabajos);

		if ($rec == "0") {
			header("location:./");
		}else{
			
	foreach($trabajos as $trabajo)
	{
		$jobtitle = $trabajo['title'];
		$jobcity = $trabajo['city'];
		$jobcountry = $trabajo['country'];
		$jobcategory = $trabajo['category'];
		$jobtype = $trabajo['type'];
		$experience = $trabajo['experience'];
		$jobdescription = $trabajo['description'];
		$jobrespo = $trabajo['responsibility'];
		$jobreq = $trabajo['requirements'];
		$closingdate = $trabajo['closing_date'];
		$tech = $trabajo['tech'];		
	}

	}
					  
}catch(PDOException $e)
 {

 }
}

?>

<!DOCTYPE html>
<html>

<head>
	<!-- Basic Page Info -->
	<meta charset="utf-8">
	<title>IDentiKIT - Editar aviso</title>

	
	<link rel="apple-touch-icon" sizes="180x180" href="logov3.png">
	<link rel="icon" type="image/png" sizes="32x32" href="logov3.png">
	<link rel="icon" type="image/png" sizes="16x16" href="logov3.png">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/datatables/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/style.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">

	
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-PHBQLSG');</script>
	<!-- End Google Tag Manager -->

</head>
<body>

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PHBQLSG"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	
	<!-- End Google Tag Manager (noscript) -->

	<?php include 'layouts/Header.php'; ?>
	<?php include 'layouts/Sidebar-menu.php'; ?>

	<div class="mobile-menu-overlay"></div>

    <div class="main-container">
        <div class="pd-ltr-20 xs-pd-20-10">
            <div class="min-height-200px">
                <div class="pd-ltr-20">
				<div class="card-box pd-20 height-100-p mb-30">
						<div class="section-title-02">
							<?php require 'constants/check_reply.php'; ?>
								<h3 class="text-left">Publicar Empleo</h3>
										</div>

										<!-- <form class="post-form-wrapper" action="app/update-job.php" method="POST" autocomplete="off"> -->
										<form class="post-form-wrapper" action="../../controllers/employercontroller.php?UpdateJob" method="POST" autocomplete="off">
								
											<div class="row gap-20">
										
												<div class="col-sm-8 col-md-8">
												
													<div class="form-group">
														<label>Puesto/Titulo del aviso</label>
														<input name="title" required type="text" class="form-control" value="<?php echo "$jobtitle"; ?>">
													</div>
													
												</div>
												
												<div class="clear"></div>
												
												<div class="col-sm-4 col-md-4">
												
													<div class="form-group">
														<label>Ciudad</label>
														<input name="city" required type="text" class="form-control" value="<?php echo "$jobcity"; ?>" >
													</div>
													
												</div>
												
												<div class="col-sm-4 col-md-4">
												
													<div class="form-group">
														<label>Pais</label>
														<select name="country" required class="selectpicker show-tick form-control" data-live-search="true">
															<option disabled value="">Seleccionar</option>
						                                     <?php
														
														   try {
														     	
                                                           $countries = $db->query("SELECT * FROM tbl_countries ORDER BY country_name");
                                                      
  
                                                           foreach($countries as $country)
                                                           {
		                                                    ?> <option <?php if ($jobcountry == $country['country_name']) { print ' selected '; } ?> value="<?php echo $country['country_name']; ?>"><?php echo $country['country_name']; ?></option> <?php
	 
	                                                        }

					  
	                                                       }catch(PDOException $e)
                                                           {

                                                           }
	
														   ?>
														</select>
													</div>
													
												</div>
												
												<div class="clear"></div>
												
												<div class="col-sm-4 col-md-4">
												
													<div class="form-group">
														<label>Categoria</label>
															<select name="category" required class="selectpicker show-tick form-control" data-live-search="true">
															<option disabled value="">Seleccionar</option>
						                                    <?php
														   
														   try {

		
																$categorias = $db->query("SELECT * FROM tbl_categories ORDER BY category");
		
																foreach($categorias as $categoria)
																{
																	?> <option <?php if ($jobcategory == $categoria['category']) { print ' selected '; } ?> value="<?php echo $categoria['category']; ?>"><?php echo $categoria['category']; ?></option> <?php
																}}catch(PDOException $e){

                                                           		}
	
														   ?>
														</select>
											
														
													</div>
													
												</div>
											    <div class="col-sm-4 col-md-4">
												
													<div class="form-group">
														<label>Tiempo de postulacion</label>
														<input name="deadline" required type="date" class="form-control" placeholder="EJ: 30-12-2031" value="<?php echo "$closingdate"; ?>">
													</div>
													
												</div>
												
												<div class="clear"></div>
												
												<div class="col-xss-12 col-xs-6 col-sm-6 col-md-4">
												
													<div class="form-group mb-20">
														<label>Tipo de empleo</label>
														<select name="jobtype" required class="selectpicker show-tick form-control" data-live-search="false" data-selected-text-format="count > 3" data-done-button="true" data-done-button-text="OK" data-none-selected-text="All">
															<option value="<?php echo "$jobtype"; ?>" selected><?php echo "$jobtype"; ?></option>
															<option value="Primer empleo" data-content="<span class='label label-danger'>Primer empleo</span>">Primer empleo</option>
															<option value="Pasantia" data-content="<span class='label label-danger'>Pasantia</span>">Pasantia</option>
															<option value="Full time" data-content="<span class='label label-warning'>Full-time</span>">Full-time</option>
															<option value="Part time" data-content="<span class='label label-danger'>Part-time</span>">Part-time</option>
															<option value="Temporario" data-content="<span class='label label-danger'>Temporario</span>">Temporario</option>
															<option value="Por contrato" data-content="<span class='label label-danger'>Por contrato</span>">Por contrato</option>
															<option value="Voluntario" data-content="<span class='label label-danger'>Voluntario</span>">Voluntario</option>
															<option value="Por horas" data-content="<span class='label label-danger'>Por horas</span>">Por horas</option>
															<option value="Fines de semana" data-content="<span class='label label-danger'>Fines de semana</span>">Fines de semana</option>
															<option value="Freelance" data-content="<span class='label label-success'>Freelance</span>">Freelance</option>
														</select>
													</div>
													
												</div>
												
												<div class="col-xss-12 col-xs-6 col-sm-6 col-md-4">
												
													<div class="form-group mb-20">
														<label>Modalidad</label>
														<select name="experience" required class="selectpicker show-tick form-control" data-live-search="false" data-selected-text-format="count > 3" data-done-button="true" data-done-button-text="OK" data-none-selected-text="All">
															<option value="<?php echo "$experience";?>" selected ><?php echo "$experience";?></option>
															<option value="Presencial">Presencial</option>
															<option value="Desde casa">Desde casa</option>
															<option value="Presencial y desde casa">Presencial y desde casa</option>
															
														</select>
													</div>
													
													
												</div>
												<div class="col-xss-12 col-xs-6 col-sm-6 col-md-4">
												
													<div class="form-group mb-20">
														<label>Tecnologias</label>
														<input type="text"class="form-control" placeholder="Presiona enter para agregar" data-role="tagsinput" name="tech" value="<?php echo $tech;?>">
													</div>
													
													
												</div>

												<div class="col-sm-12 col-md-12">
												
													<div class="form-group bootstrap3-wysihtml5-wrapper">
														<label>Descripcion</label>
														<textarea class="textarea_editor form-control border-radius-0" name="description" required placeholder="Ingresa descripcion, requerimientos y responsabilidades"><?php echo "$jobdescription"; ?></textarea>												
													</div>
													
												</div>
												<div class="clear"></div>
												<div class="clear"></div>
												<div class="clear"></div>
												<div class="clear"></div>
												<div class="clear mb-10"></div>
												<div class="clear mb-15"></div>
												<div class="clear"></div>
													<input type="hidden" name="jobid" value="<?php echo "$jobid"; ?>">
												<div class="col-sm-12 mt-30">
													<button type="submit"  onclick = "validate(this)" class="btn btn-primary btn-lg col-md-12">Actualizar</button>
												</div>
											</div>
										</form>
					</div>
				<div class="row">
						<div class="card-box height-100-p overflow-hidden col-md-12">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
  <!-- js -->
	<script src="../../../public/complements/v1/vendors/scripts/core.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/script.min.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/process.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/layout-settings.js"></script>
	<script src="../../../public/complements/v1/src/plugins/apexcharts/apexcharts.min.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/jquery.dataTables.min.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/dataTables.bootstrap4.min.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/dataTables.responsive.min.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/responsive.bootstrap4.min.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/dashboard.js"></script>
	<script src="../../../public/complements/v1/src/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>


</html>