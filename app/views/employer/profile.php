<?php 

	require '../employee/constants/check-newlogin.php';

	require_once("../../../db/db.php");
	$db = new DbPDO();

	if ($user_online == "true") {
		if ($myrole == "employee") {
			}else{
				header("location:../employer/index.php");	}
	}else{
		header("location:../principal.php");	
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">

	<title>Nombre empresa </title>

	<link rel="apple-touch-icon" sizes="180x180"    href="../../../public/img/identikit/logo.png">
	<link rel="icon" type="image/png" sizes="32x32" href="../../../public/img/identikit/logo.png">
	<link rel="icon" type="image/png" sizes="16x16" href="../../../public/img/identikit/logo.png">
	<link rel="stylesheet" type="text/css" href="../employee/profile-e.css">
	<link rel="stylesheet" type="text/css" href="../employee/styles.css">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/datatables/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/style.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/plyr/dist/plyr.css">


	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-K2NHRZT4R7"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'G-K2NHRZT4R7');
	</script>

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-5QGSH5J');</script>
	<!-- End Google Tag Manager -->


</head>
<body>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

	<script type="text/javascript">
		$(window).load(function() {
			$(function() {
				$('#file-input').change(function(e) {
					addImage(e);
				});

				function addImage(e) {
					var file = e.target.files[0],
						imageType = /image.*/;

					if (!file.type.match(imageType))
						return;

					var reader = new FileReader();
					reader.onload = fileOnload;
					reader.readAsDataURL(file);
				}

				function fileOnload(e) {
					var result = e.target.result;
					$('#imgSalida').attr("src", result);
				}
			});
		});
	</script>

	<script>
		function capturar() {
			var resultado = "";

			var porNombre = document.getElementsByName("filter");
			for (var i = 0; i < porNombre.length; i++) {
				if (porNombre[i].checked)
					resultado = porNombre[i].value;
			}

			var elemento = document.getElementById("resultado");
			if (elemento.className == "") {
				elemento.className = resultado;
				elemento.width = "600";
			} else {
				elemento.className = resultado;
				elemento.width = "600";
			}
		}
	</script>


	


		

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QGSH5J"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<?php include 'layouts/Header.php';?>
	<?php include 'layouts/Sidebar-menu.php';?>
	

	<div class="main-container">
		<div class="pd-ltr-20 xs-pd-20-10">
			<div class="min-height-200px">
				<section class="Hero">
					<a href="banner" data-toggle="modal" data-target="#banner" class="edit-avatar"><div class="Hero__img-container">
						<img src="banner.png">
					</div></a>

					<div id="banner" class="modal fade bs-example-modal-lg" tabindex="-1" data-width="550" style="display: none;" data-backdrop="static" data-keyboard="false" data-replace="true" role="dialog" aria-hidden="true">
						<div class="modal-dialog modal-lg modal-dialog-centered">
							<div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title" id="myLargeModalLabel">Banner empresa</h4>
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
								</div>
								<div class="modal-body">
									<form action="../../controllers/employeecontroller.php?NewDp" method="POST" autocomplete="off" enctype="multipart/form-data">
											<center>
												<div>

													<div id="resultado2" class=""><img id="imgSalida2" style="width: 100%; height: auto;"/></div>
													
												</div>
												<div class="form-group col-md-12 ">
													<center><label>Seleccionar imagen</label></center>
													<label for="file-input">
														<img src="../../../public/img/identikit/mas.png" title="Seleccionar imagen" style="width: 50px; height: 50px">
													</label>
													<input accept="image/*" type="file" name="image2" id="file-input2" required hidden="" />

												</div>
											

												<div class="col-md-12 modal-footer text-center">

													<button type="submit" class="col-md-12 btn btn-primary" value="actualizar">Actualizar</button>

												</div>
											</center>
										</form>

									<style type="text/css">
										.align-profile-modal {
												margin-top: 20px;
												margin-bottom: 20px;
										}
									</style>
								
									
								</div>

							</div>
						</div>
					</div>

					<div class="container pr-0">
						

						<div class="Hero__logo">
							<a href="infomodal" data-toggle="modal" data-target="#infomodal" class="edit-avatar">
							<img src="logo.png" alt="Logo de empresa"></a>
						</div>

						<div class="Hero__content d-flex justify-content-between align-items-center">
							<div class="Hero__text">
								<h2 class="Hero__brand hrc-black">Nombre de empresa</h2>
								<br>
							</div>
							<!-- De aqui sacar todos los btn hrc-btn para que queden los del commons -->
							<ul class="list-unstyled d-lg-flex d-none mb-0">
								
							</ul>

						</div>
					</div>
					<div id="infomodal" class="modal fade bs-example-modal-lg" tabindex="-1" data-width="550" style="display: none;" data-backdrop="static" data-keyboard="false" data-replace="true" role="dialog" aria-hidden="true">
						<div class="modal-dialog modal-lg modal-dialog-centered">
							<div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title" id="myLargeModalLabel">Foto de perfil</h4>
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
								</div>
								<div class="modal-body">
									<form action="../../controllers/employeecontroller.php?NewDp" method="POST" autocomplete="off" enctype="multipart/form-data">
											<center>
												<div>

													<div id="resultado" class=""><img id="imgSalida" style="width: 20rem; height: 20rem; border-radius: 13rem;" /></div>
													
												</div>
												<div class="form-group col-md-12 ">
													<center><label>Seleccionar imagen</label></center>
													<label for="file-input">
														<img src="../../../public/img/identikit/mas.png" title="Seleccionar imagen" style="width: 50px; height: 50px">
													</label>
													<input accept="image/*" type="file" name="image" id="file-input" required hidden="" />

												</div>
											

												<div class="col-md-12 modal-footer text-center">

													<button type="submit" class="col-md-12 btn btn-primary" value="actualizar">Actualizar</button>

												</div>
											</center>
										</form>

									<style type="text/css">
										.align-profile-modal {
												margin-top: 20px;
												margin-bottom: 20px;
										}
									</style>
								
									
								</div>

							</div>
						</div>
					</div>
				</section><br>
			</div>
		</div>
	</div>

	<!--<div class="blog-wrap">
	                    

	                    <div class="container pd-0">
	                        <div class="row">

	                            <div class="col-md-8 col-sm-12">
	                                <div class="blog-detail card-box overflow-hidden mb-30">
	                                    
	                                    <div class="blog-caption">
	                                        
	                                        <div class="d-flex flex-wrap align-items-center">
	                                        	<div class="card-vacancy progress-data">
	                                        		<div id="chart" ><img src="logo.png" class="card-img-wrapper"></div>
	                                        	</div>
	                                        	<div class="widget-data" style="padding: 0px 10px;">

	                                        		<div class="h4 mb-0">Nombre Vacante 3</div>
	                                        		<div class="weight-600 font-14"><i class="icon-copy fi-marker" style="color: #3b3f51;"></i> ubicacion</div>
	                                        		<div class="weight-600 font-14"><i class="icon-copy fa fa-suitcase" aria-hidden="true"style="color: #3b3f51;"> </i> Categoria</div>

	                                        
	                                        	</div>
	                                        
	                                        </div> 


	                                    </div>



	                                </div>
	                                
	                                <div class="row mt-3">
	                                	 php --
	                                	<div class="col-12 p-0 vacancyDataContainer">
	                                		<a href="vacante.php" class="text-decoration-none hrc-black">

	                                			<div class="card p-3 mb-2 hoverable rounded border-0">

	                                				<div class="card-vacancy align-items-center mb-1">
	                                					<div class="card-img-wrapper mr-2 mb-2">
	                                						<img src="logo.png" alt="image" class="card-image-left">
	                                					</div>

	                                					<div class="card-body p-0 d-flex flex-column">
	                                						<h4 class="font-black m-0 mb-2 fs-20">
	                                							Nombre vacante
	                                						</h4>
	                                						<p class="card-text text-truncate m-0 mb-1 fs-14 text-color">

	                                							<span class="font-weight-light"><i class="icon-copy fi-marker"></i> Ubicacion</span>
	                                						</p>

	                                						<p class="card-text text-truncate m-0 fs-14 text-color mb-2">

	                                							<span class="font-weight-light">
	                                								<i class="icon-copy fa fa-suitcase"></i> Categoria</span>
	                                						</p>

	                                						<div class="align-items-center justify-content-between card-responsive modalityShow">

	                                							<div class="d-flex align-items-center align-self-start">

	                                								<p class="m-0">
	                                									<span class="tag-vacancy d-flex align-items-center justify-content-center fs-12"><i class="icon-copy fa fa-building-o" aria-hidden="true"></i> 
	                                										Presencial y Remoto
	                                									</span>
	                                								</p>
	                                							</div>

	                                							<p class="card-text text-truncate align-self-end m-0 font-weight-light fs-12">
	                                								<span class="font-weight-bold hrc-green text-uppercase fs-12">
	                                                                Si la vacante es reciente-Nuevo
	                                                            </span>
	                                                        
	                                                    </p>

	                                                </div>
	                                                


	                                            </div>
	                                        </div>
	                                            Se oculta o muestra dependiendo la resolucione 
	                                            <div class="align-items-center justify-content-between card-responsive showAndHide">
	                                                <div class="d-flex align-items-center align-self-start">
	                                                                    
	                                                                            <p class="m-0">
	                                                            <span class="tag-vacancy d-flex align-items-center justify-content-center fs-12">
	                                                                <i class="hr-Company mr-1 hrc-black"></i>
	                                                                Presencial y Remoto                            </span>
	                                                        </p>
	                                                                    </div>
	                                                
	                                                <p class="card-text text-truncate align-self-end m-0 font-weight-light fs-12">
	                                                                            <span class="font-weight-bold hrc-green text-uppercase fs-12">
	                                                            Nuevo
	                                                        </span>
	                                                                        
	                                                    Hace menos de una hora                </p>
	                                            </div>
	                                    </div>
	                                </a>


	                            </div>
	                             php --

	                        </div>
	                    </div>



	                            <div class="col-md-4 col-sm-12">
	                                
	                                <div class="card-box mb-30">
	                                    <h5 class="pd-20 h5 mb-0">Nosotros</h5>
	                                    <div class="latest-post">
	                                        <ul>
	                                            <li>
	                                                <h4><a href="#">Ut enim ad minim veniam, quis nostrud exercitation ullamco</a></h4>
	                                                
	                                            </li>
	                                            
	                                        </ul>
	                                    </div>
	                                </div>
	                             Esta seccion aparece dependiendo si el usuario inserto un video en su perfil
	                                <div class="card-box mb-30">
	                                    <div class="latest-post">
	                                             <div data-type="youtube" data-video-id="bTqVqk7FSmY"></div>
	                                       
	                                    </div>
	                                </div>
	                               
	                            </div>
	                        </div>
	                    </div>
	          </div>-->

	          <div class="container mt-4 px-4 px-md-3 main-section">
	                      <div class="row mb-3 d-block d-lg-none">
	                          <div class="col-12" style="border-bottom:1px solid #dadada;">
	                              <div class="d-flex justify-content-around">
	                                  <button class="btn-toggle-section active" id="btn-vacantes">
	                                      Info.
	                                  </button>
	                                  <button class="btn-toggle-section" id="btn-empresa">
	                                      Nosotros
	                                  </button>
	                              </div>
	                          </div>
	                      </div>
	                      <div class="row">
	                          <!-- Inicio seccion vacantes -->
	                          <div class="col-lg-8" id="section-vacantes">
	                              <div class="row justify-content-between flex-nowrap">
	                                  
	                                <div class="col-lg-3 p-0 pr-lg-2 d-none d-lg-block my-auto">
	                                      
	                                  </div>
	                                   
	                                 
	                                 
	                              </div>
	                              <div class="row ">

	                              	<div class="col-12 p-0 vacancyDataContainer">

	              <div class="card p-3 mb-2  rounded border-0">
	                
	                	<div class="form-group row">
	                 		<label class="col-sm-12 col-md-12 col-form-label">Nombre de la empresa</label>
	                 		<div class="col-sm-12 col-md-12">
	                 			<input class="form-control" placeholder="Ingrese el nombre de su empresa" >
	                 		</div>
	                 	</div>

	                 	<div class="form-group row">
	                 		<label class="col-sm-12 col-md-12 col-form-label">CUIT</label>
	                 		<div class="col-sm-12 col-md-12">
	                 			<input class="form-control" placeholder="Ingrese el número de CUIT" >
	                 		</div>
	                 	</div>

	                 
	                 						<div class="col-sm-12 col-md-12">
	                 							<div class="form-group row">
	                 								<label>País</label>
	                 								<select name="country" required class="selectpicker show-tick form-control" data-live-search="true">
	                 									<option disabled value="">Seleccionar</option>
	                                                    <?php
	                 									try {
	                 											
	                 									$countries = $db->query("SELECT * FROM tbl_countries ORDER BY country_name");
	                 								
	                 
	                 									foreach($countries as $country)
	                 									{
	                 										?> <option <?php if ($country == $country['country_name']) { print ' selected '; } ?> value="<?php echo $country['country_name']; ?>"><?php echo $country['country_name']; ?></option> <?php
	                 
	                 										}

	                 
	                 									}catch(PDOException $e)
	                 									{

	                 									}
	                 
	                 								   ?>
	                 								</select>
	                 							</div>
	                 							
	                 						</div>

	                 	<div class="form-group row">
	                 		<label class="col-sm-12 col-md-12 col-form-label">Provincia</label>
	                 		<div class="col-sm-12 col-md-12">
	                 			<input class="form-control" placeholder="Ingrese su provincia" >
	                 		</div>
	                 	</div>

	                 	<div class="form-group row">
	                 		<label class="col-sm-12 col-md-12 col-form-label">Ciudad</label>
	                 		<div class="col-sm-12 col-md-12">
	                 			<input class="form-control" placeholder="Ingrese la ciudad">
	                 		</div>
	                 	</div>

	                 	<div class="form-group row">
	                 		<label class="col-sm-12 col-md-12 col-form-label">Domicilio</label>
	                 		<div class="col-sm-12 col-md-12">
	                 			<input class="form-control" placeholder="Ingrese el Domicilio" >
	                 		</div>
	                 	</div>

	                 	<div class="form-group row">
	                 		<label class="col-sm-12 col-md-12 col-form-label">Teléfono</label>
	                 		<div class="col-sm-4 col-md-4">
	                 			<input class="form-control" placeholder="Cod. Área" >
	                 		</div>
	                 		<div class="col-sm-6 col-md-6">
	                 			<input class="form-control" placeholder="Teléfono" >
	                 		</div>
	                 		<div class="col-sm-2 col-md-2">
	                 			<input class="form-control" placeholder="Int." >
	                 		</div>
	                 	</div>

	                 	<div class="form-group row">
	                 		<label class="col-sm-12 col-md-12 col-form-label">Página web</label>
	                 		<div class="col-sm-12 col-md-12">
	                 			<input class="form-control" placeholder="Ingrese la URL de su empresa" >
	                 		</div>
	                 	</div>

	                 	<center>
	                 		<input type="submit" value="Guardar cambios" class="btn btn-primary col-md-12" style="margin-top:15px">
	                 	</center>

	                 <!--<div class="form-group row">

	                 	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">

	                 	<a data-toggle="modal" href="#edit_contact" class="list-group-item list-group-item-action">Informacion de contacto</a>

	                 		<div id="edit_contact" class="modal fade bs-example-modal-lg" tabindex="-1" data-width="550" style="display: none;" data-backdrop="static" data-keyboard="false" data-replace="true" role="dialog" aria-hidden="true">
	                 			<div class="modal-dialog modal-lg modal-dialog-centered">
	                 				<div class="modal-content">
	                 					<div class="modal-header">
	                 						<h4 class="modal-title" id="myLargeModalLabel">Informacion de contacto</h4>
	                 						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	                 					</div>
	                 					<div class="modal-body">
	                 						<div class="row gap-20">
	                 							<div class="col-sm-12 col-md-12">
	                 								<div class="form-group">
	                 									<label></label>
	                 									<div class="form-group">
	                 										<label>Email</label>
	                 										<input name="email" class="form-control form-control-lg" type="email" value="<?php echo "$myemail"; ?>" disabled>
	                 									</div>
	                 									<div class="form-group">
	                 										<label>Telefono</label>
	                 										<input name="phone" class="form-control form-control-lg" type="text" value="<?php echo "$myphone"; ?>" disabled>
	                 									</div>
	                 									
	                 								</div>
	                 							</div>
	                 							
	                 						</div>
	                 					</div>
	                 				</div>
	                 			</div>
	                 		</div>


	                 	</div>
	                 </div>-->



	                     
	                      </div>
	                  </div>
	              </div>
	                              <!-- Fin seccion vacantes -->

	                              
	                              

	                             

	                              
	                          </div>

	                          <!-- Seccion derecha -->
	                          <div class="d-none d-lg-block col-lg-4 p-0 pl-md-3" id="section-empresa">
	                                                      <!-- Card texto -->
	                                  <div class="card rounded border-0 ">

	                                  	<h5 class="mt-4 ml-4 mb-2 hrc-black font-weight-bold">Nosotros</h5>
	                                      

	                                      <div class="card-body px-4 pt-1 fs-14 hrc-black description-empresa ">
											 <a href="sobrenosotros" data-toggle="modal" data-target="#sobrenosotros" class="edit-avatar" style="margin-left: 96%; background-color: #d5d5d5; border-radius: 5px; padding: 6px; color: black;"><i class="fa fa-pencil"></i></a>

											 <div id="sobrenosotros" class="modal fade bs-example-modal-lg" tabindex="-1" data-width="550" style="display: none;" data-backdrop="static" data-keyboard="false" data-replace="true" role="dialog" aria-hidden="true">
											 	<div class="modal-dialog modal-lg modal-dialog-centered">
											 		<div class="modal-content">
											 			<div class="modal-header">
											 				<h4 class="modal-title" id="myLargeModalLabel">Descripción sobre tu empresa</h4>
											 				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
											 			</div>
											 			<div class="modal-body">
											 				
											 			<div class="html-editor pd-20 card-box mb-30">
											 			
											 				<textarea class="form-control" name="" rows="3" placeholder=""><?php ?></textarea>
											 				<br>
											 				<button type="submit" class="col-md-12 btn btn-primary" value="actualizar">Guardar cambios</button>
											 				

											 			</div>
											 				
											 			</div>

											 		</div>
											 	</div>
											 </div>




	                                      	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt. </p>

	                                      	<!--
	                                                                              <div class="form-group">
	                                                                              	<label>Textarea</label>
	                                                                              	<textarea class="form-control"></textarea>
	                                                                              </div>-->
	                                                                      </div>
	                                  </div>
	                                                  <!-- Fin card texto -->

	                                                      <div class="">
	                                      <div class="mt-3">
	                                          <div data-type="youtube" data-video-id="bTqVqk7FSmY"></div>

	                                      </div>
	                                  </div>
	                              
	                                                      <!-- card contact -->
	                                   
	                                                  <!-- Fin card contact -->

	                              <div class="d-flex justify-content-center align-items-center mt-3  mb-2">
	                                 
	                              </div>
	                          </div>
	                          <!-- Fin sección derecha -->
	                      </div>
	                  </div>
</div>

	<!-- js -->
	<script src="../../../public/complements/v1/vendors/scripts/core.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/script.min.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/process.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/layout-settings.js"></script>
	<script src="../../../public/complements/v1/src/plugins/plyr/dist/plyr.js"></script>
	<script src="https://cdn.shr.one/1.0.1/shr.js"></script>
<script type="text/javascript" src="../employee/main.js"></script>
	<script>
		plyr.setup({
			tooltips: {
				controls: !0
			},
		});
	</script>

</body>
</html>