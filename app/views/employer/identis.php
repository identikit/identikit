<!DOCTYPE html>

<?php

	include '../../../config/settings.php'; 
	include 'constants/check-login.php';

	require_once("../../../db/db.php");

	$db = new DbPDO();

	if ($myrole == 'employee') {
		require 'employee/constants/check-login.php';
	} else {
		//require 'employee/constants2/check-login1.php';
	}

	$empresaId = $_SESSION['myid'];

?>
<html>

<head>
	<!-- Basic Page Info -->
	<meta charset="utf-8">
	<title>IDentiKIT - Explorar IDentis</title>

	<link rel="apple-touch-icon" sizes="180x180" href="logov3.png">
	<link rel="icon" type="image/png" sizes="32x32" href="logov3.png">
	<link rel="icon" type="image/png" sizes="16x16" href="logov3.png">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/datatables/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/style.css">

	<meta property="og:image" content="https://identikit.app/logowebog.png" />
	<meta property="og:image:secure_url" content="https://identikit.app/logowebog.png" />
	<meta property="og:image:type" content="image/png" />
	<meta property="og:image:width" content="300" />
	<meta property="og:image:height" content="300" />
	<meta property="og:image:alt" content="IDentiKIT - Tu primera experiencia laboral" />
	<meta property="og:description" content="😎 Encuentra el mejor talento tech joven en IDentiKIT a un click de distancia, fácil, rápido y seguro." />

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-K2NHRZT4R7"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'G-K2NHRZT4R7');
	</script>

	<!-- Google Tag Manager -->
	<script>
		(function(w, d, s, l, i) {
			w[l] = w[l] || [];
			w[l].push({
				'gtm.start': new Date().getTime(),
				event: 'gtm.js'
			});
			var f = d.getElementsByTagName(s)[0],
				j = d.createElement(s),
				dl = l != 'dataLayer' ? '&l=' + l : '';
			j.async = true;
			j.src =
				'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
			f.parentNode.insertBefore(j, f);
		})(window, document, 'script', 'dataLayer', 'GTM-5QGSH5J');
	</script>
	<!-- End Google Tag Manager -->
</head>

<body>

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QGSH5J" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	
	<?php include 'layouts/Header.php';?>
	<?php include 'layouts/Sidebar-menu.php';?>

	<div class="mobile-menu-overlay"></div>

	<div class="main-container">
		<div class="pd-ltr-20 xs-pd-20">
			<div class="contact-directory-list">
				<ul class="row">
					<?php
						try {
							$usuarios = $db->query('SELECT * FROM tbl_users WHERE role = "employee" ORDER BY RAND() LIMIT 500');
							
							foreach ($usuarios as $usuario) {
								$empavatar = $usuario['avatar'];
					?>
							
							<li class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
								<div class="contact-directory-box" style="-webkit-box-shadow: 0 0 28px rgb(0 0 0 / 8%); box-shadow: 0 0 28px rgb(0 0 0 / 8%);">
									<div class="contact-dire-info text-center">
										<div class="contact-avatar">
											<span>
												<?php
													if ($empavatar == null) {
														print '<center><img class="img-circle autofit2"  src="../../../public/img/identikit/logo.png" alt="image"  /></center>';
													} else {
														echo '<center><img class="img-circle autofit2" alt="image" src="data:image/png;base64,' . base64_encode($empavatar) . '"/></center>';
													}
												?>
											</span>
										</div>

										<?php
											if ($empavatar == null) {
											} else {
												echo '';
											}
										?>

										<div class="contact-name">
											<h4><?php if ($usuario['empujar'] == "1") {
													print '🌟';
												} ?> 
												<?php echo strip_tags($usuario['first_name']) ?> 
												<?php echo strip_tags($usuario['last_name']) ?></h4>
											<?php

											$title = $usuario['title'];

											?>
											<div class="work text-success"> 
												<?php 
													if ($usuario['title']) {
														echo '💻 ' . $title . ' ';
													} 
												?>
											</div>
										</div>

										<div class="contact-skill">
											<?php
												$tags = explode(",", $usuario['ability']);

												foreach($tags as $i =>$key) {
													print '<span class="badge badge-pill">' . " $key " . '</span>';
												}
											?>

										</div>
										<div class="profile-sort-desc">
											<br><br><?php echo strip_tags(substr($usuario['about'], 0, 90)); ?>...
										</div>
									</div>
									<div class="view-contact">
										<?php 
											if ($myrole == "employee") {
												print "<a href='perfil.php?empid={$usuario['member_no']}'>Ver ID</a>";
											}
											if ($myrole == "employer") {
												print "<a href='cv.php?empid={$usuario['member_no']}'>Ver ID</a>";
											}
											if ($myrole == null) {
												print "<a href='perfil.php?empid={$usuario['member_no']}'>Ver ID</a>";
											}	
										?>
									</div>
								</div>
							</li>

					<?php
						}} catch (PDOException $e) {
						}
					?>

				</ul>
			</div>
		</div>
	</div>

	<!-- js -->
	<script src="../../../public/complements/v1/vendors/scripts/core.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/script.min.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/process.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/layout-settings.js"></script>
</body>

</html>