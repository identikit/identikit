<!DOCTYPE html>
<html>

<?php

	require '../employee/constants/check-newlogin.php';
	require '../../../config/settings.php';
	require_once("../../../db/db.php");
	$db = new DbPDO();

	$fromsearch = false;

	if (isset($_GET['search']) && $_GET['search'] == "✓") {
	} else {
	}
	if (isset($_GET['country']) && ($_GET['category'])) {
		$cate = $_GET['category'];
		$country = $_GET['country'];
		$query1 = "SELECT * FROM tbl_jobs WHERE category = :cate AND city = :country ORDER BY enc_id DESC";
		$query2 = "SELECT * FROM tbl_jobs WHERE category = :cate AND city = :country ORDER BY enc_id DESC";
		$fromsearch = true;

		$slc_country = "$country";
		$slc_category = "$cate";
		$title = "$slc_category empleos en $slc_country";
	} else {
		$query1 = "SELECT * FROM tbl_jobs";
		$query2 = "SELECT * FROM tbl_jobs";
		$slc_country = "NULL";
		$slc_category = "NULL";
	}
	// if (isset($myrole)) {
	// 	if ($myrole == 'employee') {
	// 		require '../../../config/check-login.php';
	// 	} else {
	// 		require '../employee/constants2/check-login1.php';
	// 	}
	// }
?>

<style type="text/css">
	.horizontal-scroll-contenedor div {
		width: 100px;
		height: 50px;
		margin: 0 10px 0 0;
		padding: 0;
		display: inline-block;
		border: 1px red solid;
	}

	.horizontal-scroll-contenedor {
		width: auto;
		height: 70px;
		padding: 10px;
		white-space: nowrap;
		overflow-y: hidden;
		overflow-x: auto;
		text-decoration: none;
	}

	a.heart_mark {
		width: 40px;
		height: 40px;
		-webkit-border-radius: 5px;
		-moz-border-radius: 5px;
		border-radius: 5px;
		color: #00c1ff;
		font-size: 14px;
		line-height: 40px;
		text-align: center;
		display: inline-block;
		background: #EFFDF5;
		margin-right: 15px;
	}

	a.heart_mark:hover {
		background: #ff0047;
	}

	i.ti-heart {
		color: #00c1ff;
	}

	a.heart_mark:hover>i.ti-heart {
		color: white;
	}
</style>


<head>
	<!-- Basic Page Info -->
	<meta charset="utf-8">
	<title>IDentiKIT - Explorar trabajos</title>

	<link rel="apple-touch-icon" sizes="180x180"    href="../../../public/img/identikit/logo.png">
	<link rel="icon" type="image/png" sizes="32x32" href="../../../public/img/identikit/logo.png">
	<link rel="icon" type="image/png" sizes="16x16" href="../../../public/img/identikit/logo.png">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/datatables/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/style.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">

	<meta property="og:image" content="https://identikit.app/logowebog.png" />
	<meta property="og:image:secure_url" content="https://identikit.app/logowebog.png" />
	<meta property="og:image:type" content="image/png" />
	<meta property="og:image:width" content="300" />
	<meta property="og:image:height" content="300" />
	<meta property="og:image:alt" content="IDentiKIT - Tu primera experiencia laboral" />
	<meta property="og:description" content="😎Tu primer trabajo <TECH> en empresas de alto rendimiento a un click de distancia en un solo lugar.📣 Fácil, rápido y seguro" />

	<script type="text/javascript">
		function update(str) {
			var txt;
			var r = confirm("Are you sure you want to apply this job , you can not UNDO");
			if (r == true) {
				document.getElementById("data").innerHTML = "Please wait...";
				var xmlhttp;
				if (window.XMLHttpRequest) {
					xmlhttp = new XMLHttpRequest();
				} else {
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}

				xmlhttp.onreadystatechange = function() {
					if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
						document.getElementById("data").innerHTML = xmlhttp.responseText;
					}
				}

				xmlhttp.open("GET", "app/apply-job.php?opt=" + str, true);
				xmlhttp.send();
			} else {

			}
		}
	</script>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-K2NHRZT4R7"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'G-K2NHRZT4R7');
	</script>

	<!-- Google Tag Manager -->
	<script>
		(function(w, d, s, l, i) {
			w[l] = w[l] || [];
			w[l].push({
				'gtm.start': new Date().getTime(),
				event: 'gtm.js'
			});
			var f = d.getElementsByTagName(s)[0],
				j = d.createElement(s),
				dl = l != 'dataLayer' ? '&l=' + l : '';
			j.async = true;
			j.src =
				'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
			f.parentNode.insertBefore(j, f);
		})(window, document, 'script', 'dataLayer', 'GTM-5QGSH5J');
	</script>
	<!-- End Google Tag Manager -->
	
	
	


<!-- /ADS ZONE -->

</head>

<body>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QGSH5J" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<?php include 'layouts/Header.php';?>
	<?php include 'layouts/Sidebar-menu.php';?>

	<div class="mobile-menu-overlay"></div>
	<div class="main-container">
		<div class="pd-ltr-20 xs-pd-20-10">
			<div class="min-height-200px">
				<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
					<div class="horizontal-scroll-contenedor">
						<?php
						try {
							$categories = $db->query("SELECT * FROM tbl_categories ORDER BY category");
							foreach ($categories as $category) {
								$cat = $category['category'];
						?>
								<a href="works.php?category=<?= $category['category']; ?>search=✓" class="badge badge-primary" type="submit">🔥 <?= $category['category']; ?> </a>

						<?php
							}
						} catch (PDOException $e) {
						}
						?>


					</div>

					</select>
				</div>
			</div>

			<!-- <div class="col-xss-12 col-xs-6 col-sm-6 col-md-5">
				<div class="form-group form-lg">
					<select class="custom-select2 form-control" name="country" style="width: 100%; height: 38px;" required>
						<option value="">Ciudad</option>
						<?php

							try {
								$cities = $db->query("SELECT DISTINCT city FROM tbl_jobs");
								foreach ($cities as $city) {
									$cnt = array($city['city'] = strip_tags($city['city']));
									$cnt2 = array_unique($cnt);
						?>

						<option <?php if ($slc_country == "$cnt") {
									print ' selected ';
								} ?> value="<?php echo $city['city']; ?>"><?php echo $city['city']; ?></option>
						<?php
							}
						} catch (PDOException $e) {
							echo 'Excepción capturada: ',  $e->getMessage(), "\n";
						}
						?>
					</select>
				</div>
			</div> -->

			<!-- <div class="col-xss-12 col-xs-6 col-sm-4 col-md-2">
				<button name="search" value="✓" type="submit" class="btn btn-primary col-md-12">Buscar</button>
			</div> -->

		</div>
	</div>
	</form>

	<div class="product-wrap" style="padding-right:15px; padding-left:15px">
		<div class="product-list">
			<ul class="row">
				<?php
				try {
					$jobs = $db->query($query1);

					if ($fromsearch == true) {
						$jobs->bindParam(':cate', $slc_category);
						$jobs->bindParam(':country', $slc_country);
					}

					foreach ($jobs as $job) {
						$type   = $job['type'];
						$compid = $job['company'];
						$tags   = $job['tech'];
					
						$usuarios = $db->query("SELECT * FROM tbl_usuarios WHERE id = :compid", array("compid" => $compid));

						foreach ($usuarios as $usuario) {
							$complogo    = $usuario['path'];
							$TCuenta     = $usuario['tcuenta'];
							$thecompname = $usuario['name'];


							if ($type == "Freelance") {
								$sta = '<span class="job-label label label-success">Freelance</span>';
							}
							if ($type == "Part-time") {
								$sta = '<span class="job-label label label-danger">Part-time</span>';
							}
							if ($type == "Full-time") {
								$sta = '<span class="job-label label label-warning">Full-time</span>';
							}
				?>
							<li class="col-lg-12 col-md-12 col-sm-12">
								<div class="product-box">
									<div class="product-caption">
										<h4>
											<a href="#"><?php echo strip_tags($job['title']); ?></a>
											<div class="pull-right">
												<?php
													if ($user_online == true) {
														$isCheckedHeart = $db->query("select like_id from tbl_likes where like_user_id=:like_user_id and like_post_job=:like_post_job", array("like_user_id" => $_SESSION["myid"], "like_post_job" => $job["job_id"]));

														$background  = '';
														$color       = '';

														if ($isCheckedHeart != null) {
															$background = "#ff0047";
															$color      = "white";
														}
														echo '<a class="heart_mark" value=' . $job["job_id"] . ' style="background:' . $background . '; cursor:pointer">';
														echo '<i class="ti-heart" id="heart" style="color:'.$color.'"></i></a>';
													} else {
														echo '<a class="heart_mark" style="cursor:pointer">';
														echo '<i class="ti-heart" id="heart"></i></a>';
													}
												?>
											</div>
										</h4>

										<?php
											try {
												$jobsApplications = $db->query("SELECT COUNT(*) as jobs FROM tbl_job_applications WHERE company = :company", array("company" => $compid));

												$conteo = $jobsApplications[0]["jobs"];
												$num = 0;

												switch ($TCuenta) {
													case 1:
														$num = 6;
														break;
													case 2:
														$num = 30;
														break;
													case 3:
														$num = $conteo;
														break;
												}
											} catch (PDOException $e) {
											}
										?>

										<div class="text-success" style="margin-top: 6px;">📍 <b><?php echo strip_tags($job['city']); ?></b></i> 📙 <b><?php echo strip_tags($job['type']) ?></b></div>

										<div class="text-success" style="margin: 15px 0px 15px 0px;">
											<?php
											$tags = explode(",", $job['tech']);

											foreach ($tags as $i => $key) {
												print '<span class="badge badge-pill" style="color: #01c0fe; background-color: whitesmoke; margin: 5px;">' . " $key " . '</span>';
											}
											?>
										</div>

										<div class="price">
											<p class="card-text" style="margin-top: 6px;"><?php echo html_entity_decode(strip_tags(substr($job['description'], 0, 310))); ?>...</p>
											<a href="vacancy.php?identiwork=<?php echo $job['job_id']; ?>" class="btn btn-primary col-md-12" style="background-color: #01c0fe; border: none;">Postularme</a>
										</div>

									</div>
								
								</div>
								
							</li>
				<?php

						}
					}
				} catch (PDOException $e) {
				}
				?>

			</ul>
		</div>

	</div>
	</div>
	</div>
	</div>

	</div>
	</div>
	<!-- js -->
	<script src="../../../public/complements/v1/vendors/scripts/core.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/script.min.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/process.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/layout-settings.js"></script>
	<script src="../../../public/complements/v1/src/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
	
	<script type="text/javascript">
		$(".heart_mark").click(function() {
			var valor = $(this)[0].getAttribute("value");
			if (valor != null){
				$.ajax({
					type: 'GET',
					url: '../../controllers/helpercontroller.php/add_like.php?AddLike',
					data: {'job_id': valor}
				}).done(function(){
					window.location.reload();
				});
			} else {
				$("#myModal").modal("show");
			}
		});
	</script>

	<div class="modal" tabindex="-1" role="dialog"  id="myModal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			<div class="modal-header">
				<center>
					<h5 class="modal-title">No has iniciado sesión</h5>
				</center>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<center>
					<style>
						#logoModal{
							max-width: 20%;
    						height: 20%;
							padding-bottom: 7%;
						}
					</style>
				<img  src="images/default.png" id="logoModal" alt="logo" width="30%" height="30%" />
			
				</center>
				<div class="row">

					<div class="col-6">
						<a type="submit" class="btn btn-primary btn-sm btn-block"  href="login.php">Iniciar Sesión</a>
					</div>
					<div class="col-6">
						<a class="btn btn-primary btn-sm btn-block" href="registro.php">Crear cuenta nueva</a>
					</div>
				</div>				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
			</div>
			</div>
		</div>
	</div>

</body>

</html>