<!DOCTYPE html>

<?php 

	require '../employee/constants/check-newlogin.php';

	require_once("../../../db/db.php");
	$db = new DbPDO();

	if ($user_online == "true") {
		if ($myrole == "employee") {
			}else{
				header("location:../employer/index.php");	}
	}else{
		header("location:../principal.php");	
	}
?>

<html>
<head>
	<meta charset="utf-8">

	<title>🚀 Bienvenida/o a IDentiKIT  </title>

	
	<link rel="apple-touch-icon" sizes="180x180"    href="../../../public/img/identikit/logo.png">
	<link rel="icon" type="image/png" sizes="32x32" href="../../../public/img/identikit/logo.png">
	<link rel="icon" type="image/png" sizes="16x16" href="../../../public/img/identikit/logo.png">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/datatables/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/style.css">


	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-K2NHRZT4R7"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'G-K2NHRZT4R7');
	</script>

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-5QGSH5J');</script>
	<!-- End Google Tag Manager -->
	<style>
		a.heart_mark {
			width: 40px;
			height: 40px;
			-webkit-border-radius: 5px;
			-moz-border-radius: 5px;
			border-radius: 5px;
			color: #00c1ff;
			font-size: 14px;
			line-height: 40px;
			text-align: center;
			display: inline-block;
			background: #EFFDF5;
			margin-right: 15px;
		}

		a.heart_mark:hover {
			background: #ff0047;
		}

		i.ti-heart {
			color: #00c1ff;
		}

		a.heart_mark:hover>i.ti-heart {
			color: white;
		}
	</style>
</head>
<body>
	

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QGSH5J"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<?php include 'layouts/Header.php';?>
	<?php include 'layouts/Sidebar-menu.php';?>

	
    <div class="main-container">
		<div class="pd-ltr-20 xs-pd-20-10">
			<div class="min-height-200px">

            <div class="row clearfix">
<div class="col-sm-12 col-md-12 col-lg-4 mb-30">
    <div class="card card-box">
        <img class="card-img-top" src="images/img1.jpg" alt="Card image cap">
        <div class="card-body">
            <h5 class="card-title weight-500">Nombre del proyecto</h5> 
            <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
            <a href="#" class="btn btn-primary col-md-12">Ver proyecto </a>
           <!-- <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>-->
        </div>
    </div>
</div>
</div>
            </div>
        </div>
    </div>
	

	
	<!-- js -->
	<script src="../../../public/complements/v1/vendors/scripts/core.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/script.min.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/process.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/layout-settings.js"></script>
	<script src="../../../public/complements/v1/src/plugins/apexcharts/apexcharts.min.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/jquery.dataTables.min.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/dataTables.bootstrap4.min.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/dataTables.responsive.min.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/responsive.bootstrap4.min.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/dashboard.js"></script>

	<script type="text/javascript">
		$(".heart_mark").click(function() {
			var valor = $(this)[0].getAttribute("value");

			$.ajax({
				type: 'GET',
				url: '../../controllers/helpercontroller.php/add_like.php?AddLike',
				data: {'job_id': valor}
			}).done(function(){
				window.location.reload();
			});
		});
	</script>

	<style>
		.card-box {
			margin: 1%;
		}
	</style>


</body>
</html>