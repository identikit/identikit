 <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-5F5WB8FMJC"></script>
<script>
	window.dataLayer = window.dataLayer || [];
	   
	   function gtag() {
	   	dataLayer.push(arguments);
	   }
	   gtag('js', new Date());
	   
	   gtag('config', 'G-5F5WB8FMJC');
	
</script>
<!DOCTYPE html>
<html>
	<?php
		require 'constants/check-newlogin.php';
		require_once("../../../db/db.php");
		$db = new DbPDO();
		
		if ($user_online == "true") {
			if ($myrole == "employee") {
			} else {
				header("location:../");
			}
		} else {
			header("location:../");
		}
		
      ?>
<!DOCTYPE html>
<html>
<head>
	<!-- Basic Page Info -->
	<meta charset="utf-8">
	<title>IDentiKIT - Mi perfil</title>

	<link rel="apple-touch-icon"      sizes="180x180" href="../../../public/img/identikit/logo.png">
	<link rel="icon" type="image/png" sizes="32x32"   href="../../../public/img/identikit/logo.png">
	<link rel="icon" type="image/png" sizes="16x16"   href="../../../public/img/identikit/logo.png">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/datatables/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/style.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/dropzone/src/dropzone.css">


	<!-- Google Tag Manager -->
	<script>
		(function(w, d, s, l, i) {
			w[l] = w[l] || [];
			w[l].push({
				'gtm.start': new Date().getTime(),
				event: 'gtm.js'
			});
			var f = d.getElementsByTagName(s)[0],
				j = d.createElement(s),
				dl = l != 'dataLayer' ? '&l=' + l : '';
			j.async = true;
			j.src =
				'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
			f.parentNode.insertBefore(j, f);
		})(window, document, 'script', 'dataLayer', 'GTM-PHBQLSG');
	</script>
	<!-- End Google Tag Manager -->

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

	<script type="text/javascript">
		$(window).load(function() {
			$(function() {
				$('#file-input').change(function(e) {
					addImage(e);
				});

				function addImage(e) {
					var file = e.target.files[0],
						imageType = /image.*/;

					if (!file.type.match(imageType))
						return;

					var reader = new FileReader();
					reader.onload = fileOnload;
					reader.readAsDataURL(file);
				}

				function fileOnload(e) {
					var result = e.target.result;
					$('#imgSalida').attr("src", result);
				}
			});
		});
	</script>

	<script>
		function capturar() {
			var resultado = "";

			var porNombre = document.getElementsByName("filter");
			for (var i = 0; i < porNombre.length; i++) {
				if (porNombre[i].checked)
					resultado = porNombre[i].value;
			}

			var elemento = document.getElementById("resultado");
			if (elemento.className == "") {
				elemento.className = resultado;
				elemento.width = "600";
			} else {
				elemento.className = resultado;
				elemento.width = "600";
			}
		}
	</script>

	

	<style type="text/css">
		.cdQzeM {
		    font-size: 14px;
		    line-height: 20px;
		    color: rgba(0, 0, 24, 0.48);
		    display: flex;
		    flex-direction: column;
		    -webkit-box-pack: center;
		    justify-content: center;
		    -webkit-box-align: center;
		    align-items: center;
		    padding: 12px;
		    width: 100%;
		    height: 72px;
		    left: 0px;
		    top: 47px;
		    margin-top: 12px;
		    background: rgba(0, 0, 24, 0.04);
		    border: 1px dashed rgba(0, 0, 24, 0.16);
		    border-radius: 8px;
		}
			.fvOTzr{ 
			text-size-adjust: 100%;
			-webkit-tap-highlight-color: rgba(0, 0, 0, 0);
			font-family: "Hind Vadodara", sans-serif;
			font-size: 14px;
			font-weight: 400;
			line-height: 1.5;
			color: rgb(33, 33, 33);
			text-align: left;
			-webkit-font-smoothing: antialiased;
			box-sizing: content-box;
			overflow: visible;
			background-color: rgba(0, 0, 24, 0.24);
			margin: 10px 0px 12px;
			height: 1px;
			border: none;
		}
	</style>


</head>
<body>

	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PHBQLSG" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	
	<?php include 'layouts/Header.php';?>
	<?php include 'layouts/Sidebar-menu.php';?>


	<div class="mobile-menu-overlay"></div>

	

	

	<div class="main-container">
		<div class="pd-ltr-20 xs-pd-20-10">
			<div class="min-height-200px">
				<?php require 'constants/check_reply.php'; ?>
				<div class="row">
					<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-30">
						


						<div class="pd-20 card-box height-50-p">


							<div class="profile-photo">
								<a href="infomodal" data-toggle="modal" data-target="#infomodal" class="edit-avatar"><i class="fa fa-pencil"></i></a>


								<?php
										$checkVideoCv = $db->query("SELECT * FROM `tbl_users_data` where member_no=:member_no and not path_video_cv=''", array("member_no"=>$myid));
										$DescUser     = $db->query("SELECT objetivo,tags FROM `tbl_usuarios_data` where iduser=:iduser", array("iduser"=>$myid));
										$mydesc       = 'Sin objetivo laboral';
										$ability      = '';

										if(!empty($DescUser)){
											$mydesc  = $DescUser[0]['objetivo'];
											$ability = $DescUser[0]['tags'];
										}

										if ($myavatar == "") {
											
											print
													'<div class="btn-idkit center wow animated bounceInRight box1 " data-wow-delay="0.2s" data-toggle="tooltip">
														<center>
															<img  src="../../../public/img/identikit/logo.png" class="rainbow-button" title="' . $myfname . '" alt="image" data-toggle="modal" href="#infomodal" style="cursor:pointer"/>
														</center>
													</div>';
										} else { 	
											print '
													<div class="avatar" style="background-image: url(../../../'.$myavatar.')"></div>';
										}
									?>
								<style type="text/css">
										div.avatar {
										    /* cambia estos dos valores para definir el tamaño de tu círculo */
										    height: 175px;
										    width: 175px;
										    /* los siguientes valores son independientes del tamaño del círculo */
										    background-repeat: no-repeat;
										    background-position: 50%;
										    border-radius: 50%;
										    background-size: 100% auto;
										}
									</style>

								<div id="infomodal" class="modal fade bs-example-modal-lg" tabindex="-1" data-width="550" style="display: none;" data-backdrop="static" data-keyboard="false" data-replace="true" role="dialog" aria-hidden="true">
									<div class="modal-dialog modal-lg modal-dialog-centered">
										<div class="modal-content">
											<div class="modal-header">
												<h4 class="modal-title" id="myLargeModalLabel">Foto de perfil</h4>
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
											</div>
											<div class="modal-body">
												<form action="../../controllers/employeecontroller.php?NewDp" method="POST" autocomplete="off" enctype="multipart/form-data">
														<center>
															<div>

																<div id="resultado" class=""><img id="imgSalida" style="width: 20rem; height: 20rem; border-radius: 13rem;" /></div>
																
															</div>
															<div class="form-group col-md-12 ">
																<center><label>Seleccionar imagen</label></center>
																<label for="file-input">
																	<img src="../../../public/img/identikit/mas.png" title="Seleccionar imagen" style="width: 50px; height: 50px">
																</label>
																<input accept="image/*" type="file" name="image" id="file-input" required hidden="" />

															</div>
														

															<div class="col-md-12 modal-footer text-center">

																<button type="submit" class="col-md-12 btn btn-primary" value="actualizar">Actualizar</button>

															</div>
														</center>
													</form>

												<style type="text/css">
													.align-profile-modal {
															margin-top: 20px;
															margin-bottom: 20px;
													}
												</style>
											
												
											</div>

										</div>
									</div>
								</div>
							</div><br>
							<h5 class="text-center h5 mb-0"><?php echo "$myfname"; ?> <?php echo "$mylname";?></h5>
							<!--<div class="btn-list text-center pd-10">
										<?php
											if ($github) {
												print '<a href="https://github.com/' . "$github" . '" type="button" class="btn" style="border-radius: 10px;" data-bgcolor="black" data-color="#ffffff" target="_blank"><i class="fa fa-github"></i></a>';
											}

											if ($twitter) {
												print '<a href="https://twitter.com/' . "$twitter" . '" type="button" class="btn" style="border-radius: 10px;" data-bgcolor="#1da1f2" data-color="#ffffff" target="_blank"><i class="fa fa-twitter"></i></a>';
											}

											if ($instagram) {
												print '<a href="https://instagram.com/' . "$instagram" . '" type="button" class="btn" style="border-radius: 10px;" data-bgcolor="#f46f30" data-color="#ffffff" target="_blank"><i class="fa fa-instagram"></i></a>';
											}
										?>
							
							</div>-->


					
							<hr>
							<h5>Informacion de contacto</h5>
							
							
							<i class="icon-copy fa fa-envelope" aria-hidden="true" style="color: #1da1f2;"></i> <b><?php echo $myemail; ?></b> </p>

							<i class="icon-copy fa fa-phone" aria-hidden="true" style="color: #1da1f2;"></i> <b><?php echo $myphone; ?></b> </p>

							<i class="icon-copy fa fa-home" aria-hidden="true" style="color: #1da1f2;"></i> <b><?php echo $street; ?></b> </p>

							


						</div>
					</div>




					 


					<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 mb-30">
						<div class="pd-20 card-box height-50-p">

						<a data-toggle="modal" href="#edit_contact" class="list-group-item list-group-item-action">Informacion de contacto</a>
						<div id="edit_contact" class="modal fade bs-example-modal-lg" tabindex="-1" data-width="550" style="display: none;" data-backdrop="static" data-keyboard="false" data-replace="true" role="dialog" aria-hidden="true">
							<div class="modal-dialog modal-lg modal-dialog-centered">
								<div class="modal-content">
									<div class="modal-header">
										<h4 class="modal-title" id="myLargeModalLabel">Informacion de contacto</h4>
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
									</div>
									<div class="modal-body">
										<div class="row gap-20">
											<div class="col-sm-12 col-md-12">
												<div class="form-group">
													<label></label>
													<div class="form-group">
														<label>Email</label>
														<input name="email" class="form-control form-control-lg" type="email" value="<?php echo "$myemail"; ?>" disabled>
													</div>
													<div class="form-group">
														<label>Telefono</label>
														<input name="phone" class="form-control form-control-lg" type="text" value="<?php echo "$myphone"; ?>" disabled>
													</div>
													<!--<label>Usuario Github</label>
													<div class="input-group custom">
														<div class="input-group-prepend custom">
															<div class="input-group-text" id="btnGroupAddon">@</div>
														</div>
														<input type="text" class="form-control" placeholder="Ingresa tu usuario de Github" aria-label="Input group example" aria-describedby="btnGroupAddon" name="github" value="<?php echo $github ?>">
													</div>
													<label>Usuario Twitter</label>
													<div class="input-group custom">
														<div class="input-group-prepend custom">
															<div class="input-group-text" id="btnGroupAddon">@</div>
														</div>
														<input type="text" class="form-control" placeholder="Ingresa tu usuario de Twitter" aria-label="Input group example" aria-describedby="btnGroupAddon" name="twitter" value="<?php echo $twitter ?>">
													</div>
													<label>Usuario Instagram</label>
													<div class="input-group custom">
														<div class="input-group-prepend custom">
															<div class="input-group-text" id="btnGroupAddon">@</div>
														</div>
														<input type="text" class="form-control" placeholder="Ingresa tu usuario de Instagram" aria-label="Input group example" aria-describedby="btnGroupAddon" name="instagram" value="<?php echo $instagram ?>">
													</div>-->
												</div>
											</div>
											<!--<div class="col-md-12 modal-footer text-center">
												<button type="button" class="col-md-12 btn btn-primary" data-dismiss="modal">Continuar</button>
											</div>-->
										</div>
									</div>
								</div>
							</div>
						</div></div><br>


						<div class="pd-20 card-box height-50-p">


							<a data-toggle="modal" href="#edit_data" class="list-group-item list-group-item-action">Datos personales</a>
							<div id="edit_data" class="modal fade bs-example-modal-lg" tabindex="-1" data-width="550" style="display: none;" data-backdrop="static" data-keyboard="false" data-replace="true" role="dialog" aria-hidden="true">
								<div class="modal-dialog modal-lg modal-dialog-centered">
									<div class="modal-content">
										<div class="modal-header">
											<h4 class="modal-title" id="myLargeModalLabel">Datos personales</h4>
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
										</div>
										<div class="modal-body">
										<!--<form class="post-form-wrapper" action="../../controllers/employeecontroller.php?UpdateProfileData" method="POST" autocomplete="off">-->
													<div class="row">
													
														<div class="col-md-4 col-sm-12">
															<div class="form-group">
																<label>Nombre*</label>
																<input name="fname" class="form-control form-control-lg" type="text" value="<?php echo "$myfname"; ?>"disabled>
															</div>
														</div>
														<div class="col-md-4 col-sm-12">
															<div class="form-group">
																<label>Apellido*</label>
																<input name="lname" class="form-control form-control-lg" type="text" value="<?php echo "$mylname"; ?>"disabled>
															</div>
														</div>
														<div class="col-md-4 col-sm-12">
															<div class="form-group">
																<label>Provincia*</label>
																<input name="street" class="form-control form-control-lg" type="text" value="<?php echo "$street"; ?>" disabled>
															</div>
														</div>

														<div class="col-md-4 col-sm-12">

														</div>


														</div>
												
													
														

														<div class="col-md-4 col-sm-12">

														</div>

														<div class="row">

														<!--<div class="col-md-4 col-sm-12">
															<label>Tipo de documento*</label>

															<select name="typedni" required class="selectpicker show-tick form-control">
															<option disabled value="">Tipo</option>

																<option <?php if ($typedni == "D.N.I") {
																			print 'selected ';
																		} ?> value="D.N.I">D.N.I</option>

																<option <?php if ($typedni == "Cedula de identidad") {
																			print 'selected ';
																		} ?>value="Cedula de identidad">Cedula de identidad</option>
																<option <?php if ($typedni == "L.E.") {
																			print 'selected ';
																		} ?>value="L.E.">L.E.</option>

																<option <?php if ($typedni == "Pasaporte") {
																			print 'selected ';
																		} ?>value="Pasaporte">Pasaporte</option>

																<option <?php if ($typedni == "L.C.") {
																			print 'selected ';
																		} ?>value="L.C.">L.C.</option>		

															</select>
														</div>-->
														

														<div class="col-md-12 col-sm-12">
															<label>Documento*</label>
															<input name="dni" class="form-control form-control-lg " type="text" value="<?php echo "$identification"; ?>" disabled>
														</div>

														<!--<div class="col-md-4 col-sm-12">
															<label>Género*</label>

															<select name="gender" required class="selectpicker show-tick form-control" data-live-search="false">
															<option disabled value="">Género</option>
																<option <?php if ($mygender == "Male") {
																			print ' selected ';
																		} ?> value="Male">Masculino</option>
																<option <?php if ($mygender == "Female") {
																			print ' selected ';
																		} ?>value="Female">Femenino</option>
																<option <?php if ($mygender == "Other") {
																			print ' selected ';
																		} ?>value="Other">Otro</option>
															</select>
														</div>-->

														<!--<div class="col-md-4 col-sm-12">
															<div class="custom-control custom-checkbox mb-5">
																<input type="checkbox" class="custom-control-input" id="customCheck1-1">
																<label class="custom-control-label" for="customCheck1-1">Poseo licencia de conducir</label>
															</div>
														</div>

														<div class="col-md-4 col-sm-12">
															<div class="custom-control custom-checkbox mb-5">
																<input type="checkbox" class="custom-control-input" id="customCheck1-2">
																<label class="custom-control-label" for="customCheck1-2">Poseo movilidad propia</label>
															</div>
														</div>-->



														</div>
														<br>	
														

													
														</div>

												
												<!--<div class="col-md-12 modal-footer text-center">
													<button type="submit" class="col-md-12 btn btn-primary" >Actualizar</button>
												</div>
											</form>-->
											</div>


										</div>

									</div>

								</div><br>

								<div class="pd-20 card-box height-50-p">

									<form action="../../controllers/employeecontroller.php?UpdatePerfilv2" method="POST" autocomplete="off">
										<h6>Objetivo Laboral</h6><br>

										<?php

										$SQLObjetivo = $db->query("SELECT objetivo FROM `tbl_usuarios_data` where iduser=:iduser", array("iduser"=>$myid));

										$mydesc      = $SQLObjetivo[0]['objetivo'];?>

										<textarea class="form-control" name="objetivo" rows="3" placeholder="Escribe tu objetivo laboral"><?= $mydesc; ?></textarea>

										<center>
											<input type="submit" value="Actualizar" class="btn btn-primary col-md-12" style="margin-top:15px">
										</center>
									</form>

							</div>

						</div>



					</div>



				</div>


			</div>
			
		</div>


	</div>
	<!-- js -->
	

    <!-- js -->
    <script src="../../../public/complements/v1/vendors/scripts/core.js"></script>
    <script src="../../../public/complements/v1/vendors/scripts/script.min.js"></script>
    <script src="../../../public/complements/v1/vendors/scripts/process.js"></script>
    <script src="../../../public/complements/v1/vendors/scripts/layout-settings.js"></script>
    <script src="../../../public/complements/v1/src/plugins/apexcharts/apexcharts.min.js"></script>
    <script src="../../../public/complements/v1/src/plugins/datatables/js/jquery.dataTables.min.js"></script>
    <script src="../../../public/complements/v1/src/plugins/datatables/js/dataTables.bootstrap4.min.js"></script>
    <script src="../../../public/complements/v1/src/plugins/datatables/js/dataTables.responsive.min.js"></script>
    <script src="../../../public/complements/v1/src/plugins/datatables/js/responsive.bootstrap4.min.js"></script>
    <script src="../../../public/complements/v1/vendors/scripts/dashboard.js"></script>
    <script src="../../../public/complements/v1/src/plugins/dropzone/src/dropzone.js"></script>
	<script>
		Dropzone.autoDiscover = false;
		$(".dropzone").dropzone({
			addRemoveLinks: true,
			removedfile: function(file) {
				var name = file.name;
				var _ref;
				return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
			}
		});
	</script>
</body>
</html>