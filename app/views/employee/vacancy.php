<?php 

	require '../../../config/settings.php';
	require '../employee/constants/check-newlogin.php';

	require_once("../../../db/db.php");
	$db = new DbPDO();

	if (isset($_GET['identiwork'])) {
		$jobid = $_GET['identiwork'];
	try {
		$trabajos = $db->query("SELECT * FROM tbl_jobs WHERE job_id = :jobid",array("jobid"=>$jobid));

		if (count($trabajos) == "0") {
			header("location:./");	
		}
	else{
    	foreach($trabajos as $trabajo)
    	{
			$jobtitle       = $trabajo['title'];
			$jobcity        = $trabajo['city'];
			$jobcountry     = $trabajo['country'];
			$jobcategory    = $trabajo['category'];
			$jobtype        = $trabajo['type'];
			$experience     = $trabajo['experience'];
			$jobdescription = $trabajo['description'];
			$jobreq         = $trabajo['requirements'];
			$closingdate    = $trabajo['closing_date'];
			$opendate       = $trabajo['date_posted'];
			$compid         = $trabajo['company'];
			$tags           = $trabajo['tech'];

			if ($jobtype == "Freelance") {
				$sta = '<span class="label label-success">Freelance</span>';								
			}
			if ($jobtype == "Part-time") {
				$sta = '<span class="label label-danger">Part-time</span>';									
			}
			if ($jobtype == "Full-time") {
				$sta = '<span class="label label-warning">Full-time</span>';								  
			}
		}
	}}catch(PDOException $e){
		echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    }

}else{
	header("location:./");	
}


try {
	$usuarios = $db->query("SELECT A.*, B.objetivo FROM tbl_usuarios as A left join tbl_usuarios_data as B ON A.id = B.iduser WHERE A.id = :compid",array("compid"=>$compid));
    foreach($usuarios as $usuario){
		$compname  = $usuario['name'];
		$complogo  = $usuario['path'];
		$objetivo  = $usuario['objetivo'];
	}}catch(PDOException $e){
		echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    }

	$today_date = strtotime(date('Y/m/d'));
	$last_date = date_format(date_create_from_format('Y-m-d', $closingdate), 'Y/m/d');
	$post_date = date_format(date_create_from_format('Y-m-d', $closingdate), 'd');
	$post_month = date_format(date_create_from_format('Y-m-d', $closingdate), 'F');
	$post_year = date_format(date_create_from_format('Y-m-d', $closingdate), 'Y');
	$conv_date = strtotime($last_date);

	if ($today_date > $conv_date){
		$jobexpired = true;
	} else {
		$jobexpired = false;
	}


?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">

	<title>Postulate para <?php echo strip_tags($jobtitle); ?> en IDentiKIT.app </title>

	<link rel="apple-touch-icon" sizes="180x180"    href="../../../public/img/identikit/logo.png">
	<link rel="icon" type="image/png" sizes="32x32" href="../../../public/img/identikit/logo.png">
	<link rel="icon" type="image/png" sizes="16x16" href="../../../public/img/identikit/logo.png">
	
	<link rel="stylesheet" type="text/css" href="vacancy.css">
	<link rel="stylesheet" type="text/css" href="styles.css">
	<link rel="stylesheet" type="text/css" href="commons.css">
	<link rel="stylesheet" type="text/css" href="modals.css">
	<link rel="stylesheet" type="text/css" href="view-vacancy.css">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/datatables/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/style.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/plyr/dist/plyr.css">



	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-K2NHRZT4R7"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'G-K2NHRZT4R7');
	</script>

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-5QGSH5J');</script>
	<!-- End Google Tag Manager -->
	<style>
		a.heart_mark {
			width: 40px;
			height: 40px;
			-webkit-border-radius: 5px;
			-moz-border-radius: 5px;
			border-radius: 5px;
			color: #00c1ff;
			font-size: 14px;
			line-height: 40px;
			text-align: center;
			display: inline-block;
			background: #EFFDF5;
			margin-right: 15px;
		}

		a.heart_mark:hover {
			background: #ff0047;
		}

		i.ti-heart {
			color: #00c1ff;
		}

		a.heart_mark:hover>i.ti-heart {
			color: white;
		}
	</style>
</head>
<body>


		<script type="text/javascript">
			function update(str,str1){
					var txt;
					var r = confirm("Seguro que quieres aplicar a este trabajo?");
					if (r == true) {
						document.getElementById("data").innerHTML = "Aplicando...";
						var xmlhttp; 	
					if (window.XMLHttpRequest){
						xmlhttp=new XMLHttpRequest();
					}else{
						xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
					}	

				xmlhttp.onreadystatechange = function() {
					if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
						document.getElementById("data").innerHTML = xmlhttp.responseText;
					}
				}

				xmlhttp.open("GET","../../controllers/employeecontroller.php?opt="+str+"&opt1="+str1, true);
				xmlhttp.send();
				} else {

				}
			}	
		</script>

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QGSH5J"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<?php include 'layouts/Header.php';?>
	<?php include 'layouts/Sidebar-menu.php';?>
	

	<div class="main-container">
		<div class="pd-ltr-20 xs-pd-20-10">
			<div class="min-height-200px">

				<section class="Hero">

					<div class="Hero__img-container">
						<img src="banner.png">
					</div>

					<div class="hero__container-info">
						<div class="hero__container">

							<div class="hero__info">
								<div class="hero__info-items">
									<div class="hero__info-item">
										<figure class="hero__figure">
											<img class="hero__figure-img" src="logo.png" width="100%" height="auto" alt="Imagen institucional">
										</figure>

										<div class="hero__information-vacancy">
											<div class="hero__title">
                                            <h2><?php echo strip_tags($jobtitle)?></h2>
                                        </div>

                                        <div class="hero__area-ubication hideOnMoible">

                                        	<p><span class="hr-Location-pin"></span>📍 <?php echo strip_tags($jobcity);?>, <?php echo $jobcountry  ?></p>
                                            <p><span class="hr-Work-area"></span>💼 <?php echo $jobcategory?></p>
                                        </div>

                                        <div class="hero__container-tags hero__container-tags-tablet hideOnMoible">
                                        	<div class="d-flex hero__tag-items">
                                                <!-- Tag - Tipo de trabajo -->
                                                
                                                <!-- Tag - Modalidad de trabajo -->

                                                <div class="hero__tag">
                                                	<span class="hrc-black hr-Company"></span>🏢 <?php echo $experience?><!-- Este es un span dinamico, dependiendo si es remote o presencial o ambos, si es remote hr-Remote si es el otro Company -->

                                                </div>
                                            </div>

                                            <!-- Time vancacy -->
                                            <div class="hero__time--tablet">
                                            	<p class="hero__time-new"> NUEVO</p>

                                            	<p class="hero__time-new hero__time--old"> Hace 8 horas</p>
                                            </div>
                                            <!-- Fin time vacancy -->
                                        </div>

                                    </div>

                                    <div class="hero__button-time align-self-end">
                                    	<div class="d-flex flex-column hero__time-vacancy">
                                    		<?php
                                    			if ($user_online == true) {
                                    				if ($jobexpired == true) {
                                    					print '<button class="btn btn-primary disabled btn-hidden btn-lg collapsed"><i class="flaticon-line-icon-set-calendar"></i>	Este empleo ya expiro</button>';
                                    				} else {
                                    				if ($myrole == "employee") {
                                    					print '<button';?> onclick="update(this.value, <?php echo "'$jobtitle'"; ?>)" <?php print ' value="'.$jobid.'" class="btn hrc-btn hrc-btn-green hero__btn"><i class="flaticon-line-icon-set-pencil"></i>Postularse</button>';
                                    				} else {
                                    					print '<button class="btn btn-primary btn-block disabled btn-hidden btn-lg collapsed"><i class="flaticon-line-icon-set-padlock"></i> Solo los IDentis pueden aplicar!</button>';
                                    				}	
                                    			}
                                    			}else{										
                                    				print '<a href="https://identikit.app/app/views/auth/login-new.php" class="btn hrc-btn hrc-btn-green hero__btn"><i class="flaticon-line-icon-set-padlock"></i> Iniciar sesion</a>';	
                                    			}
                                    		?>

                                    		<div class="hero__time">
                                    			<p class="hero__time-new"> NUEVO</p>
                                    			<p class="hero__time-new hero__time--old"> Hace 8 horas</p>
                                    		</div>
                                    	</div>
                                    </div>
                                </div>

                                <!-- Oculto -->
                                <div class="hero__area-ubication hero__area-ubication--mobile">
                                	<p><span class="hr-Location-pin"></span>📍 <?php echo strip_tags($jobcity);?>, <?php echo $jobcountry  ?></p>
                                            <p><span class="hr-Work-area"></span>💼 <?php echo $jobcategory?></p>
                                	</div>

                                	<div class="hero__container-tags hero__container-tags--mobile">

                                		<div class="d-flex hero__tag-items">

                                			<div class="hero__tag">
                                				<span class="hrc-black hr-Company "></span>🏢 Presencial y Remoto<!-- Este es un span dinamico, dependiendo si es remote o presencial o ambos, si es remote hr-Remote si es el otro Company -->
                                            </div>
                                        </div>
                                    </div>

                                    
                            <!-- Fin oculto -->
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="button__hidden">
        	<div class="main__button-container">
        		<?php
        			if ($user_online == true) {
        				if ($jobexpired == true) {
        					print '<button class="btn btn-primary disabled btn-hidden btn-lg collapsed"><i class="flaticon-line-icon-set-calendar"></i>	Este empleo ya expiro</button>';
        				} else {
        				if ($myrole == "employee") {
        					print '<a';?> onclick="update(this.value, <?php echo "'$jobtitle'"; ?>)" <?php print ' value="'.$jobid.'" class="btn hrc-btn hrc-btn-green hero__btn"><i class="flaticon-line-icon-set-pencil"></i>Postularse</a>';
        				} else {
        					print '<button class="btn btn-primary btn-block disabled btn-hidden btn-lg collapsed"><i class="flaticon-line-icon-set-padlock"></i> Solo los IDentis pueden aplicar!</button>';
        				}	
        			}
        			}else{										
        				print '<a href="https://identikit.app/app/views/auth/login-new.php" class="btn hrc-btn hrc-btn-green hero__btn main__button col-md-12"><i class="flaticon-line-icon-set-padlock"></i> Iniciar sesion</a>';	
        			}
        		?>
        	</div>
        </section>
		
			</div>
		</div>
	</div>

	<!--<div class="blog-wrap">
	                    

	                    <div class="container pd-0">
	                        <div class="row">

	                            <div class="col-md-8 col-sm-12">
	                                <div class="blog-detail card-box overflow-hidden mb-30">
	                                    
	                                    <div class="blog-caption">
	                                        
	                                        <div class="d-flex flex-wrap align-items-center">
	                                        	<div class="card-vacancy progress-data">
	                                        		<div id="chart" ><img src="logo.png" class="card-img-wrapper"></div>
	                                        	</div>
	                                        	<div class="widget-data" style="padding: 0px 10px;">

	                                        		<div class="h4 mb-0">Nombre Vacante 3</div>
	                                        		<div class="weight-600 font-14"><i class="icon-copy fi-marker" style="color: #3b3f51;"></i> ubicacion</div>
	                                        		<div class="weight-600 font-14"><i class="icon-copy fa fa-suitcase" aria-hidden="true"style="color: #3b3f51;"> </i> Categoria</div>

	                                        
	                                        	</div>
	                                        
	                                        </div> 


	                                    </div>



	                                </div>
	                                
	                                <div class="row mt-3">
	                                	 php --
	                                	<div class="col-12 p-0 vacancyDataContainer">
	                                		<a href="vacante.php" class="text-decoration-none hrc-black">

	                                			<div class="card p-3 mb-2 hoverable rounded border-0">

	                                				<div class="card-vacancy align-items-center mb-1">
	                                					<div class="card-img-wrapper mr-2 mb-2">
	                                						<img src="logo.png" alt="image" class="card-image-left">
	                                					</div>

	                                					<div class="card-body p-0 d-flex flex-column">
	                                						<h4 class="font-black m-0 mb-2 fs-20">
	                                							Nombre vacante
	                                						</h4>
	                                						<p class="card-text text-truncate m-0 mb-1 fs-14 text-color">

	                                							<span class="font-weight-light"><i class="icon-copy fi-marker"></i> Ubicacion</span>
	                                						</p>

	                                						<p class="card-text text-truncate m-0 fs-14 text-color mb-2">

	                                							<span class="font-weight-light">
	                                								<i class="icon-copy fa fa-suitcase"></i> Categoria</span>
	                                						</p>

	                                						<div class="align-items-center justify-content-between card-responsive modalityShow">

	                                							<div class="d-flex align-items-center align-self-start">

	                                								<p class="m-0">
	                                									<span class="tag-vacancy d-flex align-items-center justify-content-center fs-12"><i class="icon-copy fa fa-building-o" aria-hidden="true"></i> 
	                                										Presencial y Remoto
	                                									</span>
	                                								</p>
	                                							</div>

	                                							<p class="card-text text-truncate align-self-end m-0 font-weight-light fs-12">
	                                								<span class="font-weight-bold hrc-green text-uppercase fs-12">
	                                                                Si la vacante es reciente-Nuevo
	                                                            </span>
	                                                        
	                                                    </p>

	                                                </div>
	                                                


	                                            </div>
	                                        </div>
	                                            Se oculta o muestra dependiendo la resolucione 
	                                            <div class="align-items-center justify-content-between card-responsive showAndHide">
	                                                <div class="d-flex align-items-center align-self-start">
	                                                                    
	                                                                            <p class="m-0">
	                                                            <span class="tag-vacancy d-flex align-items-center justify-content-center fs-12">
	                                                                <i class="hr-Company mr-1 hrc-black"></i>
	                                                                Presencial y Remoto                            </span>
	                                                        </p>
	                                                                    </div>
	                                                
	                                                <p class="card-text text-truncate align-self-end m-0 font-weight-light fs-12">
	                                                                            <span class="font-weight-bold hrc-green text-uppercase fs-12">
	                                                            Nuevo
	                                                        </span>
	                                                                        
	                                                    Hace menos de una hora                </p>
	                                            </div>
	                                    </div>
	                                </a>


	                            </div>
	                             php --

	                        </div>
	                    </div>



	                            <div class="col-md-4 col-sm-12">
	                                
	                                <div class="card-box mb-30">
	                                    <h5 class="pd-20 h5 mb-0">Nosotros</h5>
	                                    <div class="latest-post">
	                                        <ul>
	                                            <li>
	                                                <h4><a href="#">Ut enim ad minim veniam, quis nostrud exercitation ullamco</a></h4>
	                                                
	                                            </li>
	                                            
	                                        </ul>
	                                    </div>
	                                </div>
	                                sta seccion aparece dependiendo si el usuario inserto un video en su perfil
	                                <div class="card-box mb-30">
	                                    <div class="latest-post">
	                                             <div data-type="youtube" data-video-id="bTqVqk7FSmY"></div>
	                                       
	                                    </div>
	                                </div>
	                               
	                            </div>
	                        </div>
	                    </div>
	          </div>-->

	          <div class="container mt-4 px-4 px-md-3 main-section">
	                      <!--<div class="row mb-3 d-block d-lg-none">
	                          <div class="col-12" style="border-bottom:1px solid #dadada;">
	                              <div class="d-flex justify-content-around">
	                                  <button class="btn-toggle-section active" id="btn-vacantes">
	                                      Vacantes
	                                  </button>
	                                  <button class="btn-toggle-section" id="btn-empresa">
	                                      Nosotros
	                                  </button>
	                              </div>
	                          </div>
	                      </div>-->
	                      <div class="row">
	                          <!-- Inicio seccion vacantes -->
	                          <div class="col-lg-8" id="section-vacantes">
	                              
	                              <div class="row ">
	                              	<div class="main__description bg-white p-4">
	                              		<h6 class="hrc-green d-flex hrc-fs-16 font-weight-bold">
	                              			📋 Descripción del puesto
	                              		</h6>
	                              		<p class="hrc-fs-14 m-0 hrc-black"><?php echo $jobdescription; ?></p> 
	                              </div>
	                              </div>
	                              
	                              <!-- Fin seccion vacantes -->

	                              <p id="data"></p>

	                             <div class="main__button-container">
	                             	<?php
	                             		if ($user_online == true) {
	                             			if ($jobexpired == true) {
	                             				print '<button class="btn btn-primary disabled btn-hidden btn-lg collapsed"><i class="flaticon-line-icon-set-calendar"></i>	Este empleo ya expiro</button>';
	                             			} else {
	                             			if ($myrole == "employee") {
	                             				print '<button';?> onclick="update(this.value, <?php echo "'$jobtitle'"; ?>)" <?php print ' value="'.$jobid.'" class="btn hrc-btn hrc-btn-green hero__btn main__button col-md-12"><i class="flaticon-line-icon-set-pencil"></i>Postularse</button>';
	                             			} else {
	                             				print '<button class="btn btn-primary btn-block disabled btn-hidden btn-lg collapsed"><i class="flaticon-line-icon-set-padlock"></i> Solo los IDentis pueden aplicar!</button>';
	                             			}	
	                             		}
	                             		}else{										
	                             			print '<a href="https://identikit.app/app/views/auth/login-new.php" class="btn hrc-btn hrc-btn-green hero__btn main__button col-md-12"><i class="flaticon-line-icon-set-padlock"></i> Iniciar sesion</a>';	
	                             		}
	                             	?>
	                             </div>
	                             

	                              
	                          </div>


	                          <!-- Seccion derecha -->
	                          <div class="d-none d-lg-block col-lg-4 p-0 pl-md-3" id="section-empresa">
	                                                      <!-- Card texto -->
	                                  <div class="card rounded border-0">
	                                                                      <h5 class="mt-4 ml-4 mb-2 hrc-black font-weight-bold">Nosotros</h5>
	                                      
	                                      <div class="card-body px-4 pt-1 fs-14 hrc-black description-empresa">
	                                      	<?php

	                                      	
	                                      	# ingresar about de empresa

	                                      	?>
	                                      	<p><?= $objetivo   ?> </p>
	                                                                      </div>
	                                  </div>
	                                                  <!-- Fin card texto -->

	                                                      <div class="">
	                                      <div class="mt-3">
		                                      	<!-- Si la empresa ingreso link de youtube de presentacion de su empresa se muestra, sino usar el id que esta indicado en este div-->
	                                          <div data-type="youtube" data-video-id="udba5Jcmiao"></div>

	                                      </div>
	                                  </div>
	                              
	                                                      <!-- card contact -->
	                                   
	                                                  <!-- Fin card contact -->

	                              <div class="d-flex justify-content-center align-items-center mt-3  mb-2">
	                                 
	                              </div>
	                          </div>
	                          <!-- Fin sección derecha -->
	                      </div>
	                  </div>
</div>

	<!-- js -->
	<script src="../../../public/complements/v1/vendors/scripts/core.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/script.min.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/process.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/layout-settings.js"></script>
	<script src="../../../public/complements/v1/src/plugins/plyr/dist/plyr.js"></script>
	<script src="https://cdn.shr.one/1.0.1/shr.js"></script>
	<script type="text/javascript" src="main.js"></script>
	<script src="../../../public/complements/v1/src/plugins/sweetalert2/sweetalert2.all.js"></script>
	<script src="../../../public/complements/v1/src/plugins/sweetalert2/sweet-alert.init.js"></script>
	<script>
		plyr.setup({
			tooltips: {
				controls: !0
			},
		});
	</script>

</body>
</html>