const btnEmpresa = document.getElementById('btn-empresa')
const btnVacantes = document.getElementById('btn-vacantes')
const sectionEmpresa = document.getElementById('section-empresa')
const sectionVacantes = document.getElementById('section-vacantes')

btnEmpresa.addEventListener('click', () => {
    btnEmpresa.classList.add('active')
    btnVacantes.classList.remove('active')

    sectionEmpresa.classList.add('d-block')
    sectionVacantes.classList.add('d-none')
})

btnVacantes.addEventListener('click', () => {
    btnVacantes.classList.add('active')
    btnEmpresa.classList.remove('active')

    sectionEmpresa.classList.remove('d-block')
    sectionVacantes.classList.remove('d-none')
})

// function sendMessage() {
//     closeModal('modal-support')
//     openModal('modal-ok')
// }

const modules_counter = [
    'countVacancy',
    'countArea',
    'countTypeJob',
    'countModalityJob'
]

const filters = {}
let trigger = true

$(function () {

    let isFirstCharge = true

    if (totalVacancies == 0 && !isFirstCharge) {
        clearVacanciesContainer()
    }

    if (totalVacancies == 0 && isFirstCharge) {
        $('#emptyVacancies').removeClass('d-none')
        $('#noVacancy').removeClass('d-none')
        $('#input-searchText').prop('disabled', true)
        $('#openFilter').prop('disabled', true)
        $('.locationVacancy').prop('disabled', true)
        $('#areaVacancy').prop('disabled', true)
        $('.paginationLabel p').addClass('d-none')
    }
    const body = $("html,body")
    function scrollToAnchor(aid) {
        const destination = $(aid);
        $('html,body').animate({
            scrollTop: destination.offset().top
        }, 'slow');
    }

    $(document).on('click', '.paginationLinks', function () {
        scrollToAnchor('#section-vacantes');
    });

    getDataDivs(data.localitiesOptions, '#locality')
    getDataDivs(data.areasOptions, '#area')
    getDataDivs(data.typeJobsOptions, '#typeJobs')
    getDataDivs(data.workModalitiesOptions, '#modalityJob')
    loadItemsOnSelect('.locationVacancy', data.localitiesOptions)
    loadItemsOnSelect('#areaVacancy', data.areasOptions)
    $('#openFilter').click(function (e) {
        $(body).addClass("block-scroll");
        $('.container_filter').toggleClass('openFilter')
        $(".blackDiv").show();
        e.stopPropagation()
        const isSumoOpen = $('.SumoSelect').hasClass('open')
        if (isSumoOpen) {
            $('.SumoSelect').removeClass('open')
        }
    })

    $('#openedFilter').click(function () {
        $('.container_filter').toggleClass('openFilter')
    })

    $('body').on('click', '.applyFilter, .closeFilter', function () {
        $(body).removeClass("block-scroll");
        $('.container_filter').removeClass('openFilter')
        $(".blackDiv").hide();
    })

    $(".blackDiv").click(function () {
        $(body).removeClass("block-scroll");
        $('.container_filter').removeClass('openFilter')
        $(".blackDiv").hide();
    });

    $('.btnToggle').click(function () {
        if ($(this).hasClass('hr-Carret-down')) {
            $(this).removeClass('hr-Carret-down')
            $(this).addClass('hr-Carret-up')

        } else {
            $(this).removeClass('hr-Carret-up')
            $(this).addClass('hr-Carret-down')
        }
    })
    let existListprev = $('.dd_previo');
    let existListnext = $('.dd_siguiente');
    $('.actual').siblings('li:eq(0)').children('a:eq(0)').addClass('borders')

    if ($('.pagination').children().length === 3) {
        $('.actual').siblings('li').children('a:eq(0)').removeClass('borders')
        $('.actual').siblings('li').children('a:eq(0)').addClass('border-none-radius-prev')
    }

    $(this).on('click', '.dd_transition_paginator', function () {
        const page = $(this).text()
        const params = getQueryParams()

        params.selectedPage = page

        getVacancies(params)
    });

    $(this).on('click', '.dd_siguiente', function () {
        const page = parseInt($('.actual').text()) + 1
        const params = getQueryParams()

        params.selectedPage = page

        getVacancies(params);
    });

    $(this).on('click', '.dd_previo', function () {
        const page = parseInt($('.actual').text()) - 1
        const params = getQueryParams()

        params.selectedPage = page

        getVacancies(params);
    });

    // Filtros
    $('#input-searchText').on('change', function () {
        const params = getQueryParams()
        params.selectedPage = 1

        lastFilterUsed = getLastFilterUsed('text-search')

        getVacancies(params)
    })

    $('.postulantDisc input').change(function (e) {
        const params = getQueryParams()
        params.selectedPage = 1

        lastFilterUsed = ''

        getVacancies(params)
    })


    function getQueryParams() {
        const queryParams = {}

        queryParams.typePortal = typePortal // Tipo de portal

        // Valor de input de texto
        const searchText = $('#input-searchText').val()
        if (searchText && searchText !== '') {
            queryParams.searchText = searchText
        }

        if (typePortal === 'microsite') {
            queryParams.microSiteId = microSiteId
        }

        // Select de localidades
        const localitiesOptionsSelected = $('.locationVacancy option:selected')
        if (localitiesOptionsSelected.length > 0) {
            const localitiesSelectedValues = []

            localitiesOptionsSelected.each(function () {
                const optionValue = $(this).prop('value')
                localitiesSelectedValues.push(optionValue)
            })

            queryParams.searchLocalities = JSON.stringify(localitiesSelectedValues)
        }

        // Select de Ã¡reas
        const areasOptionsSelected = $('#areaVacancy option:selected')
        if (areasOptionsSelected.length > 0) {
            const areasSelectedValues = []

            areasOptionsSelected.each(function () {
                const optionValue = $(this).prop('value')
                areasSelectedValues.push(optionValue)
            })

            queryParams.searchAreas = JSON.stringify(areasSelectedValues)
        }

        // Input tipo de trabajo
        const typeJobChecked = $('#typeJobs .checkContainer input:checked')
        if (typeJobChecked.length > 0) {
            const typeJobValues = []

            typeJobChecked.each(function () {
                const optionValue = $(this).prop('id')
                typeJobValues.push(optionValue)
            })

            queryParams.searchTypeJobs = JSON.stringify(typeJobValues)
        }
        // Modalidad de trabajo
        const modalityJobChecked = $('#modalityJob .checkContainer input:checked')
        // const modalityJobNotChecked = $('#modalityJob .checkContainer input:not(:checked)')
        if (modalityJobChecked.length > 0) {
            const modalityValue = []

            modalityJobChecked.each(function () {
                const optionValue = $(this).prop('id')
                modalityValue.push(optionValue)
            })

            queryParams.searchModalityJob = JSON.stringify(modalityValue)

        }
        // Switch
        const switchParam = $('.postulantDisc input').is(':checked')
        if (switchParam) {
            queryParams.searchHandicapped = true
        } else {
            delete queryParams.searchHandicapped
        }

        return queryParams
    }

    function syncDataCombo(parameters) {
        const { optSelected, notSelected, selector } = parameters

        $(optSelected).each(function () {
            const optionValue = $(this).prop('value')
            $(selector).find(`input[id=${optionValue}]`).prop('checked', true)
        })

        $(notSelected).each(function () {
            const optionValue = $(this).prop('value')
            $(selector).find(`input[id=${optionValue}]`).prop('checked', false)
        })

    }


    function syncData(params) {
        const { target, isChecked, wrapperCheck, itemSelected } = params


        $(wrapperCheck).each(function () {
            ($(`.optWrapper .options li[data-value=${target}]`).addClass('selected'))
            $(itemSelected)[0].sumo.selectItem(target);
        })

        if (!isChecked) {
            $(`.optWrapper .options li[data-value=${target}]`).removeClass('selected')
            $(itemSelected)[0].sumo.unSelectItem(target);
        }
    }


    $('#ubication').change(function (e) {
        const params = getQueryParams()
        params.selectedPage = 1

        syncDataCombo({
            optSelected: '.locationVacancy option:selected',
            notSelected: '.locationVacancy option:not(:selected)',
            selector: '#locality'
        })

        if (trigger) {
            lastFilterUsed = getLastFilterUsed('ubication')
            getVacancies(params)
        }

    })

    $('#areaVacancy').change(function (e) {
        const params = getQueryParams()
        params.selectedPage = 1

        syncDataCombo({
            optSelected: '#areaVacancy option:selected',
            notSelected: '#areaVacancy option:not(:selected)',
            selector: '#area'
        })

        if (trigger) {
            lastFilterUsed = getLastFilterUsed('area')
            getVacancies(params)
        }
    })

    //* Checkboxs del sidebar
    $('#typeJobs').on('change', 'input:checkbox', function () {
        lastFilterUsed = getLastFilterUsed('typeJobs')
        const params = getQueryParams()
        params.selectedPage = 1

        getVacancies(params)
    })

    $('#modalityJob').on('change', 'input:checkbox', function () {
        lastFilterUsed = getLastFilterUsed('workModality')

        const params = getQueryParams()
        params.selectedPage = 1

        getVacancies(params)
    })


    $('#locality').on('change', 'input:checkbox', function (e) {
        lastFilterUsed = getLastFilterUsed('ubication')

        const valueId = e.target.id
        const isChecked = $(`.checkContainer input[id=${valueId}]`).is(':checked')

        syncData({
            target: valueId,
            isChecked: isChecked,
            wrapperCheck: '.checkContainer input:checked',
            itemSelected: 'select.locationVacancy'
        })
    })

    $('#area').on('change', 'input:checkbox', function (e) {
        lastFilterUsed = getLastFilterUsed('area')

        const valueId = e.target.id
        const isChecked = $(`.checkContainer input[id=${valueId}]`).is(':checked')

        syncData({
            target: valueId,
            isChecked: isChecked,
            wrapperCheck: '.checkContainer input:checked',
            itemSelected: 'select#areaVacancy'
        })
    })

    $('.SlectBox').SumoSelect({
        placeholder: 'UbicaciÃ³n',
        csvDispCount: 1,
        captionFormat: 'Ubicaciones {0}',
        captionFormatAllSelected: 'Ubicaciones {0}',
        up: false,
        showTitle: false,
        renderLi: (li, originalOption) => {
            const value = originalOption.prop('value')
            const counter = originalOption.attr('data-count')
            li.find('label').addClass('fw-bold hrc-black')
            li.attr('data-value', value) //* Generamos el mismo value de la opcion al li para identificarlo
            li.find('label').append(`<p class="font-weight-normal fs-14">${counter}</p>`)
            return li;
        }

    });

    $('#areaVacancy').SumoSelect({
        placeholder: 'Ãrea',
        csvDispCount: 1,
        captionFormat: 'Ãreas {0}',
        captionFormatAllSelected: 'Ãreas {0}',
        up: false,
        showTitle: false,
        renderLi: (li, originalOption) => {
            const value = originalOption.prop('value')
            const counter = originalOption.attr('data-count')
            li.find('label').addClass('fw-bold hrc-black')
            li.attr('data-value', value)
            li.find('label').append(`<p class="font-weight-normal fs-14">${counter}</p>`)

            return li;
        }
    })

    function loadFilterOptions(filterOptions) {
        if (lastFilterUsed !== 'ubication') {
            const localitiesSelect = $('#ubication')
            const localitiesList = $('#locality')

            const filteredOptions = getOptionsForFilters('.locationVacancy option:selected', filterOptions.localitiesOptions, 'select')

            localitiesSelect.html('')
            loadItemsOnSelect('#ubication', filteredOptions.filterOptions, filteredOptions.selectedOptions)
            localitiesSelect[0].sumo.reload()

            localitiesList.empty()
            getDataDivs(filteredOptions.filterOptions, '#locality', filteredOptions.selectedOptions)
        }

        if (lastFilterUsed !== 'area') {
            const areaSelect = $('#areaVacancy')
            const areaList = $('#area')

            const filteredOptions = getOptionsForFilters('#areaVacancy option:selected', filterOptions.areasOptions, 'select')

            areaSelect.html('')
            loadItemsOnSelect('#areaVacancy', filteredOptions.filterOptions, filteredOptions.selectedOptions)
            areaSelect[0].sumo.reload()

            areaList.empty()

            getDataDivs(filteredOptions.filterOptions, '#area', filteredOptions.selectedOptions)
        }

        if (lastFilterUsed !== 'typeJobs') {
            const typeJobsList = $('#typeJobs')

            const filteredOptions = getOptionsForFilters('#typeJobs .checkContainer input:checked', filterOptions.typeJobsOptions, 'check')

            typeJobsList.empty()
            getDataDivs(filteredOptions.filterOptions, '#typeJobs', filteredOptions.selectedOptions)
        }

        if (lastFilterUsed !== 'workModality') {
            const typeJobsList = $('#modalityJob')

            const filteredOptions = getOptionsForFilters('#modalityJob .checkContainer input:checked', filterOptions.workModalitiesOptions, 'check')

            typeJobsList.empty()
            getDataDivs(filteredOptions.filterOptions, '#modalityJob', filteredOptions.selectedOptions)
        }
    }

    function getOptionsForFilters(selector, filteredOptions, typeElem) {
        const filterOptions = filteredOptions
        const selectedOptionsDom = $(selector)

        const selectedOptions = []
        if (selectedOptionsDom.length > 0) {
            selectedOptionsDom.each(function () {
                let optionValue = ''
                let optionName = ''
                if (typeElem === 'select') {
                    optionValue = $(this).prop('value')
                    optionName = $(this).html()
                } else if (typeElem === 'check') {
                    optionValue = $(this).prop('id')
                    if (optionValue == 'null') {
                        optionValue = null
                    }

                    optionName = $(this).next().html()
                }

                const findResult = filterOptions.filter(option => option.value == optionValue)

                if (findResult.length == 0) {
                    filterOptions.push({ value: optionValue, name: optionName, count: 0 })
                }

                selectedOptions.push(optionValue)
            })
        }

        return { filterOptions, selectedOptions }
    }

    function loadItemsOnSelect(DomSelected, data, filterData = []) {
        let listOptions = data
        if (listOptions.length > 0) {
            listOptions = data.sort((a, b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0))
        }

        $.each(listOptions, function (index, itemData) {
            let selected = ''
            if (filterData.length > 0) {
                const elemsFiltered = filterData.filter(item => item === itemData.value)
                if (elemsFiltered.length > 0) {
                    selected = 'selected'
                }
            }

            $(DomSelected).append(`<option value="${itemData.value}" data-count=${itemData.count} ${selected}>${itemData.name}</option>`) // *identificamos a la lista gracias a la clase
        })
    }

    function getDataDivs(data, identifierDom, filterData = []) {

        if (data.length === 0 && identifierDom) {
            switch (identifierDom) {
                case '#area':
                    $('#area').append('<p class="fs-14 m-0 mt-2">No hay resultados</p>')
                    break;
                case '#locality':
                    $('#locality').append('<p class="fs-14 m-0 mt-2">No hay resultados</p>')
                    break;
                case '#typeJobs':
                    $('#typeJobs').append('<p class="fs-14 m-0 mt-2">No hay resultados</p>')
                    break;
                case '#modalityJob':
                    $('#modalityJob').append('<p class="fs-14 m-0 mt-2">No hay resultados</p>')
                    break
                default:
                    break;
            }
        }

        let listOptions = data
        if (listOptions.length > 0) {
            listOptions = data.sort((a, b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0))
        }

        $.each(listOptions, function (indexInArray, valueOfElement) {
            let checked = ''
            if (filterData.length > 0) {
                const elemsFiltered = filterData.filter(itemData => itemData == valueOfElement.value)
                if (elemsFiltered.length > 0) {
                    checked = 'checked'
                }
            }

            $(identifierDom).append(`
                <div class="d-flex align-items-center justify-content-between py-2 checkContainer">
                    <div class="dataText d-flex justify-content-start align-items-center">
                        <input id="${valueOfElement.value}" type="checkbox" class="hrc-input mr-2" ${checked}>
                        <p class="m-0 fs-14 fw-bold text-truncate">${valueOfElement.name}</p>
                    </div>
                    <span class="fs-14">${valueOfElement.count}</span>
                </div>
             `)
        });
    }

    function counterModules() {
        cleanCounterModules()
        const params = getQueryParams()

        if (params.searchLocalities && JSON.parse(params.searchLocalities).length > 0) {

            const tags_total = JSON.parse(params.searchLocalities).length
            $('#countVacancy').text(tags_total)

        }

        if (params.searchAreas && JSON.parse(params.searchAreas).length > 0) {

            const areas_total = JSON.parse(params.searchAreas).length
            $('#countArea').text(areas_total)

        }

        if (params.searchTypeJobs && JSON.parse(params.searchTypeJobs).length > 0) {
            const typeJob_total = JSON.parse(params.searchTypeJobs).length
            $('#countTypeJob').text(typeJob_total)
        }

        if (params.searchModalityJob && JSON.parse(params.searchModalityJob).length > 0) {
            const modalityJob_total = JSON.parse(params.searchModalityJob).length
            $('#countModalityJob').text(modalityJob_total)
        }
    }

    function cleanCounterModules() {
        modules_counter.forEach((value) => {
            $(`#${value}`).text('')
        })
    }


    $('#cleanFilter').on('click', function () {
        lastFilterUsed = ''
        cleanFilters()
        cleanCounterModules()
    })

    function cleanFilters() {
        trigger = false
        // Limpiamos input
        $('#input-searchText').val('')
        // Limpiamos sidebar
        $('.checkContainer input:checkbox').attr('checked', false)
        $('.postulantDisc input:checkbox').attr('checked', false)
        // Limpiamos selects que estan sincronizados con el sidebar
        $('select.descheck').empty()

        const params = getQueryParams()
        params.selectedPage = 1
        getVacancies(params)
    }

    function filterAplicated() {
        const params = getQueryParams()

        if (params.searchAreas || params.searchHandicapped || params.searchLocalities || params.searchModalityJob || params.searchText || params.searchTypeJobs) {
            $('#filterAplicate').addClass('filterAplicated')
        } else {
            $('#filterAplicate').removeClass('filterAplicated')
        }
    }

    function getVacancies(dataToSend) {
        $.ajax({
            url: '/jobs/getVacanciesForPortal/' + dataToSend.selectedPage,
            dataType: 'json',
            type: 'POST',
            data: dataToSend,
            success: function (dataResult) {

                if (dataResult.data.total_vacancies === 0) {
                    $('#emptyVacancies').removeClass('d-none')

                }
                if (dataResult.data.total_vacancies !== 0) {
                    $('#emptyVacancies').addClass('d-none')
                }
                if (dataResult.result === 'success') {

                    if (dataResult.data.total_vacancies !== 0) {
                        $('#emptyVacancies').addClass('d-none')
                        $('.btnToggle').css('pointer-events', 'auto')
                    }

                    const data = dataResult.data
                    $('.vacancyDataContainer').empty()
                    $('.vacancyDataContainer').append(data.htmlContent)

                    $('.paginationLinks').empty()
                    $('.paginationLinks').append(data.pagination)

                    $('.paginationLabel').empty()
                    $('.paginationLabel').append(data.paginationLabel)
                    existListprev = $('.dd_previo');
                    existListnext = $('.dd_siguiente')

                    if (existListprev.length === 0) {
                        $('.actual').siblings('li:eq(0)').children('a:eq(0)').addClass('borders')
                        if ($('.pagination').children().length === 3) {
                            $('.actual').siblings('li').children('a:eq(0)').removeClass('borders')
                            $('.actual').siblings('li').children('a:eq(0)').addClass('border-none-radius-prev')
                        }
                    }

                    if (existListprev.length !== 0) {
                        $('.dd_previo').removeClass('borders')
                    }
                    /* flecha next */
                    if (existListnext.length === 0) {
                        $('.actual').prev().children('a:eq(0)').addClass('borders')
                    }
                    /* Condicion cuando existen solo 2 paginas */
                    if ($('.pagination').children().length === 3) {
                        if (existListprev.length !== 0) {
                            $('.actual').prev().children('a:eq(0)').removeClass('borders')
                            $('.actual').prev().children('a:eq(0)').addClass('border-none-radius-next')
                        }
                    }

                    $('#amountVacancies').text(data.total_vacancies)
                    loadFilterOptions(data.filtersOptions, dataToSend)

                } else {
                    alert('No se pudieron obtener los datos de vacantes')
                }

                if (dataResult.data.total_vacancies === 0) { // No se encontraron vacantes
                    clearVacanciesContainer()
                }
            },
            complete: function () {
                trigger = true
                isFirstCharge = false
                $('#noVacancy').addClass('d-none')

            },
            error: function (err) {
                console.log(err);
                alert('No se pudieron obtener los datos de vacantes')
            },
            beforeSend: function () {
                filterAplicated()
                counterModules()
            }
        })
    }

    function clearVacanciesContainer() {
        $('#emptyVacancies').removeClass('d-none')
        $('#noVacancyRequest').removeClass('d-none')
        $('.paginationLabel p').addClass('d-none')

    }

    function getLastFilterUsed(lastFilterUsed) {
        let checkedElems = 0

        switch (lastFilterUsed) {
            case 'ubication':
                checkedElems = $('.locationVacancy option:selected').length
                break;
            case 'area':
                checkedElems = $('#areaVacancy option:selected').length
                break;
            case 'typeJobs':
                checkedElems = $('#typeJobs .checkContainer input:checked').length
                break;
            case 'workModality':
                checkedElems = $('#modalityJob .checkContainer input:checked').length
                break;
        }

        if (checkedElems <= 0) {
            return ''
        }

        return lastFilterUsed
    }
})