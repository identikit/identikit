<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-5F5WB8FMJC"></script>
<script>
	window.dataLayer = window.dataLayer || [];
	   
	   function gtag() {
	   	dataLayer.push(arguments);
	   }
	   gtag('js', new Date());
	   
	   gtag('config', 'G-5F5WB8FMJC');
	
</script>
<!DOCTYPE html>
<html>
	<?php
		require 'constants/check-newlogin.php';
		require_once("../../../db/db.php");
		$db = new DbPDO();
		
		if ($user_online == "true") {
			if ($myrole == "employee") {
			} else {
				header("location:../");
			}
		} else {
			header("location:../");
		}
		
      ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">

	<title>IDentiKIT.app - Mi perfil </title>

	<link rel="apple-touch-icon" sizes="180x180"    href="../../../public/img/identikit/logo.png">
	<link rel="icon" type="image/png" sizes="32x32" href="../../../public/img/identikit/logo.png">
	<link rel="icon" type="image/png" sizes="16x16" href="../../../public/img/identikit/logo.png">
	<link rel="stylesheet" type="text/css" href="../employee/profile-e.css">
	<link rel="stylesheet" type="text/css" href="../employee/styles.css">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/datatables/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/style.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">


	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-K2NHRZT4R7"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'G-K2NHRZT4R7');
	</script>

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-5QGSH5J');</script>
	<!-- End Google Tag Manager -->


</head>
<body>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

	<script type="text/javascript">
		$(window).load(function() {
			$(function() {
				$('#file-input').change(function(e) {
					addImage(e);
				});

				function addImage(e) {
					var file = e.target.files[0],
						imageType = /image.*/;

					if (!file.type.match(imageType))
						return;

					var reader = new FileReader();
					reader.onload = fileOnload;
					reader.readAsDataURL(file);
				}

				function fileOnload(e) {
					var result = e.target.result;
					$('#imgSalida').attr("src", result);
				}
			});
		});
	</script>

	<script>
		function capturar() {
			var resultado = "";

			var porNombre = document.getElementsByName("filter");
			for (var i = 0; i < porNombre.length; i++) {
				if (porNombre[i].checked)
					resultado = porNombre[i].value;
			}

			var elemento = document.getElementById("resultado");
			if (elemento.className == "") {
				elemento.className = resultado;
				elemento.width = "600";
			} else {
				elemento.className = resultado;
				elemento.width = "600";
			}
		}
	</script>


	


		

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QGSH5J"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<?php include 'layouts/Header.php';?>
	<?php include 'layouts/Sidebar-menu.php';?>
	

	<div class="main-container">
		<div class="pd-ltr-20 xs-pd-20-10">
			<div class="min-height-200px">
				<section class="Hero">
					
					<div class="Hero__img-container">
						<img src="banner.png">
					</div>

					

					<div class="container pr-0">
						
						
						

						<div class="container pr-0">
							<div class="Hero__logo">
								<?php
									$checkVideoCv = $db->query("SELECT * FROM `tbl_users_data` where member_no=:member_no and not path_video_cv=''", array("member_no"=>$myid));
									$DescUser     = $db->query("SELECT objetivo,tags FROM `tbl_usuarios_data` where iduser=:iduser", array("iduser"=>$myid));
									$mydesc       = 'Sin objetivo laboral';
									$ability      = '';

									if(!empty($DescUser)){
										$mydesc  = $DescUser[0]['objetivo'];
										$ability = $DescUser[0]['tags'];
									}

									if ($myavatar == "") {
										
										print
												'<img src="logo.png" alt="Logo de empresa">';
									} else { 	
										print '
												<img src="../../../'.$myavatar.'" alt="Logo de empresa">
												

												'

												;
									}
								?>
							</div>

							<div class="Hero__content d-flex justify-content-between align-items-center">
								<div class="Hero__text">
									<h2 class="Hero__brand hrc-black"><?php echo "$myfname"; ?> <?php echo "$mylname";?> <!--<img src="verify.png" style="height: 30px; padding: 0px 0px 5px 0px;">--></h2>
									<p class="Hero__description hrc-black">💼 Tipo de perfil 📍 <?php echo $street ?></p>

								</div>
								<!-- De aqui sacar todos los btn hrc-btn para que queden los del commons -->
								

							</div>
						</div>
						</div>

					
					</div>
					
				</section><br>
			</div>
		</div>
	</div>


	<div class="container mt-4 px-4 px-md-3 main-section">

		<div class="row mb-3 d-block d-lg-none">

			<div class="col-12" style="border-bottom:1px solid #dadada;">

				<div class="d-flex justify-content-around">

					<button class="btn-toggle-section active" id="btn-vacantes">Perfil</button>
					<button class="btn-toggle-section" id="btn-empresa">Mas info.</button>

				</div>
		
			</div>
		
		</div>

		<div class="row">

			<!-- Inicio seccion data usuario -->

			<div class="col-lg-8" id="section-vacantes">

				<div class="row justify-content-between flex-nowrap">

					<div class="col-lg-3 p-0 pr-lg-2 d-none d-lg-block my-auto">
						

					</div>

				</div>

				<div class="row ">

					<div class="col-12 p-0 vacancyDataContainer">

						<div class="main__description bg-white p-4">

							<h6 class="hrc-green d-flex hrc-fs-16 font-weight-bold">

								🎓 Educación

							</h6>

						</div>

						<div class="card p-4 mb-2  rounded border-0">

							<div class="row clearfix">

								<?php

								$TblEducation = $db->query("SELECT * FROM tbl_education WHERE member_no = :myid ORDER BY id desc",array("myid"=>$myid));

								if ($TblEducation) {

									try {

										$TblEducation = $db->query("SELECT * FROM tbl_education WHERE member_no = :myid ORDER BY id ASC",array("myid"=>$myid));

										foreach ($TblEducation as $Education) {

											$title = $Education['title'];
											$country = $Education['country'];
	                              			$type = $Education['type'];
	                              			$area = $Education['area'];
	                              			$institution = $Education['institution'];
	                              			$status = $Education['status'];
	                              			$start_date_m = $Education['start_date_m'];
	                              			$start_date_y = $Education['start_date_y'];
	                              			$end_date_m = $Education['end_date_m'];
	                              			$end_date_y = $Education['end_date_y'];
	                              			$id = $Education['id'];

	                              			print '

	                              			<div class="col-sm-12 col-md-6 mb-30">

	                              			<div class="card card-box">
	                              			<div class="card-header">'.$title.'</div>

	                              			<div class="card-body">

	                              			<blockquote class="blockquote mb-0">

	                              			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante</p>

	                              			<footer class="blockquote-footer">'.$type.' - '.$status.' <cite title="Source Title">'.$start_date_m.'/'.$start_date_y.' - '.$end_date_m.'/'.$end_date_y.', '.$country.'</cite>
	                              			</footer>
	                              			</blockquote>
	                              			</div>
	                              			</div>
	                              			</div>
	                              			';

	                              		}} catch (PDOException $e) {}
	                              		} else {

	                              			print'
	                              			';
	                              		}?>
	                              			

	                              		</div>

	                              	</div>

	                              </div>

	                          </div>
	                              <!-- Fin seccion data usuarios -->

	                          </div>

	                          <!-- Seccion derecha  data usuarios-->

	                          <div class="d-none d-lg-block col-lg-4 p-0 pl-md-3" id="section-empresa">
	                                                      <!-- Card texto -->
	                                  
	                                  <div class="card rounded border-0 ">

	                                  	<h5 class="mt-4 ml-4 mb-2 hrc-black font-weight-bold">📈 Objetivo laboral</h5>
	                                      

	                                      <div class="card-body px-4 pt-1 fs-14 hrc-black description-empresa ">

											 <?php
											 	$SQLObjetivo = $db->query("SELECT objetivo FROM `tbl_usuarios_data` where iduser=:iduser", array("iduser"=>$myid));
											 	$mydesc      = $SQLObjetivo[0]['objetivo'];
											 ?>

	                                      	<p><?= $mydesc; ?></p>
	                                      </div>

	                                  </div>

	                                  <!-- Fin card texto -->
	                                  <div class="d-flex justify-content-center align-items-center mt-3  mb-2">
	                                  	

	                                  </div>

	                                  <div class="card rounded border-0 ">

	                                  	<h5 class="mt-4 ml-4 mb-2 hrc-black font-weight-bold">🚀 Habilidades</h5>

	                                  	<div class="card-body px-4 pt-1 fs-14 hrc-black description-empresa ">

	                                  		<?php

	                                  		if(isset($ability)){

	                                  			$tags = explode(",", $ability);
	                                  			if ($tags) {

	                                  				for ($i = 0; $i <= count($tags) - 1; $i++) {
	                                  					$array = $tags[$i];
	                                  					print '

	                                  					<span class="btn btn-outline-secondary mb-10" style="color: #fff; background-color: #1da1f2; border-color: #1da1f2;">' . " $array" . '</span>';
	                                  				}
	                                  				}else{
	                                  					print '
	                                  					<div class="Grid__Row-sc-flyz1b-3 bnfsRw">
	                                  					<div class="Grid__Col-sc-flyz1b-4 dexUcY">
	                                  					<div class="sc-jGxEUC cdQzeM">
	                              						<i class="icon-copy fa fa-tv" aria-hidden="true" color="rgba(0, 0, 24, 0.48)" size="24"></i>
	                              						</div>
	                              						Agregá tus conocimientos y habilidades</div>
	                              						</div>
	                              						</div>';
	                              					}
	                              				}?>	
	                              			</div>
	                              		</div>

	                              		<div class="d-flex justify-content-center align-items-center mt-3  mb-2">
	                              			
	                              		</div>

	                              		<div class="card rounded border-0 ">

	                              			<h5 class="mt-4 ml-4 mb-2 hrc-black font-weight-bold">📁 Archivos</h5>

	                              			<div class="card-body px-4 pt-1 fs-14 hrc-black description-empresa">
	                              				

	                              			</div>
	                              		</div>
	                              	</div>
	                              </div>
	                              <!-- Fin sección derecha -->

	                          </div>

	                      </div>
	                  </div>

	<!-- js -->
	<script src="../../../public/complements/v1/vendors/scripts/core.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/script.min.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/process.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/layout-settings.js"></script>
	<script src="../../../public/complements/v1/src/plugins/plyr/dist/plyr.js"></script>
	<script src="https://cdn.shr.one/1.0.1/shr.js"></script>
	<script type="text/javascript" src="../employee/main.js"></script>
	<script src="../../../public/complements/v1/src/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

	<script>
		plyr.setup({
			tooltips: {
				controls: !0
			},
		});
	</script>

</body>
</html>