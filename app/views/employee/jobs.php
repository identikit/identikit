<?php

	require '../employee/constants/check-newlogin.php';
	require '../../../config/settings.php';
	require_once("../../../db/db.php");
	$db = new DbPDO();

	$fromsearch = false;

	if (isset($_GET['search']) && $_GET['search'] == "✓") {
	} else {
	}
	if (isset($_GET['country']) && ($_GET['category'])) {
		$cate = $_GET['category'];
		$country = $_GET['country'];
		$query1 = "SELECT * FROM tbl_jobs WHERE category = :cate AND city = :country ORDER BY enc_id ASC";
		$query2 = "SELECT * FROM tbl_jobs WHERE category = :cate AND city = :country ORDER BY enc_id ASC";
		$fromsearch = true;

		$slc_country = "$country";
		$slc_category = "$cate";
		$title = "$slc_category empleos en $slc_country";
	} else {
		$query1 = "SELECT * FROM tbl_jobs ORDER BY job_id asc";
		$query2 = "SELECT * FROM tbl_jobs ORDER BY job_id asc";
		$slc_country = "NULL";
		$slc_category = "NULL";
	}
	// if (isset($myrole)) {
	// 	if ($myrole == 'employee') {
	// 		require '../../../config/check-login.php';
	// 	} else {
	// 		require '../employee/constants2/check-login1.php';
	// 	}
	// }
?>

<!DOCTYPE html>
<html>

<head>
	<!-- Basic Page Info -->
	<meta charset="utf-8">
	<title>IDentiKIT - Explorar trabajos</title>

	<link rel="apple-touch-icon" sizes="180x180"    href="../../../public/img/identikit/logo.png">
	<link rel="icon" type="image/png" sizes="32x32" href="../../../public/img/identikit/logo.png">
	<link rel="icon" type="image/png" sizes="16x16" href="../../../public/img/identikit/logo.png">
	<link rel="stylesheet" type="text/css" href="profile-e.css">
	<link rel="stylesheet" type="text/css" href="styles.css">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/datatables/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/style.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/plyr/dist/plyr.css">
	<link rel="stylesheet" type="text/css" href="https://coderhouse.hiringroom.com/assets/js/interviu_me/microsite/multipleselect/sumoselect.css?v=464072538338f33632793f090b2af9b0">

	<script type="text/javascript">
		function update(str) {
			var txt;
			var r = confirm("Are you sure you want to apply this job , you can not UNDO");
			if (r == true) {
				document.getElementById("data").innerHTML = "Please wait...";
				var xmlhttp;
				if (window.XMLHttpRequest) {
					xmlhttp = new XMLHttpRequest();
				} else {
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}

				xmlhttp.onreadystatechange = function() {
					if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
						document.getElementById("data").innerHTML = xmlhttp.responseText;
					}
				}

				xmlhttp.open("GET", "app/apply-job.php?opt=" + str, true);
				xmlhttp.send();
			} else {

			}
		}
	</script>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-K2NHRZT4R7"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'G-K2NHRZT4R7');
	</script>

	<!-- Google Tag Manager -->
	<script>
		(function(w, d, s, l, i) {
			w[l] = w[l] || [];
			w[l].push({
				'gtm.start': new Date().getTime(),
				event: 'gtm.js'
			});
			var f = d.getElementsByTagName(s)[0],
				j = d.createElement(s),
				dl = l != 'dataLayer' ? '&l=' + l : '';
			j.async = true;
			j.src =
				'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
			f.parentNode.insertBefore(j, f);
		})(window, document, 'script', 'dataLayer', 'GTM-5QGSH5J');
	</script>
	<!-- End Google Tag Manager -->
	
	
	


</head>

<body>

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QGSH5J"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<?php include 'layouts/Header.php';?>
	<?php include 'layouts/Sidebar-menu.php';?>
	

	<div class="main-container">
		<div class="pd-ltr-20 xs-pd-20-10">
			<!--<div class="min-height-200px">
				<section class="Hero">



					
				</section><br>
			</div>-->
		</div>
	</div>
	

		          <div class="container mt-4 px-4 px-md-3 main-section">
		                      
		                      <div class="row">
		                          <!-- Inicio seccion vacantes -->
		                          <div class="col-lg-12" id="section-vacantes">
		                     <div class="row">
		                      <div class="col-md-3 col-sm-12">
		                      	
		                      		<div class="form-group">

	                      					<label>Categoria</label>
		                      				<select name="" required class="selectpicker show-tick form-control" data-live-search="true">
		                      				<option disabled value="">Seleccionar</option>
		                      				<?php
		                      					try {
		                      						$categories = $db->query("SELECT * FROM tbl_categories ORDER BY category");
		                      				
		                      						foreach($categories as $category)
		                      						{
		                      							?> <option value="<?php echo $category['category']; ?>"><?php echo $category['category']; ?></option> <?php
		                      						}

		                      
		                      					}catch(PDOException $e)
		                      					{

		                      					}
		                      
		                      				?>
		                      			</select>
		                      		</div>

		                      	</div>

		                      	<div class="col-md-3 col-sm-12">
		                      		
		                      			<div class="form-group">
		                      					<label>País</label>

		                      					<select name="" required class="selectpicker show-tick form-control" data-live-search="true">
		                      					<option disabled value="">Seleccionar</option>


		                      					<?php

		                      						try {
		                      							$locationC = $db->query("SELECT DISTINCT country FROM tbl_jobs ORDER BY country ");
		                      					
		                      							foreach($locationC as $locationCountry)
		                      							{
		                      								?> <option value="<?php echo $locationCountry['country']; ?>"><?php echo $locationCountry['country']; ?></option> <?php
		                      							}

		                      	
		                      						}catch(PDOException $e)
		                      						{

		                      						}
		                      	
		                      					?>
		                      				</select>
		                      			</div>
		                      			
		                      		</div>

		                      		<div class="col-md-3 col-sm-12">
		                      			
		                      				<div class="form-group">
		                      					<label>Ciudad</label>
		                      						<select name="" required class="selectpicker show-tick form-control" data-live-search="true">
		                      						<option disabled value="">Seleccionar</option>


		                      						<?php

		                      							try {
		                      								$locationCi = $db->query("SELECT DISTINCT city FROM tbl_jobs ORDER BY city ");
		                      						
		                      								foreach($locationCi as $locationCity)
		                      								{
		                      									?> <option value="<?php echo $locationCity['city']; ?>"><?php echo $locationCity['city']; ?></option> <?php
		                      								}

		                      		
		                      							}catch(PDOException $e)
		                      							{

		                      							}
		                      		
		                      						?>
		                      					</select>
		                      				</div>
		                      				
		                      			</div>

		                      			<div class="col-md-3 col-sm-12">
		                      				
		                      					<div class="form-group">
		                      						<label></label><br>
													<button type="submit" class="col-md-12 btn btn-primary" >Buscar</button>
		                      						
		                      					</div>
		                      					
		                      				</div>
		                      	</div>



		                              
		                              <div class="row mt-3">
		                              	<?php
		                              	try {
		                              		$jobs = $db->query($query1);

		                              		if ($fromsearch == true) {
		                              			$jobs->bindParam(':cate', $slc_category);
		                              			$jobs->bindParam(':country', $slc_country);
		                              		}

		                              		foreach ($jobs as $job) {
		                              			$type   = $job['type'];
		                              			$compid = $job['company'];
		                              			$tags   = $job['tech'];
		                              		
		                              			$usuarios = $db->query("SELECT * FROM tbl_usuarios WHERE id = :compid", array("compid" => $compid));

		                              			foreach ($usuarios as $usuario) {
		                              				$complogo    = $usuario['path'];
		                              				$TCuenta     = $usuario['tcuenta'];
		                              				$thecompname = $usuario['name'];


		                              				if ($type == "Freelance") {
		                              					$sta = '<span class="job-label label label-success">Freelance</span>';
		                              				}
		                              				if ($type == "Part-time") {
		                              					$sta = '<span class="job-label label label-danger">Part-time</span>';
		                              				}
		                              				if ($type == "Full-time") {
		                              					$sta = '<span class="job-label label label-warning">Full-time</span>';
		                              				}
		                              	?>
		                              	<?php
		                              		try {
		                              			$jobsApplications = $db->query("SELECT COUNT(*) as jobs FROM tbl_job_applications WHERE company = :company", array("company" => $compid));

		                              			$conteo = $jobsApplications[0]["jobs"];
		                              			$num = 0;

		                              			switch ($TCuenta) {
		                              				case 1:
		                              					$num = 6;
		                              					break;
		                              				case 2:
		                              					$num = 30;
		                              					break;
		                              				case 3:
		                              					$num = $conteo;
		                              					break;
		                              			}
		                              		} catch (PDOException $e) {
		                              		}
		                              	?>

		                              	<div class="col-12 p-0 vacancyDataContainer">
		                              		<a href="vacancy.php?identiwork=<?php echo $job['job_id']; ?>" class="text-decoration-none hrc-black">

		                              			<div class="card p-3 mb-2 hoverable rounded border-0">

		                              				<div class="card-vacancy align-items-center mb-1">

		                              					<div class="card-img-wrapper mr-2 mb-2">

		                              						<img src="logo.png" alt="image" class="card-image-left">

		                              					</div>

		                              					<div class="card-body p-0 d-flex flex-column">

		                              						<h4 class="font-black m-0 mb-2 fs-20">
		                              							<?php echo strip_tags($job['title']); ?>
		                              						</h4>

		                              						<p class="card-text text-truncate m-0 mb-1 fs-14 text-color">

		                              							<span class="font-weight-light">📍 <?php echo strip_tags($job['city']); ?>, <?php echo strip_tags($job['country']); ?></span>

		                              						</p>

		                              						<p class="card-text text-truncate m-0 fs-14 text-color mb-2">

		                              							<span class="font-weight-light">💼 <?php echo strip_tags($job['category']) ?></span>

		                              						</p>

		                              						<div class="align-items-center justify-content-between card-responsive modalityShow">

		                              							<div class="d-flex align-items-center align-self-start">

		                              								<p class="m-0">

		                              									<span class="tag-vacancy d-flex align-items-center justify-content-center fs-12">

		                              									 <i class="hrc-black hr-Company mr-1"></i>🏢 <?php echo strip_tags($job['experience']) ?></span>

		                              								</p>

		                              							</div>

		                              							<p class="card-text text-truncate align-self-end m-0 font-weight-light fs-12">
		                              								<span class="font-weight-bold hrc-green text-uppercase fs-12">Nuevo</span>
		                              							</p>

		                              						</div>

		                              					</div>

		                              				</div>

		                              				<!-- Se oculta o muestra dependiendo la resolucione -->

		                              				<div class="align-items-center justify-content-between card-responsive showAndHide">

		                              					<div class="d-flex align-items-center align-self-start">

		                              						<p class="m-0">

		                              							<span class="tag-vacancy d-flex align-items-center justify-content-center fs-12">
		                              								<i class="hr-Company mr-1 hrc-black"></i>🏢 <?php echo strip_tags($job['experience']) ?></span>
		                              						</p>

		                              					</div>

		                              					<p class="card-text text-truncate align-self-end m-0 font-weight-light fs-12">
		                              						<span class="font-weight-bold hrc-green text-uppercase fs-12">Nuevo</span>
		                              					
		                              					</p>

		                              				</div>
		                              			</div>

		                              		</a>
		                              	<?php

		                              			}
		                              		}
		                              	} catch (PDOException $e) {
		                              	}
		                              	?>
		                              	</div>

		                              </div>
		                              <!-- Fin seccion vacantes -->

		                              
		                              

		                             

		                              
		                          </div>

		                          </div>
		                          <!-- Fin sección derecha -->
		                      </div>
		                  </div>
	</div>

		<!-- js -->
		<script src="../../../public/complements/v1/vendors/scripts/core.js"></script>
		<script src="../../../public/complements/v1/vendors/scripts/script.min.js"></script>
		<script src="../../../public/complements/v1/vendors/scripts/process.js"></script>
		<script src="../../../public/complements/v1/vendors/scripts/layout-settings.js"></script>
		<script src="../../../public/complements/v1/src/plugins/plyr/dist/plyr.js"></script>
		<script src="https://cdn.shr.one/1.0.1/shr.js"></script>
		<script type="text/javascript" src="main.js"></script>
		<script type="text/javascript" src="https://coderhouse.hiringroom.com/assets/js/interviu_me/microsite/multipleselect/jquery.sumoselect.min.js?v=464072538338f33632793f090b2af9b0"></script>
		<script>
			plyr.setup({
				tooltips: {
					controls: !0
				},
			});
		</script>



</body>
</html>