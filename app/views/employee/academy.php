<!DOCTYPE html>

<?php 


// if (isset($myrole)) {
// 	if ($myrole == 'employee') {
// 		require '../employee/constants/check-login.php';
// 	} else {
// 		require '../employee/constants2/check-login1.php';
// 	}
// }
	require '../../../config/settings.php';
	//require '../../../config/check-login.php';
	require_once("../../../db/db.php");

	require '../employee/constants/check-login.php';
	// if ($myrole == "employer") {
	// 	//require '../employee/config/check-login.php';
	// }else{
	// 	require '../employee/constants2/check-login1.php';
	
	// }
	$db = new DbPDO();
?>



<html>
<head>
	<meta charset="utf-8">

	<title>IDentiKIT - Academia</title>

	
	<link rel="apple-touch-icon" sizes="180x180"    href="../../../public/img/identikit/logo.png">
	<link rel="icon" type="image/png" sizes="32x32" href="../../../public/img/identikit/logo.png">
	<link rel="icon" type="image/png" sizes="16x16" href="../../../public/img/identikit/logo.png">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/datatables/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/style.css">

	<meta property="og:description" content="Mejora tus habilidades y expande tus conocimientos con la academia IDentiKIT" />
	<meta name="keywords" content="identikit, IDentiKIT, idkit, IDKIT, primera experiencia laboral, trabajos jr, junior, trabajos junior, empresas que contratan juniors, talento joven, talento tech, jovenes, empresas sin experiencia" />
	<meta name="author" content="IDentiKIT">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta property="og:image" content="https://identikit.app/logowebog.png" />
	<meta property="og:image:secure_url" content="https://identikit.app/logowebog.png" />
	<meta property="og:image:type" content="image/png" />
	<meta property="og:image:width" content="300" />
	<meta property="og:image:height" content="300" />
	<meta property="og:image:alt" content="IDentiKIT - Tu primera experiencia laboral" />


	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-K2NHRZT4R7"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'G-K2NHRZT4R7');
	</script>

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-5QGSH5J');</script>
	<!-- End Google Tag Manager -->

	
</head>
<body>

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QGSH5J"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	
	<?php include 'layouts/Header.php';?>
	<?php include 'layouts/Sidebar-menu.php';?>

	<div class="mobile-menu-overlay"></div>
	<div class="main-container">
		<div class="pd-ltr-20 xs-pd-20">
			<div class="container pd-0">
				<div class="card-box pd-20 height-100-p mb-30">
					<div class="row align-items-center">
						<div class="col-md-4">
							<img src="../../../public/img/academy/banner-img.png" alt="">
						</div>
								
						<div class="col-md-8">
							<br>
							<h4 class="font-20 weight-500 mb-10 text-capitalize">Hola!</h4><h4 class="weight-600 font-30 text-blue"> 
								<?php 
									if(isset($myfname)){
										echo strip_tags("$myfname"); 
									}
								?>
							</h4>
							<p class="font-18 max-width-600">En esta seccion podras encotrar cursos, webinars y charlas totalmente gratuito y 100% ONLINE! Si tenes alguna sugerencia para nosotros hacenos saber por nuestras redes.</p>
							<a href="https://www.instagram.com/identikit_app/" type="button" class="btn" data-bgcolor="#f46f30" data-color="#ffffff" style="color: rgb(255, 255, 255); background-color: rgb(244, 111, 48);"><i class="fa fa-instagram"></i></a>	
							<a href="https://twitter.com/IdentikitA" type="button" class="btn" data-bgcolor="#1da1f2" data-color="#ffffff" style="color: rgb(255, 255, 255); background-color: rgb(29, 161, 242);"><i class="fa fa-twitter"></i></a>
							<a href="https://www.facebook.com/Identikit-111822167615589" type="button" class="btn" data-bgcolor="#3b5998" data-color="#ffffff" style="color: rgb(255, 255, 255); background-color: rgb(59, 89, 152);"><i class="fa fa-facebook"></i></a>	
						</div>
					</div>
				</div>

				<div class="row clearfix">	
					<?php
						$CursosAcademy = $db->query("SELECT * FROM academy WHERE fecha < now() AND active = 1 ORDER BY id DESC");

						foreach($CursosAcademy as $curso) {
					?>

					<div class="col-md-4 mb-30 ">
						<div class="card card-box">
							<a href="academyinfo.php?slug=<?=$curso['slug']?>"><img class="card-img-top" src="../../../public/img/academy/<?=$curso['imagen']?>" alt="Card image cap"></a>
							<div class="card-body">
								<h5 class="card-title weight-500"><?=$curso['titulo']?> </h5>
								<div class="work text-success"><i class="ion-android-person"></i> Autor: <?=$curso['usuario']?></div>
								<p class="card-text"><?php echo strip_tags(substr($curso['contenido'], 0, 300))?>...</p>	
								<a href="academyinfo.php?slug=<?=$curso['slug']?>" class="btn btn-primary col-md-12" >Mas info </a>
							</div>
						</div>
					</div>

					<?php } ?>
				</div>
			</div>
		</div>			
	</div>

	<!-- js -->
	<script src="../../../public/complements/v1/vendors/scripts/core.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/script.min.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/process.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/layout-settings.js"></script>
	<script src="../../../public/complements/v1/src/plugins/apexcharts/apexcharts.min.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/jquery.dataTables.min.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/dataTables.bootstrap4.min.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/dataTables.responsive.min.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/responsive.bootstrap4.min.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/dashboard.js"></script>
</body>
</html>