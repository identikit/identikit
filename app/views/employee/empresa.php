<?php
	require '../../../config/settings.php';
	require '../../../config/db_config.php';
	require '../employee/constants/check-login.php';

	require_once("../../../db/db.php");
	$db = new DbPDO();

	if ($myrole == "employer") {
		require '../employee/constants2/check-login1.php';
	} else {
		require '../employee/constants/check-login.php';
	}

	if (isset($_GET['ref'])) {
		$company_id = $_GET['ref'];
		try {
			$usuarios    = $db->query("SELECT * FROM tbl_users WHERE member_no = :memberno and role = 'employer'",array("memberno"=>$company_id));
			$rec = count($usuarios);
			if ($rec == "0") {
				header("location:./");
			} else {
				foreach ($usuarios as $usuario) {
					$compname = $usuario['first_name'];
					$compesta = $usuario['byear'];
					$compmail  = $usuario['email'];
					$comptype = $usuario['title'];
					$compphone = $usuario['phone'];
					$compcity = $usuario['city'];
					$compstreet = $usuario['street'];
					$compzip = $usuario['zip'];
					$compcountry = $usuario['country'];
					$compbout = $usuario['about'];
					$complogo = $usuario['avatar'];
					$compserv = $usuario['services'];
					$compexp = $usuario['expertise'];
					$compweb = $usuario['website'];
					$comppeopl = $usuario['people'];
				}
			}} catch (PDOException $e) {
				
			}} else {
				header("location:./");
			}
?>

<html>

<head>

	<meta charset="UTF-8">

	<title><?php echo strip_tags($compname); ?> en IDentiKIT </title>


	<link rel="apple-touch-icon" sizes="180x180" href="../../../public/img/identikit/logo.png">
	<link rel="icon" type="image/png" sizes="32x32" href="../../../public/img/identikit/logo.png">
	<link rel="icon" type="image/png" sizes="16x16" href="../../../public/img/identikit/logo.png">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/datatables/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/style.css">

	<meta property="og:description" content="<?php echo substr($compbout, 0, 200); ?>" />
	<meta name="keywords" content="identikit, IDentiKIT, idkit, IDKIT, primera experiencia laboral, trabajos jr, junior, trabajos junior, empresas que contratan juniors, talento joven, talento tech, jovenes, empresas sin experiencia" />
	<meta name="author" content="IDentiKIT">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta property="og:image" content="https://identikit.app/logowebog.png" />
	<meta property="og:image:secure_url" content="https://identikit.app/logowebog.png" />
	<meta property="og:image:type" content="image/png" />
	<meta property="og:image:width" content="300" />
	<meta property="og:image:height" content="300" />
	<meta property="og:image:alt" content="IDentiKIT - Tu primera experiencia laboral" />

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-K2NHRZT4R7"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'G-K2NHRZT4R7');
	</script>

	<!-- Google Tag Manager -->
	<script>
		(function(w, d, s, l, i) {
			w[l] = w[l] || [];
			w[l].push({
				'gtm.start': new Date().getTime(),
				event: 'gtm.js'
			});
			var f = d.getElementsByTagName(s)[0],
				j = d.createElement(s),
				dl = l != 'dataLayer' ? '&l=' + l : '';
			j.async = true;
			j.src =
				'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
			f.parentNode.insertBefore(j, f);
		})(window, document, 'script', 'dataLayer', 'GTM-5QGSH5J');
	</script>
	<!-- End Google Tag Manager -->

</head>

<body>


	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QGSH5J" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<?php include 'layouts/Header.php';?>
	<?php include 'layouts/Sidebar-menu.php';?>

	<div class="mobile-menu-overlay"></div>
	<div class="main-container">
		<div class="pd-ltr-20 xs-pd-20">
			<div class="row">
				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-30">
					<div class="pd-20 card-box height-100-p">
						<div class="profile-photo">
							<style>
								#fotop {
									display: block;
									margin: 0 auto 20px;
									width: 150px;
									height: 150px;
									-webkit-box-shadow: 0 0 10px rgba(0, 0, 0, .3);
									box-shadow: 0 0 10px rgba(0, 0, 0, .3);
									border-radius: 100%;
									overflow: hidden;
								}
							</style>
							<?php
							if ($complogo == null) {
								print '<center><img  src="../../../public/img/identikit/logo.png" title="" alt="image"  /></center>';
							} else {
								echo '<center><img id="fotop" alt="image" title="' . $compname . '" width="180" height="100" src="data:image/jpeg;base64,' . base64_encode($complogo) . '"/></center>';
							}
							?>

						</div>
						<h5 class="text-center h5 mb-0"><?php echo strip_tags($compname); ?></h5>
						<p class="text-center text-muted font-14"><i class="icon-copy ion-ios-location"></i> <?php echo "$compstreet"; ?>, <?php echo "$compcity"; ?></p>
						<div class="profile-info">
							<h5 class="mb-20 h5 ">Informacion</h5>
							<ul>
								<li>
									<span>Rubro:</span>
									<?php echo "$comptype"; ?>
								</li>

								<li>
									<span>Personas:</span>
									<?php echo "$comppeopl"; ?>
								</li>

								<li>
									<span>Telefono:</span>
									<a href="tel:<?php echo strip_tags($compphone); ?>"> <?php echo strip_tags($compphone); ?></a>
								</li>
								<li>
									<span>Ubicacion:</span>
									<?php echo strip_tags($compcountry); ?>, <?php echo strip_tags($compcity); ?>
								</li>
								<li>
									<span>Direccion:</span>
									<?php echo strip_tags($compstreet); ?>
								</li>
								<li>
									<span>Sitio web:</span>
									<a href="<?=  strip_tags($compweb); ?>" target="_blank"><?= strip_tags($compweb); ?></a>
								</li>
							</ul><br>
							<h5 class="mb-20 h5 text-blue">Sobre <?php echo strip_tags($compname); ?></h5>

							<p><?php echo strip_tags($compbout); ?></p>


							<h5 class="mb-20 h5 text-blue">Servicios</h5>

							<p><?php echo strip_tags($compserv); ?></p>


						</div>
					</div>
				</div>
				<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 mb-30">
					<div class="card-box height-100-p overflow-hidden">
						<div class="profile-tab height-100-p">
							<div class="tab height-100-p">
								<div class="page-header">
									<div class="col-md-12 col-sm-12">
										<div class="title">
											<h4>Puestos disponibles en <?php echo strip_tags($compname); ?></h4>
										</div>

									</div>
								</div>
								<div class="tab-content">
									<!-- Timeline Tab start -->
									<div class="tab-pane fade show active" id="timeline" role="tabpanel">
										<div class="pd-20">
											<div class="product-wrap">
												<div class="product-list">
													<ul class="row">
														<?php
														try {
															$usuarios    = $db->query("SELECT * FROM tbl_jobs WHERE company = :compid ORDER BY enc_id DESC",array("compid"=>$company_id));

															foreach ($usuarios as $usuario) {
																$post_date = date_format(date_create_from_format('Y-m-d', $usuario['closing_date']), 'd');
																$post_month = date_format(date_create_from_format('Y-m-d', $usuario['closing_date']), 'F');
																$post_year = date_format(date_create_from_format('Y-m-d', $usuario['closing_date']), 'Y');
																$type = $usuario['type'];
																
														?>
																<li class="col-lg-12 col-md-12 col-sm-12">
																	<div class="product-box">
																		<div class="product-caption">
																			<h4><a href="#"><?php echo strip_tags($usuario['title']); ?></a></h4>
																			<div class="text-success" style="margin-top: 6px;"><i class="icon-copy ion-pin"></i> <?php echo strip_tags($usuario['category']); ?></div>
																			<div class="text-success"></div>
																			<div class="text-success" style="margin: 15px 0px 15px 0px;">
																				<?php
																					$tags = explode(",", $usuario['tech']);
																					foreach($tags as $i =>$key) {
																						print '
																							<span class="badge badge-pill" style="color: #01c0fe; background-color: whitesmoke; margin: 5px;">' . " $key " . '</span>';
																					}
																				?>
																			</div>

																			<div class="price">
																				<p class="card-text" style="margin-top: 6px;"><?= strip_tags(substr($usuario['description'], 0, 310)); ?>...</p>
																				<a href="work-detail.php?identiwork=<?= $usuario['job_id']; ?>" class="btn btn-primary col-md-12" style="background-color: #01c0fe; border: none;">Postularme</a>
																			</div>

																		</div>
																	</div>
																</li>
														<?php

															}
														} catch (PDOException $e) {
														}
														?>

													</ul>
												</div>

											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
	</div>



	<!-- js -->
	<script src="../../../public/complements/v1/vendors/scripts/core.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/script.min.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/process.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/layout-settings.js"></script>
	<script src="../../../public/complements/v1/src/plugins/apexcharts/apexcharts.min.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/jquery.dataTables.min.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/dataTables.bootstrap4.min.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/dataTables.responsive.min.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/responsive.bootstrap4.min.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/dashboard.js"></script>
</body>

</html>