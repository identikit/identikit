<style>
	@media only screen and (min-width: 768px) {
		#MenuDown { 
			display:none;
		}
		#searchdiv {
			display:none;
		}
		#logoidentikit{
			display:none;
		}
	}
	@media only screen and (max-width: 600px) {
		/* #MenuDown { 
			display:block;
			position: fixed;
			background-color: #1da1f2;
			bottom: 0; 
			height: 40px;
			width: 100%;
			z-index:4;
		} */
/* 		
		#logoidentikit{
			display:block;
		} */
		#MenuDown ul li{ 
			display:inline;
		}
		#MenuDown ul li img {
			height: 20px; 
			width:  20px;
			margin-left: 11%;
			margin-top: 10px;
		} 
		#searchMobile {
			color: white; 
			width:20px; 
			height:20px;
			display:inline-block; 
			margin-left: 11%;
			margin-top: 10px;
		}
		#searchdiv{
			display:none;
		}
	}
</style>

<div class="header">
		<div class="header-left">
			<div class="menu-icon icon-copy ti-menu" id="hamburger" style="color: white;"></div>
			<!-- <img  src="../../../public/img/identikit/logo.png" alt="image" id="logoidentikit" style="padding-left:5px; width:40px; height:35px;" /> -->
			
			<!--<div class="search-toggle-icon dw dw-search2" style="color: #ffde00;" data-toggle="header_search" id="searchdiv"></div>
			
			<div class="header-search">
				<form method="post" action="../busqueda.php">
					<div class="form-group mb-0">
						<i class="dw dw-search2 search-icon" style="color: #ffde00;"></i>
						<input type="text" class="form-control search-input" placeholder="Buscar en IDentiKIT" required name="PalabraClave">
						<input name="buscar" type="hidden" class="form-control mb-2" id="inlineFormInput" value="v">
					</div>
				</form>
			</div>-->
		</div>
		<div class="header-right">
		    
			<?php
				if(isset($_SESSION["myid"])){
					require_once "alerts.php"; 
				} 
			?>
			<div class="user-info-dropdown">
				<div class="dropdown">
					<a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" style="color: #ffde00;">
						<span class="user-icon">
							<?php
								if(isset($myavatar)){
									if ($myavatar == "") {
										print '<center><img  src="../../../public/img/identikit/logo.png" title="' . $myfname . '" alt="image"  /></center>';
									} else {
										print '<center><img  src="../../../'.$myavatar.'" title="logo" alt="image"  /></center>';
									}
								} 
							?>
						</span>
					</a>
					<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
						<?php
							if ($user_online == true) {
								print '
									<a class="dropdown-item" href="profile.php"><i class="dw dw-user1"></i> Perfil</a>
									<!--<a class="dropdown-item" href="#"><i class="dw dw-settings2"></i> Ajustes</a>
									<a class="dropdown-item" href="#"><i class="dw dw-help"></i> Ayuda</a>-->
									<a class="dropdown-item" href="../../controllers/employeecontroller.php?logout"><i class="dw dw-logout"></i> Salir</a>';
							} else {
								print '
									<a class="dropdown-item" href="https://identikit.app/"><i class="dw dw-user1"></i> Ingresar</a>
									<a class="dropdown-item" href="https://identikit.app/"><i class="dw dw-user1"></i> Registrate</a>
								';
							}
						?>
					</div>
				</div>
			</div>
		</div>
	</div> 

	<div id="MenuDown">
         <ul>
			 <li id="home">
				<a href="index.php">
			 		<img  src="../../../public/img/menudown/home.png" title="Home.png" alt="image" />
				 </a>
			 </li>
			 <li style="margin-top:5px !important">
				<div class="search-toggle-icon dw dw-search2" data-toggle="header_search" id="searchMobile"></div>
				
			 <li>
				 <a href="IDentiVideo.php">
				 	<img  src="../../../public/img/menudown/finger.png" title="finger.png" alt="image"  />
				 </a>
			 </li>
			 <li>
				<a href="aplicados.php">
			 		<img  src="../../../public/img/menudown/blocks.png" title="blocks.png" alt="image"  />	
				</a>
			 </li>
			 <li>
			 	<a href="miperfil.php">
					<?php
						if(isset($myavatar)){
							if ($myavatar == null) {
								print '<img  src="../../../public/img/identikit/logo.png" title="' . $myfname . '" alt="image"  />';
							} else {
								print '<img  src="../../../'.$myavatar.'" title="logo" alt="image"  />';
							}
						}
					?>
				</a>
			 </li>
		 </ul>

	</div>
