<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-5F5WB8FMJC"></script>
<script>
	window.dataLayer = window.dataLayer || [];
	   
	   function gtag() {
	   	dataLayer.push(arguments);
	   }
	   gtag('js', new Date());
	   
	   gtag('config', 'G-5F5WB8FMJC');
	
</script>
<!DOCTYPE html>
<html>
	<?php
		require 'constants/check-newlogin.php';
		require_once("../../../db/db.php");
		$db = new DbPDO();
		
		if ($user_online == "true") {
			if ($myrole == "employee") {
			} else {
				header("location:../");
			}
		} else {
			header("location:../");
		}
		
      ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">

	<title>IDentiKIT.app - Mi perfil </title>

	<link rel="apple-touch-icon" sizes="180x180"    href="../../../public/img/identikit/logo.png">
	<link rel="icon" type="image/png" sizes="32x32" href="../../../public/img/identikit/logo.png">
	<link rel="icon" type="image/png" sizes="16x16" href="../../../public/img/identikit/logo.png">
	<link rel="stylesheet" type="text/css" href="../employee/profile-e.css">
	<link rel="stylesheet" type="text/css" href="../employee/styles.css">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/datatables/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/style.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">


	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-K2NHRZT4R7"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'G-K2NHRZT4R7');
	</script>

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-5QGSH5J');</script>
	<!-- End Google Tag Manager -->


</head>
<body>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

	<script type="text/javascript">
		$(window).load(function() {
			$(function() {
				$('#file-input').change(function(e) {
					addImage(e);
				});

				function addImage(e) {
					var file = e.target.files[0],
						imageType = /image.*/;

					if (!file.type.match(imageType))
						return;

					var reader = new FileReader();
					reader.onload = fileOnload;
					reader.readAsDataURL(file);
				}

				function fileOnload(e) {
					var result = e.target.result;
					$('#imgSalida').attr("src", result);
				}
			});
		});
	</script>

	<script>
		function capturar() {
			var resultado = "";

			var porNombre = document.getElementsByName("filter");
			for (var i = 0; i < porNombre.length; i++) {
				if (porNombre[i].checked)
					resultado = porNombre[i].value;
			}

			var elemento = document.getElementById("resultado");
			if (elemento.className == "") {
				elemento.className = resultado;
				elemento.width = "600";
			} else {
				elemento.className = resultado;
				elemento.width = "600";
			}
		}
	</script>


	


		

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QGSH5J"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<?php include 'layouts/Header.php';?>
	<?php include 'layouts/Sidebar-menu.php';?>
	

	<div class="main-container">
		<div class="pd-ltr-20 xs-pd-20-10">
			<div class="min-height-200px">

				<section class="Hero">
					
					<div class="Hero__img-container">
						<img src="banner.png">
					</div>

					
					
					<div class="container pr-0">
						
						
						

						<div class="container pr-0">

							<a href="fotoperfil" data-toggle="modal" data-target="#fotoperfil"><div class="Hero__logo">

								

								
								<?php
									$checkVideoCv = $db->query("SELECT * FROM `tbl_users_data` where member_no=:member_no and not path_video_cv=''", array("member_no"=>$myid));
									$DescUser     = $db->query("SELECT objetivo,tags,ability FROM `tbl_usuarios_data` where iduser=:iduser", array("iduser"=>$myid));
									$mydesc       = 'Sin objetivo laboral';
									$ability      = '';

									if(!empty($DescUser)){
										$mydesc  = $DescUser[0]['objetivo'];
										$ability = $DescUser[0]['tags'];
										$tperfil = $DescUser[0]['ability'];


									}

									if ($myavatar == "") {
										
										print
												'<img src="logo.png" alt="Logo de usuario">';
									} else { 	
										print '

												<img src="../../../'.$myavatar.'" alt="Logo de empresa">
												

												'

												;
									}
								?>

							</div></a> 

							<div id="fotoperfil" class="modal fade bs-example-modal-lg" tabindex="-1" data-width="550" style="display: none;" data-backdrop="static" data-keyboard="false" data-replace="true" role="dialog" aria-hidden="true">
								<div class="modal-dialog modal-lg modal-dialog-centered">
									<div class="modal-content">
										<div class="modal-header">
											<h4 class="modal-title" id="myLargeModalLabel">Foto de perfil</h4>
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
										</div>
										<div class="modal-body">
											<form action="../../controllers/employeecontroller.php?NewDp" method="POST" autocomplete="off" enctype="multipart/form-data">
													<center>
														<div>

															<div id="resultado" class=""><img id="imgSalida" style="object-fit: cover; object-position: center; border-radius: 8px;" /></div>
															
														</div>
														<div class="form-group col-md-12 ">
															<center><label>Seleccionar imagen</label></center>
															<label for="file-input">
																<img src="../../../public/img/identikit/mas.png" title="Seleccionar imagen" style="width: 50px; height: 50px">
															</label>
															<input accept="image/*" type="file" name="image" id="file-input" required hidden="" />

														</div>
													

														<div class="col-md-12 modal-footer text-center">

															<button type="submit" class="col-md-12 btn btn-primary" value="actualizar">Actualizar</button>

														</div>
													</center>
												</form>

											<style type="text/css">
												.align-profile-modal {
														margin-top: 20px;
														margin-bottom: 20px;
												}


											</style>
										
											
										</div>

									</div>
								</div>
							</div>


							<div class="Hero__content d-flex justify-content-between align-items-center">
								<div class="Hero__text">
									<h2 class="Hero__brand hrc-black"><?php echo "$myfname"; ?> <?php echo "$mylname";?> <a href="edit_profile" data-toggle="modal" data-target="#edit_profile"><img src="edit.png" style="height: 30px; padding: 0px 0px 5px 0px; transform: scaleX(-1);"></a><!--<img src="verify.png" style="height: 30px; padding: 0px 0px 5px 0px;">--></h2>
									<p class="Hero__description hrc-black">💼 <?= $TPAbility ?> 📍 <?php echo $street ?></p>

								</div>
								<!-- De aqui sacar todos los btn hrc-btn para que queden los del commons -->
								<div id="edit_profile" class="modal fade bs-example-modal-lg" tabindex="-1" data-width="550" style="display: none;" data-backdrop="static" data-keyboard="false" data-replace="true" role="dialog" aria-hidden="true">
									<div class="modal-dialog modal-lg modal-dialog-centered">
										<div class="modal-content">
											<div class="modal-header">
												<h4 class="modal-title" id="myLargeModalLabel">Datos personales </h4>
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
											</div>
											<div class="modal-body">

												<form action="../../controllers/employeecontroller.php?UpdateProfile" method="POST" autocomplete="off" enctype="multipart/form-data">

													<div class="row">
													
														<div class="col-md-6 col-sm-12">
															<div class="form-group">
																<label>Nombre*</label>
																<input name="fname" class="form-control form-control-lg" type="text" value="<?php echo "$myfname"; ?>">
															</div>
														</div>
														<div class="col-md-6 col-sm-12">
															<div class="form-group">
																<label>Apellido*</label>
																<input name="lname" class="form-control form-control-lg" type="text" value="<?php echo "$mylname"; ?>">
															</div>
														</div>

														<div class="col-md-6 col-sm-12">
															<label>Documento/ID*</label>
															<input name="dni" class="form-control form-control-lg " type="text" value="<?php echo "$identification"; ?>" >
														</div>

														<div class="col-md-6 col-sm-12 ">
															
																<div class="form-group">
																	<label>Tipo de perfil</label>
																		<select name="category" required class="selectpicker show-tick form-control" data-live-search="true">
																		<option disabled value="">Seleccionar</option>
																		<?php
																			try {
																				$categories = $db->query("SELECT * FROM tbl_categories ORDER BY category");
																		
																				foreach($categories as $category)
																				{
																					?> <option value="<?php echo $category['category']; ?>"><?php echo $category['category']; ?></option> <?php
																				}

														
																			}catch(PDOException $e)
																			{

																			}
														
																		?>
																	</select>
																</div>
															</div>

														

														<div class="col-md-6 col-sm-12">
															<div class="form-group">
																<label>País*</label>
																<input name="country" class="form-control form-control-lg" type="text" value="<?php echo "$country"; ?>" >
															</div>
														</div>
														

														<div class="col-md-6 col-sm-12">
															<div class="form-group">
																<label>Provincia*</label>
																<input name="street" class="form-control form-control-lg" type="text" value="<?php echo "$street"; ?>" >
															</div>
														</div>


														</div>
													
													
														

														<div class="col-md-4 col-sm-12">

														</div>

														<div class="row">

														<!--<div class="col-md-4 col-sm-12">
															<label>Tipo de documento*</label>

															<select name="typedni" required class="selectpicker show-tick form-control">
															<option disabled value="">Tipo</option>

																<option <?php if ($typedni == "D.N.I") {
																			print 'selected ';
																		} ?> value="D.N.I">D.N.I</option>

																<option <?php if ($typedni == "Cedula de identidad") {
																			print 'selected ';
																		} ?>value="Cedula de identidad">Cedula de identidad</option>
																<option <?php if ($typedni == "L.E.") {
																			print 'selected ';
																		} ?>value="L.E.">L.E.</option>

																<option <?php if ($typedni == "Pasaporte") {
																			print 'selected ';
																		} ?>value="Pasaporte">Pasaporte</option>

																<option <?php if ($typedni == "L.C.") {
																			print 'selected ';
																		} ?>value="L.C.">L.C.</option>		

															</select>
														</div>-->
														

														

														<!--<div class="col-md-4 col-sm-12">
															<label>Género*</label>

															<select name="gender" required class="selectpicker show-tick form-control" data-live-search="false">
															<option disabled value="">Género</option>
																<option <?php if ($mygender == "Male") {
																			print ' selected ';
																		} ?> value="Male">Masculino</option>
																<option <?php if ($mygender == "Female") {
																			print ' selected ';
																		} ?>value="Female">Femenino</option>
																<option <?php if ($mygender == "Other") {
																			print ' selected ';
																		} ?>value="Other">Otro</option>
															</select>
														</div>-->

														<!--<div class="col-md-4 col-sm-12">
															<div class="custom-control custom-checkbox mb-5">
																<input type="checkbox" class="custom-control-input" id="customCheck1-1">
																<label class="custom-control-label" for="customCheck1-1">Poseo licencia de conducir</label>
															</div>
														</div>

														<div class="col-md-4 col-sm-12">
															<div class="custom-control custom-checkbox mb-5">
																<input type="checkbox" class="custom-control-input" id="customCheck1-2">
																<label class="custom-control-label" for="customCheck1-2">Poseo movilidad propia</label>
															</div>
														</div>-->



														</div>
														<br>	
														

													
														
												<div class="form-group">
													<label></label>
													<div class="form-group">
														<label>Email</label>
														<input name="email" class="form-control form-control-lg" type="email" value="<?php echo "$myemail"; ?>" >
													</div>
													<div class="form-group">
														<label>Telefono</label>
														<input name="phone" class="form-control form-control-lg" type="text" value="<?php echo "$myphone"; ?>" >
													</div>
													<!--<label>Usuario Github</label>
													<div class="input-group custom">
														<div class="input-group-prepend custom">
															<div class="input-group-text" id="btnGroupAddon">@</div>
														</div>
														<input type="text" class="form-control" placeholder="Ingresa tu usuario de Github" aria-label="Input group example" aria-describedby="btnGroupAddon" name="github" value="<?php echo $github ?>">
													</div>
													<label>Usuario Twitter</label>
													<div class="input-group custom">
														<div class="input-group-prepend custom">
															<div class="input-group-text" id="btnGroupAddon">@</div>
														</div>
														<input type="text" class="form-control" placeholder="Ingresa tu usuario de Twitter" aria-label="Input group example" aria-describedby="btnGroupAddon" name="twitter" value="<?php echo $twitter ?>">
													</div>
													<label>Usuario Instagram</label>
													<div class="input-group custom">
														<div class="input-group-prepend custom">
															<div class="input-group-text" id="btnGroupAddon">@</div>
														</div>
														<input type="text" class="form-control" placeholder="Ingresa tu usuario de Instagram" aria-label="Input group example" aria-describedby="btnGroupAddon" name="instagram" value="<?php echo $instagram ?>">
													</div>-->
												</div>
												</div>
												<div class="col-md-12 modal-footer text-center">
													<button type="submit" class="col-md-12 btn btn-primary" >Add</button>
												</div>
											</div>
											</form>
										</div>
									</div>
								</div>

							</div>
						</div>
						</div>

					
					</div>
					
				</section><br>
			</div>
		</div>
	</div>

	

	          <div class="container mt-4 px-4 px-md-3 main-section">
	                      <div class="row mb-3 d-block d-lg-none">
	                          <div class="col-12" style="border-bottom:1px solid #dadada;">
	                              <div class="d-flex justify-content-around">
	                                  <button class="btn-toggle-section active" id="btn-vacantes">
	                                      Perfil
	                                  </button>
	                                  <button class="btn-toggle-section" id="btn-empresa">
	                                      Mas info.
	                                  </button>
	                              </div>
	                          </div>
	                      </div>
	                      <div class="row">
	                          <!-- Inicio seccion vacantes -->
	                          <div class="col-lg-8" id="section-vacantes">
	                              <div class="row justify-content-between flex-nowrap">
	                                  
	                                <div class="col-lg-3 p-0 pr-lg-2 d-none d-lg-block my-auto">
	                                      
	                                  </div>
	                                   
	                                 
	                                 
	                              </div>
	                              <div class="row ">

	                              	<div class="col-12 p-0 vacancyDataContainer">
	                              		<?php require 'constants/check_reply.php'; ?>



	                              			<div class="main__description bg-white p-4">
	                              				<h6 class="hrc-green d-flex hrc-fs-16 font-weight-bold">
	                              					🎓 Educación
	                              					<a href="add-education" data-toggle="modal" data-target="#add-education" style="background-color: #d5d5d5; color: black; padding: 5px; border-radius: 5px; margin-left: 3%; margin-top: -3px;"><i class="icon-copy fa fa-plus" ></i></a>
	                              				</h6>

	                              		</div>

	                              		<div class="card p-4 mb-2  rounded border-0">


	                              			<div class="row clearfix">
	                              				<div id="add-education" class="modal fade bs-example-modal-lg" tabindex="-1" data-width="550" style="display: none;" data-backdrop="static" data-keyboard="false" data-replace="true" role="dialog" aria-hidden="true">
	                              					<div class="modal-dialog modal-lg modal-dialog-centered">
	                              						<div class="modal-content">
	                              							<div class="modal-header">
	                              								<h4 class="modal-title" id="myLargeModalLabel">Sumar estudio</h4>
	                              								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	                              							</div>
	                              							<div class="modal-body">
	                              								<form action="../../controllers/employeecontroller.php?AddEducation" method="POST" autocomplete="off" enctype="multipart/form-data">
	                              								<div class="row">
	                              									<div class="col-md-6 col-sm-12">
	                              										<div class="form-group">
	                              											<label>Título / Carrera*</label>
	                              											<input name="educationtitle" class="form-control form-control-lg" type="text">
	                              										</div>
	                              									</div>
	                              									<div class="col-md-6 col-sm-12">
	                              										<div class="form-group">
	                              											<label>País*</label>
	                              											<select name="ecountry" required class="selectpicker show-tick form-control" data-live-search="true">
	                              											<option disabled value="">Seleccionar</option>
	                              											<?php
	                              												try {
	                              													$usuarios    = $db->query("SELECT * FROM tbl_countries ORDER BY country_name");
	                              													foreach ($usuarios as $usuario) {
	                              												?> 
	                              											<option 
	                              												<?php 
	                              													?> 
	                              												><?php echo $usuario['country_name']; ?></option>
	                              											<?php
	                              												}} catch (PDOException $e) {
	                              												
	                              												}
	                              												?>
	                              											</select>
	                              										</div>
	                              									</div>
	                              									<div class="col-md-6 col-sm-12">
	                              										<div class="form-group">
	                              											<label>Tipo de estudio*</label>
	                              											<select name="typeed" required="" class="selectpicker show-tick form-control">
	                              											<option value=""></option>
	                              											<option value="Secundario">Bootcamp</option>
	                              											<option value="Secundario">Secundario</option>
	                              											<option value="Curso">Curso</option>
	                              											<option value="Terciario">Terciario</option>
	                              											<option value="Universitario">Universitario</option>
	                              											<option value="Posgrado">Posgrado</option>
	                              											<option value="Master">Master</option>
	                              											<option value="Doctorado">Doctorado</option>
	                              											<option value="Otro">Otro</option>
	                              											</select>
	                              										</div>
	                              									</div>
	                              									<!--<div class="col-md-6 col-sm-12">
	                              										<div class="form-group">
	                              											<label>Área de estudio*</label>
	                              											<select name="earea" required class="selectpicker show-tick form-control" data-live-search="true">
	                              											<option  value=""></option>
	                              											<option value="Tecnologia">Tecnologia</option>
	                              											<option value="Agronomia">Agronomia</option>
	                              											<option value="Economia">Economia</option>
	                              											</select>
	                              										</div>
	                              									</div>-->
	                              									<div class="col-md-6 col-sm-12">
	                              										<div class="form-group">
	                              											<label>Institución*</label>
	                              											<input name="einstitution" class="form-control form-control-lg" type="text">
	                              										</div>
	                              									</div>
	                              									<div class="col-md-12 col-sm-12">
	                              										<div class="form-group">
	                              											<label>Estado*</label>
	                              											<select name="estatus" required class="selectpicker show-tick form-control" data-live-search="true">
	                              											<option  value=""></option>
	                              											<option value="En Curso">En Curso</option>
	                              											<option value="Graduado">Graduado</option>
	                              											<option value="Abandonado">Abandonado</option>
	                              											</select>
	                              										</div>
	                              									</div>
	                              									<div class="col-md-3 col-sm-3">
	                              										<div class="form-group">
	                              											<label>Mes Inicio*</label>
	                              											<select name="startdate_m" required class="selectpicker form-control" data-live-search="false">
	                              											<option disabled value="">Mes</option>
	                              												<?php
	                              													$x       = 1;
	                              													$mymonth = 1;
	                              													while ($x <= 12) {
	                              														if ($x < 10) {
	                              															$x = "0$x";
	                              															print '<option ';
	                              															if ($mymonth == $x) {
	                              																print ' selected ';
	                              															}
	                              															print ' value="' . $x . '">' . $x . '</option>';
	                              														} else {
	                              															print '<option ';
	                              															if ($mymonth == $x) {
	                              																print ' selected ';
	                              															}
	                              															print ' value="' . $x . '">' . $x . '</option>';
	                              														}
	                              														$x++;
	                              													}
	                              												?>
	                              											</select>
	                              										</div>
	                              									</div>
	                              									<div class="col-md-3 col-sm-3">
	                              										<div class="form-group">
	                              											<label>Año Inicio*</label>
	                              											<select name="startdate_y" class="selectpicker form-control" data-live-search="false">
	                              											<option disabled value="">Año</option>
	                              											<?php
	                              												$x = date('Y');
	                              												$yr = 60;
	                              												$y2 = $x - $yr;
	                              												$myyear = 0;
	                              												while ($x > $y2) {
	                              													print '<option ';
	                              													if ($myyear == $x) {
	                              														print ' selected ';
	                              													}
	                              													print ' value="' . $x . '">' . $x . '</option>';
	                              													$x = $x - 1;
	                              													}
	                              												?>
	                              											</select>
	                              										</div>
	                              									</div>
	                              									<div class="col-md-3 col-sm-3">
	                              										<div class="form-group">
	                              											<label>Mes fin*</label>
	                              											<select name="enddate_m" required class="selectpicker form-control" data-live-search="false">
	                              											<option disabled value="">Mes</option>
	                              											<?php
	                              												$x = 1;
	                              												while ($x <= 12) {
	                              													if ($x < 10) {
	                              														$x = "0$x";
	                              														print '<option ';
	                              														if ($mymonth == $x) {
	                              															print ' selected ';
	                              														}
	                              														print ' value="' . $x . '">' . $x . '</option>';
	                              													} else {
	                              														print '<option ';
	                              														if ($mymonth == $x) {
	                              															print ' selected ';
	                              														}
	                              														print ' value="' . $x . '">' . $x . '</option>';
	                              													}
	                              													$x++;
	                              												}
	                              												?>
	                              											</select>
	                              										</div>
	                              									</div>
	                              									<div class="col-md-3 col-sm-3">
	                              										<div class="form-group">
	                              											<label>Año fin*</label>
	                              											<select name="enddate_y" class="selectpicker form-control" data-live-search="false">
	                              											<option disabled value="">Año</option>
	                              											<?php
	                              												$x = date('Y');
	                              												$yr = 60;
	                              												$y2 = $x - $yr;

	                              												while ($x > $y2) {
	                              													print '<option ';
	                              													if ($myyear == $x) {
	                              														print ' selected ';
	                              													}
	                              													print ' value="' . $x . '">' . $x . '</option>';
	                              													$x = $x - 1;
	                              													}
	                              												?>
	                              											</select>
	                              										</div>
	                              									</div>

	                              									
	                              								</div>
	                              								<div class="form-group bootstrap3-wysihtml5-wrapper">
	                              									<label>Descripcion</label>
	                              									<textarea class="form-control" name="description-edu" ></textarea>												
	                              								</div>
	                              								<div class="col-md-12 modal-footer text-center">
	                              									<button type="submit" class="col-md-12 btn btn-primary" >Añadir</button>
	                              								</div>
	                              							</div>
	                              							</form>
	                              						</div>
	                              					</div>
	                              				</div>
	                              				

	                              				
	                              					<?php
	                              						$TblEducation = $db->query("SELECT * FROM tbl_education WHERE member_no = :myid ORDER BY id desc",array("myid"=>$myid));
	                              						
	                              						if ($TblEducation) {
	                              							try {
	                              								$TblEducation = $db->query("SELECT * FROM tbl_education WHERE member_no = :myid ORDER BY id desc",array("myid"=>$myid));
	                              								
	                              								foreach ($TblEducation as $Education) {
	                              									$title = $Education['title'];
	                              									$country = $Education['country'];
	                              									$type = $Education['type'];
	                              									$area = $Education['area'];
	                              									$institution = $Education['institution'];
																	$descripcion = $Education['descripcion'];
	                              									$status = $Education['status'];
	                              									$start_date_m = $Education['start_date_m'];
	                              									$start_date_y = $Education['start_date_y'];
	                              									$end_date_m = $Education['end_date_m'];
	                              									$end_date_y = $Education['end_date_y'];
	                              									$id = $Education['id'];
	                              						
	                              						
	                              									print '
	                              									<div class="col-sm-12 col-md-6 mb-30">

	                              									<div class="card card-box">
	                              										<div class="card-header">
	                              											'.$title.'
	                              										</div>

	                              										<div class="card-body">
	                              											<blockquote class="blockquote mb-0">
	                              												<p>'.$descripcion.'</p>
	                              												<footer class="blockquote-footer">'.$type.' - '.$status.' <cite title="Source Title">'.$start_date_m.'/'.$start_date_y.' - '.$end_date_m.'/'.$end_date_y.', '.$country.'</cite>
	                              												</footer>
	                              											</blockquote>
	                              										</div>
	                              									</div></div>

	                              										
	                              									';
	                              								}} catch (PDOException $e) {}
	                              						} else {
	                              							print'
	                              								';
	                              						}
	                              						
	                              						?>
	                              					
	                              				



	                              			</div>



	                              		<div class="profile-timeline-list">
	                              			
	                              		</div>
	                              		</div>
	                              	</div>

	                              </div>
	                              <!-- Fin seccion vacantes -->

	                              
	                              

	                             

	                              
	                          </div>

	                          <!-- Seccion derecha -->
	                          <div class="d-none d-lg-block col-lg-4 p-0 pl-md-3" id="section-empresa">
	                                                      <!-- Card texto -->
	                                  <div class="card rounded border-0 ">

	                                  	<h5 class="mt-4 ml-4 mb-2 hrc-black font-weight-bold">📈 Objetivo laboral</h5>
	                                      

	                                      <div class="card-body px-4 pt-1 fs-14 hrc-black description-empresa ">
											 <a href="sobrenosotros" data-toggle="modal" data-target="#sobrenosotros" class="edit-avatar" style="margin-left: 96%; background-color: #d5d5d5; border-radius: 5px; padding: 6px; color: black;"><i class="fa fa-pencil"></i></a>

											 <div id="sobrenosotros" class="modal fade bs-example-modal-lg" tabindex="-1" data-width="550" style="display: none;" data-backdrop="static" data-keyboard="false" data-replace="true" role="dialog" aria-hidden="true">
											 	<div class="modal-dialog modal-lg modal-dialog-centered">
											 		<div class="modal-content">
											 			<div class="modal-header">
											 				<h4 class="modal-title" id="myLargeModalLabel">Ingresa tu objetivo laboral</h4>

											 				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
											 			</div>
											 			<div class="modal-body">
											 				<p class="text-muted">El objetivo profesional es dónde quieres llegar profesionalmente, este marca tu camino hacia tu futuro profesional y personal. Fijarte un objetivo te ayudará a encontrar la profesión que mejor se adecue a tus intereses y aptitudes, y también, en planificar tu trayectoria formativa y laboral para poder lograrlo.</p>
											 			<div class="html-editor pd-20 card-box mb-30">

											 				<form action="../../controllers/employeecontroller.php?UpdatePerfilv2" method="POST" autocomplete="off">
											 					<h6>Objetivo Laboral</h6><br>

											 					<?php

											 					$SQLObjetivo = $db->query("SELECT objetivo FROM `tbl_usuarios_data` where iduser=:iduser", array("iduser"=>$myid));

											 					$mydesc      = $SQLObjetivo[0]['objetivo'];?>

											 					<textarea class="form-control" name="objetivo" rows="3" placeholder="Escribe tu objetivo laboral" maxlength="300"><?= $mydesc; ?></textarea>

											 					<center>
											 						<input type="submit" value="Actualizar" class="btn btn-primary col-md-12" style="margin-top:15px">
											 					</center>
											 				</form>
											 				

											 			</div>
											 				
											 			</div>

											 		</div>
											 	</div>
											 </div>



											 <?php
											 	$SQLObjetivo = $db->query("SELECT objetivo FROM `tbl_usuarios_data` where iduser=:iduser", array("iduser"=>$myid));
											 	$mydesc      = $SQLObjetivo[0]['objetivo'];
											 ?>

	                                      	<p><?= $mydesc; ?></p>

	                                      	<!--
	                                                                              <div class="form-group">
	                                                                              	<label>Textarea</label>
	                                                                              	<textarea class="form-control"></textarea>
	                                                                              </div>-->
	                                                                      </div>
	                                  </div>



	                                                  <!-- Fin card texto -->


	                              
	                                                      <!-- card contact -->
	                                   
	                                                  <!-- Fin card contact -->

	                              <div class="d-flex justify-content-center align-items-center mt-3  mb-2">
	                                 
	                              </div>

	                                                               <div class="card rounded border-0 ">

	                                                                	<h5 class="mt-4 ml-4 mb-2 hrc-black font-weight-bold">🚀 Habilidades</h5>
	                                                                    

	                                                                    <div class="card-body px-4 pt-1 fs-14 hrc-black description-empresa ">
	                              										 <a href="add-tags" data-toggle="modal" data-target="#add-tags" class="edit-avatar" style="margin-left: 96%; background-color: #d5d5d5; border-radius: 5px; padding: 6px; color: black;"><i class="fa fa-pencil"></i></a>

	                              										<div id="add-tags" class="modal fade bs-example-modal-lg" tabindex="-1" data-width="550" style="display: none;" data-backdrop="static" data-keyboard="false" data-replace="true" role="dialog" aria-hidden="true">
	                              											<div class="modal-dialog modal-lg modal-dialog-centered">
	                              												<div class="modal-content">
	                              													<div class="modal-header">
	                              														<h4 class="modal-title" id="myLargeModalLabel">Agrega tus conocimientos y habilidades</h4>
	                              														<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	                              													</div>
	                              													<div class="modal-body">
	                              														<form action="../../controllers/employeecontroller.php?UpdateTags" method="POST" autocomplete="off" enctype="multipart/form-data">
	                              														<div class="row">
	                              															<div class="col-md-12 col-sm-12">
	                              																<div class="form-group">
	                              																	<label>Sumar conocimientos y habilidades</label>
	                              																	<p class="text-muted">Tenés 45 conocimientos y habilidades para agregar. Escribí una palabra simple o compuesta. Ej: Microsoft power point</p>
	                              																	<input type="text" class="form-control form-control-lg" data-role="tagsinput" name="ability" placeholder="Presiona ENTER para agregar" value="<?php echo "$ability"; ?>">
	                              																</div>
	                              															</div>
	                              														</div>
	                              														<div class="col-md-12 modal-footer text-center">
	                              															<button type="submit" class="col-md-12 btn btn-primary" >Añadir</button>
	                              														</div>
	                              													</div>
	                              													</form>
	                              												</div>
	                              											</div>
	                              										</div>


	                              										 <?php
	                              										 	if(isset($ability)){
	                              										 		$tags = explode(",", $ability);
	                              										 		if ($tags) {
	                              										 			for ($i = 0; $i <= count($tags) - 1; $i++) {
	                              										 				$array = $tags[$i];
	                              										 				print '
	                              										 					<span class="btn btn-outline-secondary mb-10" style="color: #fff; background-color: #1da1f2; border-color: #1da1f2;">' . " $array" . '</span>';
	                              										 			}
	                              										 		}else{ 
	                              										 			print '
	                              										 				<div class="Grid__Row-sc-flyz1b-3 bnfsRw">
	                              										 					<div class="Grid__Col-sc-flyz1b-4 dexUcY">
	                              										 						<div class="sc-jGxEUC cdQzeM">
	                              										 							<i class="icon-copy fa fa-tv" aria-hidden="true" color="rgba(0, 0, 24, 0.48)" size="24"></i>
	                              										 						</div>
	                              										 						Agregá tus conocimientos y habilidades</div>
	                              										 					</div>
	                              										 				</div>';
	                              										 		}
	                              										 	}
	                              										 ?>	
	                              										</div>

	                                                                </div>

	                                                                    <div class="d-flex justify-content-center align-items-center mt-3  mb-2" >
	                                                                       
	                                                                    </div>

	                                                                                                    <!-- <div class="card rounded border-0 ">
	                                                                                                     <div class="card rounded border-0" style="display:none">

	                                                                                                      	<h5 class="mt-4 ml-4 mb-2 hrc-black font-weight-bold">📁 Archivos</h5>
	                                                                                                          

	                                                                                                          <div class="card-body px-4 pt-1 fs-14 hrc-black description-empresa ">
	                                                                    										

	                                                                    										 


	                                                                    										
	                                                                    										</div>
	                                                                                                      </div>-->
	                                                                </div>
	                          </div>





	                          <!-- Fin sección derecha -->
	                      </div>
	                  </div>
</div>

	<!-- js -->
	<script src="../../../public/complements/v1/vendors/scripts/core.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/script.min.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/process.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/layout-settings.js"></script>
	<script src="../../../public/complements/v1/src/plugins/plyr/dist/plyr.js"></script>
	<script src="https://cdn.shr.one/1.0.1/shr.js"></script>
	<script type="text/javascript" src="../employee/main.js"></script>
	<script src="../../../public/complements/v1/src/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

	<script>
		plyr.setup({
			tooltips: {
				controls: !0
			},
		});
	</script>

</body>
</html>