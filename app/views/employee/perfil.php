<?php
   	require '../../../config/settings.php';
    require '../../../config/check-login.php';
	require_once("../../../db/db.php");
	
    $db = new DbPDO();

    if ($myrole == 'employee') {
        require '../../../config/check-login.php';
    } else {
        //require 'employee/constants2/check-login1.php';
    }

    if (isset($_GET['empid'])) {
        $empid = $_GET['empid'];

        try {
            $usuarios = $db->query("SELECT * FROM tbl_users WHERE role = 'employee' AND member_no = :empid",array("empid"=>$empid));
            $rec = count($usuarios);
            if ($rec == "0") {
                header("location:./");
            } else {

                foreach ($usuarios as $usuario) {
                    $myfname = $usuario['first_name'];
                    $mylname = $usuario['last_name'];
                    $bdate = $usuario['bdate'];
                    $bmonth = $usuario['bmonth'];
                    $byear = $usuario['byear'];
                    $mycountry = $usuario['country'];
                    $mycity = $usuario['city'];
                    $myphone = $usuario['phone'];
                    $about = $usuario['about'];
                    $empavatar = $usuario['avatar'];
                    $current_year = date('Y');
                    $myedu = $usuario['education'];
                    $mytitle = $usuario['title'];
                    $mymail = $usuario['email'];
                    $ability = $usuario['ability'];
                    $github = $usuario['github'];
                    $twitter = $usuario['twitter'];
                    $instagram = $usuario['instagram'];
                    $empujar = $usuario['empujar'];
                }
            }
        } catch (PDOException $e) {
        }
    } else {
        header("location:./");
    }

?>

<!DOCTYPE html>
<html>

<head>
    <!-- Basic Page Info -->
    <meta charset="utf-8">
    <title><?php echo strip_tags("$myfname"); ?> <?php echo strip_tags("$mylname"); ?> en IDentiKIT </title>

    <link rel="apple-touch-icon" sizes="180x180" href="../../../public/img/identikit/logo.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../../../public/img/identikit/logo.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../../../public/img/identikit/logo.png">

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/core.css">
    <link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/icon-font.min.css">
    <link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/datatables/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/style.css">

    <meta property="og:description" content="<?php echo strip_tags(substr($about, 0, 200)); ?>" />
    <meta name="keywords" content="identikit, IDentiKIT, idkit, IDKIT, primera experiencia laboral, trabajos jr, junior, trabajos junior, empresas que contratan juniors, talento joven, talento tech, jovenes, empresas sin experiencia" />
    <meta name="author" content="IDentiKIT">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta property="og:image" content="https://identikit.app/logowebog.png" />
    <meta property="og:image:secure_url" content="https://identikit.app/logowebog.png" />
    <meta property="og:image:type" content="image/png" />
    <meta property="og:image:width" content="300" />
    <meta property="og:image:height" content="300" />


    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-K2NHRZT4R7"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'G-K2NHRZT4R7');
    </script>

    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-5QGSH5J');
    </script>
    <!-- End Google Tag Manager -->



    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

    <script src="chili-1.8.js"></script>
    <script src="js/jquery.expander.js"></script>

    <script>
        $(document).ready(function() {
            var opts = {
                collapseTimer: 4000,
                expandEffect: 'fadeIn',
                collapseEffect: 'fadeOut',
            };

            $.each(['beforeExpand', 'afterExpand', 'onCollapse'], function(i, callback) {
                opts[callback] = function(byUser) {
                    var msg = '<div class="success">' + callback;

                    if (callback === 'onCollapse') {
                        msg += ' (' + (byUser ? 'user' : 'timer') + ')';
                    }
                    msg += '</div>';

                    $(this).parent().parent().append(msg);
                };
            });

            $('dl.expander dd').eq(0).expander();
            $('dl.expander dd').slice(1).expander(opts);

            $('ul.expander li').expander({
                slicePoint: 50,
                widow: 2,
                expandSpeed: 0,
                userCollapseText: '[^]'
            });

            $('div.expander').expander();
        });
    </script>


</head>

<body>

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QGSH5J" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <?php include 'layouts/Header.php';?>
	<?php include 'layouts/Sidebar-menu.php';?>

    <div class="mobile-menu-overlay"></div>

    <style type="text/css">
        .btn-idkit {
            display: block;
            width: 70px;
            height: 70px;
            color: #fff;
            position: fixed;
            right: 20px;
            bottom: 20px;
            border-radius: 60%;
            line-height: 80px;
            text-align: center;
            z-index: 999;
        }

        .rainbow-button {

            background-image: linear-gradient(90deg, #01c0fe 0%, #1bff00 49%, #fd0098 80%, #5600ff 100%);

            display: flex;
            align-items: center;
            justify-content: center;
            text-transform: uppercase;
            font-size: 3vw;
            font-weight: bold;
            border-radius: 100px;

            content: attr(alt);

            background-color: #191919;
            display: flex;
            align-items: center;
            justify-content: center;

            animation: slidebg 2s linear infinite;
            height: auto !important;


        }


        @keyframes slidebg {
            to {
                background-position: 20vw;
            }
        }
    </style>

    <div class="btn-idkit center wow animated bounceInRight box1 " data-wow-delay="0.2s" data-toggle="tooltip" title="Perfil Verificado">
        <a href="#">
            <img src="../../../public/img/identikit/logo.png" alt="Contactar" class="rainbow-button">
        </a>
    </div>

    <div class="main-container">
        <div class="pd-ltr-20 xs-pd-20">
            <div class="min-height-200px">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 mb-30">
                        <div class="pd-20 card-box height-100-p">
                            <div class="profile-photo">
                                <style>
                                    #fotop {
                                        display: block;
                                        margin: 0 auto 20px;
                                        width: 200px;
                                        height: 150px;
                                        -webkit-box-shadow: 0 0 10px rgba(0, 0, 0, .3);
                                        box-shadow: 0 0 10px rgba(0, 0, 0, .3);
                                        border-radius: 100%;
                                        overflow: hidden;
                                    }
                                </style>

                                <?php
                                if ($empavatar == null) {
                                    print '<center><img class="img-circle autofit2" src="../../../public/img/identikit/logo.png"  alt="image" /></center>';
                                } else {
                                    echo '<center><img class="img-circle autofit2" alt="image"id="fotop" src="data:image/jpeg;base64,' . base64_encode($empavatar) . '"/></center>';
                                }
                                ?>
                                <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-body pd-5">
                                                <div class="img-container">
                                                    <?php
                                                    if ($empavatar == null) {
                                                        print '<center><img class="img-circle autofit2" src="images/default.jpg"  alt="image"  /></center>';
                                                    } else {
                                                        echo '<center><img class="img-circle autofit2" alt="image" src="data:image/jpeg;base64,' . base64_encode($empavatar) . '"/></center>';
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <input type="submit" value="Update" class="btn btn-primary">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h5 class="text-center h5 mb-0 "><?php if ($usuario['empujar'] == "1") {
                                                                    print '🌟';
                                                                } ?><?php echo strip_tags("$myfname"); ?> <?php echo strip_tags("$mylname"); ?></h5>



                            <?php

                            //$title = $row['mytitle'];

                            ?>
                            <!--<div class="work text-success text-center pd-10"> <?php if ($usuario['title']) {
                                                                                    echo '💻 ' . strip_tags($mytitle) . ' ';
                                                                                } ?></div>-->



                            <div class="btn-list text-center pd-10">
                                <?php

                                    if ($github) {
                                        print '<a href="https://github.com/' . "$github" . '" type="button" class="btn" style="border-radius: 10px;" data-bgcolor="black" data-color="#ffffff" target="_blank"><i class="fa fa-github"></i></a>';
                                    }

                                    if ($twitter) {
                                        print '<a href="https://twitter.com/' . "$twitter" . '" type="button" class="btn" style="border-radius: 10px;" data-bgcolor="#1da1f2" data-color="#ffffff" target="_blank"><i class="fa fa-twitter"></i></a>';
                                    }

                                    if ($instagram) {
                                        print '<a href="https://instagram.com/' . "$instagram" . '" type="button" class="btn" style="border-radius: 10px;" data-bgcolor="#f46f30" data-color="#ffffff" target="_blank"><i class="fa fa-instagram"></i></a>';
                                    }

                                ?>
                                
                                

                            </div>
                            

                            <div class="expander">
                                <p class="text-center text-muted font-14"><?php echo strip_tags("$about"); ?></p>
                            </div>

                            <div class="contact-directory-box pd-0">
                                <div class="contact-dire-info text-center pd-0">


                                    <div class="contact-skill pd-0">
                                        <?php

                                        $tags = explode(",", $ability);

                                        foreach ($tags as $i => $key) {
                                            print '<span class="badge badge-pill">' . " $key " . '</span>';
                                        }
                                        ?>


                                    </div>
                                    <div class="timeline mb-30">
                                        <h5 class="text-center mb-20 h5 mb-0 pd-10">Proyectos </h5>
                                        <ul>
                                            <?php
                                            //require 'constants/db_config.php';
                                            try {
                                                $experiencias = $db->query("SELECT * FROM tbl_experience WHERE member_no = :empid AND view = 'mostrar' ORDER BY id", array("empid"=>$empid));
                                                $rec = count($experiencias);
                                                if ($rec == "0") {
                                                } else {

                                                    foreach ($experiencias as $experiencia) {
                                            ?>
                                                        <li>
                                                            <div class="timeline-date">
                                                                <?= $experiencia['start_date']; ?> <b>al</b> <?= $experiencia['end_date']; ?>
                                                            </div>
                                                            <div class="timeline-desc card-box">
                                                                <div class="pd-20">
                                                                    <h4 class="mb-10 h4"><?= $experiencia['title']; ?></h4>
                                                                    <?php
                                                                    $tags = explode(",", $experiencia['techno']);

                                                                    for ($i = 0; $i <= count($tags) - 1; $i++) {
                                                                        $array = $tags[$i];
                                                                        print '<span class="badge badge-pill" style="background-color: whitesmoke; margin: 5px; color: #01c0fe;">' . " $array" . '</a></span>';
                                                                    }
                                                                    ?> <br><br>
                                                                    <p class="text-center text-muted font-14"><?= $experiencia['duties']; ?></p>
                                                                    <p class="text-center text-muted font-14"><a href="<?= $experiencia['link']; ?>"><?= $experiencia['link']; ?></a></p>
                                                                </div>
                                                            </div>
                                                        </li>
                                            <?php
                                                    }
                                                }
                                            } catch (PDOException $e) {
                                            } ?>


                                        </ul>
                                    </div>
                                </div>
                            </div>





                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    <!-- js -->
    <script src="../../../public/complements/v1/vendors/scripts/core.js"></script>
    <script src="../../../public/complements/v1/vendors/scripts/script.min.js"></script>
    <script src="../../../public/complements/v1/vendors/scripts/process.js"></script>
    <script src="../../../public/complements/v1/vendors/scripts/layout-settings.js"></script>
    <script src="../../../public/complements/v1/src/plugins/apexcharts/apexcharts.min.js"></script>
    <script src="../../../public/complements/v1/src/plugins/datatables/js/jquery.dataTables.min.js"></script>
    <script src="../../../public/complements/v1/src/plugins/datatables/js/dataTables.bootstrap4.min.js"></script>
    <script src="../../../public/complements/v1/src/plugins/datatables/js/dataTables.responsive.min.js"></script>
    <script src="../../../public/complements/v1/src/plugins/datatables/js/responsive.bootstrap4.min.js"></script>
    <script src="../../../public/complements/v1/vendors/scripts/dashboard.js"></script>
</body>

</html>