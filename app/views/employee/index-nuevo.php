<!DOCTYPE html>

<?php 

	require '../employee/constants/check-newlogin.php';

	require_once("../../../db/db.php");
	$db = new DbPDO();

	if ($user_online == "true") {
		if ($myrole == "employee") {
			}else{
				header("location:../employer/index.php");	}
	}else{
		header("location:../principal.php");	
	}
?>

<html>
<head>
	<meta charset="utf-8">

	<title>🚀 Bienvenida/o a IDentiKIT  </title>

	
	<link rel="apple-touch-icon" sizes="180x180"    href="../../../public/img/identikit/logo.png">
	<link rel="icon" type="image/png" sizes="32x32" href="../../../public/img/identikit/logo.png">
	<link rel="icon" type="image/png" sizes="16x16" href="../../../public/img/identikit/logo.png">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/datatables/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/style.css">


	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-K2NHRZT4R7"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'G-K2NHRZT4R7');
	</script>

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-5QGSH5J');</script>
	<!-- End Google Tag Manager -->
	<style>
		a.heart_mark {
			width: 40px;
			height: 40px;
			-webkit-border-radius: 5px;
			-moz-border-radius: 5px;
			border-radius: 5px;
			color: #00c1ff;
			font-size: 14px;
			line-height: 40px;
			text-align: center;
			display: inline-block;
			background: #EFFDF5;
			margin-right: 15px;
		}

		a.heart_mark:hover {
			background: #ff0047;
		}

		i.ti-heart {
			color: #00c1ff;
		}

		a.heart_mark:hover>i.ti-heart {
			color: white;
		}
	</style>
</head>
<body>
	

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QGSH5J"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<?php include 'layouts/Header.php';?>
	<?php include 'layouts/Sidebar-menu.php';?>

	<!-- <div class="mobile-menu-overlay"></div> -->

	<!-- <div class="main-container" style="padding: 20px !important;">
		<div class="pd-ltr-20 xs-pd-20">
			<div class="container pd-0">							
				<div class="container pd-0">
					
					<div class="card-box pd-20 height-100-p mb-30">
						<div class="row align-items-center">
							
							<div class="col-md-8">
								<h4 class="font-20 weight-500 mb-10 text-capitalize">
									<img src="images/waving-hand.png" style="width: 8%; height: auto !important;"> Hola <div class="weight-600  text-blue"> <?= $myfname . ' ' . $mylname ?> </div>
								</h4>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-xl-8 mb-30">
							<div class="card-box height-100-p pd-20">
								<h2 class="h4 mb-20">Módulos</h2>
								<div id="chart5"></div>

								<div class="row">
										<div class="card-box height-100-p widget-style1">
											<div class="d-flex flex-wrap align-items-center">
												<div class="progress-data">
													<div id="chart"><img src="images/man-teacher.png" style="width: 100%; height: auto !important;"></div>
												</div>
												<div class="widget-data">
													<a href="module-detail.php?sectiontype=capacitaciones"><div class="h4 mb-0">Módulo 1</div>
													<div class="weight-600 font-14">Capacitaciones</div></a>
												</div>
											</div>
										</div>

										<div class="card-box height-100-p widget-style1">
											<div class="d-flex flex-wrap align-items-center">
												<div class="progress-data">
													<div id="chart"><img src="images/speech-balloon.png" style="width: 100%; height: auto !important;"></div>
												</div>
												<div class="widget-data">
													<a href="module-detail.php?sectiontype=entrenamientos"><div class="h4 mb-0">Módulo 2</div>
													<div class="weight-600 font-14">Entrenamientos</div></a>
												</div>
											</div>
										</div>

										<div class="card-box height-100-p widget-style1">
											<div class="d-flex flex-wrap align-items-center">
												<div class="progress-data">
													<div id="chart"><img src="images/users.png" style="width: 100%; height: auto !important;"></div>
												</div>
												<div class="widget-data">
													<a href="module-detail.php?sectiontype=apps"><div class="h4 mb-0">Módulo 3</div>
													<div class="weight-600 font-14">Apps</div></a>
												</div>
											</div>
										</div>

										<div class="card-box height-100-p widget-style1">
											<div class="d-flex flex-wrap align-items-center">
												<div class="progress-data">
													<div id="chart"><img src="images/rocket.png" style="width: 100%; height: auto !important;"></div>
												</div>
												<div class="widget-data">
													<a href="module-detail.php?sectiontype=habilidad"><div class="h4 mb-0">Módulo 4</div>
													<div class="weight-600 font-14">Habilidad</div></a>
												</div>
											</div>
										</div>
								</div>
							</div>

						</div>
						<div class="col-xl-4 mb-30">
							<div class="card-box height-100-p pd-20">
								<h2 class="h4 mb-20">Anuncios</h2>
								<div id="chart6"></div>

								<div class="browser-visits">
									<ul>
										<li class="d-flex flex-wrap align-items-center">
											<div class="icon"><img src="images/bell.png" style="width: 60%; height: auto !important;"></div>
											<a href="" class="browser-name">Conoce a nuestros mentores</a>
										</li>
										<li class="d-flex flex-wrap align-items-center">
											<div class="icon"><img src="images/bell.png" style="width: 60%; height: auto !important;"></div>
											<a href="" class="browser-name">Nuevo contenido en Módulo 1</a>
										</li>
										<li class="d-flex flex-wrap align-items-center">
											<div class="icon"><img src="images/bell.png" style="width: 60%; height: auto !important;"></div>
											<a href="" class="browser-name">MercadoLibre nos cuentan sobre ellos</a>
										</li>
										
									</ul>
								</div>
							</div>
						</div>
					</div>
					
				</div>
			</div>
			
		</div>
	</div> -->

	<div class="flex-container">
        <div class="menu">
        </div>
        <div class="content">
        </div>
    </div>

	<!-- <div style="padding-top:20vh; position: absolute;">
		hola mundo
	</div> -->

	<div class="flex">
		<div>
			<div class="card shadow-lg">
				<img class="card-img-top" src="images/man-teacher.png" alt="Card image cap" style="width: 50%; height: auto !important; margin: 0 auto; position: relative; top:-60px">
				<div class="card-body">
					<h5 class="card-title">Módulo 1</h5>
					<p class="card-text">Capacitaciones</p>
					<a href="module-detail.php?sectiontype=capacitaciones" class="btn btn-primary">IR A LA CLASE</a>
				</div>
			</div>
		</div>
		<div>
		<div class="card"> 
			<img class="card-img-top" src="images/speech-balloon.png" alt="Card image cap" style="width: 50%; height: auto !important; margin: 0 auto;position: relative; top:-60px"> 
			<div class="card-body">
				<h5 class="card-title">Módulo 2</h5>
				<p class="card-text">Entrenamientos</p>
				<a href="module-detail.php?sectiontype=entrenamientos" class="btn btn-primary">IR A LA CLASE</a>
			</div>
			</div>
		</div>
		<div>
		<div class="card">
			<img class="card-img-top" src="images/users.png" alt="Card image cap"  style="width: 50%; height: auto !important; margin: 0 auto;position: relative; top:-80px">
			<div class="card-body">
				<h5 class="card-title">Módulo 3</h5>
				<p class="card-text">Apps</p>
				<a href="module-detail.php?sectiontype=apps" class="btn btn-primary">IR A LA CLASE</a>
			</div>
			</div>
		</div>
		<div>
			<div class="card">
				<img class="card-img-top" src="images/rocket.png" alt="Card image cap"  style="width: 50%; height: auto !important; margin: 0 auto;position: relative; top:-60px">
				<div class="card-body">
					<h5 class="card-title">Módulo 4 </h5>
					<p class="card-text">Habilidad</p>
					<a href="module-detail.php?sectiontype=habilidad" class="btn btn-primary disabled" style="border-radius: 10px !important; color: ;">IR A LA CLASE</a>
			</div>
			</div>
		</div>
	</div>

	<style>
		.flex-container { 
			display: flex; 
			flex-direction: column;
			position:fixed;
			height: 100vh;
			width: 100%;
		}
		.menu {
			flex-shrink:0;
			height: 50vh;
		}
		.content { 
			background-color: #1da1f2;
			height: 50vh; 
		}
		.flex {
			display: flex;
			justify-content: center;
			align-items: center;
			height: 100vh; 
		}

		.flex div { 
			margin: 5%;
		}
		.card {
			text-align: center;
			width: 130%;
		}

		@media only screen and (max-width: 600px) {
			.flex {
				display: block !important;
				margin-top: 100px;
			}
			.flex div { 
				margin: 0 auto;
			}
			.card { 
				margin-top: 100px !important;
				width: 18rem;
			}
		}

	</style>
	<!-- js -->
	<script src="../../../public/complements/v1/vendors/scripts/core.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/script.min.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/process.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/layout-settings.js"></script>
	<script src="../../../public/complements/v1/src/plugins/apexcharts/apexcharts.min.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/jquery.dataTables.min.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/dataTables.bootstrap4.min.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/dataTables.responsive.min.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/responsive.bootstrap4.min.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/dashboard.js"></script>

	<script type="text/javascript">
		$(".heart_mark").click(function() {
			var valor = $(this)[0].getAttribute("value");

			$.ajax({
				type: 'GET',
				url: '../../controllers/helpercontroller.php/add_like.php?AddLike',
				data: {'job_id': valor}
			}).done(function(){
				window.location.reload();
			});
		});
	</script>

	<style>
		.card-box {
			margin: 1%;
		}
	</style>


</body>
</html>