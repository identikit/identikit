<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-5F5WB8FMJC"></script>
<script>
	window.dataLayer = window.dataLayer || [];

	function gtag() {
		dataLayer.push(arguments);
	}
	gtag('js', new Date());

	gtag('config', 'G-5F5WB8FMJC');
</script>
<!DOCTYPE html>
<html>
<?php

	require 'constants/check-login.php';
	require_once("../../../db/db.php");
	$db = new DbPDO();

	if ($user_online == "true") {
		if ($myrole == "employee") {
		} else {
			header("location:../");
		}
	} else {
		header("location:../");
	}

?>

<head>
	<!-- Basic Page Info -->
	<meta charset="utf-8">
	<title>IDentiKIT - Mi perfil</title>

	<link rel="apple-touch-icon"      sizes="180x180" href="../../../public/img/identikit/logo.png">
	<link rel="icon" type="image/png" sizes="32x32"   href="../../../public/img/identikit/logo.png">
	<link rel="icon" type="image/png" sizes="16x16"   href="../../../public/img/identikit/logo.png">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/datatables/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/style.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">

	<!-- Google Tag Manager -->
	<script>
		(function(w, d, s, l, i) {
			w[l] = w[l] || [];
			w[l].push({
				'gtm.start': new Date().getTime(),
				event: 'gtm.js'
			});
			var f = d.getElementsByTagName(s)[0],
				j = d.createElement(s),
				dl = l != 'dataLayer' ? '&l=' + l : '';
			j.async = true;
			j.src =
				'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
			f.parentNode.insertBefore(j, f);
		})(window, document, 'script', 'dataLayer', 'GTM-PHBQLSG');
	</script>
	<!-- End Google Tag Manager -->

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

	<script type="text/javascript">
		$(window).load(function() {
			$(function() {
				$('#file-input').change(function(e) {
					addImage(e);
				});

				function addImage(e) {
					var file = e.target.files[0],
						imageType = /image.*/;

					if (!file.type.match(imageType))
						return;

					var reader = new FileReader();
					reader.onload = fileOnload;
					reader.readAsDataURL(file);
				}

				function fileOnload(e) {
					var result = e.target.result;
					$('#imgSalida').attr("src", result);
				}
			});
		});
	</script>

	<script>
		function capturar() {
			var resultado = "";

			var porNombre = document.getElementsByName("filter");
			for (var i = 0; i < porNombre.length; i++) {
				if (porNombre[i].checked)
					resultado = porNombre[i].value;
			}

			var elemento = document.getElementById("resultado");
			if (elemento.className == "") {
				elemento.className = resultado;
				elemento.width = "600";
			} else {
				elemento.className = resultado;
				elemento.width = "600";
			}
		}
	</script>

	<style>

        .rainbow-button {
			background-image: linear-gradient(90deg, #01c0fe 0%, #1bff00 49%, #fd0098 80%, #5600ff 100%);
			display: flex;
			align-items: center;
			justify-content: center;
			text-transform: uppercase;
			font-size: 3vw;
			font-weight: bold;
			border-radius: 100px;
			content: attr(alt);
			background-color: #191919;
			display: flex;
			align-items: center;
			justify-content: center;
			animation: slidebg 2s linear infinite;
			height: auto !important;
		}

		@keyframes slidebg {
            to {
                background-position: 20vw;
            }
        }

	</style>
	


</head>

<body>
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PHBQLSG" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	
	<?php include 'layouts/Header.php';?>
	<?php include 'layouts/Sidebar-menu.php';?>


	<div class="mobile-menu-overlay"></div>
	<div class="main-container">
		<div class="pd-ltr-20 ">
			<?php require 'constants/check_reply.php'; ?>
			<div class="row">
				<div class="card-box col-md-12">
					<h5 class="h4 mb-20 pd-10">Mi perfil</h5>
					<div class="tab">
						<ul class="nav nav-tabs" role="tablist">
							<li class="nav-item">
								<a class="nav-link active " data-toggle="tab" href="#home" role="tab" aria-selected="true">Mi ID</a>
							</li>
							<li class="nav-item">
								<a class="nav-link " data-toggle="tab" href="#profile" role="tab" aria-selected="false">Info</a>
							</li>
							<li class="nav-item">
								<a class="nav-link " data-toggle="tab" href="#experiencia" role="tab" aria-selected="false">Proyectos</a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane fade show active" id="home" role="tabpanel">
								<div class="row pd-30">
									<div class="col-12">
										<div class="profile-photo">
											<a href="modal" data-toggle="modal" data-target="#modal" class="edit-avatar" style="padding: 8px;"><i class="fa fa-pencil"></i></a>
											<?php
												$checkVideoCv = $db->query("SELECT * FROM `tbl_users_data` where member_no=:member_no and not path_video_cv=''", array("member_no"=>$myid));

												if ($myavatar == null) {
													if ($checkVideoCv){
														print
															'<div class="btn-idkit center wow animated bounceInRight box1 " data-wow-delay="0.2s" data-toggle="tooltip">
																<center>
																	<img  src="../../../public/img/identikit/logo.png" class="rainbow-button" title="' . $myfname . '" alt="image" data-toggle="modal" href="#videocv" style="cursor:pointer"/>
																</center>
															</div>';
													} else {
														print '
															<img  src="../../../public/img/identikit/logo.png"  title="' . $myfname . '" alt="image" />';
													}
												} else {
													if ($checkVideoCv){
														print
															'<div class="btn-idkit center wow animated bounceInRight box1 " data-wow-delay="0.2s" data-toggle="tooltip">
																<center>
																	<img alt="image"  class="rainbow-button" title="' . $myfname . '"  src="data:image/png;base64,' . base64_encode($myavatar) . '" data-toggle="modal" href="#videocv" style="cursor:pointer"/>
																</center>
															</div>';
													} else {
														echo '<center><img alt="image" title="' . $myfname . '"  src="data:image/png;base64,' . base64_encode($myavatar) . '"/></center>';
													}
												}
											?>
										</div>
										
										<center>
										<button type="button" class="btn btn-primary" data-toggle="modal" href="#uploadvideocv">
										<?php
											if (!$checkVideoCv){
												echo "Subir IDentiVideo";
											} else {
												echo "Actualizar IDentiVideo";
											}
										?>
										</button>
									</center>
								<br>
										
										<h5 class="text-center h5 mb-0 ">
										<?php 
											if ($empujar == "1") {
												print '🌟';
											} 
										?> 

										<?php 
											echo "$myfname"; 
										?> 
										
										<?php 
											echo "$mylname"; 
										?>
									</h5>
										<div class="expander">
										<p class="text-center text-muted font-14 "><?php echo "$mydesc"; ?></p>
									</div>
									
									<!--<div class="work text-success text-center pd-10">💻 <?php echo "$mytitle"; ?></div>-->

									<div class="contact-directory-box pd-0" style="min-height:auto;margin-top:-30px">
										<div class="contact-dire-info text-center pd-0">
											<div class="contact-skill pd-0">
												<?php
													$tags = explode(",", $ability);
													for ($i = 0; $i <= count($tags) - 1; $i++) {
														$array = $tags[$i];
														print '
															<span class="badge badge-pill">' . " $array" . '</a></span>';
													}
												?>
											</div>
										</div>
									</div>
							
									
									<center style="display:none">
										<form action="../../controllers/employeecontroller.php" method="post" enctype="multipart/form-data">
											<br><input type="file" name="fileToUpload" id="fileToUpload"> <br> <br>
											<input type="hidden" name="hiddenId" id="hiddenId" value="<?= $myid ?>"/>
											<div class="form-group">
															<label>Contanos un poco sobre vos </label>
															<textarea class="form-control" name="about" maxlength="500"><?php echo "$mydesc"; ?></textarea>
														</div>
											<button type="submit" class="btn btn-primary">Subir IdentiVideo</button>
										</form>
									</center>
								
									<div class="btn-list text-center pd-10">
										<?php
											if ($github) {
												print '<a href="https://github.com/' . "$github" . '" type="button" class="btn" style="border-radius: 10px;" data-bgcolor="black" data-color="#ffffff" target="_blank"><i class="fa fa-github"></i></a>';
											}

											if ($twitter) {
												print '<a href="https://twitter.com/' . "$twitter" . '" type="button" class="btn" style="border-radius: 10px;" data-bgcolor="#1da1f2" data-color="#ffffff" target="_blank"><i class="fa fa-twitter"></i></a>';
											}

											if ($instagram) {
												print '<a href="https://instagram.com/' . "$instagram" . '" type="button" class="btn" style="border-radius: 10px;" data-bgcolor="#f46f30" data-color="#ffffff" target="_blank"><i class="fa fa-instagram"></i></a>';
											}
										?>
									</div>

									<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
									<script src="chili-1.8.js"></script>
									<script src="jquery.expander.js"></script>
									
									<script>
										$(document).ready(function() {
											var opts = {
												collapseTimer: 4000,
												expandEffect: 'fadeIn',
												collapseEffect: 'fadeOut',
											};

											$.each(['beforeExpand', 'afterExpand', 'onCollapse'], function(i, callback) {
												opts[callback] = function(byUser) {
													var msg = '<div class="success">' + callback;

													if (callback === 'onCollapse') {
														msg += ' (' + (byUser ? 'user' : 'timer') + ')';
													}
													msg += '</div>';

													$(this).parent().parent().append(msg);
												};
											});

											$('dl.expander dd').eq(0).expander();
											$('dl.expander dd').slice(1).expander(opts);

											$('ul.expander li').expander({
												slicePoint: 50,
												widow: 2,
												expandSpeed: 0,
												userCollapseText: '[^]'
											});

											$('div.expander').expander();
										});
									</script>

									</div>
									<div class="col-12">
										<div class="contact-directory-box pd-0">
											<div class="timeline mb-30">
												<h5 class="text-center mb-20 h5 mb-0 pd-10">Proyectos </h5>
												<ul>
													<?php
														try {
															$TblExperiencia = $db->query("SELECT * FROM tbl_experience WHERE member_no = :myid ORDER BY id desc",array("myid"=>$myid));
															
															foreach ($TblExperiencia as $Experiencia) {
																$title = $Experiencia['title'];
																$supervisor = $Experiencia['supervisor'];
																$phone = $Experiencia['supervisor_phone'];
																$start_date = $Experiencia['start_date'];
																$end_date = $Experiencia['end_date'];
																$duties = $Experiencia['duties'];
																$view = $Experiencia['view'];
																$expid = $Experiencia['id'];
																$techno = $Experiencia['techno'];
																$link = $Experiencia['link'];
													?>

															<li>

																<div class="timeline-date">
																	<?php echo "$start_date"; ?> <b> al </b> <?php echo "$end_date"; ?>
																</div>

																<div class="timeline-desc card-box">
																	<div class="pd-20">
																		<h4 class="mb-10 h4"><?php echo "$title"; ?> </h4>
																		<?php
																		$tags = explode(",", $techno);

																		for ($i = 0; $i <= count($tags) - 1; $i++) {
																			$array = $tags[$i];
																			print '<span class="badge badge-pill" style="background-color: whitesmoke; margin: 5px; color: #01c0fe;">' . " $array" . '</a></span>';
																		}
																		?> <br><br>

																		<p class="text-center text-muted font-14"><?php echo $Experiencia['duties']; ?></p>
																		<p class="text-center text-muted font-14"><a href="<?php echo $Experiencia['link']; ?>"><?php echo $Experiencia['link']; ?></a></p>
																		<a href="../../controllers/employeecontroller.php?DropExperience=<?php echo $Experiencia['id']; ?>" onclick="return confirm('Seguro que quieres borrar esto?')" class="btn btn-danger btn-sm btn-inverse ">Borrar</a>																
																		<a data-toggle="modal" href="#edit<?php echo $Experiencia['id']; ?>" class="btn btn-primary btn-sm btn-inverse ">Editar</a>

																		<div id="edit<?=$Experiencia['id']; ?>" class="modal fade bs-example-modal-lg" tabindex="-1" data-width="550" style="display: none;" data-backdrop="static" data-keyboard="false" data-replace="true" role="dialog" aria-hidden="true">
																			<div class="modal-dialog modal-lg modal-dialog-centered">
																				<div class="modal-content">
																					<div class="modal-header">
																						<h4 class="modal-title" id="myLargeModalLabel">Editar <?="$title"; ?></h4>
																						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
																					</div>
																					<div class="modal-body">
																						<form action="../../controllers/employeecontroller.php?UpdateExperience" method="POST" autocomplete="off" enctype="multipart/form-data"> 
																							<div class="row gap-20">
																								<div class="col-sm-12 col-md-12">
																									<div class="form-group">
																										<label>Titulo del proyecto </label>
																										<input value="<?="$title"; ?>" class="form-control " placeholder="" type="text" name="jobtitle" required>
																									</div>
																								</div>
																								<div class="col-sm-6 col-md-6">
																									<div class="form-group">
																										<label>Fecha inicio </label>
																										<input class="form-control" placeholder="Ej: 01 Marzo 2019" type="date" name="startdate" value="<?php echo "$start_date" ?>">
																									</div>
																								</div>

																								<div class="col-sm-6 col-md-6">
																									<div class="form-group">
																										<label>Fecha finalizacion </label>
																										<input class="form-control" placeholder="Ej: 01 Junio 2019" type="date" name="enddate" value="<?php echo "$end_date" ?>">
																									</div>
																								</div>

																								<input type="hidden" name="expid" value="<?php echo "$expid"; ?>">

																								<div class="col-sm-6 col-md-12">
																									<div class="form-group">
																										Opciones de visibilidad:
																										<select class="custom-select" name="view">
																											<option value="<?php echo $view; ?>"><?php echo $view; ?></option>
																											<option value="mostrar">Mostrar</option>
																											<option value="ocultar">Ocultar</option>
																										</select>
																									</div>
																								</div>

																								<div class="col-sm-12 col-md-12">
																									<div class="form-group ">
																										<label>Habilidades / tecnologia aplicadas (5 máximo)</label>
																										<input type="text" class="form-control form-control-lg" data-role="tagsinput" name="techno" placeholder="Presiona ENTER para agregar" value="<?php echo $techno; ?>">
																									</div>
																								</div>

																								<div class="col-sm-12 col-md-12">
																									<div class="form-group ">
																										<label>Link</label>
																										<input type="text" class="form-control form-control-lg" name="link" placeholder="Ingresa link del proyecto" value="<?php echo $link; ?>">
																									</div>
																								</div>

																								<div class="col-sm-12 col-md-12">
																									<div class="form-group">
																										<label>Descripcion del proyecto</label>
																										<textarea class="form-control" name="duties"><?php echo "$duties"; ?> </textarea>
																									</div>
																								</div>
																							</div>
																						</div>

																						<div class="modal-footer text-center">
																							<button type="submit" class="btn btn-primary">Actualizar</button>
																							<button type="button" data-dismiss="modal" class="btn btn-primary btn-inverse">Cerrar</button>
																						</div>
																					</form>
																				</div>
																			</div>
																		</div>
																		<?php
																			if ($view == 'mostrar') {
																				$idv = $Experiencia["id"];
																				print '
																					<a href="#edit' . $idv . '	" data-toggle="modal" ><i class="icon-copy fa fa-eye" aria-hidden="true" name="view"></i></a>';
																			} else {
																				print '
																						<i class="icon-copy fa fa-eye-slash" aria-hidden="true"></i>';
																			}
																		?>
																	</div>

																</div>
															</li>
													<?php
														}
													} catch (PDOException $e) {
													}
													?>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>



							<div class="tab-pane fade" id="profile" role="tabpanel">
								<div class="">
									<div class="profile-setting">
										<form class="post-form-wrapper" action="../../controllers/employeecontroller.php?UpdateProfile" method="POST" autocomplete="off">
											<ul class="profile-edit-list row">
												<li class="weight-500 col-md-12">
													<h4 class=" h5 mb-20">Informacion</h4>
													<div class="form-group">
														<label>Nombre</label>
														<input name="fname" class="form-control form-control-lg" type="text" value="<?php echo "$myfname"; ?>">
													</div>
													<div class="form-group">
														<label>Apellido</label>
														<input name="lname" class="form-control form-control-lg" type="text" value="<?php echo "$mylname"; ?>">
													</div>
													<!--<div class="form-group">
														<label>Ocupacion</label>
														<select name="title" required class="selectpicker show-tick form-control" data-live-search="true">
															<option disabled value="">Seleccionar</option>
															<option <?php if ($mytitle == "Front-End Developer") {
																		print ' selected ';
																	} ?> value="Front-End Developer">Front-End Developer</option>

															<option <?php if ($mytitle == "Back-End Developer") {
																		print ' selected ';
																	} ?> value="Back-End Developer">Back-End Developer</option>

															<option <?php if ($mytitle == "Full Stack Web Developer") {
																		print ' selected ';
																	} ?> value="Full Web Stack Developer">Full Stack Web Developer</option>

															<option <?php if ($mytitle == "Mobile Developer") {
																		print ' selected ';
																	} ?> value="Mobile Developer">Mobile Developer</option>

															<option <?php if ($mytitle == "SalesForce Developer") {
																		print ' selected ';
																	} ?> value="SalesForce Developer">SalesForce Developer</option>

															<option <?php if ($mytitle == "DevOps Engineer") {
																		print ' selected ';
																	} ?> value="DevOps Engineer">DevOps Engineer</option>

															<option <?php if ($mytitle == "QA Engineer") {
																		print ' selected ';
																	} ?> value="QA Engineer">QA Engineerr</option>

															<option <?php if ($mytitle == "IOT Developer") {
																		print ' selected ';
																	} ?> value="IOT Developer">IOT Developer</option>

															<option <?php if ($mytitle == "Sysadmin") {
																		print ' selected ';
																	} ?> value="Sysadmin">Sysadmin</option>

															<option <?php if ($mytitle == "Desktop Developer") {
																		print ' selected ';
																	} ?> value="Desktop Developer">Desktop Developer</option>

															<option <?php if ($mytitle == "Technical Support") {
																		print ' selected ';
																	} ?> value="Technical Support">Technical Support</option>

															<option <?php if ($mytitle == "BI Developer") {
																		print ' selected ';
																	} ?> value="BI Developer">BI Developer</option>


															<option <?php if ($mytitle == "Data Analyst") {
																		print ' selected ';
																	} ?> value="Data Analyst">Data Analyst</option>


															<option <?php if ($mytitle == "Database Administrator (DBA)") {
																		print ' selected ';
																	} ?> value="Database Administrator (DBA)">Database Administrator (DBA)</option>


															<option <?php if ($mytitle == "Architect Developer") {
																		print ' selected ';
																	} ?> value="Architect Developer">Architect Developer</option>

															<option <?php if ($mytitle == "Cyber Security") {
																		print ' selected ';
																	} ?> value="Cyber Security">Cyber Security</option>


															<option <?php if ($mytitle == "Devops Analyst") {
																		print ' selected ';
																	} ?> value="Devops Analyst">Devops Analyst</option>


															<option <?php if ($mytitle == "Functional Analyst	") {
																		print ' selected ';
																	} ?> value="Functional Analyst	">Functional Analyst </option>

															<option <?php if ($mytitle == "Middleware Administrator") {
																		print ' selected ';
																	} ?> value="Middleware Administrator">Middleware Administrator</option>


															<option <?php if ($mytitle == "Project Manager") {
																		print ' selected ';
																	} ?> value="Project Manager">Project Manager</option>


															<option <?php if ($mytitle == "Web Designer	") {
																		print ' selected ';
																	} ?> value="Web Designer">Web Designer</option>


															<option <?php if ($mytitle == "UX Designer	") {
																		print ' selected ';
																	} ?> value="UX Designer">UX Designer</option>

															<option <?php if ($mytitle == "UI Designer	") {
																		print ' selected ';
																	} ?> value="UI Designer">UI Designer</option>

															<option <?php if ($mytitle == "UI & UX Designer") {
																		print ' selected ';
																	} ?> value="UI & UX Designer">UI & UX Designer</option>

															<option <?php if ($mytitle == "Business Analyst") {
																		print ' selected ';
																	} ?> value="Business Analyst">Business Analyst</option>

															<option <?php if ($mytitle == "Product Owner") {
																		print ' selected ';
																	} ?> value="Product Owner">Product Owner</option>

															<option <?php if ($mytitle == "Scrum Master") {
																		print ' selected ';
																	} ?> value="Scrum Master">Scrum Master</option>

															<option <?php if ($mytitle == "Team Leader") {
																		print ' selected ';
																	} ?> value="Team Leader">Team Leader</option>

															<option <?php if ($mytitle == "Base de Datos") {
																		print ' selected ';
																	} ?> value="Base de Datos">Base de Datos</option>

															<option <?php if ($mytitle == "Desarrollo de Software") {
																		print ' selected ';
																	} ?> value="Desarrollo de Software">Desarrollo de Software</option>

															<option <?php if ($mytitle == "Front End Web Developer") {
																		print ' selected ';
																	} ?> value="Front End Web Developer">Front End Web Developer</option>

															<option <?php if ($mytitle == "Software Engineer") {
																		print ' selected ';
																	} ?> value="Software Engineer">Software Engineer</option>

															<option <?php if ($mytitle == "Testing QA") {
																		print ' selected ';
																	} ?> value="Testing QA">Testing QA</option>

															<option <?php if ($mytitle == "Diseño Digital") {
																		print ' selected ';
																	} ?> value="Diseño Digital">Diseño Digital</option>

															<option <?php if ($mytitle == "CAD Drafter") {
																		print ' selected ';
																	} ?> value="CAD Drafter">CAD Drafter</option>

															<option <?php if ($mytitle == "Graphic Designer") {
																		print ' selected ';
																	} ?> value="Graphic Designer">Graphic Designer</option>

															<option <?php if ($mytitle == "Motion Designer") {
																		print ' selected ';
																	} ?> value="Motion Designer">Motion Designer</option>


															<option <?php if ($mytitle == "Infraestructura") {
																		print ' selected ';
																	} ?> value="Infraestructura">Infraestructura</option>


															<option <?php if ($mytitle == "Network Administrator") {
																		print ' selected ';
																	} ?> value="Network Administrator">Network Administrator</option>


															<option <?php if ($mytitle == "Marketing Digital") {
																		print ' selected ';
																	} ?> value="Marketing Digital">Marketing Digital</option>


															<option <?php if ($mytitle == "Community Manager") {
																		print ' selected ';
																	} ?> value="Community Manager">Community Manager</option>


															<option <?php if ($mytitle == "Content Manager") {
																		print ' selected ';
																	} ?> value="Content Manager">Content Manager</option>


															<option <?php if ($mytitle == "eCommerce Manager") {
																		print ' selected ';
																	} ?> value="eCommerce Manager">eCommerce Manager</option>


															<option <?php if ($mytitle == "Online Marketing") {
																		print ' selected ';
																	} ?> value="Online Marketing">Online Marketing</option>


															<option <?php if ($mytitle == "Analyst") {
																		print ' selected ';
																	} ?> value="Analyst">Analyst</option>


															<option <?php if ($mytitle == "SEO/SEM Analyst") {
																		print ' selected ';
																	} ?> value="SEO/SEM Analyst">SEO/SEM Analyst</option>
														</select>
													</div>-->
													<div class="form-group">
														<label>Tecnologias/habilidades</label>
														<input type="text" value="<?php echo "$ability"; ?>" class="form-control form-control-lg" data-role="tagsinput" name="ability" placeholder="Presiona ENTER para agregar">
														<small class="form-text text-muted">
															Recuerda presionar enter para agregar una tecnologia/habilidad nueva.
														</small>
													</div>
													<li class="weight-500 col-md-12">
														<div class="form-group">
															<label>Contanos un poco sobre vos </label>
															<textarea class="form-control" name="about" maxlength="500"><?php echo "$mydesc"; ?></textarea>
														</div>
													</li>
											</ul>

											<div class="col-lg-12 col-md-12 col-sm-12 mb-30">
												<div class="list-group">
													<a data-toggle="modal" href="#edit_contact" class="list-group-item list-group-item-action">Informacion de contacto</a>
													<div id="edit_contact" class="modal fade bs-example-modal-lg" tabindex="-1" data-width="550" style="display: none;" data-backdrop="static" data-keyboard="false" data-replace="true" role="dialog" aria-hidden="true">
														<div class="modal-dialog modal-lg modal-dialog-centered">
															<div class="modal-content">
																<div class="modal-header">
																	<h4 class="modal-title" id="myLargeModalLabel">Informacion de contacto</h4>
																	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
																</div>
																<div class="modal-body">
																	<div class="row gap-20">
																		<div class="col-sm-12 col-md-12">
																			<div class="form-group">
																				<label></label>
																				<div class="form-group">
																					<label>Email</label>
																					<input name="email" class="form-control form-control-lg" type="email" value="<?php echo "$myemail"; ?>">
																				</div>
																				<div class="form-group">
																					<label>Telefono</label>
																					<input name="phone" class="form-control form-control-lg" type="text" value="<?php echo "$myphone"; ?>">
																				</div>
																				<label>Usuario Github</label>
																				<div class="input-group custom">
																					<div class="input-group-prepend custom">
																						<div class="input-group-text" id="btnGroupAddon">@</div>
																					</div>
																					<input type="text" class="form-control" placeholder="Ingresa tu usuario de Github" aria-label="Input group example" aria-describedby="btnGroupAddon" name="github" value="<?php echo $github ?>">
																				</div>
																				<label>Usuario Twitter</label>
																				<div class="input-group custom">
																					<div class="input-group-prepend custom">
																						<div class="input-group-text" id="btnGroupAddon">@</div>
																					</div>
																					<input type="text" class="form-control" placeholder="Ingresa tu usuario de Twitter" aria-label="Input group example" aria-describedby="btnGroupAddon" name="twitter" value="<?php echo $twitter ?>">
																				</div>
																				<label>Usuario Instagram</label>
																				<div class="input-group custom">
																					<div class="input-group-prepend custom">
																						<div class="input-group-text" id="btnGroupAddon">@</div>
																					</div>
																					<input type="text" class="form-control" placeholder="Ingresa tu usuario de Instagram" aria-label="Input group example" aria-describedby="btnGroupAddon" name="instagram" value="<?php echo $instagram ?>">
																				</div>
																			</div>
																		</div>
																		<div class="col-md-12 modal-footer text-center">
																			<button type="button" class="col-md-12 btn btn-primary" data-dismiss="modal">Continuar</button>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>

													<br>
													<a data-toggle="modal" href="#edit_data" class="list-group-item list-group-item-action">Datos personales</a>
													<div id="edit_data" class="modal fade bs-example-modal-lg" tabindex="-1" data-width="550" style="display: none;" data-backdrop="static" data-keyboard="false" data-replace="true" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myLargeModalLabel">Datos personales</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					<div class="row gap-20">
						<div class="col-sm-12 col-md-12">
							<div class="form-group">
								<label></label>
								<div class="form-group">
									<label>Fecha de nacimiento</label>
									<div class="row gap-5">
										<div class="col-xs-3 col-sm-3">
											<select name="date" required class="selectpicker form-control" data-live-search="false">
												<option disabled value="">Dia</option>
												<?php
													$x = 1;

													while ($x <= 31) {
														if ($x < 10) {
															$x = "0$x";
															print '<option ';
															if ($mydate == $x) {
																print ' selected ';
															}
															print ' value="' . $x . '">' . $x . '</option>';
															} else {
																print '<option ';
																if ($mydate == $x) {
																	print ' selected ';
																}
																print ' value="' . $x . '">' . $x . '</option>';
															}
															$x++;
														}
													?>
											</select>
										</div><br><br><br>
										<div class="col-xs-5 col-sm-5">
											<select name="month" required class="selectpicker form-control" data-live-search="false">
												<option disabled value="">Mes</option>
												<?php
													$x = 1;
													while ($x <= 12) {
														if ($x < 10) {
															$x = "0$x";
															print '<option ';
															if ($mymonth == $x) {
																print ' selected ';
															}
															print ' value="' . $x . '">' . $x . '</option>';
														} else {
															print '<option ';
															if ($mymonth == $x) {
																print ' selected ';
															}
															print ' value="' . $x . '">' . $x . '</option>';
														}
														$x++;
													}
												?>
											</select>
										</div><br><br><br>

										<div class="col-xs-4 col-sm-4">
											<select name="year" class="selectpicker form-control" data-live-search="false">
												<option disabled value="">Año</option>
												<?php
													$x = date('Y');
													$yr = 60;
													$y2 = $x - $yr;
													while ($x > $y2) {
														print '<option ';
														if ($myyear == $x) {
															print ' selected ';
														}
														print ' value="' . $x . '">' . $x . '</option>';
														$x = $x - 1;
														}
												?>
											</select>
										</div>
									</div>
								</div>

								<div class="form-group">
									<label>Genero</label>
									<form>
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<select name="gender" required class="selectpicker show-tick form-control" data-live-search="false">
													<option disabled value="">Seleccionar</option>
														<option <?php if ($mygender == "Male") {
																	print ' selected ';
																} ?> value="Male">Masculino</option>
														<option <?php if ($mygender == "Female") {
																	print ' selected ';
																} ?>value="Female">Femenino</option>
														<option <?php if ($mygender == "Other") {
																	print ' selected ';
																} ?>value="Other">Otro</option>
													</select>
												</div>
											</div>
										</div>
								</div>

																				<div class="form-group">
																					<label>DNI</label>
																					<input name="dni" class="form-control form-control-lg" type="text" value="<?php echo "$dni"; ?>">
																				</div>

																				<div class="form-group">
																					<label>Pais</label>
																					<select name="country" required class="selectpicker show-tick form-control" data-live-search="true">
																						<option disabled value="">Seleccionar</option>
																						<?php
																							try {
																								$usuarios    = $db->query("SELECT * FROM tbl_countries ORDER BY country_name");
																								foreach ($usuarios as $usuario) {
																						?> 
																						<option 
																							<?php 
																								if ($mycountry == $usuario['country_name']) {
																									print ' selected ';
																								} 
																							?> 
																							value="<?php 
																								echo $usuario['country_name']; 
																								?>"><?php echo $usuario['country_name']; ?></option> 
																							<?php
																								}} catch (PDOException $e) {

																								}
																							?>
																					</select>
																				</div>
																				<div class="form-group">
																					<label>Calle</label>
																					<input name="street" class="form-control form-control-lg" type="text" value="<?php echo "$street"; ?>">
																				</div>

																				<div class="form-group">
																					<label>Ciudad</label>
																					<input name="city" class="form-control form-control-lg" type="text" value="<?php echo "$mycity"; ?>">
																				</div>

																				<div class="form-group">
																					<label>Codigo postal</label>
																					<input name="zip" class="form-control form-control-lg" type="text" value="<?php echo "$myzip"; ?>">
																				</div>
																			</div>
																		</div>
																		<div class="col-md-12 modal-footer text-center">
																			<button type="button" class="col-md-12 btn btn-primary" data-dismiss="modal">Continuar</button>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="form-group mb-0">
												<button type="submit" class="btn btn-primary col-md-12">Actualizar</button>
											</div>
										</form>
									</div>
								</div>
							</div>

							<div class="tab-pane fade" id="experiencia" role="tabpanel">
								<div class="timeline mb-30">
									<h5 class="text-center mb-20 h5 mb-0 pd-10">Proyectos</h5>
									<center><b style="color:#990000">En esta sección podrás incluir los proyectos en los que has trabajado, recuerda que los proyectos propios tambien cuentan, no olvides cargarlos! (Además esto te posiciona mucho mejor con las empresas)</b></center>
									<form action="../../controllers/employeecontroller.php?AddExperience" method="POST" autocomplete="off" enctype="multipart/form-data"> 
										<div class="row gap-20">
											<div class="col-sm-12 col-md-12">
												<div class="form-group">
													<label>Titulo del proyecto </label>
													<input class="form-control" placeholder="Ingresa el titulo del proyecto" type="text" name="jobtitle" required>
												</div>
											</div>
											<div class="col-sm-12 col-md-12">
												<div class="form-group">
													<label>Descripción</label>
													<p class="text-muted">Este campo será visible en la vista detallada del proyecto. Describe los detalles principales del proyecto de forma clara y concisa.</p>
													<textarea class="form-control" name="duties"> </textarea>
												</div>
											</div>
											<div class="col-sm-12 col-md-12">
												<div class="form-group ">
													<label>Habilidades/tecnologias aplicadas (5 máximo)</label>
													<input type="text" class="form-control form-control-lg" data-role="tagsinput" name="techno" placeholder="Presiona ENTER para agregar">
												</div>
											</div>
											<div class="col-sm-12 col-md-12">
												<div class="form-group">
													<label>Link del proyecto (opcional)</label>
													<input class="form-control" placeholder="Ingresa link del proyecto" type="text" name="link" value="https://">
												</div>
											</div>
											<div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label>Fecha Inicio (opcional)</label>
													<input class="form-control" placeholder="Ej: 01 Marzo 2019" type="date" name="startdate">
												</div>
											</div>
											<div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label>Fecha Finalizacion (opcional)</label>
													<input class="form-control" placeholder="Ej: 01 Junio 2019" type="date" name="enddate">
												</div>
											</div>
											<div class="col-sm-6 col-md-12">
												<div class="form-group">
													<select class="custom-select" name="view" required="">
														<option value="">Opciones de visibilidad</option>
														<option value="mostrar">Mostrar</option>
														<option value="ocultar">Ocultar</option>
													</select>
												</div>
											</div>
										</div>
										<button type="submit" class="btn btn-primary col-md-12">Añadir</button>
									</form>
								</div>
							</div>
						</div>
					</div>

				</div>

			</div>
		</div>
	</div>
	</div>
	</div>
	</div>
	</div> 
	</div>
	<!-- js -->
	
	<!-- <script src="../../../public/complements/v1/vendors/scripts/core.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/script.min.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/process.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/layout-settings.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/jquery.dataTables.min.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/dataTables.bootstrap4.min.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/dataTables.responsive.min.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/responsive.bootstrap4.min.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/dashboard.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/core.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/script.min.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/process.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/layout-settings.js"></script>
	<script src="../../../public/complements/v1/src/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
	<script src="../../../public/complements/v1/src/plugins/switchery/switchery.min.js"></script>

	<script src="../../../public/complements/v1/src/plugins/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/advanced-components.js"></script>
	<script src="../../../public/complements/v1/src/plugins/sweetalert2/sweetalert2.all.js"></script>
	<script src="../../../public/complements/v1/src/plugins/sweetalert2/sweet-alert.init.js"></script>
 	-->

	<script src="../../../public/complements/v1/vendors/scripts/core.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/script.min.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/process.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/layout-settings.js"></script>
	<script src="../../../public/complements/v1/src/plugins/apexcharts/apexcharts.min.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/jquery.dataTables.min.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/dataTables.bootstrap4.min.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/dataTables.responsive.min.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/responsive.bootstrap4.min.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/dashboard.js"></script>

	<script src="../../../public/complements/v1/src/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
	<script src="../../../public/complements/v1/src/plugins/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>

	<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<form action="../../controllers/employeecontroller.php?NewDp" method="POST" enctype="multipart/form-data">
					<center>
						<div>
							<div id="resultado" class=""><img id="imgSalida" style="width: 200px; height: 200px; border-radius: 200px;" /></div>
						</div>
						<div class="form-group col-md-12 ">
							<center><label>Seleccionar imagen</label></center>
							<label for="file-input">
								<img src="../../../public/img/identikit/mas.png" title="Seleccionar imagen" style="width: 50px; height: 50px">
							</label>
							<input accept="image/*" type="file" name="image" id="file-input" required hidden="" />
						</div>
						<div class="modal-footer">
							<input type="submit" value="Actualizar" class="btn btn-primary">
							<button type="button" class="btn btn-default" data-dismiss="modal">cerrar</button>
						</div>
					</center>
				</form>
			</div>
		</div>
	</div>
	
	<div class="modal fade" id="uploadvideocv" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
        			<h5 class="modal-title">Sube tu IDentiVideo</h5>
        			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          				<span aria-hidden="true">&times;</span>
 		       		</button>
      			</div>

				<div class="modal-body">
					<form action="../../controllers/employeecontroller.php" method="post" enctype="multipart/form-data">
						<center>
						<div class="form-group">
						    <div class="video" style="width: 40%;">
						<?php 
							$getVideoCv  = $db->query("SELECT path_video_cv FROM `tbl_users_data` where member_no=:member_no", array("member_no"=>$myid));
							echo '<video class="video__player" src="../../../'.$getVideoCv[0]['path_video_cv'].'"></video';
							$getInfoUser = $db->query("SELECT first_name, last_name, about from `tbl_users` where member_no=:member_no",array("member_no"=>$myid));
							$fullname    = $getInfoUser[0]["first_name"] . " " . $getInfoUser[0]["last_name"]; 
						?>
 
						
						
						</div>
						
					</div><br>
                            <div class="custom-file">
                                <input type="file" name="fileToUpload" id="fileToUpload" class="custom-file-input">
                                <input type="hidden" name="hiddenId" id="hiddenId" value="<?= $myid ?>" />
                                <label class="custom-file-label">Seleccionar IDentiVideo</label>
                            </div>
                        </div>
						<center>
							<button type="submit" class="btn btn-primary">Subir</button>
						</center>
						
                        
	
					</form>
				</div>

			</div>
		</div>
	</div>
										
	<div class="modal fade" id="videocv" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="app__videos">
					<!-- video starts -->
					<div class="video">
						<?php 
							$getVideoCv  = $db->query("SELECT path_video_cv FROM `tbl_users_data` where member_no=:member_no", array("member_no"=>$myid));
							echo '<video class="video__player" src="../../../'.$getVideoCv[0]['path_video_cv'].'"></video';
							$getInfoUser = $db->query("SELECT first_name, last_name, about from `tbl_users` where member_no=:member_no",array("member_no"=>$myid));
							$fullname    = $getInfoUser[0]["first_name"] . " " . $getInfoUser[0]["last_name"]; 
						?>
 
						<!-- sidebar -->
						<div class="videoSidebar">
						
						</div>
						<!-- footer -->
						<div class="videoFooter">
						<div class="videoFooter__text">
							<h3><?= $fullname ?></h3>
							<p class="videoFooter__description">
							<?= $getInfoUser[0]["about"]; ?>
							</p>            
						</div>
						
						</div>
					</div>
					<!-- video ends -->
				</div>
			</div>
			<!-- video ends -->
		</div>
	</div>

	<link rel="stylesheet" href="../../../public/css/videocv.css" />

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />

	<script>
      const videos = document.querySelectorAll('video');

      for (const video of videos) {
        video.addEventListener('click', function () {
          console.log('clicked');
          if (video.paused) {
            video.play();
          } else {
            video.pause();
          }
        });
      }
    </script>

	
</body>

</html>