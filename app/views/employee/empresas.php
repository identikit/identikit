<!DOCTYPE html>

<?php
	require '../../../config/settings.php';
	require '../../../config/check-login.php';
	require '../employee/constants/check-login.php';

	require_once("../../../db/db.php");
	$db = new DbPDO();

	if ($myrole == "employee") {
		require '../../../config/check-login.php';
	} else {
		require '../employee/constants2/check-login1.php';
	}

	if ($user_online == "true") {
		if ($myrole == "employee" or "employer") {
		} else {
			header("location:../login.php");
		}
	} else {
		header("location:../login.php");
	}
?>

<html>

<head>
	<!-- Basic Page Info -->
	<meta charset="utf-8">
	<title>IDentiKIT - Explorar empresas</title>

	<link rel="apple-touch-icon"      sizes="180x180"    href="../../../public/img/identikit/logo.png">
	<link rel="icon" type="image/png" sizes="32x32"      href="../../../public/img/identikit/logo.png">
	<link rel="icon" type="image/png" sizes="16x16"      href="../../../public/img/identikit/logo.png">
 
	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/datatables/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/style.css">

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-K2NHRZT4R7"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'G-K2NHRZT4R7');
	</script>

	<!-- Google Tag Manager -->
	<script>
		(function(w, d, s, l, i) {
			w[l] = w[l] || [];
			w[l].push({
				'gtm.start': new Date().getTime(),
				event: 'gtm.js'
			});
			var f = d.getElementsByTagName(s)[0],
				j = d.createElement(s),
				dl = l != 'dataLayer' ? '&l=' + l : '';
			j.async = true;
			j.src =
				'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
			f.parentNode.insertBefore(j, f);
		})(window, document, 'script', 'dataLayer', 'GTM-5QGSH5J');
	</script>
	<!-- End Google Tag Manager -->

</head>

<body>



	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QGSH5J" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<?php include 'layouts/Header.php';?>
	<?php include 'layouts/Sidebar-menu.php';?>

	

	<div class="mobile-menu-overlay"></div>

	<div class="main-container">
		<div class="pd-ltr-20 xs-pd-20-10">
			<div class="min-height-200px">
				<div class="col-sm-12 col-md-12 col-lg-4 mb-30">
				</div>
				<div class="page-header">
					<div class="contact-directory-list">
						<ul class="row">
							<?php
							
								try {
									$usuarios = $db->query("SELECT * FROM tbl_users WHERE role = 'employer' ORDER BY RAND()");

									foreach ($usuarios as $usuario) {
										$complogo = $usuario['avatar'];
							?>
							<li class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
								<div class="contact-directory-box">
									<div class="contact-dire-info text-center">
										<div class="contact-avatar">
											<span>
												<?php
													if ($complogo == null) {
														print '<center><img class="autofit2" alt="image"  src="../../../public/img/identikit/logo.png"/></center>';
													} else {
														echo '<center><img class="autofit2" alt="image"  src="data:image/jpeg;base64,' . base64_encode($complogo) . '"/></center>';
													}
												?>
											</span>
										</div>
										<div class="contact-name">
											<h4><?= strip_tags($usuario['first_name']); ?> <?= strip_tags($usuario['last_name']); ?></h4>
											<div class="work text-success">💡 <?= $usuario['title'] ?></div>
											<div class="text-success" style="margin-top: 6px;">📍 <?= $usuario['city']; ?></i></div>
										</div>
										<div class="profile-sort-desc">
											<?= strip_tags(substr($usuario['about'], 0, 90)); ?>...
										</div>
									</div>
									<div class="view-contact">
										<a href="empresa.php?ref=<?=$usuario['member_no']; ?>">Ver empresa</a>
									</div>
								</div>
							</li>
							<?php
							}} catch (PDOException $e) {
							
							}
							?>

						</ul>
					</div>
				</div>


			</div>
		</div>

	</div>
	</div>
	<!-- js -->
	<script src="../../../public/complements/v1/vendors/scripts/core.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/script.min.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/process.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/layout-settings.js"></script>
</body>

</html>