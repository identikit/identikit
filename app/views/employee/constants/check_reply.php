<?php

    if (isset($_GET['r'])) {
        $error_code = $_GET['r'];

        try { 
            $alertas = $db->query("SELECT * FROM tbl_alerts WHERE code = :errorcode",array("errorcode" => $error_code));
            foreach($alertas as $alerta)
            {
                $description = $alerta['description'];
                $type = $alerta['type'];
                print '
                <div class="alert alert-dismissible fade show alert-'.$type.' " role="alert">
                '.$description.'
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                ';
            }	  
	}catch(PDOException $e){

    }
}

?>