<?php 

	require '../employee/constants/check-newlogin.php';

	require_once("../../../db/db.php");
	$db = new DbPDO();

	// if ($user_online == "true") {
	// 	if ($myrole == "employee") {
	// 		}else{
	// 			header("location:../employer/index.php");	}
	// } else {
	// 	header("location:../principal.php");	
	// }	

	class video {
		public $id; 
		public $path;
		public $titulo;
		public $descripcion;
		public $pathImage;
	}

	if($_GET["sectiontype"]){

		$sectiontype = $_GET["sectiontype"];

		switch($sectiontype){
			case "capacitaciones":
				$idmodule = 1;
				break;
			case "entrenamientos":
				$idmodule = 2;
				break;
			case "apps":
				$idmodule = 3;
				break;
			case "habilidad":
				$idmodule = 4;
				break;
		}
		$idgrupo   = $_SESSION["idgrupo"];
		$getVideos =  $db->query("SELECT * FROM tbl_cursos where idmodulo=".$idmodule." AND (idgrupo=".$idgrupo." or idgrupo = 0) order by idcontinuo limit 4");
		$videos    = [];

		if(isset($_GET["id"])){
			$currentId = $_GET["id"];
			$getNextVideos = $db->query("SELECT * FROM `tbl_cursos` where idmodulo=".$idmodule." AND (idgrupo=".$idgrupo." or idgrupo = 0) and idcontinuo >= ".$currentId." limit 4");

			$i     = 0;
			foreach($getNextVideos as $video){
				$tempobjectvideo = new video();
				$tempobjectvideo->id          = $video["id"];
				$tempobjectvideo->path        = $video["path"];
				$tempobjectvideo->titulo      = $video["titulo"];
				$tempobjectvideo->descripcion = $video["descripcion"];
				$tempobjectvideo->pathImage   = $video["pathImage"];
				$videos[$i] = $tempobjectvideo; 
				unset($tempobjectvideo);
				$i++;
			}
		} else { 
			
			$i     = 0;
			foreach($getVideos as $video){
				$tempobjectvideo = new video();
				$tempobjectvideo->id          = $video["id"];
				$tempobjectvideo->path        = $video["path"];
				$tempobjectvideo->titulo      = $video["titulo"];
				$tempobjectvideo->descripcion = $video["descripcion"];
				$tempobjectvideo->pathImage   = $video["pathImage"];
				$videos[$i] = $tempobjectvideo; 
				unset($tempobjectvideo);
				$i++;
			}
		}
	} 

?>	

<?php 
	$path   = "";
	$titulo = "";
	$descripcion = "";
	if(count($videos) > 0){
		$path    = $videos[0]->path;
		$titulo  = $videos[0]->titulo;
		$descripcion  = $videos[0]->descripcion;
		$idvideo = $videos[0]->id;
	} else {
		$idvideo = "0";
	}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">

	<title><?= $titulo ?> </title>

	<link rel="apple-touch-icon" sizes="180x180"    href="../../../public/img/identikit/logo.png">
	<link rel="icon" type="image/png" sizes="32x32" href="../../../public/img/identikit/logo.png">
	<link rel="icon" type="image/png" sizes="16x16" href="../../../public/img/identikit/logo.png">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/datatables/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/style.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/plyr/dist/plyr.css">


	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-K2NHRZT4R7"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'G-K2NHRZT4R7');
	</script>

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-5QGSH5J');</script>
	<!-- End Google Tag Manager -->
	<style>
		a.heart_mark {
			width: 40px;
			height: 40px;
			-webkit-border-radius: 5px;
			-moz-border-radius: 5px;
			border-radius: 5px;
			color: #00c1ff;
			font-size: 14px;
			line-height: 40px;
			text-align: center;
			display: inline-block;
			background: #EFFDF5;
			margin-right: 15px;
		}

		a.heart_mark:hover {
			background: #ff0047;
		}

		i.ti-heart {
			color: #00c1ff;
		}

		a.heart_mark:hover>i.ti-heart {
			color: white;
		}
	</style>
</head>
<body>
		<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QGSH5J"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<?php include 'layouts/Header.php';?>
	<?php include 'layouts/Sidebar-menu.php';?>

	<div class="mobile-menu-overlay"></div>

	<div class="main-container">
		<div class="pd-ltr-20 xs-pd-20-10">
			<div class="min-height-200px">
				
				<div class="product-wrap">
					<div class="product-detail-wrap mb-30">
						<div class="row">
							<div class="col-lg-6 col-md-12 col-sm-12">
								<div class="product-slider slider-arrow">

								<div data-type="youtube" data-video-id="<?= $path ?>"></div><br>
								<h5><?= $titulo?></h5>
								<p class="text-muted"><?= $descripcion ?> </p>
								</div>
								
							</div>
							<div class="col-lg-6 col-md-12 col-sm-12">
								<div class="product-detail-desc pd-20 card-box height-100-p" id="app">
									<h4 class="mb-20 pt-20">Comentarios / Preguntas</h4>
									
									<div id="chart5"></div>
										<div class="chat-box">
											<div class="chat-desc customscroll">
												<ul>
													<li class="clearfix" v-for="mensaje in mensajes">
														<span class="chat-img">
															<div v-if="mensaje.path != ''">
																<img :src="'../../../' + mensaje.path" alt="" class="light-logo" width="60">
															</div>
															<div v-else>
																<img  src="../../../public/img/identikit/logo.png" title="' . $myfname . '" alt="image"  /></center>
															</div>
														</span>
														<div class="chat-body clearfix">
															<p>{{mensaje.pregunta}}</p>
															<div class="chat_time">{{mensaje.created_at}}</div>
														</div> 
													</li>
												</ul>
											</div>

											<div class="chat-footer" style="display: <?= $ShowChatFooter ?>">
												<!-- <div class="file-upload"><a href="#"><i class="fa fa-paperclip"></i></a></div> -->
													
												<div class="chat_text_area">
													<textarea v-model="pregunta" placeholder="Escribe tu pregunta" id="mensaje" rows="10"></textarea>
												</div>
												<div class="chat_send">
													<button class="btn btn-link" @click="mensaje()"><i class="icon-copy ion-paper-airplane"></i></button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<h4 class="mb-20">Próximo</h4>
					<div class="product-list">
						<ul class="row">

							<?php 
								foreach($videos as $index => $video){										
									if ($index < 1) continue;
							?>
							
							<li class="col-lg-4 col-md-6 col-sm-12">
								<a href="module-detail.php?sectiontype=<?=$sectiontype?>&id=<?= $video->id?>">
								<div class="product-box">
									<div class="producct-img"><img src="<?= $video->pathImage?>" alt="" style="width: 100%; height: auto !important;"></div>
									<div class="product-caption">
										<h4><a href="module-detail.php?sectiontype=<?=$sectiontype?>&id=<?= $video->id?>"><?= $video->titulo?></a></h4>
										<div class="price">
											<?= substr($video->descripcion, 0, -25); ?>...
										</div>
										
									</div>
								</div></a>
							</li>
							
							<?php 
								}
							?>
						</ul>
					</div>
				</div>
			</div>
			
		</div>
	</div>

<!-- js -->
	<script src="../../../public/complements/v1/vendors/scripts/core.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/script.min.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/process.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/layout-settings.js"></script>
	<script src="../../../public/complements/v1/src/plugins/apexcharts/apexcharts.min.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/jquery.dataTables.min.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/dataTables.bootstrap4.min.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/dataTables.responsive.min.js"></script>
	<script src="../../../public/complements/v1/src/plugins/datatables/js/responsive.bootstrap4.min.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/dashboard.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/vue@2"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.27.2/axios.min.js"></script>

    <script>

		new Vue({
			el: '#app',
				data: {
					mensajes: [],
					pregunta: '',
				},

			mounted(){
				this.loadmensajes();
			}, 	
			
			methods: {
				loadmensajes(){ 
					let formData = new FormData();
					let newidVideo =  <?= $idvideo ?>;

					formData.append('idvideo', newidVideo);
					var contact = {};

					formData.forEach(function(value, key){
						contact[key] = value;
					});

					var that = this;

					axios({
						method:   'POST',
						url:      '../../controllers/employeecontroller.php?action=' + newidVideo,
						// data:     formData,
						// config:   { headers: {'Content-Type': 'application/json' }}
					})
					.then(function (response) {
						that.mensajes = response.data;
						that.scrolldown();
					})
					.catch(function (error) {
						console.log('Error: ' + error);
					}); 
				},
				mensaje: function(){
					let formData   = new FormData();
					let newidVideo =  <?= $idvideo ?>;

					formData.append('pregunta', this.pregunta);
					formData.append('idvideo', newidVideo);

					var that = this;

					axios({
						method:   'POST',
						url:      '../../controllers/employeecontroller.php?idvideo=' + newidVideo + '&pregunta=' + this.pregunta,
						// data:     formData,
						// config:   { headers: {'Content-Type': 'application/json' }}
					})
					.then(function (response) {
						that.pregunta = ''; 
						that.loadmensajes();
					})
					.catch(function (error) {
						console.log('Error: ' + error);
					})

				}, 
				scrolldown: function(){
					var objDiv          = document.getElementById("mCSB_3_dragger_vertical");
					var objDivfather    = document.getElementById("mCSB_3_container");
					objDiv.style.top    = "unset";
					objDiv.style.bottom = "0px";
					let scrollheight = -objDivfather.scrollHeight + 100 + 'px';
					objDivfather.style.top = scrollheight;
				}
			}
		});
	</script>									

	<script type="text/javascript">		
		$(".heart_mark").click(function() {
			var valor = $(this)[0].getAttribute("value");

			$.ajax({
				type: 'GET',
				url: '../../controllers/helpercontroller.php/add_like.php?AddLike',
				data: {'job_id': valor}
			}).done(function(){
				window.location.reload();
			});
		});
	</script>

	<script src="../../../public/complements/v1/src/plugins/plyr/dist/plyr.js"></script>
	<script src="https://cdn.shr.one/1.0.1/shr.js"></script>
	<script>
		plyr.setup({
			tooltips: {
				controls: !0
			},
		});
	</script>

</body>
</html>