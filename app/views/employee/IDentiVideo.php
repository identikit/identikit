<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>IDentiKIT- IDentiVideo </title>
    <!-- Basic Page Info -->
	<meta charset="utf-8">

	<link rel="apple-touch-icon"      sizes="180x180"  href="../../../public/img/identikit/logo.png">
	<link rel="icon" type="image/png" sizes="32x32"    href="../../../public/img/identikit/logo.png">
	<link rel="icon" type="image/png" sizes="16x16"    href="../../../public/img/identikit/logo.png">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/datatables/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/style.css">


	<meta property="og:image"            content="https://identikit.app/logowebog.png" />
	<meta property="og:image:secure_url" content="https://identikit.app/logowebog.png" />
	<meta property="og:image:type"       content="image/png" />
	<meta property="og:image:width"      content="300" />
	<meta property="og:image:height"     content="300" />
	<meta property="og:image:alt"        content="IDentiKIT - Tu primera experiencia laboral" />
	<meta property="og:description"      content="😎 Encuentra el mejor talento tech joven en IDentiKIT a un click de distancia, fácil, rápido y seguro." />

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-K2NHRZT4R7"></script>

	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'G-K2NHRZT4R7');
	</script>

	<!-- Google Tag Manager -->
	<script>
		(function(w, d, s, l, i) {
			w[l] = w[l] || [];
			w[l].push({
				'gtm.start': new Date().getTime(),
				event: 'gtm.js'
			});
			var f = d.getElementsByTagName(s)[0],
				j = d.createElement(s),
				dl = l != 'dataLayer' ? '&l=' + l : '';
			j.async = true;
			j.src =
				'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
			f.parentNode.insertBefore(j, f);
		})(window, document, 'script', 'dataLayer', 'GTM-5QGSH5J');
	</script>

    <?php
        include '../../../config/settings.php';
        include 'constants/check-login.php';
        require_once("../../../db/db.php");
		    $db = new DbPDO();
    ?>
    
    <style>
      /* fallback */
      @font-face {
        font-family: 'Material Icons';
        font-style: normal;
        font-weight: 400;
        src: url(https://fonts.gstatic.com/s/materialicons/v103/flUhRq6tzZclQEJ-Vdg-IuiaDsNc.woff2) format('woff2');
      }

      .material-icons {
        font-family: 'Material Icons';
        font-weight: normal;
        font-style: normal;v
        font-size: 24px;
        line-height: 1;
        letter-spacing: normal;
        text-transform: none;
        display: inline-block;
        white-space: nowrap;
        word-wrap: normal;
        direction: ltr;
        -webkit-font-feature-settings: 'liga';
        -webkit-font-smoothing: antialiased;
      }
      
			a.heart_mark {
				width: 40px;
				height: 40px;
				-webkit-border-radius: 5px;
				-moz-border-radius: 5px;
				border-radius: 5px;
				color: #00c1ff;
				font-size: 14px;
				line-height: 40px;
				text-align: center;
				display: inline-block;
				background: #EFFDF5;
				margin-right: 15px;
			}

			a.heart_mark:hover {
				background: #ff0047;
			}

			a.heart_mark:hover>i.ti-heart {
				color: rgb(0, 193, 255);
			} 
				
    </style>

    <style>
      * {
        margin: 0;
        box-sizing: border-box;
      }

      html {
        scroll-snap-type: y mandatory;
      }

      body {
        color: white;
        background-color: black;
        height: 100vh;
        display: grid;
        place-items: center;
      }

      .app__videos {
        position: relative;
        height: 650px;
        background-color: white;
        overflow: scroll;
        width: 100%;
        max-width: 400px;
        scroll-snap-type: y mandatory;
        border-radius: 20px;
      }

      .app__videos::-webkit-scrollbar {
        display: none;
      }

      .app__videos {
        -ms-overflow-style: none;
        scrollbar-width: none;
      }

      .video {
        position: relative;
        height: 100%;
        width: 100%;
        background-color: white;
        scroll-snap-align: start;
      }

      .video__player {
        object-fit: cover;
        width: 100%;
        height: 100%;
      }

      .videoSidebar {
        position: absolute;
        top: 74%;
        right: 10px;
      }

      .videoSidebar .material-icons {
        font-size: 28px;
        cursor: pointer;
      }

      .videoSidebar__button {
        padding: 20px;
        text-align: center;
      }

      .videoFooter {
        position: relative;
        bottom: 190px;
        margin-left: 20px;
        color: white;
        display: flex;
      }

      @keyframes spinTheRecord {
        from {
          transform: rotate(0deg);
        }
        to {
          transform: rotate(360deg);
        }
      }

      .videoFooter__record {
        /* animation: spinTheRecord infinite 5s linear; */
        height: 50px;
        /* filter: invert(1); */
        position: absolute;
        bottom: 15px;
        right: 20px;
      }

      .videoFooter__text {
        flex: 1;
        padding: 0px 0px 20px 0px;
      }

      .videoFooter__text h3 {
        padding-bottom: 20px;
        font-size: 20px;
        color:white;
      }

      .videoFooter__icon {
        position: absolute;
      }

      .videoFooter__ticker {
        width: 400px;
        display: flex;
        align-items: center;
      }

      .videoFooter__ticker marquee {
        height: fit-content;
        margin-left: 30px;
        width: 60%;
      }

      .videoFooter__description {
        padding-bottom: 20px;
        margin-right: 100px;
      }

      @media (max-width: 425px) {
        .app__videos {
          width: 100%;
          max-width: 100%;
          border-radius: 0;
        }
      }



    </style>

  </head>
  <body>

     	
	<?php include 'layouts/Header.php';?>
	<?php include 'layouts/Sidebar-menu.php';?>

  <div id="app">
    <div class="app__videos">
      <!-- video starts-->
      <div class="video" v-for="VideoCv in VideoCvs">
        <video class="video__player" :src="'../../../' + VideoCv.Path_video_cv + ''"></video>   
        <div class="videoFooter">
          <div class="videoFooter__text" v-if='!VideoCv.heart'>
            <a class="heart_mark float-right" v-on:click="clickHeart(VideoCv.member_no)"
            style="cursor:pointer">
            <i class="ti-heart"></i></a>
              <h3>{{VideoCv.first_name}}  {{VideoCv.last_name}}</h3>
              <p class="videoFooter__description">
              {{ VideoCv.About.length < 0 ? VideoCv :  VideoCv.About.substring(0,120) + "..."}}  
            </div>

          <div class="videoFooter__text" v-else>
            <a class="heart_mark float-right" 
            style="background:#ff0047; color:white; cursor:pointer" v-on:click="clickHeart(VideoCv.member_no)">
            <i class="ti-heart" id="heart"></i></a>
            
            <h3>{{VideoCv.first_name}} {{VideoCv.last_name}}</h3>
            <p class="videoFooter__description">
              {{ VideoCv.About.length < 0 ? VideoCv :  VideoCv.About.substring(0,120) + "..."}} 
            </p>            
          </div>

        </div>        
      </div> 
    </div>
  </div>
  <script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script>
  <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

  <script>
        var url_api = '../../api/employee/api-identikit.php';
        var app = new Vue({
                el: '#app',
                data () {
                    return {
                        sucursales:         [],
                        VideoCvs:           null,
                        isPlaying:          false,
                        session: {
                          currentSession:   "<?= $_SESSION["myid"] ?>",
                          Owner_videoCv:    ""  
                        },
                        txt:                ""
                    }    
                },                
                mounted() {
                    this.loadVideoCv().then((returnVal) => {
                      this.ClickVideo();
                    });              
                },
                methods: {
                  loadVideoCv: async function(){
                        return axios.get(url_api + "?action=VideoCvs&memberNo=<?= $_SESSION["myid"] ?>")
                      .then(
                          response => (
                              console.log(response.data.VideoCvs),
                              this.VideoCvs = response.data.VideoCvs
                          ))  
                    },

                    clickHeart: function(event){
                      this.session.Owner_videoCv = event;
                      var formData = this.toFormData(this.session);
                      return axios
                      .post(url_api + "?action=UpdateHeart", formData)
                      .then(response => (
                        this.loadVideoCv()
                      ))  
                    },

                    toFormData: function (obj) {
                        var form_data = new FormData();
                        for (var key in obj) {
                            form_data.append(key, obj[key]);
                        }
                        return form_data;
                    },
                    
                    ClickStartVideoCv(event){
                      event.play();
                    },

                    play: function(event){
                      this.$refs.myvideo.play();
                    },

                    ClickVideo: function(){
                      const videos = document.querySelectorAll('video');

                      for (const video of videos) {
                        video.addEventListener('click', function () {

                          if (video.paused) {
                              const lastvideos = document.querySelectorAll('video');

                              for (const lastvideo of lastvideos){
                                lastvideo.pause();
                              }
                            video.play();
                            
                          } else {
                            const lastvideos = document.querySelectorAll('video');
                            
                            for (const lastvideo of lastvideos){
                              lastvideo.pause();
                            }
                          }
                        });
                      }
                  },
                }
        })
    </script>



  

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="../../../public/complements/v1/vendors/scripts/core.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/script.min.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/process.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/layout-settings.js"></script>
  </body>
</html>
