<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-5F5WB8FMJC"></script>
<script>
	window.dataLayer = window.dataLayer || [];
	   
	   function gtag() {
	   	dataLayer.push(arguments);
	   }
	   gtag('js', new Date());
	   
	   gtag('config', 'G-5F5WB8FMJC');
	
</script>
<!DOCTYPE html>
<html>
	<?php
		require 'constants/check-newlogin.php';
		require_once("../../../db/db.php");
		$db = new DbPDO();
		
		if ($user_online == "true") {
			if ($myrole == "employee") {
			} else {
				header("location:../");
			}
		} else {
			header("location:../");
		}
		
      ?>
	<!DOCTYPE html>
	<html>
	<head>
		<!-- Basic Page Info -->
		<meta charset="utf-8">
		<title>IDentiKIT - Mi perfil</title>
		<link rel="apple-touch-icon" sizes="180x180" href="../../../public/img/identikit/logo.png">
		<link rel="icon" type="image/png" sizes="32x32" href="../../../public/img/identikit/logo.png">
		<link rel="icon" type="image/png" sizes="16x16" href="../../../public/img/identikit/logo.png">
		<!-- Mobile Specific Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<!-- Google Font -->
		<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
		<!-- CSS -->
		<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/core.css">
		<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/icon-font.min.css">
		<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
		<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/datatables/css/responsive.bootstrap4.min.css">
		<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/style.css">
		<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
		<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/src/plugins/dropzone/src/dropzone.css">
		<!-- Google Tag Manager -->
		<script>
			(function(w, d, s, l, i) {
			            	w[l] = w[l] || [];
			            	w[l].push({
			            		'gtm.start': new Date().getTime(),
			            		event: 'gtm.js'
			            	});
			            	var f = d.getElementsByTagName(s)[0],
			            		j = d.createElement(s),
			            		dl = l != 'dataLayer' ? '&l=' + l : '';
			            	j.async = true;
			            	j.src =
			            		'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
			            	f.parentNode.insertBefore(j, f);
			            })(window, document, 'script', 'dataLayer', 'GTM-PHBQLSG');
			
		</script>
		<!-- End Google Tag Manager -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script type="text/javascript">
			$(window).load(function() {
			            	$(function() {
			            		$('#file-input').change(function(e) {
			            			addImage(e);
			            		});
			            
			            		function addImage(e) {
			            			var file = e.target.files[0],
			            				imageType = /image.*/;
			            
			            			if (!file.type.match(imageType))
			            				return;
			            
			            			var reader = new FileReader();
			            			reader.onload = fileOnload;
			            			reader.readAsDataURL(file);
			            		}
			            
			            		function fileOnload(e) {
			            			var result = e.target.result;
			            			$('#imgSalida').attr("src", result);
			            		}
			            	});
			            });
			
		</script>
		<script>
			function capturar() {
			            	var resultado = "";
			            
			            	var porNombre = document.getElementsByName("filter");
			            	for (var i = 0; i < porNombre.length; i++) {
			            		if (porNombre[i].checked)
			            			resultado = porNombre[i].value;
			            	}
			            
			            	var elemento = document.getElementById("resultado");
			            	if (elemento.className == "") {
			            		elemento.className = resultado;
			            		elemento.width = "600";
			            	} else {
			            		elemento.className = resultado;
			            		elemento.width = "600";
			            	}
			            }
			
		</script>
		<style type="text/css">
			.cdQzeM {
			            font-size: 14px;
			            line-height: 20px;
			            color: rgba(0, 0, 24, 0.48);
			            display: flex;
			            flex-direction: column;
			            -webkit-box-pack: center;
			            justify-content: center;
			            -webkit-box-align: center;
			            align-items: center;
			            padding: 12px;
			            width: 100%;
			            height: 72px;
			            left: 0px;
			            top: 47px;
			            margin-top: 12px;
			            background: rgba(0, 0, 24, 0.04);
			            border: 1px dashed rgba(0, 0, 24, 0.16);
			            border-radius: 8px;
			            }
			            .fvOTzr{ 
			            text-size-adjust: 100%;
			            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
			            font-family: "Hind Vadodara", sans-serif;
			            font-size: 14px;
			            font-weight: 400;
			            line-height: 1.5;
			            color: rgb(33, 33, 33);
			            text-align: left;
			            -webkit-font-smoothing: antialiased;
			            box-sizing: content-box;
			            overflow: visible;
			            background-color: rgba(0, 0, 24, 0.24);
			            margin: 10px 0px 12px;
			            height: 1px;
			            border: none;
			            }
			            .button-add {
			            -webkit-box-shadow: 0 2px 4px rgb(0 0 0 / 40%);
			            box-shadow: 0 2px 4px rgb(0 0 0 / 40%);
			            width: 30px;
			            height: 30px;
			            line-height: 30px;
			            padding: 8px;
			            border-radius: 100%;
			            font-size: 14px;
			            text-align: center;
			            margin-left: 8px;
			            top: 0;
			            position: relative;
			            }
			
		</style>
	</head>
	<body>
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PHBQLSG" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<?php include 'layouts/Header.php';?>
		<?php include 'layouts/Sidebar-menu.php';?>
		<div class="mobile-menu-overlay"></div>

		<div class="main-container">
			<div class="pd-ltr-20 xs-pd-20-10">
				<div class="min-height-200px">
					<?php require 'constants/check_reply.php'; ?>
			
					<div class="row">
						<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-30">
							<div class="pd-20 card-box height-50-p">
								<div class="profile-photo">
									<a href="edit_profile.php" class="edit-avatar"><i class="fa fa-pencil"></i></a> 
									<?php
										$checkVideoCv = $db->query("SELECT * FROM `tbl_users_data` where member_no=:member_no and not path_video_cv=''", array("member_no"=>$myid));
										$DescUser     = $db->query("SELECT objetivo,tags FROM `tbl_usuarios_data` where iduser=:iduser", array("iduser"=>$myid));
										$mydesc       = 'Sin objetivo laboral';
										$ability      = '';

										if(!empty($DescUser)){
											$mydesc  = $DescUser[0]['objetivo'];
											$ability = $DescUser[0]['tags'];
										}

										if ($myavatar == "") {
											
											print
													'<div class="btn-idkit center wow animated bounceInRight box1 " data-wow-delay="0.2s" data-toggle="tooltip">
														<center>
															<img  src="../../../public/img/identikit/logo.png" class="rainbow-button" title="' . $myfname . '" alt="image" data-toggle="modal" href="#infomodal" style="cursor:pointer"/>
														</center>
													</div>';
										} else { 	
											print '
													
													<div class="avatar" style="background-image: url(../../../'.$myavatar.')"></div>

													'

													;
										}
									?>
									<style type="text/css">
										div.avatar {
										    /* cambia estos dos valores para definir el tamaño de tu círculo */
										    height: 175px;
										    width: 175px;
										    /* los siguientes valores son independientes del tamaño del círculo */
										    background-repeat: no-repeat;
										    background-position: 50%;
										    border-radius: 50%;
										    background-size: 100% auto;
										}
									</style>
									<div id="infomodal" class="modal fade bs-example-modal-lg" tabindex="-1" data-width="550" style="display: none;" data-backdrop="static" data-keyboard="false" data-replace="true" role="dialog" aria-hidden="true">
										<div class="modal-dialog modal-lg modal-dialog-centered">
											<div class="modal-content">
												<div class="modal-header">
													<h4 class="modal-title" id="myLargeModalLabel">Datos personales</h4>
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
												</div>
												<div class="modal-body">
													<form action="../../controllers/employeecontroller.php?NewDp" method="POST" autocomplete="off" enctype="multipart/form-data">
														<center>
															<div>
																<div id="resultado" class=""><img id="imgSalida" style="width: 200px; height: 200px; border-radius: 200px;" /></div>
																<!-- <img  src="../../../<?= $myavatar?>"  title="' . $myfname . '" alt="image"  data-toggle="modal" href="#infomodal" style="cursor:pointer; width: 200px; height: 200px; border-radius: 200px;"> -->
															</div>
															<div class="form-group col-md-12 ">
																<center><label>Seleccionar imagen</label></center>
																<label for="file-input">
																	<img src="../../../public/img/identikit/mas.png" title="Seleccionar imagen" style="width: 50px; height: 50px">
																</label>
																<input accept="image/*" type="file" name="image" id="file-input" required hidden="" />

															</div>
															<div class="modal-footer">
																<input type="submit" value="Actualizar Imagen" class="btn btn-primary">
															</div>
														</center>
													</form>
													<form action="../../controllers/employeecontroller.php?UpdatePerfilv2" method="POST" autocomplete="off">
														<h6>Objetivo Laboral</h6>
														<br>
														<?php
															$SQLObjetivo = $db->query("SELECT objetivo FROM `tbl_usuarios_data` where iduser=:iduser", array("iduser"=>$myid));
															$mydesc      = $SQLObjetivo[0]['objetivo'];
														?>
														<textarea class="form-control" name="objetivo" rows="3"><?= $mydesc; ?></textarea>
														<center>
															<input type="submit" value="Actualizar Objetivo" class="btn btn-primary" style="margin-top:15px">
														</center>
													</form>
													<!-- <form class="post-form-wrapper" action="../../controllers/employeecontroller.php?UpdateProfile" method="POST" autocomplete="off">
														<div class="row">
															<div class="col-md-4 col-sm-12">
																<div class="form-group">
																	<label>Nombre*</label>
																	<input name="fname" class="form-control form-control-lg" type="text" value="<?php echo "$name"; ?>">
																</div>
															</div>
															<div class="col-md-4 col-sm-12">
																<div class="form-group">
																	<label>Apellido*</label>
																	<input name="lname" class="form-control form-control-lg" type="text" value="<?php echo "$mylname"; ?>">
																</div>
															</div>
															<div class="col-md-4 col-sm-12">
																<div class="form-group">
																	<label>Nacionalidad*</label>
																	<select name="country" required class="selectpicker show-tick form-control" data-live-search="true">
																		<option disabled value="">Seleccionar</option>
																		<?php
																			try {
																				$usuarios    = $db->query("SELECT * FROM tbl_countries ORDER BY country_name");
																				foreach ($usuarios as $usuario) {
																		?> 
																		<option 
																			<?php 
																				if ($mycountry == $usuario['country_name']) {
																					print ' selected ';
																				} 
																			?> 
																			value="<?php 
																				echo $usuario['country_name']; 
																				?>"><?php echo $usuario['country_name']; ?></option> 
																			<?php
																				}} catch (PDOException $e) {

																				}
																			?>
																	</select>
																</div>
															</div>
														</div>
														<div class="form-group">
															<div class="form-group">
																<label>Fecha de nacimiento*</label>
																<div class="row gap-5">
																	<div class="col-xs-4 col-sm-4">
																		<select name="date" required class="selectpicker form-control" data-live-search="false">
																			<option disabled value="">Dia</option>
																			<?php
																				$x = 1;

																				while ($x <= 31) {
																					if ($x < 10) {
																						$x = "0$x";
																						print '<option ';
																						if ($mydate == $x) {
																							print ' selected ';
																						}
																						print ' value="' . $x . '">' . $x . '</option>';
																						} else {
																							print '<option ';
																							if ($mydate == $x) {
																								print ' selected ';
																							}
																							print ' value="' . $x . '">' . $x . '</option>';
																						}
																						$x++;
																					}
																				?>
																		</select>
																	</div><br><br><br>
																	<div class="col-xs-4 col-sm-4">
																		<select name="month" required class="selectpicker form-control" data-live-search="false">
																			<option disabled value="">Mes</option>
																			<?php
																				$x = 1;
																				while ($x <= 12) {
																					if ($x < 10) {
																						$x = "0$x";
																						print '<option ';
																						if ($mymonth == $x) {
																							print ' selected ';
																						}
																						print ' value="' . $x . '">' . $x . '</option>';
																					} else {
																						print '<option ';
																						if ($mymonth == $x) {
																							print ' selected ';
																						}
																						print ' value="' . $x . '">' . $x . '</option>';
																					}
																					$x++;
																				}
																			?>
																		</select>
																	</div><br><br><br>
																	<div class="col-xs-4 col-sm-4">
																		<select name="year" class="selectpicker form-control" data-live-search="false">
																			<option disabled value="">Año</option>
																			<?php
																				$x = date('Y');
																				$yr = 60;
																				$y2 = $x - $yr;
																				while ($x > $y2) {
																					print '<option ';
																					if ($myyear == $x) {
																						print ' selected ';
																					}
																					print ' value="' . $x . '">' . $x . '</option>';
																					$x = $x - 1;
																					}
																			?>
																		</select>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-4 col-sm-12">
																	<label>Tipo de documento*</label>
																	<select name="typedni" required class="selectpicker show-tick form-control" data-live-search="false">
																		<option disabled value="">Tipo</option>

																			<option <?php if ($typedni == "D.N.I") {
																						print 'selected ';
																					} ?> value="D.N.I">D.N.I</option>

																			<option <?php if ($typedni == "Cedula de identidad") {
																						print 'selected ';
																					} ?>value="Cedula de identidad">Cedula de identidad</option>
																			<option <?php if ($typedni == "L.E.") {
																						print ' selected ';
																					} ?>value="L.E.">L.E.</option>

																			<option <?php if ($typedni == "Pasaporte") {
																						print 'selected ';
																					} ?>value="Pasaporte">Pasaporte</option>

																			<option <?php if ($typedni == "L.C.") {
																						print 'selected ';
																					} ?>value="L.C.">L.C.</option>		
																	</select>
																</div>
															

																<div class="col-md-4 col-sm-12">
																	<label>Numero</label>
																	<input name="dni" class="form-control form-control-lg" type="text" value="<?php echo "$dni"; ?>">
																</div>

																<div class="col-md-4 col-sm-12">
																	<label>Género*</label>

																	<select name="gender" required class="selectpicker show-tick form-control" data-live-search="false">
																	<option disabled value="">Género</option>
																		<option <?php if ($mygender == "Male") {
																					print ' selected ';
																				} ?> value="Male">Masculino</option>
																		<option <?php if ($mygender == "Female") {
																					print ' selected ';
																				} ?>value="Female">Femenino</option>
																		<option <?php if ($mygender == "Other") {
																					print ' selected ';
																				} ?>value="Other">Otro</option>
																	</select>
																</div>
															</div>
															<br>	
														</div>
														<div class="row gap-20">
															<div class="col-md-12 modal-footer text-center">
																<button type="submit" class="col-md-12 btn btn-primary" >Actualizar</button>
															</div>
														</div>
													</form> -->
												</div>
											</div>
										</div>
        							</div>
								</div>
								<h5 class="text-center h5 mb-0"><?php echo "$myfname"; ?> <?php echo "$mylname";?></h5>
								<!--<div class="btn-list text-center pd-10">
									<?php
										$github    = "";
										$twitter   = "";
										$instagram = "";

										if ($github == "") {
											print '<a href="https://github.com/' . "$github" . '" type="button" class="btn" style="border-radius: 10px;" data-bgcolor="black" data-color="#ffffff" target="_blank"><i class="fa fa-github"></i></a>';
										}
										if ($twitter == "") {
											print '<a href="https://twitter.com/' . "$twitter" . '" type="button" class="btn" style="border-radius: 10px;" data-bgcolor="#1da1f2" data-color="#ffffff" target="_blank"><i class="fa fa-twitter"></i></a>';
										}
										if ($instagram == "") {
											print '<a href="https://instagram.com/' . "$instagram" . '" type="button" class="btn" style="border-radius: 10px;" data-bgcolor="#f46f30" data-color="#ffffff" target="_blank"><i class="fa fa-instagram"></i></a>';
										}
									?>
								</div>-->
								<div class="modal fade" id="uploadvideocv" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
									<div class="modal-dialog modal-dialog-centered" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title">Sube tu IDentiVideo</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<form action="../../controllers/employeecontroller.php" method="post" enctype="multipart/form-data">
													<center>
														<div class="form-group">
															<div class="video" style="width: 40%; border-radius: 100px !important;">
																<?php 
																	$getVideoCv  = $db->query("SELECT path_video_cv FROM `tbl_users_data` where member_no=:member_no", array("member_no"=>$myid));
																	echo '<video class="video__player" src="../../../'.$getVideoCv[0]['path_video_cv'].'"></video';
																	$getInfoUser = $db->query("SELECT first_name, last_name, about from `tbl_users` where member_no=:member_no",array("member_no"=>$myid));
																	$fullname    = $getInfoUser[0]["first_name"] . " " . $getInfoUser[0]["last_name"]; 
																?>						
															</div>
														</div><br>
														<div class="custom-file">
															<input type="file" name="fileToUpload" id="fileToUpload" class="custom-file-input">
															<input type="hidden" name="hiddenId" id="hiddenId" value="<?= $myid ?>" />
															<label class="custom-file-label">Seleccionar IDentiVideo</label>
														</div>
													</center>
													<center>
														<button type="submit" class="btn btn-primary">Subir</button>
													</center>
												</form>
											</div>
										</div>										
									</div>
								</div>
								<div class="modal fade" id="videocv" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
									<div class="modal-dialog modal-dialog-centered" role="document">
										<div class="modal-content">
											<div class="app__videos">
												<!-- video starts -->
												<div class="video">
													<?php 
														$getVideoCv  = $db->query("SELECT path_video_cv FROM `tbl_users_data` where member_no=:member_no", array("member_no"=>$myid));
														echo '<video class="video__player" src="../../../'.$getVideoCv[0]['path_video_cv'].'"></video';
														$getInfoUser = $db->query("SELECT first_name, last_name, about from `tbl_users` where member_no=:member_no",array("member_no"=>$myid));
														$fullname    = $getInfoUser[0]["first_name"] . " " . $getInfoUser[0]["last_name"]; 
													?>
							
													<!-- sidebar -->
													<div class="videoSidebar">
													
													</div>
													<!-- footer -->
													<div class="videoFooter">
													<div class="videoFooter__text">
														<h3><?= $fullname ?></h3>
														<p class="videoFooter__description">
														<?= $getInfoUser[0]["about"]; ?>
														</p>            
													</div>
													
													</div>
												</div>
												<!-- video ends -->
											</div>
										</div>
										<!-- video ends -->
									</div>
								</div>
								<link rel="stylesheet" href="../../../public/css/videocv.css" />
								<input type="checkbox" class="read-more-state" id="post-1" />
									<p class="read-more-wrap text-center text-muted font-14">
										<span class="read-more-target"> <?php echo "$mydesc"; ?> </span>
									</p>
        						<label for="post-1" class="read-more-trigger"></label>
							</div><br>
							<div class="pd-20 card-box height-50-p">
    							<h5>Datos de contacto</h5>
    							<hr>
									<i class="icon-copy fa fa-envelope" aria-hidden="true" style="color: #1da1f2;"></i> <b><?php echo $myemail; ?></b> </p>
									<i class="icon-copy fa fa-phone" aria-hidden="true" style="color: #1da1f2;"></i> <b><?php echo $myphone; ?></b> </p>
									<i class="icon-copy fa fa-home" aria-hidden="true" style="color: #1da1f2;"></i> <b><?php echo $street; ?></b> </p>
							</div>
						</div>
						<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 mb-30">
							<div class="card-box height-100-p overflow-hidden">
								<div class="profile-tab height-100-p">	
									<div class="tab height-100-p">
										<ul class="nav nav-tabs customtab" role="tablist">
											<!-- <li class="nav-item">
												<a class="nav-link active" data-toggle="tab" href="#education" role="tab">Educación</a>
											</li>
											<li class="nav-item">
												<a class="nav-link" data-toggle="tab" href="#project" role="tab">Proyectos</a>
											</li> -->
											<li class="nav-item">
												<a class="nav-link active" data-toggle="tab" href="#profile" role="tab">Perfil</a>
											</li>
										</ul>
										<div class="tab-content">
											<!-- Timeline Tab start -->
											<div class="tab-pane fade show active" id="education" role="tabpanel">
												<div class="pd-20">
													<div class="profile-timeline">
														<div class="timeline-month ">
															<h5>Formación académica <a href="add-education" data-toggle="modal" data-target="#add-education" class="button-add"><i class="icon-copy fa fa-plus"></i></a></h5>
														</div>
														<div id="add-education" class="modal fade bs-example-modal-lg" tabindex="-1" data-width="550" style="display: none;" data-backdrop="static" data-keyboard="false" data-replace="true" role="dialog" aria-hidden="true">
															<div class="modal-dialog modal-lg modal-dialog-centered">
																<div class="modal-content">
																	<div class="modal-header">
																		<h4 class="modal-title" id="myLargeModalLabel">Sumar estudio</h4>
																		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
																	</div>
																	<div class="modal-body">
																		<form action="../../controllers/employeecontroller.php?AddEducation" method="POST" autocomplete="off" enctype="multipart/form-data">
																		<div class="row">
																			<div class="col-md-6 col-sm-12">
																				<div class="form-group">
																					<label>Título / Carrera*</label>
																					<input name="educationtitle" class="form-control form-control-lg" type="text">
																				</div>
																			</div>
																			<div class="col-md-6 col-sm-12">
																				<div class="form-group">
																					<label>País*</label>
																					<select name="ecountry" required class="selectpicker show-tick form-control" data-live-search="true">
																					<option disabled value="">Seleccionar</option>
																					<?php
																						try {
																							$usuarios    = $db->query("SELECT * FROM tbl_countries ORDER BY country_name");
																							foreach ($usuarios as $usuario) {
																						?> 
																					<option 
																						<?php 
																							?> 
																						><?php echo $usuario['country_name']; ?></option>
																					<?php
																						}} catch (PDOException $e) {
																						
																						}
																						?>
																					</select>
																				</div>
																			</div>
																			<div class="col-md-6 col-sm-12">
																				<div class="form-group">
																					<label>Tipo de estudio*</label>
																					<select name="typeed" required="" class="selectpicker show-tick form-control">
																					<option value=""></option>
																					<option value="Secundario">Secundario</option>
																					<option value="Curso">Curso</option>
																					<option value="Terciario">Terciario</option>
																					<option value="Universitario">Universitario</option>
																					<option value="Posgrado">Posgrado</option>
																					<option value="Master">Master</option>
																					<option value="Doctorado">Doctorado</option>
																					<option value="Otro">Otro</option>
																					</select>
																				</div>
																			</div>
																			<div class="col-md-6 col-sm-12">
																				<div class="form-group">
																					<label>Área de estudio*</label>
																					<select name="earea" required class="selectpicker show-tick form-control" data-live-search="true">
																					<option  value=""></option>
																					<option value="Tecnologia">Tecnologia</option>
																					<option value="Agronomia">Agronomia</option>
																					<option value="Economia">Economia</option>
																					</select>
																				</div>
																			</div>
																			<div class="col-md-6 col-sm-12">
																				<div class="form-group">
																					<label>Institución*</label>
																					<input name="einstitution" class="form-control form-control-lg" type="text">
																				</div>
																			</div>
																			<div class="col-md-6 col-sm-12">
																				<div class="form-group">
																					<label>Estado*</label>
																					<select name="estatus" required class="selectpicker show-tick form-control" data-live-search="true">
																					<option  value=""></option>
																					<option value="En Curso">En Curso</option>
																					<option value="Graduado">Graduado</option>
																					<option value="Abandonado">Abandonado</option>
																					</select>
																				</div>
																			</div>
																			<div class="col-md-3 col-sm-3">
																				<div class="form-group">
																					<label>Mes Inicio*</label>
																					<select name="startdate_m" required class="selectpicker form-control" data-live-search="false">
																					<option disabled value="">Mes</option>
																						<?php
																							$x       = 1;
																							$mymonth = 1;
																							while ($x <= 12) {
																								if ($x < 10) {
																									$x = "0$x";
																									print '<option ';
																									if ($mymonth == $x) {
																										print ' selected ';
																									}
																									print ' value="' . $x . '">' . $x . '</option>';
																								} else {
																									print '<option ';
																									if ($mymonth == $x) {
																										print ' selected ';
																									}
																									print ' value="' . $x . '">' . $x . '</option>';
																								}
																								$x++;
																							}
																						?>
																					</select>
																				</div>
																			</div>
																			<div class="col-md-3 col-sm-3">
																				<div class="form-group">
																					<label>Año Inicio*</label>
																					<select name="startdate_y" class="selectpicker form-control" data-live-search="false">
																					<option disabled value="">Año</option>
																					<?php
																						$x = date('Y');
																						$yr = 60;
																						$y2 = $x - $yr;
																						$myyear = 0;
																						while ($x > $y2) {
																							print '<option ';
																							if ($myyear == $x) {
																								print ' selected ';
																							}
																							print ' value="' . $x . '">' . $x . '</option>';
																							$x = $x - 1;
																							}
																						?>
																					</select>
																				</div>
																			</div>
																			<div class="col-md-3 col-sm-3">
																				<div class="form-group">
																					<label>Mes fin*</label>
																					<select name="enddate_m" required class="selectpicker form-control" data-live-search="false">
																					<option disabled value="">Mes</option>
																					<?php
																						$x = 1;
																						while ($x <= 12) {
																							if ($x < 10) {
																								$x = "0$x";
																								print '<option ';
																								if ($mymonth == $x) {
																									print ' selected ';
																								}
																								print ' value="' . $x . '">' . $x . '</option>';
																							} else {
																								print '<option ';
																								if ($mymonth == $x) {
																									print ' selected ';
																								}
																								print ' value="' . $x . '">' . $x . '</option>';
																							}
																							$x++;
																						}
																						?>
																					</select>
																				</div>
																			</div>
																			<div class="col-md-3 col-sm-3">
																				<div class="form-group">
																					<label>Año fin*</label>
																					<select name="enddate_y" class="selectpicker form-control" data-live-search="false">
																					<option disabled value="">Año</option>
																					<?php
																						$x = date('Y');
																						$yr = 60;
																						$y2 = $x - $yr;

																						while ($x > $y2) {
																							print '<option ';
																							if ($myyear == $x) {
																								print ' selected ';
																							}
																							print ' value="' . $x . '">' . $x . '</option>';
																							$x = $x - 1;
																							}
																						?>
																					</select>
																				</div>
																			</div>
																		</div>
																		<div class="col-md-12 modal-footer text-center">
																			<button type="submit" class="col-md-12 btn btn-primary" >Add</button>
																		</div>
																	</div>
																	</form>
																</div>
															</div>
														</div>
														<hr class="fvOTzr">
														<div class="profile-timeline-list">
															<?php
																$TblEducation = $db->query("SELECT * FROM tbl_education WHERE member_no = :myid ORDER BY id desc",array("myid"=>$myid));
																
																if ($TblEducation) {
																	try {
																		$TblEducation = $db->query("SELECT * FROM tbl_education WHERE member_no = :myid ORDER BY id desc",array("myid"=>$myid));
																		
																		foreach ($TblEducation as $Education) {
																			$title = $Education['title'];
																			$country = $Education['country'];
																			$type = $Education['type'];
																			$area = $Education['area'];
																			$institution = $Education['institution'];
																			$status = $Education['status'];
																			$start_date_m = $Education['start_date_m'];
																			$start_date_y = $Education['start_date_y'];
																			$end_date_m = $Education['end_date_m'];
																			$end_date_y = $Education['end_date_y'];
																			$id = $Education['id'];
																
																
																			print '
																				<ul>
																					<li>
																						<div class="date" style="font-size: 17px !important;">'.$title.'</div><br>
																						<div class="task-name"></div>
																							<p>'.$type.' - '.$status.'</p>
																						<div class="task-time"> '.$start_date_m.'/'.$start_date_y.' - '.$end_date_m.'/'.$end_date_y.', '.$country.' </div>
																					</li>
																				</ul>
																			';
																		}} catch (PDOException $e) {}
																} else {
																	print'
																		<div class="Grid__Row-sc-flyz1b-3 bnfsRw">
																			<div class="Grid__Col-sc-flyz1b-4 dexUcY">
																				<div class="sc-jGxEUC cdQzeM">
																					<i class="icon-copy fa fa-book" aria-hidden="true" color="rgba(0, 0, 24, 0.48)" size="24"></i>
																				</div>Agregá tu primer estudio
																			</div>
																		</div>';
																}
																
																?>
														</div>
														<br>
														<div class="timeline-month">
															<h5>¿Qué idiomas hablás?<a href="add-lang" data-toggle="modal" data-target="#add-lang" class="button-add"><i class="icon-copy fa fa-plus"></i></a></h5>
														</div>
														<div id="add-lang" class="modal fade bs-example-modal-lg" tabindex="-1" data-width="550" style="display: none;" data-backdrop="static" data-keyboard="false" data-replace="true" role="dialog" aria-hidden="true">
															<div class="modal-dialog modal-lg modal-dialog-centered">
																<div class="modal-content">
																	<div class="modal-header">
																		<h4 class="modal-title" id="myLargeModalLabel">Sumar idioma</h4>
																		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
																	</div>
																	<div class="modal-body">
																		<form action="../../controllers/employeecontroller.php?AddLang" method="POST" autocomplete="off" enctype="multipart/form-data">
																		<div class="row">
																			<div class="col-md-12 col-sm-12">
																				<div class="form-group">
																					<label>Idioma*</label>
																					<input name="lang" class="form-control form-control-lg" type="text">
																				</div>
																			</div>
																			<div class="col-md-12 col-sm-12">
																				<div class="form-group">
																					<label>Nivel escrito*</label>
																					<select name="l_write" required class="selectpicker show-tick form-control">
																					<option disabled value=""></option>
																					<option value="Básico">Básico</option>
																					<option value="Intermedio">Intermedio</option>
																					<option value="Avanzado">Avanzado</option>
																					<option value="Nativo">Nativo</option>
																					</select>
																				</div>
																			</div>
																			<div class="col-md-12 col-sm-12">
																				<div class="form-group">
																					<label>Nivel oral*</label>
																					<select name="l_read" required class="selectpicker show-tick form-control">
																					<option disabled value=""></option>
																					<option value="Básico">Básico</option>
																					<option value="Intermedio">Intermedio</option>
																					<option value="Avanzado">Avanzado</option>
																					<option value="Nativo">Nativo</option>
																					</select>
																				</div>
																			</div>
																		</div>
																		<div class="col-md-12 modal-footer text-center">
																			<button type="submit" class="col-md-12 btn btn-primary" >Añadir</button>
																		</div>
																	</div>
																	</form>
																</div>
															</div>
														</div>
														<hr class="fvOTzr">
														<div class="profile-timeline-list">
															<?php
																$TblLang = $db->query("SELECT * FROM tbl_lang WHERE member_no = :myid ORDER BY id desc",array("myid"=>$myid));
																
																if ($TblLang) {
																	try {
																		$TblLang = $db->query("SELECT * FROM tbl_lang WHERE member_no = :myid ORDER BY id desc",array("myid"=>$myid));
																		
																		foreach ($TblLang as $Lang) {
																			$language = $Lang['lang'];
																			$l_write = $Lang['l_write'];
																			$l_read = $Lang['l_read'];
																			$id = $Lang['id'];

																			print '
																				<div class="profile-timeline-list">
																					<ul>
																						<li>
																							<div class="date" style="font-size: 17px !important;">'.$language.'</div>  <br>
																							<div class="task-name"></div>
																							<p><b>Escrito:</b> '.$l_write.' <br> <b>Oral:</b>	 '.$l_read.'</p>
																							
																						</li>
																				</ul>';
																		}
																			} catch (PDOException $e) {
																			}
																	}else{
																		print'
																			<div class="Grid__Row-sc-flyz1b-3 bnfsRw">
																				<div class="Grid__Col-sc-flyz1b-4 dexUcY">
																					<div class="sc-jGxEUC cdQzeM">
																						<i class="icon-copy fa fa-commenting-o" aria-hidden="true" color="rgba(0, 0, 24, 0.48)" size="24"></i>
																					</div>Agregá tu primer idioma
																				</div>
																			</div>';
																	}
															?>
														</div>
														<br>
														<div class="timeline-month">
															<h5>Conocimientos y habilidades <a href="add-tags" data-toggle="modal" data-target="#add-tags" class="button-add"><i class="icon-copy fa fa-plus"></i></a></h5>
														</div>
														<div id="add-tags" class="modal fade bs-example-modal-lg" tabindex="-1" data-width="550" style="display: none;" data-backdrop="static" data-keyboard="false" data-replace="true" role="dialog" aria-hidden="true">
															<div class="modal-dialog modal-lg modal-dialog-centered">
																<div class="modal-content">
																	<div class="modal-header">
																		<h4 class="modal-title" id="myLargeModalLabel">Sumar conocimientos y habilidades</h4>
																		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
																	</div>
																	<div class="modal-body">
																		<form action="../../controllers/employeecontroller.php?UpdateTags" method="POST" autocomplete="off" enctype="multipart/form-data">
																		<div class="row">
																			<div class="col-md-12 col-sm-12">
																				<div class="form-group">
																					<label>Sumar conocimientos y habilidades</label>
																					<p class="text-muted">Tenés 45 conocimientos y habilidades para agregar. Escribí una palabra simple o compuesta. Ej: Microsoft power point</p>
																					<input type="text" class="form-control form-control-lg" data-role="tagsinput" name="ability" placeholder="Presiona ENTER para agregar" value="<?php echo "$ability"; ?>">
																				</div>
																			</div>
																		</div>
																		<div class="col-md-12 modal-footer text-center">
																			<button type="submit" class="col-md-12 btn btn-primary" >Añadir</button>
																		</div>
																	</div>
																	</form>
																</div>
															</div>
														</div>
													</div>
													<hr class="fvOTzr">
													<div class="profile-timeline-list">
														<?php
															if(isset($ability)){
																$tags = explode(",", $ability);
																if ($tags) {
																	for ($i = 0; $i <= count($tags) - 1; $i++) {
																		$array = $tags[$i];
																		print '
																			<span class="btn btn-outline-secondary">' . " $array" . '</span>';
																	}
																}else{ 
																	print '
																		<div class="Grid__Row-sc-flyz1b-3 bnfsRw">
																			<div class="Grid__Col-sc-flyz1b-4 dexUcY">
																				<div class="sc-jGxEUC cdQzeM">
																					<i class="icon-copy fa fa-tv" aria-hidden="true" color="rgba(0, 0, 24, 0.48)" size="24"></i>
																				</div>
																				Agregá tus conocimientos y habilidades</div>
																			</div>
																		</div>';
																}
															}
														?>									
                     								</div>
												</div>
											</div>
											<div class="tab-pane fade" id="project" role="tabpanel">
												<div class="pd-20">
													<div class="profile-timeline">
														<div class="timeline-month">
															<h5>Agregá tus proyectos<a href="add-projects" data-toggle="modal" data-target="#add-projects" class="button-add"><i class="icon-copy fa fa-plus"></i></a></h5>
															<p class="text-muted font-14">En esta sección podrás incluir los proyectos en los que has trabajado, recuerda que los proyectos propios tambien cuentan, no olvides cargarlos! (Además esto te posiciona mucho mejor con las empresas)</p>
														</div>
														<div id="add-projects" class="modal fade bs-example-modal-lg" tabindex="-1" data-width="550" style="display: none;" data-backdrop="static" data-keyboard="false" data-replace="true" role="dialog" aria-hidden="true">
															<div class="modal-dialog modal-lg modal-dialog-centered">
																<div class="modal-content">
																	<div class="modal-header">
																		<h4 class="modal-title" id="myLargeModalLabel">Sumar proyecto</h4>
																		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
																	</div>
																	<div class="modal-body">
																		<form action="../../controllers/employeecontroller.php?AddExperience" method="POST" autocomplete="off" enctype="multipart/form-data">
																			<div class="row">
																			<div class="col-sm-12 col-md-12">
																				<div class="form-group">
																					<label>Titulo del proyecto </label>
																					<input class="form-control" placeholder="Ingresa el titulo del proyecto" type="text" name="jobtitle" required>
																				</div>
																			</div>
																			<div class="col-sm-12 col-md-12">
																				<div class="form-group">
																					<label>Descripción</label>
																					<p class="text-muted">Este campo será visible en la vista detallada del proyecto. Describe los detalles principales del proyecto de forma clara y concisa.</p>
																					<textarea class="form-control" name="duties"> </textarea>
																				</div>
																			</div>
																			<div class="col-sm-12 col-md-12">
																				<div class="form-group ">
																					<label>Habilidades/tecnologias aplicadas (5 máximo)</label>
																					<input type="text" class="form-control form-control-lg" data-role="tagsinput" name="techno" placeholder="Presiona ENTER para agregar">
																				</div>
																			</div>
																			<div class="col-sm-12 col-md-12">
																				<div class="form-group">
																					<label>Link del proyecto (opcional)</label>
																					<input class="form-control" placeholder="Ingresa link del proyecto" type="text" name="link" value="https://">
																				</div>
																			</div>
																			<div class="col-sm-6 col-md-12">
																				<div class="form-group">
																					<select class="selectpicker show-tick form-control" name="view" required="">
																						<option value="">Opciones de visibilidad</option>
																						<option value="mostrar">Mostrar</option>
																						<option value="ocultar">Ocultar</option>
																					</select>
																				</div>
																			</div>
																			</div>
																			<div class="col-md-12 modal-footer text-center">
																			<button type="submit" class="col-md-12 btn btn-primary" >Añadir</button>
																			</div>
																	</div>
																	</form>
																</div>
															</div>
														</div>
														<hr class="fvOTzr">
														<div class="profile-timeline-list">
															<?php
																$TblExperiencia = $db->query("SELECT * FROM tbl_experience WHERE member_no = :myid ORDER BY id desc",array("myid"=>$myid));
																
																if ($TblExperiencia) {
																	try {
																		$TblExperiencia = $db->query("SELECT * FROM tbl_experience WHERE member_no = :myid ORDER BY id desc",array("myid"=>$myid));
																		
																		foreach ($TblExperiencia as $Experiencia) {
																			$title = $Experiencia['title'];
																			$start_date = $Experiencia['start_date'];
																			$end_date = $Experiencia['end_date'];
																			$duties = $Experiencia['duties'];
																			$view = $Experiencia['view'];
																			$expid = $Experiencia['id'];
																			$techno = $Experiencia['techno'];
																			$link = $Experiencia['link'];
																
																			$tags = explode(",", $techno);
																
																			for ($i = 0; $i <= count($tags) - 1; $i++) {
																				$array = $tags[$i];  }
																			print '
																			<div class="profile-timeline-list">
																				
																				<ul>
																					<li>
																						<div class="date" style="font-size: 17px !important;">'.$title.'</div>  <br>
																
																					
																						<div class="task-name"><span class="badge badge-pill" style="background-color: whitesmoke; margin: 5px; color: #01c0fe;">'. $array.'</span> </div>
																						<p>'.$duties .'</p>
																						<div class="task-time"> '.$start_date.' - '.$end_date.'</div>
																					</li>
																				</ul> 
																
																			';
																				}
																			} catch (PDOException $e) {
																			}
																	}else{
																		print'
																			<div class="Grid__Row-sc-flyz1b-3 bnfsRw"><div class="Grid__Col-sc-flyz1b-4 dexUcY">
																			<div class="sc-jGxEUC cdQzeM">
																			<i class="icon-copy fa fa-flash" aria-hidden="true" color="rgba(0, 0, 24, 0.48)" size="24"></i>
																			</div>Agregá tus proyectos</div>
																			</div>
																	';
																	}
																
															?>	
														</div>
													</div>
												</div>
											</div>
											<div class="tab-pane fade" id="profile" role="tabpanel">
												<div class="pd-20">
													<div class="timeline-month">
														<h5>Objetivo laboral <a href="add-oblaboral" data-toggle="modal" data-target="#add-oblaboral" class="button-add"><i class="icon-copy fa fa-pencil"></i></a></h5>
													</div>
													<div id="add-oblaboral" class="modal fade bs-example-modal-lg" tabindex="-1" data-width="550" style="display: none;" data-backdrop="static" data-keyboard="false" data-replace="true" role="dialog" aria-hidden="true">
													<div class="modal-dialog modal-lg modal-dialog-centered">
														<div class="modal-content">
															<div class="modal-header">
																<h4 class="modal-title" id="myLargeModalLabel">Objetivo laboral</h4>
																<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
															</div>
															<div class="modal-body">
																<form action="../../controllers/employeecontroller.php?UpdateOblab" method="POST" autocomplete="off" enctype="multipart/form-data">
																<div class="row">
																	<div class="col-sm-12 col-md-12">
																		<div class="form-group">
																			<label>¿Cuáles son tus expectativas a nivel profesional?*</label>
																			<textarea class="form-control" name="about" maxlength="500"><?php echo "$mydesc"; ?></textarea>
																		</div>
																	</div>
																</div>
																<div class="col-md-12 modal-footer text-center">
																	<button type="submit" class="col-md-12 btn btn-primary">Add</button>
																</div>
															</div>
															</form>
														</div>
													</div>
													</div>
													<hr class="fvOTzr">
													<div class="profile-timeline-list">
													<?php
														$obejtivolaboral = $db->query("SELECT about FROM tbl_users WHERE member_no = :myid ORDER BY member_no desc",array("myid"=>$myid));
														
														if ($obejtivolaboral) {
															try {
																$obejtivolaboral = $db->query("SELECT about FROM tbl_users WHERE member_no = :myid ORDER BY member_no desc",array("myid"=>$myid));
																
																foreach ($obejtivolaboral as $obejtivo) {
																	$oblab = $obejtivo['about'];
																	
																	$oblabid = $obejtivo['member_no'];
																	
																	print '
																	<div class="profile-timeline-list">
																		
																																				
																				<p>'.$oblab .'</p>
																			
																	';
																		}
																	} catch (PDOException $e) {
																	}
															}else{
																print'
																<div class="Grid__Row-sc-flyz1b-3 bnfsRw"><div class="Grid__Col-sc-flyz1b-4 dexUcY">
																	<div class="sc-jGxEUC cdQzeM">
																		<i class="icon-copy fa fa-flash" aria-hidden="true" color="rgba(0, 0, 24, 0.48)" size="24"></i>
																		<div>Contanos cuáles son tus expectativas profesionales</div>
																	</div>
																</div>
														';
															}
														
															?>		
													</div>
												</div>
												<br>
												<div class="timeline-month">
													<h5>Preferencia salarial <a href="add-oblaboral-salario" data-toggle="modal" data-target="#add-oblaboral-salario" class="button-add"><i class="icon-copy fa fa-pencil"></i></a></h5>
												</div>
												<div id="add-oblaboral-salario" class="modal fade bs-example-modal-lg" tabindex="-1" data-width="550" style="display: none;" data-backdrop="static" data-keyboard="false" data-replace="true" role="dialog" aria-hidden="true">
													<div class="modal-dialog modal-lg modal-dialog-centered">
													<div class="modal-content">
														<div class="modal-header">
															<h4 class="modal-title" id="myLargeModalLabel">Preferencia salarial</h4>
															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
														</div>
														<div class="modal-body">
															<form action="../../controllers/employeecontroller.php?UpdateSalarium" method="POST" autocomplete="off" enctype="multipart/form-data">
																<div class="row">
																<div class="col-sm-12 col-md-12">
																	<div class="form-group">
																		<label>¿Cuánto querés ganar?*</label>
																		<?php $salarium = $db->query("SELECT salario FROM tbl_laboral WHERE member_no = :myid");?>
																		<input name="salarium" class="form-control form-control-lg" type="text">
																	</div>
																</div>
																</div>
																<div class="col-md-12 modal-footer text-center">
																<button type="submit" class="col-md-12 btn btn-primary">Add</button>
																</div>
														</div>
														</form>
													</div>
													</div>
												</div>
												<hr class="fvOTzr">
												<div class="profile-timeline-list">
													<?php
													$salarium = $db->query("SELECT salario FROM tbl_laboral WHERE member_no = :myid ORDER BY id desc",array("myid"=>$myid));
													
													if ($salarium) {
														try {
															$salarium = $db->query("SELECT salario FROM tbl_laboral WHERE member_no = :myid ORDER BY id desc",array("myid"=>$myid));
															
															foreach ($salarium as $salario) {
																$salariado = $salario['salario'];
																
																$salariadoid = $salario['id'];
																
																print '
																	<div class="profile-timeline-list">
																	<p>Sueldo en mano pretendido: <b>$'.$salariado .'<b></p>	';
																	}
																} catch (PDOException $e) {
																}
														}else{
															print'
															<div class="Grid__Row-sc-flyz1b-3 bnfsRw">
															<div class="Grid__Col-sc-flyz1b-4 dexUcY">
																	<div class="sc-jGxEUC cdQzeM">
																		<i class="icon-copy fa fa-flash" aria-hidden="true" color="rgba(0, 0, 24, 0.48)" size="24"></i>
																	</div>Contanos cuánto querés ganar
																</div>
															</div>';
														}
													
														?>		
												</div>
											</div>
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>	
		</div>
		<style type="text/css">
			.read-more-state {
				display: none;
			}

			.read-more-target {
				opacity: 0;
				max-height: 0;
				font-size: 0;
				transition: .30s ease;
			}

			.read-more-state:checked ~ .read-more-wrap .read-more-target {
				opacity: 1;
				font-size: inherit;
			}

			.read-more-state ~ .read-more-trigger:before {
				content: 'Leer objetivo laboral';
			}

			.read-more-state:checked ~ .read-more-trigger:before {
				content: 'Ocultar';
			}

			.read-more-trigger {
				cursor: pointer;
				display: inline-block;
				padding: 0 .5em;
				color: #666;
				font-size: .9em;
				line-height: 2;
				border: 1px solid #ddd;
				border-radius: .25em;
				width: 100%;
				text-align: center;
			}

        	/* Other style */ 
    	</style>	
		<!-- js -->
		<script>
			const videos = document.querySelectorAll('video');
			            
			            for (const video of videos) {
			              video.addEventListener('click', function () {
			                console.log('clicked');
			                if (video.paused) {
			                  video.play();
			                } else {
			                  video.pause();
			                }
			              });
			            }
			
		</script>
		<!-- js -->
		<script src="../../../public/complements/v1/vendors/scripts/core.js"></script>
		<script src="../../../public/complements/v1/vendors/scripts/script.min.js"></script>
		<script src="../../../public/complements/v1/vendors/scripts/process.js"></script>
		<script src="../../../public/complements/v1/vendors/scripts/layout-settings.js"></script>
		<script src="../../../public/complements/v1/src/plugins/apexcharts/apexcharts.min.js"></script>
		<script src="../../../public/complements/v1/src/plugins/datatables/js/jquery.dataTables.min.js"></script>
		<script src="../../../public/complements/v1/src/plugins/datatables/js/dataTables.bootstrap4.min.js"></script>
		<script src="../../../public/complements/v1/src/plugins/datatables/js/dataTables.responsive.min.js"></script>
		<script src="../../../public/complements/v1/src/plugins/datatables/js/responsive.bootstrap4.min.js"></script>
		<script src="../../../public/complements/v1/vendors/scripts/dashboard.js"></script>
		<script src="../../../public/complements/v1/src/plugins/dropzone/src/dropzone.js"></script>
		<script src="../../../public/complements/v1/src/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
		<script>
			Dropzone.autoDiscover = false;
			            $(".dropzone").dropzone({
			            	addRemoveLinks: true,
			            	removedfile: function(file) {
			            		var name = file.name;
			            		var _ref;
			            		return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
			            	}
			            });
			
		</script>
	</body>
	</html>