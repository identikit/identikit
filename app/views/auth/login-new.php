<?php
            include '../../../config/settings.php';
            include '../../../config/check-newlogin.php';

            //Include Configuration File
            //include('config.php');

            $login_button = '';

            if (isset($_GET["code"])) {

              $token = $google_client->fetchAccessTokenWithAuthCode($_GET["code"]);
              if (!isset($token['error'])) {

                $google_client->setAccessToken($token['access_token']);

                $_SESSION['access_token'] = $token['access_token'];

                $google_service = new Google_Service_Oauth2($google_client);

                $data = $google_service->userinfo->get();

                if (!empty($data['given_name'])) {
                  $_SESSION['user_first_name'] = $data['given_name'];
                }

                if (!empty($data['family_name'])) {
                  $_SESSION['user_last_name'] = $data['family_name'];
                }

                if (!empty($data['email'])) {
                  $_SESSION['user_email_address'] = $data['email'];
                }

                if (!empty($data['gender'])) {
                  $_SESSION['user_gender'] = $data['gender'];
                }

                if (!empty($data['picture'])) {
                  $_SESSION['user_image'] = $data['picture'];
                }
              }
            }

            ?>
            <!DOCTYPE html>
            <html lang="es">
            <head>
               <!-- SEO Meta Tags -->
                   <meta name="description" content="">
                   <meta name="author" content="IDentiKIT.app ">

                   <!-- OG Meta Tags to improve the way the post looks when you share the page on LinkedIn, Facebook, Google+ -->
                   <meta property="og:site_name" content="IDentiKIT - Tu primera experiencia laboral" /> <!-- website name -->
                   <meta property="og:site" content="https://identikit.app/" /> <!-- website link -->
                   <meta property="og:title" content="¡Consigue tu primera experiencia laboral! - IDentiKIT.app"/> <!-- title shown in the actual shared post -->
                   <meta property="og:description" content="IDentiKIT es una plataforma que restalta la empleabilidad joven. Aplica ahora y mejora tu futuro." />

                    <!-- description shown in the actual shared post -->
                   
                   <meta property="og:url" content="https://identikit.app/" /> <!-- where do you want your post to link to -->
                   <meta property="og:type" content="article" />
                   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
                  
                   <meta property="og:image" content="http://identikit.app/landingv1/v3/images/logo-og.png" />
                   <meta property="og:image:secure_url" content="https://identikit.app/landingv1/v3/images/logo-og.png" />
                   <meta property="og:image:type" content="image/png" />
                   <meta property="og:image:width" content="300" />
                   <meta property="og:image:height" cosntent="300" />



                   <!-- Website Title -->
                   <title>Iniciar sesión - IDentiKIT.app</title>
                   
                
                <!-- Styles -->
                <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700&display=swap&subset=latin-ext" rel="stylesheet">
                <link href="https://identikit.app/css/bootstrap.css" rel="stylesheet">
                <link href="https://identikit.app/css/fontawesome-all.css" rel="stylesheet">
                <link href="https://identikit.app/css/swiper.css" rel="stylesheet">
              <link href="https://identikit.app/css/magnific-popup.css" rel="stylesheet">
              <link href="https://identikit.app/css/styles.css" rel="stylesheet">
              
              <!-- Favicon  -->
                <link rel="icon" href="https://identikit.app/images/logo.png">

                <script type="text/javascript">
                  function update(str) {

                    if (document.getElementById('mymail').value == "") {
                      alert("Porfavor Ingresa tu email");

                    } else {
                      document.getElementById("data").innerHTML = "Procesando...";
                      var xmlhttp;

                      if (window.XMLHttpRequest) {
                        xmlhttp = new XMLHttpRequest();
                      } else {
                        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                      }

                      xmlhttp.onreadystatechange = function() {
                        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                          document.getElementById("data").innerHTML = xmlhttp.responseText;
                        }
                      }

                      xmlhttp.open("GET", "../../controllers/authcontroller.php?opt=" + str, true);
                      xmlhttp.send();
                    }

                  }

                  function reset_text() {
                    document.getElementById('mymail').value = "";
                    document.getElementById("data").innerHTML = "";
                  }
                </script>
            </head>
            <body data-spy="scroll" data-target=".fixed-top">
               <?php require_once "../../../wsp.php";?>
                   <!-- Preloader --> 
                   <div class="spinner-wrapper">
                       <div class="spinner">
                           <div class="bounce1"></div>
                           <div class="bounce2"></div>
                           <div class="bounce3"></div>
                       </div>
                   </div>
                   <!-- end of preloader -->
                   

                   <!-- Navigation -->
                  <?php include_once "nav.php";?>


                <!-- Header -->
                <header id="header" class="ex-2-header">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <h1>Iniciar sesión</h1>
                               <p>Bienvenid@ a IDentiKIT.app</p> 
                                <!-- Sign Up Form -->
                                <div class="form-container">
                                    <?php include '../../../config/check_reply.php'; ?>                            
                                    <form name="frm" action="../../controllers/authcontroller.php?Newauth" method="POST" >
                                        <div class="form-group">
                                            <input type="email" class="form-control-input" name="email" required>
                                            <label class="label-control" >Email</label>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <input type="password" class="form-control-input" name="password" required>
                                            <label class="label-control" >Contraseña</label>
                                            <div class="help-block with-errors"></div>
                                        </div>

                                        <div class="form-group">
                                            <button type="submit" class="form-control-submit-button">Iniciar Sesión</button>
                                        </div>

                                        <div class="form-message">
                                            <div id="lmsgSubmit" class="h3 text-center hidden"></div>
                                        </div>
                                  </form>
                                </div> <!-- end of form container -->
                                <!-- end of sign up form -->

                            </div> <!-- end of col -->
                        </div> <!-- end of row -->
                    </div> <!-- end of container -->
                </header> <!-- end of ex-header -->
                <!-- end of header -->


                <!-- Scripts -->
                <script src="https://identikit.app/js/jquery.min.js"></script> <!-- jQuery for Bootstrap's JavaScript plugins -->
                <script src="https://identikit.app/js/popper.min.js"></script> <!-- Popper tooltip library for Bootstrap -->
                <script src="https://identikit.app/js/bootstrap.min.js"></script> <!-- Bootstrap framework -->
                <script src="https://identikit.app/js/jquery.easing.min.js"></script> <!-- jQuery Easing for smooth scrolling between anchors -->
                <script src="https://identikit.app/js/swiper.min.js"></script> <!-- Swiper for image and text sliders -->
                <script src="https://identikit.app/js/jquery.magnific-popup.js"></script> <!-- Magnific Popup for lightboxes -->
                <script src="https://identikit.app/js/validator.min.js"></script> <!-- Validator.js - Bootstrap plugin that validates forms -->
                <script src="https://identikit.app/js/scripts.js"></script> <!-- Custom scripts -->
                <script src="https://identikit.app/js_whatsapp.js" type="text/javascript"></script>

                <script src="../../../public/complements/v1/vendors/scripts/script.min.js"></script>
                <script src="../../../public/complements/v1/vendors/scripts/process.js"></script>

            </body>
            </html>