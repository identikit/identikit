<?php 
  include '../../../config/settings.php';
  include '../../../config/check-newlogin.php';

  if (isset($_GET['token'])) {
    $token = $_GET['token'];  
  }else{
    header("location:./");
  }

?>  
<!DOCTYPE html>
<html>
<head>
  <!-- Basic Page Info -->
  <meta charset="utf-8">
  <title>IDentiKIT - Restablecer tu contraseña</title>

  <!-- Site favicon -->
  <link href="logo.png" rel="icon" type="image/png">

  <!-- Mobile Specific Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

  <!-- Google Font -->
  <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
  <!-- CSS -->
  <link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/core.css">
  <link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/icon-font.min.css">
  <link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/style.css">

    <script type="text/javascript">
     function update(str)
     {

    if(document.getElementById('mymail').value == "")
     {
    alert("Porfavor Ingresa tu email");

      }else{
        document.getElementById("data").innerHTML = "Please wait...";
        var xmlhttp;

        if (window.XMLHttpRequest)
        {
          xmlhttp=new XMLHttpRequest();
        }
        else
        {
          xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        } 

        xmlhttp.onreadystatechange = function() {
          if(xmlhttp.readyState == 4 && xmlhttp.status == 200)
          {
            document.getElementById("data").innerHTML = xmlhttp.responseText;
          }
        }

        xmlhttp.open("GET","app/reset-pw.php?opt="+str, true);
        xmlhttp.send();
  }

    }
    
     function reset_text()
     {  
     document.getElementById('mymail').value = "";
     document.getElementById("data").innerHTML = "";
     }

  </script>

  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-PHBQLSG');</script>
  <!-- End Google Tag Manager -->
</head>
<body>

   <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PHBQLSG"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->

  <div class="login-header box-shadow">
    <div class="container-fluid d-flex justify-content-between align-items-center">
      <div class="brand-logo">
        <a href="principal.php">
          <img src="logo.png" alt="" width="50">
        </a>
      </div>
      
    </div>
  </div>
  <div class="login-wrap d-flex align-items-center flex-wrap justify-content-center">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-3">
          <img src="" alt="">
        </div>
        <div class="col-md-6">
          <div class="login-box bg-white box-shadow border-radius-10">
            <div class="login-title">
              <h2 class="text-center text-primary">Reestablece tu cuenta</h2>
            </div>
            <p class="mb-20 text-muted">Estas en un ambiente seguro y cerrado para restablecer tu cuenta</p>

            <form name="frm" action="../../controllers/authcontroller.php?changeNewPassLogin" method="POST">

            <?php include '../../../config/check_reply.php'; ?> 
              <?php
                
                try {
                  $tokens = $db->query("SELECT * FROM tbl_tokens WHERE token = :token limit 1", 
                    array(
                      "token" => $token
                    ));

                  $res = count($tokens);
                
                  if ($res == "0") {
                    print '
                      <div class="alert alert-warning">
                        <strong>Este token de seguridad no se puede usar por algunas de estas razones</strong><br>
                        <li>El token es invalido</li>
                        <li>El token expiro</li>
                      </div>';

                 $invalid_token = true;
                 
                }else{
                  foreach($tokens as $token){
                  ?>
              <div class="input-group custom">
                <input type="password" class="form-control form-control-lg" placeholder="Nueva contraseña" name="password" id="myInput">
                <div class="input-group-append custom">
                  <span class="input-group-text"><i class="dw dw-padlock1"></i></span>
                </div>
              </div>
              <div class="input-group custom">
                <input type="password" class="form-control form-control-lg" placeholder="Confirmar nueva contraseña" name="confirmpassword" id="myInput2">
                <div class="input-group-append custom">
                  <span class="input-group-text"><i class="dw dw-padlock1"></i></span>
                </div>

              </div>

                <?php
                  $_SESSION['resetmail'] = $token['email'];
                ?>
                <?php
                
 
                              }
                }

            
                              }catch(PDOException $e)
                                {
           
                                }
  
  
                                ?>
  

                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                      <div class="custom-control custom-checkbox mb-5">
                                        <input type="checkbox" onclick="myFunction() "  class="custom-control-input" id="customCheck1">
                                        <label class="custom-control-label" for="customCheck1">Ver contraseña</label>
                                      </div>
                                    </div>
                                  </div>
                                </div>
              <div class="row align-items-center">
                <div class="col-5">
                  <div class="input-group mb-0">

                    <!--
                      use code for form submit
                      <input class="btn btn-primary btn-lg btn-block" type="submit" value="Submit">
                    -->
                    <center><button type="submit" class="btn btn-primary btn-sm-12 col-md-12">Reestablecer</button></center>
                  </div>

                                  <?php
                  if (isset($invalid_token)) {
                    
                  }else{
                  print '
                  <div class="modal-footer text-center">
                  </div>';  
                  }

                  ?>
                  <script>
                  function myFunction() {
                    var x = document.getElementById("myInput");
                    if (x.type === "password") {
                      x.type = "text";
                    } else {
                      x.type = "password";
                    }
                    var x2 = document.getElementById("myInput2");
                    if (x2.type === "password") {
                      x2.type = "text";
                    } else {
                      x2.type = "password";
                    }
                  }

                 
                  </script>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- js -->
  <script src="../../../public/complements/v1/vendors/scripts/core.js"></script>
  <script src="../../../public/complements/v1/vendors/scripts/script.min.js"></script>
  <script src="../../../public/complements/v1/vendors/scripts/process.js"></script>
  <script src="../../../public/complements/v1/vendors/scripts/layout-settings.js"></script>
</body>
</html>