<?php require_once "../../../ad.php"; ?>
 <nav class="navbar navbar-expand-lg navbar-dark navbar-custom fixed-top">
        <div class="container">

            <!-- Image Logo -->
            <a class="navbar-brand logo-image" href="index.php"><img src="https://identikit.app/images/logo-og.png" alt="alternative" style="width: 60px; border-radius: 10px;"></a> 
            
            <!-- Mobile Menu Toggle Button -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-awesome fas fa-bars"></span>
                <span class="navbar-toggler-awesome fas fa-times"></span>
            </button>
            <!-- end of mobile menu toggle button -->

            <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="https://identikit.app/index.php#header">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="https://identikit.app/empresas.php">Empresas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="https://identikit.app/app/views/employee/jobs.php">Vacantes</a>
                    </li>

                </ul>
                <span class="nav-item">
                    <a class="btn-outline-sm-2" href="https://identikit.app/apply.php" style="text-decoration: none;">Aplicar en IDentiKIT</a>
                </span>
                <span class="nav-item">
                    <a class="btn-outline-sm" href="https://identikit.app/app/views/auth/login-new.php">Iniciar sesión</a>
                </span>
            </div>
        </div> <!-- end of container -->
    </nav> <!-- end of navbar -->
    <!-- end of navigation -->