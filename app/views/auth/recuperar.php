<!DOCTYPE html>
<html>

<head>
	<!-- Basic Page Info -->
	<meta charset="utf-8">
	<title>IDentiKIT - Generar contraseña</title>

	<!-- Site favicon -->
	<link rel="apple-touch-icon" sizes="180x180" href="logo.png">
	

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="../../../public/complements/v1/vendors/styles/style.css">


	<script type="text/javascript">
	//    function update(str)
	//    {

	// 	if(document.getElementById('mymail').value == "")
	//    {
	// 	alert("Porfavor Ingresa tu email");

	//     }else{
	// 		  document.getElementById("data").innerHTML = "Please wait...";
	//       var xmlhttp;

	//       if (window.XMLHttpRequest)
	//       {
	//         xmlhttp=new XMLHttpRequest();
	//       }
	//       else
	//       {
	//         xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	//       }	

	//       xmlhttp.onreadystatechange = function() {
	//         if(xmlhttp.readyState == 4 && xmlhttp.status == 200)
	//         {
	//           document.getElementById("data").innerHTML = xmlhttp.responseText;
	//         }
	//       }

	//       xmlhttp.open("GET","../../controllers/authcontroller?opt="+str, true);
	//       xmlhttp.send();
	// }

	//   }
	  
	//    function reset_text()
	//    {  
	//    document.getElementById('mymail').value = "";
	//    document.getElementById("data").innerHTML = "";
	//    }

	</script>
</head>

<body>
<div class="login-header box-shadow">
    <div class="container-fluid d-flex justify-content-between align-items-center">
      <div class="brand-logo">
        <a href="principal.php">
          <img src="logo.png" alt="" width="50">
        </a>
      </div>
      
    </div>
	</div>
	<div class="login-wrap d-flex align-items-center flex-wrap justify-content-center">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-md-3">
					<img src="" alt="">
				</div>
				<div class="col-md-6">
					<div class="login-box bg-white box-shadow border-radius-10">
						<div class="login-title">
							<h2 class="text-center text-primary">Generar contraseña</h2>
						</div>
						<p class="mb-20 text-muted">Ingresa tu correo electrónico para buscar tu cuenta.</p>

	   					<?php
						if (isset($_GET["r"]) == "3091") {
							print '
							<div class="alert alert-info">
								Te Enviamos un correo con instrucciones para crear la cuenta. (Si no encuentras el email por favor revisa la carpeta de spam)
							</div>';
						}
					  ?>

						<form name="frm" method="GET" action="../../controllers/authcontroller.php?ResetPw" >
						
							<div class="input-group custom">
								<input type="text" class="form-control form-control-lg" placeholder="Email" required name="email" id="mymail">
								<div class="input-group-append custom">
									<span class="input-group-text"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
								</div>
							</div>
							<div class="row align-items-center">
								<div class="col-12">
									<div class="input-group mb-0">
										<!--
											use code for form submit
											<input class="btn btn-primary btn-lg btn-block" type="submit" value="Submit">
										-->
										<button class="btn btn-primary btn-lg btn-block" type="submit">Enviar</button>
									</div>
								</div>
								
								
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- js -->
	<script src="../../../public/complements/v1/vendors/scripts/core.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/script.min.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/process.js"></script>
	<script src="../../../public/complements/v1/vendors/scripts/layout-settings.js"></script>
</body>

</html>