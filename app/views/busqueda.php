﻿<!DOCTYPE html>
<html>

<?php 
	
	require '../../config/settings.php';
	require_once("../../db/db.php");
	require 'employee/constants/check-login.php';


	$db = new DbPDO();

	$fromsearch = false;

	if (isset($_GET['search']) && $_GET['search'] == "✓") {

	}else{

	}

	if (isset($_GET['page'])) {
		$page = $_GET['page'];
	if ($page=="" || $page=="1"){
		$page1 = 0;
		$page = 1;
	}else{
		$page1 = ($page*16)-16;
	}}else{
		$page1 = 0;
		$page = 1;	
	}

	if (isset($_GET['country']) && ($_GET['category']) ){
		$cate = $_GET['category'];
		$country = $_GET['country'];	
		$query1 = "SELECT * FROM tbl_jobs WHERE category = :cate AND country = :country ORDER BY enc_id DESC";
		$query2 = "SELECT * FROM tbl_jobs WHERE category = :cate AND country = :country ORDER BY enc_id DESC";
		$fromsearch = true;

		$slc_country = "$country";
		$slc_category = "$cate";
		$title = "$slc_category empleos en $slc_country";
	}else{
		$query1 = "SELECT * FROM tbl_jobs ORDER BY enc_id DESC ";
		$query2 = "SELECT * FROM tbl_jobs ORDER BY enc_id DESC";	
		$slc_country = "NULL";
		$slc_category = "NULL";	
		$title = "Lista de Empleos";
	}


	if ($myrole == 'employee'){
		require 'employee/constants/check-login.php';
	}else {
		require 'employee/constants2/check-login1.php';
		
	}

?>


<head>
	<!-- Basic Page Info -->
	<meta charset="utf-8">

	<?php
	$busqueda = $_POST['PalabraClave'];
	?>
	<title>IDentiKIT - Resultados para <?php echo $busqueda;?> </title>

	<link rel="apple-touch-icon" sizes="180x180" href="logov3.png">
	<link rel="icon" type="image/png" sizes="32x32" href="logov3.png">
	<link rel="icon" type="image/png" sizes="16x16" href="logov3.png">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="../../public/complements/v1/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="../../public/complements/v1/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="../../public/complements/v1/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../../public/complements/v1/src/plugins/datatables/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../../public/complements/v1/vendors/styles/style.css">


	<script type="text/javascript">
		function update(str)
		{
			
			var txt;
			var r = confirm("Are you sure you want to apply this job , you can not UNDO");
			if (r == true) {
				document.getElementById("data").innerHTML = "Please wait...";
				var xmlhttp;

			if (window.XMLHttpRequest)
			{
				xmlhttp=new XMLHttpRequest();
			}
			else
			{
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}	

			xmlhttp.onreadystatechange = function() {
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
				document.getElementById("data").innerHTML = xmlhttp.responseText;
				}
			}

			xmlhttp.open("GET","app/apply-job.php?opt="+str, true);
			xmlhttp.send();
			} else {

			}

		}
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-K2NHRZT4R7"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-K2NHRZT4R7');
</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5QGSH5J');</script>
<!-- End Google Tag Manager -->



</head>
<body>

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QGSH5J"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<div class="header">
		<div class="header-left">
			<div class="menu-icon icon-copy ti-menu" style="color: white;"></div>

			<div class="search-toggle-icon dw dw-search2" style="color: #ffde00;" data-toggle="header_search"></div>
			
			<div class="header-search">
				<form method="post" action="../busqueda.php">
					<div class="form-group mb-0">
						<i class="dw dw-search2 search-icon" style="color: #ffde00;"></i>
						<input type="text" class="form-control search-input" placeholder="Buscar en IDentiKIT" required name="PalabraClave">
						<input name="buscar" type="hidden" class="form-control mb-2" id="inlineFormInput" value="v">
					</div>
				</form>
			</div>
		</div>
		<div class="header-right">
			<?php
				if(isset($_SESSION["myid"])){
					require_once "employee/alerts.php"; 
				} 
			?>
			<div class="user-info-dropdown">
				<div class="dropdown">
					<a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" style="color: #ffde00;">
						<span class="user-icon">
							<?php
								if(isset($myavatar)){
									if ($myavatar == null) {
										print '<center><img  src="../../public/img/identikit/logo.png" title="' . $myfname . '" alt="image"  /></center>';
									} else {
										echo '<center><img alt="image" title="' . $myfname . '"  src="data:image/png;base64,' . base64_encode($myavatar) . '"/></center>';
									}
								} else {
									print '<center><img  src="../../public/img/identikit/logo.png" title="logo" alt="image"  /></center>';
								}
							?>
						</span>
					</a>
					<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
						<?php
							if ($user_online == true) {
								print '
									<a class="dropdown-item" href="miperfil.php"><i class="dw dw-user1"></i> Perfil</a>
									<!--<a class="dropdown-item" href="#"><i class="dw dw-settings2"></i> Ajustes</a>
									<a class="dropdown-item" href="#"><i class="dw dw-help"></i> Ayuda</a>-->
									<a class="dropdown-item" href="../controllers/employeecontroller.php?logout"><i class="dw dw-logout"></i> Salir</a>';
							} else {
								print '
									<a class="dropdown-item" href="../auth/login.php"><i class="dw dw-user1"></i> Ingresar</a>
									<a class="dropdown-item" href="../auth/registro.php"><i class="dw dw-user1"></i> Registrate</a>
								';
							}
						?>
					</div>
				</div>
			</div>
		</div>
	</div> 

	<div class="left-side-bar">
		<div class="brand-logo">
			<a href="#">
				<img src="../../public/img/identikit/logo.png" alt="" class="light-logo" width="60"><span class="mtext" style="margin:10px;">IDentiKIT</span>
			</a>
			<div class="close-sidebar" data-toggle="left-sidebar-close">
				<i class="ion-close-round"></i>
			</div>
		</div>
		<div class="menu-block customscroll">
			<div class="sidebar-menu">
				<ul id="accordion-menu">
					<?php
						if(isset($myrole)){
							if ($myrole == "employee") {
								print '<a href="employee/index.php" class="dropdown-toggle no-arrow ">
								<span class="micon icon-copy fa fa-home"></span><span class="mtext">Inicio</span></a>

								<li class="dropdown">
								<a href="javascript:;" class="dropdown-toggle">
									<span class="micon icon-copy fa fa-bandcamp"></span><span class="mtext">Explorar</span>
								</a>
								<ul class="submenu">
									<li><a href="employee/identis.php" > IDentis</a></li>
									<li><a href="employee/works.php"> Trabajos</a></li>
									<li><a href="employee/empresas.php"> Empresas</a></li>
								</ul>
								</li>

									<li>
								<a href="employee/aplicados.php" class="dropdown-toggle no-arrow">
									<span class="micon ion-android-done-all"></span><span class="mtext">Aplicados</span>

								</a>
							</li>
							<li>
								<a href="employee/chat.php" class="dropdown-toggle no-arrow">
								<span class="micon icon-copy fa fa-comment"></span>
								<span class="mtext">Chat</span></a>
							</li>
							<li>
								<a href="employee/academy.php" class="dropdown-toggle no-arrow">
									<span class="micon icon-copy fa fa-free-code-camp"></span><span class="mtext">Academia</span>
								</a>
							</li>
							<li>
								<a href="employee/videocv.php" class="dropdown-toggle no-arrow">
									<span class="micon icon-copy fa fa-file-video-o"></span>
									<span class="mtext">Video CV</span>
								</a>
							</li>';
							} 
						} else {
							print '<a href="auth/login.php" class="dropdown-toggle no-arrow ">
							<span class="micon icon-copy fa fa-lock"></span><span class="mtext">Iniciar sesion</span></a>
							<a href="auth/registro.php" class="dropdown-toggle no-arrow ">
							<span class="micon icon-copy fa fa-user"></span><span class="mtext">Registrarme</span></a>';
						}
					?>					
				</ul>
			</div>
		</div>
	</div>




	<div class="mobile-menu-overlay"></div>

	<style> 

		@media (max-width: 767px){
			.main-container {
				padding-top: 0px; 
			}
		}

	</style>


<?php
 
if(!empty($_POST))

      $aKeyword = explode(" ", $_POST['PalabraClave']);

      $query ="SELECT * FROM tbl_jobs WHERE title like '%" . $aKeyword[0] . "%' OR city like '%" . $aKeyword[0] . "%'";
      $query2 ="SELECT * FROM tbl_users WHERE first_name like '%" . $aKeyword[0] . "%' OR last_name like '%" . $aKeyword[0] . "%'";

     for($i = 1; $i < count($aKeyword); $i++) {
        if(!empty($aKeyword[$i])) {
            $query .= " OR last_name like '%" . $aKeyword[$i] . "%'";

            $query2 .= " OR last_name like '%" . $aKeyword[$i] . "%'";
        }
      }
      
	date_default_timezone_set("America/Argentina/Buenos_Aires");

	$search_date        = date("Y/m/d");
	$search_hour        = date("h:i:sa");
	$idSession          = '';
	
	if(trim($busqueda) != ''){
		if (isset($_SESSION["myid"])){
			$idSession      = $_SESSION['myid'];
		} else {
			if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
				$ip = $_SERVER['HTTP_CLIENT_IP'];
			} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
				$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			} else {
				$ip = $_SERVER['REMOTE_ADDR'];
			}
			$idSession      = $ip;
		}
		
		$busquedainsert = "insert into tbl_search (search_user_id,search,search_date,search_hour) VALUES('".$idSession."','".$busqueda."','".$search_date."','".$search_hour."')";	
		$db->query($busquedainsert);
	}

	$result = $db->query($query);
                     
     if(count($result) > 0) {
        $row_count=0;
        foreach($result as $row) {   
            $row_count++;                         

			echo '
			
			<div class="main-container">
		<div class="pd-ltr-20 xs-pd-20-10">
		<br><br><br>
			<div class="min-height-200px">
				
				<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
					

				<div class="product-wrap">
					<div class="product-list">

			<ul class="row">
			</div>
			<div class="product-caption">
			<h4><a href="#">🔥 '.strip_tags($row["title"]).'</a></h4>
			<div class="text-success" style="margin-top: 6px;">📍 <strong>'. strip_tags($row['city']) .' </strong></i> </div>
			<div class="text-success" style="margin-top: 6px;">📙 '. strip_tags($row["type"]).' </i> </div>
			<div class="price">
			<p class="card-text" style="margin-top: 6px;"> '. strip_tags(substr($row["description"],0,210)).' </p>
			<a href="employee/work-detail.php?identiwork='.$row["job_id"].'" class="btn btn-primary col-md-12" style="background-color: #01c0fe; border: none;">Postularme</a>
			</div>
			</div>
			</div>
			</li>
			</ul>
			</div>
					
				</div>
			</div>
				</div>
			</div>
			
		</div>
	</div>'; 

        }
	
    }
    else {
        echo '
        	<div class="main-container">
        	<div class="pd-ltr-20 xs-pd-20-10">
        	<br><br><br>
        		<div class="min-height-200px">
        			
        			<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
        				

        			<div class="product-wrap">
        				<div class="product-list">

        		<ul class="row">
        		</div>
        		<div class="product-caption">
        		<h4>No hay resultados para tu busqueda</h4>
        		
        		</div>
        		</div>
        		</div>
        		</li>
        		</ul>
        		</div>
        				
        			</div>
        		</div>
        			</div>
        		</div>
        		
        	</div>
        </div>';
		
    }

?>

	<!-- js -->
	<script src="../../public/complements/v1/vendors/scripts/core.js"></script>
	<script src="../../public/complements/v1/vendors/scripts/script.min.js"></script>
	<script src="../../public/complements/v1/vendors/scripts/process.js"></script>
	<script src="../../public/complements/v1/vendors/scripts/layout-settings.js"></script>
</body>
</html>