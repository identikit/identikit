<!DOCTYPE html>
<html lang="es">
<title>DuHire - 🚀 Contrata talento IT en una sola APP | Blog</title>

<meta charset="UTF-8">

<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="entradas/style2.css">

<link href="../assets/css/style.css" rel="stylesheet">

<link href='https://fonts.googleapis.com/css?family=Rubik' rel='stylesheet' type='text/css' />

<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel='stylesheet' type='text/css' />

<link href="https://fonts.googleapis.com/css?family=Ubuntu" rel='stylesheet' type='text/css'/>

<link href="../assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<link href="../assets/vendor/icofont/icofont.min.css" rel="stylesheet">

<link href="../assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">

<link href="../assets/vendor/venobox/venobox.css" rel="stylesheet">

<link href="../assets/vendor/aos/aos.css" rel="stylesheet">

<style type="text/css">
  a :hover{
    text-decoration: none!important;
    color: #fa8100 ;

  }

  a {
    text-decoration: none !important;
  }
</style>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-107494114-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-107494114-1');
</script>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KWSXZF9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<body class="w3-light-grey">


<!-- ======= Menu ======= -->
  <header id="header">
    
    <div class="container d-flex" style="margin-top: -20px;">
      <div class="logo mr-auto" >
        <span><a href="../" style="color: black; font-size: 30px; font-family: Ubuntu; max-width: 45px;"><img src="logo.png" alt="" class="img-fluid" ></span></a>
      </div>

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li><a href="../">Inicio</a></li>
          <li><a href="../empresas/">Contrata talento</a></li>
          <li><a href="../recruiters/">Recluta freelance</a></li>
          <li class="active"><a href="#">Blog</a></li>
          <li class="get-started"><a href="https://app.duhire.com" style="right: -50px;">Ingresar</a></li>
        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- Menu -->



<div class="w3-content" style="max-width:1400px">
<br><br><br>

<header class="w3-container w3-center w3-padding-32"> 
  <h1><b>BLOG</b></h1>
  <p><span class="w3-tag">DuHire</span></p>
</header>

<div class="w3-row">

<div class="w3-col l8 s12">
  <?php

    require ("admin/config/config.php");

    //$hoy = $conexion->query("SELECT CURDATE()");
    $entradas = $conexion->query("SELECT * FROM academy WHERE fecha < now() AND active = 1 ORDER BY id DESC");
    while ($row = $entradas->fetch_assoc()) { 
    ?>

  <div class="w3-card-4 w3-margin w3-white">
    
    <a href="entradas/post.php?slug=<?php echo ($row['slug']);?>"><img src="images/<?php echo ($row['imagen']);?>" alt="DuHire Imagen" style="width:100%"></a>
    
    <div class="w3-container" style="margin-top: 20px;">
     
      <h3><b><?php echo ($row['titulo']);?>  </b></h3>
      
    </div>

    <div class="w3-container">
     
      <p><?php echo strip_tags(substr($row['contenido'], 0, 110))?>...</p>
      
      <div class="w3-row">
       
        <div class="w3-col m8 s12">
          
          <p><a href="entradas/post.php?slug=<?php echo ($row['slug']);?>">
            <button class="w3-button w3-padding-large w3-white w3-border"><b>Leer Más »</b></button></a></p>
        </div>
        
      </div>

    </div>

  </div><?php } ?>
  
  <hr>

 
</div>

<div class="w3-col l4" >
  
  <hr>
  
  <div class="w3-card w3-margin"  >
  
    <div class="w3-container w3-padding">
  
      <h4>Especialmente para vos</h4>
  
    </div>
    
    <?php

    require ("admin/config/config.php");

    $entradas = $conexion->query("SELECT * FROM academy WHERE fecha < now() AND active = 1 ORDER BY fecha ASC LIMIT 0,3");
    while ($row = $entradas->fetch_assoc()) { 
    ?>
    
    <ul class="w3-ul w3-hoverable w3-white">
    
      <a href="entradas/post.php?slug=<?php echo ($row['slug']);?>"><li class="w3-padding-16">
    
        <img src="images/<?php echo ($row['imagen']);?>" alt="DuHimage" class="w3-left w3-margin-right" style="width:50px">
    
        <span class="w3-large"><?php echo strip_tags(($row['titulo']));?> </span><br>
    
      
    
      </li>
    </a>

    </ul>

<?php } ?>

  </div>

  <hr> 
 
  <!-- Labels / tags 
  <div class="w3-card w3-margin">
    <div class="w3-container w3-padding">
      <h4>Tags</h4>
    </div>
    <div class="w3-container w3-white">
    <p><span class="w3-tag w3-black w3-margin-bottom">Travel</span> <span class="w3-tag w3-light-grey w3-small w3-margin-bottom">New York</span> <span class="w3-tag w3-light-grey w3-small w3-margin-bottom">London</span>
      <span class="w3-tag w3-light-grey w3-small w3-margin-bottom">IKEA</span> <span class="w3-tag w3-light-grey w3-small w3-margin-bottom">NORWAY</span> <span class="w3-tag w3-light-grey w3-small w3-margin-bottom">DIY</span>
      <span class="w3-tag w3-light-grey w3-small w3-margin-bottom">Ideas</span> <span class="w3-tag w3-light-grey w3-small w3-margin-bottom">Baby</span> <span class="w3-tag w3-light-grey w3-small w3-margin-bottom">Family</span>
      <span class="w3-tag w3-light-grey w3-small w3-margin-bottom">News</span> <span class="w3-tag w3-light-grey w3-small w3-margin-bottom">Clothing</span> <span class="w3-tag w3-light-grey w3-small w3-margin-bottom">Shopping</span>
      <span class="w3-tag w3-light-grey w3-small w3-margin-bottom">Sports</span> <span class="w3-tag w3-light-grey w3-small w3-margin-bottom">Games</span>
    </p>
    </div>
  </div>
 -->

</div>

</div>
<br>

</div>

<?php require_once "../vistas/chat.php"; ?>
<?php require "../vistas/footer.php";?> 
</body>

<script src="../assets/vendor/jquery/jquery.min.js"></script>
<script src="../assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../assets/vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="../assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
<script src="../assets/vendor/venobox/venobox.min.js"></script>
<script src="../assets/vendor/aos/aos.js"></script>
<script src="../assets/js/main.js"></script>
</html>
