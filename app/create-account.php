<?php
date_default_timezone_set('America/Argentina/Buenos_Aires');

use PHPMailer\PHPMailer\PHPMailer;

require_once "../PHPMailer/PHPMailer.php"; 
require_once "../PHPMailer/SMTP.php"; 
require_once "../PHPMailer/Exception.php"; 

if (isset($_POST['reg_mode'])) {
checkemail();	
}else{
header("location:../");		
}

function checkemail() {
	
try {
	require '../constants/db_config.php';
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$email = $_POST['email'];
	$account_type = $_POST['acctype'];
	
    $stmt = $conn->prepare("SELECT * FROM tbl_users WHERE email = :email");
	$stmt->bindParam(':email', $email);
    $stmt->execute();
    $result = $stmt->fetchAll();
	$records = count($result);
	
	if ($account_type == "101") {
	$role = "Employee";	
	}else{
	$role = "Employer";	
	}
	
	if ($records > 0) {
	header("location:../registro1.php?p=$role&r=0927");	
		
	}else{
	
	if ($account_type == "101") {
	register_as_employee();
	}else{
	register_as_employer();
	}
	
		
	}
					  
	}catch(PDOException $e)
    {
    header("location:../login.php?p=$role&r=4568");	
    }
}

function register_as_employee() {

try {
	require '../constants/db_config.php';
	require '../constants/uniques.php';
	$role = 'employee';
    $account_type = $_POST['acctype'];
    $empujar = $_POST['empujar'];
    $last_login = date('d-m-Y h:m A [T P]');
	$member_no = 'EM'.get_rand_numbers(9).'';
    $fname = ucwords($_POST['fname']);
    $lname = ucwords($_POST['lname']);
    $email = $_POST['email'];
    $login = md5($_POST['password']);
	
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $stmt = $conn->prepare("INSERT INTO tbl_users (first_name, last_name, email, last_login, login, role, member_no, empujar) 
	VALUES (:fname, :lname, :email, :lastlogin, :login, :role, :memberno, :empujar)");
    $stmt->bindParam(':fname', $fname);
    $stmt->bindParam(':lname', $lname);
    $stmt->bindParam(':email', $email);
	$stmt->bindParam(':lastlogin', $last_login);
    $stmt->bindParam(':login', $login);
    $stmt->bindParam(':role', $role);
	$stmt->bindParam(':memberno', $member_no);
    $stmt->bindParam(':empujar', $empujar);


    ## aca se supone que """ envia """"""" el mail ###

    $user = $fname . " " . $lname;
    SendMail($email, $user);

    $stmt->execute();
	
	header("location:../login.php?p=identi&r=1123");				  
	}catch(PDOException $e)
	

    {
     header("location:../login.php?p=identi&r=4568");
    }	
	
}

function SendMail($email, $user){
    $mail = new PHPMailer(); 

    $mail->isSMTP(); 
    $mail->Host = 'smtp.hostinger.com';
    $mail->SMTPAuth = true; 
    $mail->Username = "hola@idkit.com.ar"; 
    $mail->Password = "bB*TX27gR#Fs"; 
    $mail->Port = 465; 
    $mail->SMTPSecure = "ssl"; 
    $mail->CharSet = 'UTF-8';
    
    $mail->isHTML(true); 
    $mail->setFrom("hola@idkit.com.ar","<no reply>");
    $mail->addAddress($email); 
    $mail->Subject = ("Bienvenido a IDentiKIT"); 
    $mail->AddEmbeddedImage('../images/Bienvenida.png', 'IBienvenida', 'attachment', 'base64', 'image/png');
    
    $mail->Body = "    
        <center>
            <img src='cid:ILogo' class='img-logo' width='100' height='100' />
            <h1 style='padding:0 0 10px 0;width:100%;text-align:center;font-family:Arial;font-size:22px;font-weight:bold;line-height:28px;'>¡Bienvenido/a, " .$user. " !</h1>
            <p style='padding:0 0 25px 0;width:100%;text-align:center;font-family:Arial;font-size:16px;line-height:23px;'>
                ¡Te damos la bienvenida a IDentiKIT, ahora puedes postularte en las mejores empresas tecnologicas y obtener tu primera experiencia laboral.¡Recuerda completar tu perfil para que nuestra plataforma pueda ayudarte!.
            </p>
            <a href='https://idkit.com.ar/login.php' style='padding:0.6em 2em;border-radius: 40px;color:#fff;font-size:1.1em; border:0;cursor:pointer;margin:1em; background-color:#01c0fe; text-decoration:none'>Iniciar sesión</a>
            <br>
            <br>
            <img src='cid:IBienvenida' class='img-logo' />
        </center>
    ";
    
    
    if (preg_match('/(.*)@(hotmail)\.(.*)/', $email) != false) { 
        $mail->addCustomHeader('Mime-Version','1.0');
        $mail->addCustomHeader('Content-Type: text/html; charset=ISO-8859-1');
    }   

    
    if($mail->send()){
        return;
    } else {
        $status = "failed";
        $response = "Something is wrong: <br>";
    }
    
    
    exit(json_encode(array("status" => $status, "response" => $response)));
    
}

function register_as_employer() {
try {
	require '../constants/db_config.php';
	require '../constants/uniques.php';
	$role = 'employer';
    $account_type = $_POST['acctype'];
    $last_login = date('d-m-Y h:m A [T P]');
    $comp_no = 'CM'.get_rand_numbers(9).'';
    $cname = ucwords($_POST['company']);
    $ctype = ucwords($_POST['type']);
    $email = $_POST['email'];
    $login = md5($_POST['password']);
	
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $stmt = $conn->prepare("INSERT INTO tbl_users (first_name, title, email, last_login, login, role, member_no) 
	VALUES (:fname, :title, :email, :lastlogin, :login, :role, :memberno)");
    $stmt->bindParam(':fname', $cname);
    $stmt->bindParam(':title', $ctype);
    $stmt->bindParam(':email', $email);
	$stmt->bindParam(':lastlogin', $last_login);
    $stmt->bindParam(':login', $login);
    $stmt->bindParam(':role', $role);
	$stmt->bindParam(':memberno', $comp_no);
    $stmt->execute();
	header("location:../login.php?p=empresa&r=1123");				  
	}catch(PDOException $e)
    {
    header("location:../registro2.php?p=empresa&r=4568");
    }	
	
}

?>


