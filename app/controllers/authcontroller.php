<?php   
    require_once '../models/auth.php';
    $Auth = new Auth();
    
    if (isset($_GET['logout'])) {
        $Auth->logout();
    }

    if (isset($_GET['auth'])) {
        $Auth->Auth($_REQUEST);
    }

    if (isset($_GET['Newauth'])) {
        $Auth->Newauth($_REQUEST);
    }

    if (isset($_GET['CreateAccountEmployee'])) {
        $Auth->checkEmail($_REQUEST);
    }

    if (isset($_GET['CreateAccountEmployer'])) {
        $Auth->checkEmail($_REQUEST);
    }

    if (isset($_GET['email'])) {
        $Auth->ResetPw($_GET['email']);
    }
    
    if (isset($_GET['changePass'])) {
        $Auth->ChangePass($_REQUEST);
    }
    if (isset($_GET['changeNewPassLogin'])) {
        $Auth->changeNewPassLogin($_REQUEST);
    }
?>