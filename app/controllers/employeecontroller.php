<?php    

    require_once '../models/employee.php';
    $employee = new employee();
    
    // RECIVE VALUE AND CALL THE CORRECT FUNCTION
    if (isset($_POST['mensaje'])) {
        $result = $employee->agregarconversacion();
        echo json_encode($result);
    }

    //Actualiza perfil
    if (isset($_GET['UpdateProfile'])){
        $employee->UpdateProfile($_REQUEST);
    }

    //Agrega experiencia
    if (isset($_GET['AddExperience'])){
        $employee->AddExperience($_REQUEST);
    }    

    //Elimina experiencia 
    if (isset($_GET['DropExperience'])){
        $employee->DropExperience($_GET['DropExperience']);
    } 

    //New dp
    if (isset($_GET['NewDp'])){
        $employee->NewDp($_REQUEST);
    }  

    if (isset($_GET['UpdatePerfilv2'])){
        $employee->UpdatePerfilv2($_REQUEST);
    }  

    //Actualiza experiencia laboral
    if (isset($_GET['UpdateExperience'])){
        $employee->UpdateExperience($_REQUEST);
    }    

    //Actualiza experiencia laboral
    if (isset($_POST['hiddenId'])){
        $employee->uploadVideoCv($_REQUEST);
    }    

    //Actualiza experiencia laboral
    if (isset($_GET['opt']) && isset($_GET['opt1'])){
        $employee->ApplyJob($_REQUEST);
    }   
    
    
    //Agregar idioma
    // if (isset($_GET['opt']) && isset($_GET['opt1'])){
    //     $employee->ApplyJob($_REQUEST);
    // }    

    if (isset($_POST['ability'])){
        $employee->UpdateTags($_REQUEST);
    }    

    if (isset($_POST['educationtitle'])){
        $employee->AddEducation($_REQUEST);
    }    

    if (isset($_GET['action'])){
        $result = $employee->getAll($_REQUEST);
        echo json_encode($result);
    } 


    
    if (isset($_POST['lang'])){
        $employee->AddLang($_REQUEST);
    }    

    //Elimina Experiencia Laboral
    // if (isset($_GET['DropExperience'])){
    //     $employee->DropJob($_GET['DropExperience']);
    // }

    //Finaliza sesion
    if (isset($_GET['logout'])){
        $employee->logout();
    }

    // AGREGAR PREGUNTA
    // if (isset($_GET['AddPregunta'])){
    //     $employee->AddQuestion($_REQUEST);
    // }

    if (isset($_GET['pregunta'])) {
        $result = $employee->AddQuestion();
        echo json_encode(true);
    }


?>