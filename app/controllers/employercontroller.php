<?php
    
    require_once '../models/employer.php';
    
    $employer = new employer();
    
    // RECIVE VALUE AND CALL THE CORRECT FUNCTION
    if (isset($_POST['conversacion'])) {
        $result = $employer->agregarconversacion();
        echo json_encode($result);
    }

    if (isset($_GET['cambiarStatus'])) {
        $employer->cambiarStatus();
    }

    //Finaliza sesion
    if (isset($_GET['logout'])){
        $employer->logout();
    }

    //Actualiza imagen 
    if (isset($_GET['UpdateImage'])){
        $employer->UpdateImage($_REQUEST);
    }

    //Actualiza perfil 
    if (isset($_GET['UpdateProfile'])){
        $employer->UpdateProfile($_REQUEST);
    }

    //Postea trabajo
    if (isset($_GET['postjob'])){
        $employer->postJob($_REQUEST);
    }

    // Actualiza trabajo
    if (isset($_GET['UpdateJob'])){
        $employer->UpdateJob($_REQUEST);
    }

    //Elimina trabajo
    if (isset($_GET['DropJob'])){
        $employer->DropJob($_GET['DropJob']);
    }

?>





    