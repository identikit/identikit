<?php 
include 'constants/settings.php'; 
include 'constants/check-login.php';
?>
<!DOCTYPE html>
<html>
<head>
  <!-- Basic Page Info -->
  <meta charset="utf-8">
  <title>IDentiKIT - Registro</title>

  <!-- Site favicon -->
  <link href="logov3.png" rel="icon" type="image/png">
  <!-- Mobile Specific Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

  <meta name="description" content="Registrate como IDenti o empresa en IDentiKIT" />
  <meta name="keywords" content="identikit, IDentiKIT, idkit, IDKIT, primera experiencia laboral, trabajos jr, junior, trabajos junior, empresas que contratan juniors, talento joven, talento tech, jovenes, empresas sin experiencia" />
  <meta name="author" content="IDentiKIT">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta property="og:image" content="https://identikit.app/logowebog.png" />
  <meta property="og:image:secure_url" content="https://identikit.app/logowebog.png" />
  <meta property="og:image:type" content="image/png" />
  <meta property="og:image:width" content="300" />
  <meta property="og:image:height" content="300" />
  <meta property="og:image:alt" content="IDentiKIT - Tu primera experiencia laboral" />
  <meta property="og:url" content="https://identikit.app/" />
  <meta property="og:type" content="website" />

  <!-- Google Font -->
  <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
  <!-- CSS -->
  <link rel="stylesheet" type="text/css" href="v1/vendors/styles/core.css">
  <link rel="stylesheet" type="text/css" href="v1/vendors/styles/icon-font.min.css">
  <link rel="stylesheet" type="text/css" href="v1/vendors/styles/style.css">

    <script type="text/javascript">
     function update(str)
     {

    if(document.getElementById('mymail').value == "")
     {
    alert("Porfavor Ingresa tu email");

      }else{
        document.getElementById("data").innerHTML = "Please wait...";
        var xmlhttp;

        if (window.XMLHttpRequest)
        {
          xmlhttp=new XMLHttpRequest();
        }
        else
        {
          xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        } 

        xmlhttp.onreadystatechange = function() {
          if(xmlhttp.readyState == 4 && xmlhttp.status == 200)
          {
            document.getElementById("data").innerHTML = xmlhttp.responseText;
          }
        }

        xmlhttp.open("GET","app/reset-pw.php?opt="+str, true);
        xmlhttp.send();
  }

    }
    
     function reset_text()
     {  
     document.getElementById('mymail').value = "";
     document.getElementById("data").innerHTML = "";
     }

  </script>

  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-K2NHRZT4R7"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-K2NHRZT4R7');
</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5QGSH5J');</script>
<!-- End Google Tag Manager -->



  
</head>
<body class="login-page">

 <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QGSH5J"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


  <div class="login-header box-shadow">
    <div class="container-fluid d-flex justify-content-between align-items-center">
      <div class="brand-logo">
        <a href="principal.php">
          <img src="logo.png" alt="" width="50">
        </a>
      </div>
      
    </div>
  </div>
  <div class="login-wrap d-flex align-items-center flex-wrap justify-content-center">
    <div class="container">
      <div class="row align-items-center">
        
        <div class="col-md-6 col-lg-12">
          <div class="login-box bg-white box-shadow border-radius-10">
            <div class="login-title">
              <center><img src="logo.png" class="text-center text-primary" style="width: 30%; height: 30%;"></center>
            </div> 
            <form>
              <div class="login-title">
              <center><h3>Registro</h3></center>
            </div> 
              <div class="select-role">

                <div class="btn-group btn-group-toggle" data-toggle="buttons">

                 <label class="btn">
                    <a href="registro1.php">
                    <input type="radio" name="options" id="user">
                    <div class="icon"><img src="v1/vendors/images/persona.png" class="svg" alt="" style="color: yellow; "></div>
                    <span>Soy</span>
                    IDenti
                    </a>
                  </label>

                  <label class="btn active">
                    <a href="registro2.php">
                    <input type="radio" name="options" id="admin">
                    <div class="icon"><img src="v1/vendors/images/empresa.png" class="svg" alt="" style="color: yellow; "></div>
                    <span>Soy</span>
                    Empresa
                    </a>
                  </label>
                  
                </div>
              </div>
              
              
              <div class="row">

                <div class="col-sm-12">
                  <div class="font-16 weight-600 pt-10 pb-10 text-center" data-color="#707373">O</div>

                  <div class="input-group mb-0">
                   
                    <a href="login.php" type="submit" class="btn btn-primary btn-sm btn-block">Iniciar Sesión</a>
                  </div>
              
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- js -->
  <script src="v1/vendors/scripts/core.js"></script>
  <script src="v1/vendors/scripts/script.min.js"></script>
  <script src="v1/vendors/scripts/process.js"></script>
  <script src="v1/vendors/scripts/layout-settings.js"></script>
</body>
</html>