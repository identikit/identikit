<!DOCTYPE html>

<?php

require 'constants/settings.php';
require 'employee/constants/check-login.php';

require_once("./config/Db.php");
$db = new DbPDO();

if ($user_online == "true") {
	if ($myrole == "employee") {
	} else {
		header("location:login.php");
	}
} else {
	header("location:login.php");
}
?>

<html>

<head>

	<meta charset="utf-8">
	<title>IDentiKIT - Tus aplicados</title>

	<link rel="apple-touch-icon" sizes="180x180" href="logov3.png">
	<link rel="icon" type="image/png" sizes="32x32" href="logov3.png">
	<link rel="icon" type="image/png" sizes="16x16" href="logov3.png">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link href="logov3.png" rel="icon" type="image/png">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->


	<link rel="stylesheet" type="text/css" href="v1/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="v1/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="v1/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="v1/src/plugins/datatables/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="v1/vendors/styles/style.css">

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-K2NHRZT4R7"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'G-K2NHRZT4R7');
	</script>

	<!-- Google Tag Manager -->
	<script>
		(function(w, d, s, l, i) {
			w[l] = w[l] || [];
			w[l].push({
				'gtm.start': new Date().getTime(),
				event: 'gtm.js'
			});
			var f = d.getElementsByTagName(s)[0],
				j = d.createElement(s),
				dl = l != 'dataLayer' ? '&l=' + l : '';
			j.async = true;
			j.src =
				'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
			f.parentNode.insertBefore(j, f);
		})(window, document, 'script', 'dataLayer', 'GTM-5QGSH5J');
	</script>
	<!-- End Google Tag Manager -->

</head>

<body>


	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QGSH5J" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->



	<div class="header">
		<div class="header-left">
			<div class="menu-icon icon-copy ti-menu" style="color: white;"></div>
			<div class="search-toggle-icon dw dw-search2" style="color: #ffde00;" data-toggle="header_search"></div>
			<div class="header-search">
				<form method="post" action="busqueda.php">
					<div class="form-group mb-0">
						<i class="dw dw-search2 search-icon" style="color: #ffde00;"></i>
						<input type="text" class="form-control search-input" placeholder="Buscar en IDentiKIT" required name="PalabraClave">
						<input name="buscar" type="hidden" class="form-control mb-2" id="inlineFormInput" value="v">
					</div>
				</form>
			</div>
		</div>
		<div class="header-right">
			<div class="user-info-dropdown">
				<div class="dropdown">
					<a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" style="color: #ffde00;">
						<span class="user-icon">
							<?php
								if ($myavatar == null) {
									print '<center><img  src="images/default.png" title="' . $myfname . '" alt="image"  /></center>';
								} else {
									echo '<center><img alt="image" title="' . $myfname . '"  src="data:image/png;base64,' . base64_encode($myavatar) . '"/></center>';
								}
							?>
						</span>
					</a>
					<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
						<?php
							if ($user_online == true) {
								print '
									<a class="dropdown-item" href="' . $myrole . '"><i class="dw dw-user1"></i> Perfil</a>
									<!--<a class="dropdown-item" href="#"><i class="dw dw-settings2"></i> Ajustes</a>
									<a class="dropdown-item" href="#"><i class="dw dw-help"></i> Ayuda</a>-->
									<a class="dropdown-item" href="logout.php"><i class="dw dw-logout"></i> Salir</a>';
							} else {
								print '
									<li><a href="login.php">ingresar</a></li>
									<li><a data-toggle="modal" href="#registerModal">registrate</a></li>';
							}
						?>
					</div>
				</div>
			</div>

		</div>
	</div>



	<div class="left-side-bar">
		<div class="brand-logo">
			<a href="#">
				<img src="logov3.png" alt="" class="light-logo" width="60"> <span class="mtext"> IDentiKIT</span>
			</a>
			<div class="close-sidebar" data-toggle="left-sidebar-close">
				<i class="ion-close-round"></i>
			</div>
		</div>
		<div class="menu-block customscroll">
			<div class="sidebar-menu">
				<ul id="accordion-menu">
					<li>
						<a href="index.php" class="dropdown-toggle no-arrow ">
							<span class="micon icon-copy fa fa-home"></span><span class="mtext">Inicio</span>
						</a>
					</li>

					<li class="dropdown">
						<a href="javascript:;" class="dropdown-toggle">
							<span class="micon icon-copy fa fa-bandcamp"></span><span class="mtext">Explorar</span>
						</a>
						<ul class="submenu">
							<li><a href="identis.php"> IDentis</a></li>
							<li><a href="works.php"> Trabajos</a></li>
							<li><a href="empresas.php"> Empresas</a></li>
						</ul>
					</li>

					<li>
						<a href="aplicados.php" class="dropdown-toggle no-arrow active">
							<span class="micon ion-android-done-all"></span><span class="mtext">Aplicados</span>
						</a>
					</li>

					<li>
						<a href="employee/chat.php" class="dropdown-toggle no-arrow"><span class="micon icon-copy fa fa-comment"></span><span class="mtext">Chat</span></a>
					</li>

					<li>
						<a href="academy.php" class="dropdown-toggle no-arrow ">
							<span class="micon icon-copy fa fa-free-code-camp"></span><span class="mtext">Academia</span>
						</a>
					</li>

				</ul>
			</div>
		</div>
	</div>
	<div class="mobile-menu-overlay"></div>

	<div class="main-container">
		<div class="pd-ltr-20 xs-pd-20">

			<div class="container pd-0">
				<div class="pd-20 card-box mb-30">
					<div class="clearfix mb-20">
						<div class="pull-left">
							<h4 class="text-blue h4">Trabajos aplicados</h4>
							<p>Hola! <?php echo "$myfname"; ?>, en esta seccion se encuentran los trabajos en los que aplicaste, <strong>recorda que aplicar no significa que estes contratado, esto quiere decir que la empresa ya recibio tu perfil de IDentiKIT</strong>.</p>
						</div>

					</div>
					<div class="table-responsive">
						<table class="table table-striped">
							<thead>
								<tr>
									<th scope="col">Trabajo</th>
									<th scope="col">Empresa</th>
									<th scope="col">Estado</th>
								</tr>
							</thead>
							<?php require 'constants/check_reply.php'; ?>
							<tbody>
								<?php

									try {
										$job_applications  = $db->query("SELECT * FROM tbl_job_applications WHERE member_no = :myid ORDER BY id DESC",array("myid"=>$myid));


										foreach ($job_applications as $job_application) {
											$post_date  = date_format(date_create_from_format('m/d/Y', $job_application['application_date']), 'd');
											$post_month = date_format(date_create_from_format('m/d/Y', $job_application['application_date']), 'F');
											$post_year  = date_format(date_create_from_format('m/d/Y', $job_application['application_date']), 'Y');
											$job_id     = $job_application['job_id'];
											$EVacante   = $job_application['EVacante'];

											$job_applications  = $db->query("SELECT * FROM tbl_jobs WHERE job_id = :job_id",array("job_id"=>$job_id));

											foreach ($job_applications as $job_application) {
												$job_title = $job_application['title'];
												$jobcountry = $job_application['country'];
												$jobcity = $job_application['city'];
												$jobtype = $job_application['type'];
												$compid = $job_application['company'];

												if ($jobtype == "Freelance") {
													$sta = '<div class="job-label label label-success">
																Freelance
															</div>';
												}
												if ($jobtype == "Part-time") {
													$sta = '<div class="job-label label label-danger">
																Part-time
															</div>';
												}
												if ($jobtype == "Full-time") {
													$sta = '<div class="job-label label label-warning">
																Full-time
															</div>';
												}
												
												$users  = $db->query("SELECT * FROM tbl_users WHERE member_no = :compid AND role = 'employer'",array("compid"=>$compid));

												foreach ($users as $user) {
													$compname = $user['first_name'];
													$complogo = $user['avatar'];
												}

								?>
								<tr>
									<td><?= $job_title ?></td>
									<td><?= $compname  ?></td>
									<?php
										$status = $EVacante;
										$color = "";
										$text = "";
										switch ($status) {
											case 1:
												$color = "secondary";
												$text  = "Enviado";
												break;
											case 2:
												$color = "warning";
												$text  = "Revisado";
												break;
											case 3:
												$color = "info";
												$text  = "En proceso";
												break;
											case 4:
												$color = "success";
												$text  = "Aceptado! :)";
												break;
											case 5:
												$color = "danger";
												$text  = "Rechazado";
												break;
										}
									?>
									<td>
										<span class="badge badge-<?= $color ?>"> <?= $text ?></span>
									</td>
								</tr>

								<?php
										}
									}
								} catch (PDOException $e) {
								} ?>
							</tbody>
						</table>
					</div>

				</div>
				<h4 class="mb-20">Quizás te interese </h4>
				<ul class="row">
					<?php
					try {
						$trabajos = $db->query("SELECT * FROM tbl_jobs ORDER BY RAND() limit 3");

						foreach ($trabajos as $trabajo) {
							$jobcity = $trabajo['city'];
							$jobcountry = $trabajo['country'];
							$type = $trabajo['type'];
							$title = $trabajo['title'];
							$closingdate = $trabajo['closing_date'];
							$company_id = $trabajo['company'];

							$usuarios    = $db->query("SELECT * FROM tbl_users WHERE member_no = :company and role = 'employer'",array("company"=>$company_id));

							foreach ($usuarios as $usuario) {
								$complogo = $usuario['avatar'];
								$thecompname = $usuario['first_name'];
							}

					?>
							<li class="col-lg-4 col-md-6 col-sm-12">
								<div class="product-box">
									<div class="product-caption">
										<h4><a href="#"><?php echo strip_tags($trabajo['title']); ?></a></h4>
										<div class="text-success" style="margin-top: 6px;">
											💡 <?= strip_tags($thecompname); ?>
											📍 <?= strip_tags($jobcity); ?></i></div>
										<div class="text-success"></div>
										<div class="price">
											<p><?=strip_tags(substr($trabajo['description'], 0, 200)); ?>...</p>
										</div>
										<a href="work-detail.php?identiwork=<?=$trabajo['job_id']; ?>" class="btn btn-primary col-md-12">Postularme</a>
									</div>
								</div>
							</li>
					<?php
						}} catch (PDOException $e) {
							echo 'Excepción capturada: ',  $e->getMessage(), "\n";
						}
					?>

				</ul>
			</div>
		</div>
	</div>

	</div>
	</div>

	</div>
	</div>
	<!-- js -->
	<script src="v1/vendors/scripts/core.js"></script>
	<script src="v1/vendors/scripts/script.min.js"></script>
	<script src="v1/vendors/scripts/process.js"></script>
	<script src="v1/vendors/scripts/layout-settings.js"></script>
	<script src="v1/src/plugins/apexcharts/apexcharts.min.js"></script>
	<script src="v1/src/plugins/datatables/js/jquery.dataTables.min.js"></script>
	<script src="v1/src/plugins/datatables/js/dataTables.bootstrap4.min.js"></script>
	<script src="v1/src/plugins/datatables/js/dataTables.responsive.min.js"></script>
	<script src="v1/src/plugins/datatables/js/responsive.bootstrap4.min.js"></script>
	<script src="v1/vendors/scripts/dashboard.js"></script>
</body>

</html>