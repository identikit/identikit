<!DOCTYPE html>

<?php 
require 'constants/settings.php'; 
require 'constants/check-login.php';


if ($myrole == 'employee'){
	require 'employee/constants/check-login.php';
}else {
	require 'employee/constants2/check-login1.php';
	
}

?>
<html>
<head>
	<!-- Basic Page Info -->
	<meta charset="utf-8">
	<title>IDentiKIT - Chats</title>

	
	<link rel="apple-touch-icon" sizes="180x180" href="logov3.png">
	<link rel="icon" type="image/png" sizes="32x32" href="logov3.png">
	<link rel="icon" type="image/png" sizes="16x16" href="logov3.png">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="v1/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="v1/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="v1/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="v1/src/plugins/datatables/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="v1/vendors/styles/style.css">

		<meta property="og:image" content="https://identikit.app/logowebog.png" />
	  <meta property="og:image:secure_url" content="https://identikit.app/logowebog.png" />
	  <meta property="og:image:type" content="image/png" />
	  <meta property="og:image:width" content="300" />
	  <meta property="og:image:height" content="300" />
	  <meta property="og:image:alt" content="IDentiKIT - Tu primera experiencia laboral" />
	  <meta property="og:description" content="😎 Encuentra el mejor talento tech joven en IDentiKIT a un click de distancia, fácil, rápido y seguro." />



	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-K2NHRZT4R7"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-K2NHRZT4R7');
</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5QGSH5J');</script>
<!-- End Google Tag Manager -->







</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QGSH5J"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

	
	
	<div class="header">
		<div class="header-left">
			<div class="menu-icon icon-copy ti-menu" style="color: white;"></div>
			
			<div class="search-toggle-icon dw dw-search2" style="color: #ffde00;" data-toggle="header_search"></div>
			<div class="header-search">
				<form method="post" action="busqueda.php" >
					<div class="form-group mb-0" >
						
					<i class="dw dw-search2 search-icon" style="color: #ffde00;"></i>
					
						<input type="text" class="form-control search-input" placeholder="Buscar en IDentiKIT" required name="PalabraClave">
						<input name="buscar" type="hidden" class="form-control mb-2" id="inlineFormInput" value="v">


					</div>
				</form>
			</div>
		</div>
		<div class="header-right">
			
			<!--<div class="user-notification">
				<div class="dropdown">
					<a class="dropdown-toggle no-arrow" href="#" role="button" data-toggle="dropdown">
						<i class="icon-copy dw dw-notification"></i>
						<span class="badge notification-active"></span>
					</a>
					<div class="dropdown-menu dropdown-menu-right">
						<div class="notification-list mx-h-350 customscroll">
							<ul>
								<li>
									<a href="#">
										<img src="vendors/images/img.jpg" alt="">
										<h3>John Doe</h3>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed...</p>
									</a>
								</li>
								
							</ul>
						</div>
					</div>
				</div>
			</div>-->
			<div class="user-info-dropdown">
				<div class="dropdown">
					<a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" style="color: #ffde00;">
					
						<span class="user-icon">
						<?php 
						if ($myavatar == null) {
							
							print '<center><img  src="images/default.png" title="" alt="image"  /></center>';
						}else{
							echo '<center><img alt="image" title=""  src="data:image/png;base64,'.base64_encode($myavatar).'"/></center>';	
						}
						?>	
																	
						</span>
					</a>
					<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
					<?php
						if ($user_online == true) {
						print '
							<a class="dropdown-item" href="'.$myrole.'"><i class="dw dw-user1"></i> Perfil</a>
							<!--<a class="dropdown-item" href="#"><i class="dw dw-settings2"></i> Ajustes</a>
							<a class="dropdown-item" href="#"><i class="dw dw-help"></i> Ayuda</a>-->
							<a class="dropdown-item" href="logout.php"><i class="dw dw-logout"></i> Salir</a>
';
						}else{
						print '
							<a class="dropdown-item" href="login.php"><i class="dw dw-lock"></i> Ingresar</a>
							<a class="dropdown-item"  href="registro.php"><i class="dw dw-user1"></i> Registrate</a>';						
						}
						
						?>
						
					</div>
				</div>
			</div>
			
		</div>
	</div>

	<div class="left-side-bar">
		<div class="brand-logo">
			<a href="#">
				<img src="logov3.png" alt="Perfil verificado por IDentiKIT" class="light-logo" width="60"> <span class="mtext"> IDentiKIT</span>
			</a>
			<div class="close-sidebar" data-toggle="left-sidebar-close">
				<i class="ion-close-round"></i>
			</div>
		</div>
		<div class="menu-block customscroll">
			<div class="sidebar-menu">
				<ul id="accordion-menu">
					<li>
					
					<?php
					if ($myrole == "employee") {
						print '<a href="index.php" class="dropdown-toggle no-arrow ">
						<span class="micon icon-copy fa fa-home"></span><span class="mtext">Inicio</span></a>

						<li class="dropdown">
						<a href="javascript:;" class="dropdown-toggle">
							<span class="micon icon-copy fa fa-bandcamp"></span><span class="mtext">Explorar</span>
						</a>
						<ul class="submenu">
							<li><a href="identis.php" class="active"> IDentis</a></li>
							<li><a href="works.php"> Trabajos</a></li>
							<li><a href="empresas.php"> Empresas</a></li>
						</ul>
						</li>

							<li>
						<a href="aplicados.php" class="dropdown-toggle no-arrow">
							<span class="micon ion-android-done-all"></span><span class="mtext">Aplicados</span>

						</a>
					</li>

					<li>
						<a href="academy.php" class="dropdown-toggle no-arrow ">
							<span class="micon icon-copy fa fa-free-code-camp"></span><span class="mtext">Academia</span>

						</a>
					</li>
						';
					} if ($myrole == "employer") {
						print '<a href="'.$myrole.'/index.php" class="dropdown-toggle no-arrow ">
						<span class="micon icon-copy fa fa-home"></span><span class="mtext">Panel</span></a>
						<a href="identis.php" class="dropdown-toggle no-arrow"><span class="micon icon-copy fa fa-bandcamp"></span><span class="mtext">identis</span></a>
						
						
						
						';
					}

					if ($myrole == null) {
						print '<a href="login.php" class="dropdown-toggle no-arrow ">
						<span class="micon icon-copy fa fa-lock"></span><span class="mtext">Iniciar sesion</span></a>

						<a href="registro.php" class="dropdown-toggle no-arrow ">
						<span class="micon icon-copy fa fa-user"></span><span class="mtext">Registrarme</span></a>

						
					
						
						
						
						';
					
					}

						
						?>
							

						
					</li>
									


				
					
				</ul>
			</div>
		</div>
	</div>
	<div class="mobile-menu-overlay"></div>

	<div class="main-container">
		<div class="pd-ltr-20 xs-pd-20-10">
			<div class="min-height-200px">
				<div class="page-header">
					<div class="row">
						<div class="col-md-6 col-sm-12">
							<div class="title">
								<h4>Chat</h4>
							</div>
							<nav aria-label="breadcrumb" role="navigation">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="index.php">Inicio</a></li>
									<li class="breadcrumb-item active" aria-current="page">Chats</li>
								</ol>
							</nav>
						</div>
					</div>
				</div>
				<div class="bg-white border-radius-4 box-shadow mb-30">
					<div class="row no-gutters">
						<div class="col-lg-3 col-md-4 col-sm-12">
							<div class="chat-list bg-light-gray">
								<div class="chat-search">
									<span class="ti-search"></span>
									<input type="text" placeholder="Search Contact">
								</div>
								<div class="notification-list chat-notification-list customscroll">
									<ul>
										<li>
											<a href="#">
												<img src="v1/vendors/images/img.jpg" alt="">
												<h3 class="clearfix">John Doe</h3>
												<p><i class="fa fa-circle text-light-green"></i> online</p>
											</a>
										</li>
										<li class="active">
											<a href="#">
												<img src="v1/vendors/images/img.jpg" alt="">
												<h3 class="clearfix">John Doe</h3>
												<p><i class="fa fa-circle text-light-green"></i> online</p>
											</a>
										</li>
										<li>
											<a href="#">
												<img src="v1/vendors/images/img.jpg" alt="">
												<h3 class="clearfix">John Doe</h3>
												<p><i class="fa fa-circle text-light-green"></i> online</p>
											</a>
										</li>
										<li>
											<a href="#">
												<img src="v1/vendors/images/img.jpg" alt="">
												<h3 class="clearfix">John Doe</h3>
												<p><i class="fa fa-circle text-warning"></i> active 5 min</p>
											</a>
										</li>
										<li>
											<a href="#">
												<img src="v1/vendors/images/img.jpg" alt="">
												<h3 class="clearfix">John Doe</h3>
												<p><i class="fa fa-circle text-warning"></i> active 4 min</p>
											</a>
										</li>
										<li>
											<a href="#">
												<img src="v1/vendors/images/img.jpg" alt="">
												<h3 class="clearfix">John Doe</h3>
												<p><i class="fa fa-circle text-warning"></i> active 3 min</p>
											</a>
										</li>
										<li>
											<a href="#">
												<img src="v1/vendors/images/img.jpg" alt="">
												<h3 class="clearfix">John Doe</h3>
												<p><i class="fa fa-circle text-light-orange"></i> offline</p>
											</a>
										</li>
										<li>
											<a href="#">
												<img src="v1/vendors/images/img.jpg" alt="">
												<h3 class="clearfix">John Doe</h3>
												<p><i class="fa fa-circle text-light-orange"></i> offline</p>
											</a>
										</li>
										<li>
											<a href="#">
												<img src="v1/vendors/images/img.jpg" alt="">
												<h3 class="clearfix">John Doe</h3>
												<p><i class="fa fa-circle text-light-orange"></i> offline</p>
											</a>
										</li>
										<li>
											<a href="#">
												<img src="v1/vendors/images/img.jpg" alt="">
												<h3 class="clearfix">John Doe</h3>
												<p><i class="fa fa-circle text-light-orange"></i> offline</p>
											</a>
										</li>
										<li>
											<a href="#">
												<img src="v1/vendors/images/img.jpg" alt="">
												<h3 class="clearfix">John Doe</h3>
												<p><i class="fa fa-circle text-light-orange"></i> offline</p>
											</a>
										</li>
										<li>
											<a href="#">
												<img src="v1/vendors/images/img.jpg" alt="">
												<h3 class="clearfix">John Doe</h3>
												<p><i class="fa fa-circle text-light-orange"></i> offline</p>
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-9 col-md-8 col-sm-12">
							<div class="chat-detail">
								<div class="chat-profile-header clearfix">
									<div class="left">
										<div class="clearfix">
											<div class="chat-profile-photo">
												<img src="v1/vendors/images/profile-photo.jpg" alt="">
											</div>
											<div class="chat-profile-name">
												<h3>Rachel Curtis</h3>
												<span>New York, USA</span>
											</div>
										</div>
									</div>
									<!--<div class="right text-right">
										<div class="dropdown">
											<a class="btn btn-outline-primary dropdown-toggle" href="#" role="button" data-toggle="dropdown">
												Setting
											</a>
											<div class="dropdown-menu dropdown-menu-right">
												<a class="dropdown-item" href="#">Export Chat</a>
												<a class="dropdown-item" href="#">Search</a>
												<a class="dropdown-item text-light-orange" href="#">Delete Chat</a>
											</div>
										</div>
									</div>-->
								</div>
								<div class="chat-box">
									<div class="chat-desc customscroll">
										<ul>
											<li class="clearfix admin_chat">
												<span class="chat-img">
													<img src="v1/vendors/images/chat-img2.jpg" alt="">
												</span>
												<div class="chat-body clearfix">
													<p>Maybe you already have additional info?</p>
													<div class="chat_time">09:40PM</div>
												</div>
											</li>
											<li class="clearfix admin_chat">
												<span class="chat-img">
													<img src="v1/vendors/images/chat-img2.jpg" alt="">
												</span>
												<div class="chat-body clearfix">
													<p>It is to early to provide some kind of estimation here. We need user stories.</p>
													<div class="chat_time">09:40PM</div>
												</div>
											</li>
											<li class="clearfix">
												<span class="chat-img">
													<img src="v1/vendors/images/chat-img1.jpg" alt="">
												</span>
												<div class="chat-body clearfix">
													<p>We are just writing up the user stories now so will have requirements for you next week. We are just writing up the user stories now so will have requirements for you next week.</p>
													<div class="chat_time">09:40PM</div>
												</div>
											</li>
											<li class="clearfix">
												<span class="chat-img">
													<img src="v1/vendors/images/chat-img1.jpg" alt="">
												</span>
												<div class="chat-body clearfix">
													<p>Essentially the brief is for you guys to build an iOS and android app. We will do backend and web app. We have a version one mockup of the UI, please see it attached. As mentioned before, we would simply hand you all the assets for the UI and you guys code. If you have any early questions please do send them on to myself. Ill be in touch in coming days when we have requirements prepared. Essentially the brief is for you guys to build an iOS and android app. We will do backend and web app. We have a version one mockup of the UI, please see it attached. As mentioned before, we would simply hand you all the assets for the UI and you guys code. If you have any early questions please do send them on to myself. Ill be in touch in coming days when we have.</p>
													<div class="chat_time">09:40PM</div>
												</div>
											</li>
											<li class="clearfix admin_chat">
												<span class="chat-img">
													<img src="v1/vendors/images/chat-img2.jpg" alt="">
												</span>
												<div class="chat-body clearfix">
													<p>Maybe you already have additional info?</p>
													<div class="chat_time">09:40PM</div>
												</div>
											</li>
											<li class="clearfix admin_chat">
												<span class="chat-img">
													<img src="v1/vendors/images/chat-img2.jpg" alt="">
												</span>
												<div class="chat-body clearfix">
													<p>It is to early to provide some kind of estimation here. We need user stories.</p>
													<div class="chat_time">09:40PM</div>
												</div>
											</li>
											<li class="clearfix">
												<span class="chat-img">
													<img src="v1/vendors/images/chat-img1.jpg" alt="">
												</span>
												<div class="chat-body clearfix">
													<p>We are just writing up the user stories now so will have requirements for you next week. We are just writing up the user stories now so will have requirements for you next week.</p>
													<div class="chat_time">09:40PM</div>
												</div>
											</li>
											<li class="clearfix">
												<span class="chat-img">
													<img src="v1/vendors/images/chat-img1.jpg" alt="">
												</span>
												<div class="chat-body clearfix">
													<p>Essentially the brief is for you guys to build an iOS and android app. We will do backend and web app. We have a version one mockup of the UI, please see it attached. As mentioned before, we would simply hand you all the assets for the UI and you guys code. If you have any early questions please do send them on to myself. Ill be in touch in coming days when we have requirements prepared. Essentially the brief is for you guys to build an iOS and android app. We will do backend and web app. We have a version one mockup of the UI, please see it attached. As mentioned before, we would simply hand you all the assets for the UI and you guys code. If you have any early questions please do send them on to myself. Ill be in touch in coming days when we have.</p>
													<div class="chat_time">09:40PM</div>
												</div>
											</li>
											<li class="clearfix upload-file">
												<span class="chat-img">
													<img src="v1/vendors/images/chat-img1.jpg" alt="">
												</span>
												<div class="chat-body clearfix">
													<div class="upload-file-box clearfix">
														<div class="left">
															<img src="vendors/images/upload-file-img.jpg" alt="">
															<div class="overlay">
																<a href="#">
																	<span><i class="fa fa-angle-down"></i></span>
																</a>
															</div>
														</div>
														<div class="right">
															<h3>Big room.jpg</h3>
															<a href="#">Download</a>
														</div>
													</div>
													<div class="chat_time">09:40PM</div>
												</div>
											</li>
											<li class="clearfix upload-file admin_chat">
												<span class="chat-img">
													<img src="v1/vendors/images/chat-img2.jpg" alt="">
												</span>
												<div class="chat-body clearfix">
													<div class="upload-file-box clearfix">
														<div class="left">
															<img src="v1/vendors/images/upload-file-img.jpg" alt="">
															<div class="overlay">
																<a href="#">
																	<span><i class="fa fa-angle-down"></i></span>
																</a>
															</div>
														</div>
														<div class="right">
															<h3>Big room.jpg</h3>
															<a href="#">Download</a>
														</div>
													</div>
													<div class="chat_time">09:40PM</div>
												</div>
											</li>
										</ul>
									</div>
									<div class="chat-footer">
										<div class="file-upload"><a href="#"><i class="fa fa-paperclip"></i></a></div>
										<div class="chat_text_area">
											<textarea placeholder="Type your message…"></textarea>
										</div>
										<div class="chat_send">
											<button class="btn btn-link" type="submit"><i class="icon-copy ion-paper-airplane"></i></button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	<!-- js -->
	<script src="v1/vendors/scripts/core.js"></script>
	<script src="v1/vendors/scripts/script.min.js"></script>
	<script src="v1/vendors/scripts/process.js"></script>
	<script src="v1/vendors/scripts/layout-settings.js"></script>
</body>
</html>