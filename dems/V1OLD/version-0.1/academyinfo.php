<!DOCTYPE html>

<?php 
	
	require 'constants/settings.php'; 
	require 'constants/check-login.php';
	require 'constants/db_config.php';

	if ($myrole == 'employee') {
		require 'employee/constants/check-login.php';
	}else{
		require 'employee/constants2/check-login1.php';
	}

?>

<?php require_once 'academy/model.php'; ?>
<?php $post = getPostById($_GET['slug']); ?>

<html>
<head>
	<meta charset="utf-8">
	<title>IDentiKIT - Estudia <?php echo $post['titulo'] ?></title>
	
	<link rel="apple-touch-icon" sizes="180x180" href="logov3.png">
	<link rel="icon" type="image/png" sizes="32x32" href="logov3.png">
	<link rel="icon" type="image/png" sizes="16x16" href="logov3.png">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="v1/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="v1/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="v1/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="v1/src/plugins/datatables/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="v1/vendors/styles/style.css">


	<?php
		$descOG = $post['contenido']; 
	?>
		
	<meta property="og:description" content="<?php echo substr($descOG, 0, 200); ?>" />
	<meta name="keywords" content="identikit, IDentiKIT, idkit, IDKIT, primera experiencia laboral, trabajos jr, junior, trabajos junior, empresas que contratan juniors, talento joven, talento tech, jovenes, empresas sin experiencia" />
	<meta name="author" content="IDentiKIT">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta property="og:image" content="https://identikit.app/logowebog.png" />
	<meta property="og:image:secure_url" content="https://identikit.app/logowebog.png" />
	<meta property="og:image:type" content="image/png" />
	<meta property="og:image:width" content="300" />
	<meta property="og:image:height" content="300" />
	<meta property="og:image:alt" content="IDentiKIT - Tu primera experiencia laboral" />
  
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-K2NHRZT4R7"></script>
	<script>
  		window.dataLayer = window.dataLayer || [];
  		function gtag(){dataLayer.push(arguments);}
  		gtag('js', new Date());
  		gtag('config', 'G-K2NHRZT4R7');
	</script>

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-5QGSH5J');</script>
	<!-- End Google Tag Manager -->

</head>
<body>


	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QGSH5J"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

	<div class="header">
		<div class="header-left">
			<div class="menu-icon icon-copy ti-menu" style="color: white;"></div>
			
			<div class="search-toggle-icon dw dw-search2" style="color: #ffde00;" data-toggle="header_search"></div>
			<div class="header-search">
			<?php include 'conexion.php';?>

				<form method="post" action="busqueda.php" >
					<div class="form-group mb-0" >
					<i class="dw dw-search2 search-icon" style="color: #ffde00;"></i>
						<input type="text" class="form-control search-input" placeholder="Buscar en IDentiKIT" required name="PalabraClave">
						<input name="buscar" type="hidden" class="form-control mb-2" id="inlineFormInput" value="v">
					</div>
				</form>

			</div>
		</div>
		<div class="header-right">
			<div class="user-info-dropdown">
				<div class="dropdown">
					<a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" style="color: #ffde00;">
						<span class="user-icon">
							<?php 
								if ($myavatar == null) {
									print '<center><img  src="images/default.png" title="'.$myfname.'" alt="image"  /></center>';
								}else{
									echo '<center><img alt="image" title="'.$myfname.'"  src="data:image/png;base64,'.base64_encode($myavatar).'"/></center>';	
								}
							?>										
						</span>
					</a>
					<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
						<?php
							if ($user_online == true) {
								print '<a class="dropdown-item" href="'.$myrole.'"><i class="dw dw-user1"></i> Perfil</a>
									<!--<a class="dropdown-item" href="#"><i class="dw dw-settings2"></i> Ajustes</a>
									<a class="dropdown-item" href="#"><i class="dw dw-help"></i> Ayuda</a>-->
									<a class="dropdown-item" href="logout.php"><i class="dw dw-logout"></i> Salir</a>';
							}else{
								print '<a class="dropdown-item" href="login.php"><i class="dw dw-lock"></i> Ingresar</a>
								<a class="dropdown-item"  href="registro.php"><i class="dw dw-user1"></i> Registrate</a>';						
							}
						?>
					</div>
				</div>
			</div>
			
		</div>
	</div>

	<div class="left-side-bar">
		<div class="brand-logo">
			<a href="#">
				<img src="logov3.png" alt="" class="light-logo" width="60"><span class="mtext" style="margin:10px;">IDentiKIT</span>
			</a>
			<div class="close-sidebar" data-toggle="left-sidebar-close">
				<i class="ion-close-round"></i>
			</div>
		</div>
		<div class="menu-block customscroll">
			<div class="sidebar-menu">
				<ul id="accordion-menu">
					<?php
						if ($myrole == "employee") {
							print '<a href="index.php" class="dropdown-toggle no-arrow ">
							<span class="micon icon-copy fa fa-home"></span><span class="mtext">Inicio</span></a>

							<li class="dropdown">
							<a href="javascript:;" class="dropdown-toggle">
								<span class="micon icon-copy fa fa-bandcamp"></span><span class="mtext">Explorar</span>
							</a>
							<ul class="submenu">
								<li><a href="identis.php" > IDentis</a></li>
								<li><a href="works.php"> Trabajos</a></li>
								<li><a href="empresas.php"> Empresas</a></li>
							</ul>
							</li>

								<li>
							<a href="aplicados.php" class="dropdown-toggle no-arrow">
								<span class="micon ion-android-done-all"></span><span class="mtext">Aplicados</span>

							</a>
						</li>

						<li>
							<a href="academy.php" class="dropdown-toggle no-arrow active">
								<span class="micon icon-copy fa fa-free-code-camp"></span><span class="mtext">Academia</span>

							</a>
						</li>
							';
						} if ($myrole == "employer") {
							print '<a href="'.$myrole.'/index.php" class="dropdown-toggle no-arrow ">
							<span class="micon icon-copy fa fa-home"></span><span class="mtext">Panel</span></a>
							<a href="identis.php" class="dropdown-toggle no-arrow"><span class="micon icon-copy fa fa-bandcamp"></span><span class="mtext">identis</span></a>
							
							
							
							';
						}

						if ($myrole == null)  {
							print '<a href="login.php" class="dropdown-toggle no-arrow ">
							<span class="micon icon-copy fa fa-lock"></span><span class="mtext">Iniciar sesion</span></a>

							<a href="registro.php" class="dropdown-toggle no-arrow ">
							<span class="micon icon-copy fa fa-user"></span><span class="mtext">Registrarme</span></a>';
						}

							
					?>
				</ul>
			</div>
		</div>
	</div>

	<div class="mobile-menu-overlay"></div>
	<div class="main-container">
		<div class="pd-ltr-20 xs-pd-20">	
			<div class="container pd-0">	
				<div class="row clearfix">	
					<div class="blog-wrap">
						<div class="pd-0">
							<div class="row">
								<div class="col-md-12">
									<div class="blog-detail card-box overflow-hidden mb-30">
										<div class="blog-img">
											<img src="vendors/images/img2.jpg" alt="">
										</div>
										<div class="blog-caption">
											<h4 class="mb-10"><?php echo $post['titulo'] ?></h4>
											<span>
												<?php echo $post['contenido'] ?>
											</span>
											<a href="<?php echo $post['url'] ;?>" class="btn btn-primary col-md-12" >Inscribirse</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>			
			</div>
		</div>
	</div>
			
	<!-- js -->
	<script src="v1/vendors/scripts/core.js"></script>
	<script src="v1/vendors/scripts/script.min.js"></script>
	<script src="v1/vendors/scripts/process.js"></script>
	<script src="v1/vendors/scripts/layout-settings.js"></script>
	<script src="v1/src/plugins/apexcharts/apexcharts.min.js"></script>
	<script src="v1/src/plugins/datatables/js/jquery.dataTables.min.js"></script>
	<script src="v1/src/plugins/datatables/js/dataTables.bootstrap4.min.js"></script>
	<script src="v1/src/plugins/datatables/js/dataTables.responsive.min.js"></script>
	<script src="v1/src/plugins/datatables/js/responsive.bootstrap4.min.js"></script>
	<script src="v1/vendors/scripts/dashboard.js"></script>
</body>
</html>