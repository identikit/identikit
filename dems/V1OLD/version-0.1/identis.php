<!DOCTYPE html>

<?php

	require_once("./config/Db.php");
	$db = new DbPDO();

	require 'constants/settings.php';
	require 'constants/check-login.php';

	if ($myrole == 'employee') {
		require 'employee/constants/check-login.php';
	} else {
		require 'employee/constants2/check-login1.php';
	}

	$empresaId              = $_SESSION['myid'];

?>
<html>

<head>
	<!-- Basic Page Info -->
	<meta charset="utf-8">
	<title>IDentiKIT - Explorar IDentis</title>


	<link rel="apple-touch-icon" sizes="180x180" href="logov3.png">
	<link rel="icon" type="image/png" sizes="32x32" href="logov3.png">
	<link rel="icon" type="image/png" sizes="16x16" href="logov3.png">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="v1/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="v1/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="v1/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="v1/src/plugins/datatables/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="v1/vendors/styles/style.css">

	<meta property="og:image" content="https://identikit.app/logowebog.png" />
	<meta property="og:image:secure_url" content="https://identikit.app/logowebog.png" />
	<meta property="og:image:type" content="image/png" />
	<meta property="og:image:width" content="300" />
	<meta property="og:image:height" content="300" />
	<meta property="og:image:alt" content="IDentiKIT - Tu primera experiencia laboral" />
	<meta property="og:description" content="😎 Encuentra el mejor talento tech joven en IDentiKIT a un click de distancia, fácil, rápido y seguro." />



	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-K2NHRZT4R7"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'G-K2NHRZT4R7');
	</script>

	<!-- Google Tag Manager -->
	<script>
		(function(w, d, s, l, i) {
			w[l] = w[l] || [];
			w[l].push({
				'gtm.start': new Date().getTime(),
				event: 'gtm.js'
			});
			var f = d.getElementsByTagName(s)[0],
				j = d.createElement(s),
				dl = l != 'dataLayer' ? '&l=' + l : '';
			j.async = true;
			j.src =
				'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
			f.parentNode.insertBefore(j, f);
		})(window, document, 'script', 'dataLayer', 'GTM-5QGSH5J');
	</script>
	<!-- End Google Tag Manager -->
</head>

<body>

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QGSH5J" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->


	<div class="header">
		<div class="header-left">
			<div class="menu-icon icon-copy ti-menu" style="color: white;"></div>
			<div class="search-toggle-icon dw dw-search2" style="color: #ffde00;" data-toggle="header_search"></div>
			<div class="header-search">
				<form method="post" action="busqueda.php">
					<div class="form-group mb-0">
						<i class="dw dw-search2 search-icon" style="color: #ffde00;"></i>
						<input type="text" class="form-control search-input" placeholder="Buscar en IDentiKIT" required name="PalabraClave">
						<input name="buscar" type="hidden" class="form-control mb-2" id="inlineFormInput" value="v">
					</div>
				</form>
			</div>
		</div>
		<div class="header-right">
			<div class="user-info-dropdown">
				<div class="dropdown">
					<a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" style="color: #ffde00;">
						<span class="user-icon">
							<?php
								if ($myavatar == null) {
									print '<center><img  src="images/default.png" title="' . $myfname . '" alt="image"  /></center>';
								} else {
									echo '<center><img alt="image" title="' . $myfname . '"  src="data:image/png;base64,' . base64_encode($myavatar) . '"/></center>';
								}
							?>
						</span>
					</a>
					<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
						<?php
							if ($user_online == true) {
								print '
									<a class="dropdown-item" href="' . $myrole . '"><i class="dw dw-user1"></i> Perfil</a>
									<!--<a class="dropdown-item" href="#"><i class="dw dw-settings2"></i> Ajustes</a>
									<a class="dropdown-item" href="#"><i class="dw dw-help"></i> Ayuda</a>-->
									<a class="dropdown-item" href="logout.php"><i class="dw dw-logout"></i> Salir</a>';
							} else {
								print '
									<li><a href="login.php">ingresar</a></li>
									<li><a data-toggle="modal" href="#registerModal">registrate</a></li>';
							}
						?>
					</div>
				</div>
			</div>

		</div>
	</div>

	
	<div class="left-side-bar">
		<div class="brand-logo">
			<a href="#">
				<img src="v1/logov3.png" alt="" class="light-logo" width="60"> <span class="mtext">IDentiKIT</span>
			</a>
			<div class="close-sidebar" data-toggle="left-sidebar-close">
				<i class="ion-close-round"></i>
			</div>
		</div>
		<div class="menu-block customscroll">
			<div class="sidebar-menu">
				<ul id="accordion-menu">
					<?php
					if ($myrole == "employee") {
						print '<a href="index.php" class="dropdown-toggle no-arrow ">
						<span class="micon icon-copy fa fa-home"></span><span class="mtext">Inicio</span></a>

						<li class="dropdown">
						<a href="javascript:;" class="dropdown-toggle">
							<span class="micon icon-copy fa fa-bandcamp"></span><span class="mtext">Explorar</span>
						</a>
						<ul class="submenu">
							<li><a href="../identis.php" class="active"> IDentis</a></li>
							<li><a href="works.php"> Trabajos</a></li>
							<li><a href="empresas.php"> Empresas</a></li>
						</ul>
						</li>

							<li>
						<a href="aplicados.php" class="dropdown-toggle no-arrow">
							<span class="micon ion-android-done-all"></span><span class="mtext">Aplicados</span>
						</a>
						<a href="employee/chat.php" class="dropdown-toggle no-arrow"><span class="micon icon-copy fa fa-comment"></span><span class="mtext">Chat</span></a>

					</li>';
					} else if ($myrole == "employer") {
						print '
						<a href="index.php" class="dropdown-toggle no-arrow">
						<span class="micon icon-copy fa fa-home"></span><span class="mtext">Panel</span></a>
						<a href="" class="dropdown-toggle no-arrow"><span class="micon icon-copy fa fa-bandcamp"></span><span class="mtext">identis</span></a>
						<a href="chat.php" class="dropdown-toggle no-arrow"><span class="micon icon-copy fa fa-comment"></span><span class="mtext">Chat</span></a>';
					}

					else {
						print '<a href="login.php" class="dropdown-toggle no-arrow ">
						<span class="micon icon-copy fa fa-lock"></span><span class="mtext">Iniciar Sesion</span></a>

						<a href="registro.php" class="dropdown-toggle no-arrow ">
						<span class="micon icon-copy fa fa-user"></span><span class="mtext">Registrarme</span></a>';
					}
						?>
					</li>
				</ul>
			</div>
		</div>
	</div>


	<div class="mobile-menu-overlay"></div>

	<div class="main-container">
		<div class="pd-ltr-20 xs-pd-20">
			<div class="contact-directory-list">
				<ul class="row">
					<?php
						try {
							$usuarios = $db->query("SELECT * FROM tbl_users WHERE role = 'employee' ORDER BY RAND()");

							foreach ($usuarios as $usuario) {
								$empavatar = $usuario['avatar'];
					?>

							<li class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
								<div class="contact-directory-box" style="-webkit-box-shadow: 0 0 28px rgb(0 0 0 / 8%); box-shadow: 0 0 28px rgb(0 0 0 / 8%);">
									<div class="contact-dire-info text-center">
										<div class="contact-avatar">
											<span>
												<?php
													if ($empavatar == null) {
														print '<center><img class="img-circle autofit2" src="images/default.png" alt="image"  /></center>';
													} else {
														echo '<center><img class="img-circle autofit2" alt="image" src="data:image/png;base64,' . base64_encode($empavatar) . '"/></center>';
													}
												?>
											</span>
										</div>

										<?php
											if ($empavatar == null) {
											} else {
												echo '';
											}
										?>

										<div class="contact-name">
											<h4><?php if ($usuario['empujar'] == "1") {
													print '🌟';
												} ?> 
												<?php echo strip_tags($usuario['first_name']) ?> 
												<?php echo strip_tags($usuario['last_name']) ?></h4>
											<?php

											$title = $usuario['title'];

											?>
											<div class="work text-success"> 
												<?php 
													if ($usuario['title']) {
														echo '💻 ' . $title . ' ';
													} 
												?>
											</div>
										</div>

										<div class="contact-skill">
											<?php
												$tags = explode(",", $usuario['ability']);

												foreach($tags as $i =>$key) {
													print '<span class="badge badge-pill">' . " $key " . '</span>';
												}
											?>

										</div>
										<div class="profile-sort-desc">
											<br><br><?php echo strip_tags(substr($usuario['about'], 0, 90)); ?>...
										</div>
									</div>
									<div class="view-contact">
										<?php 
											if ($myrole == "employee") {
												print "<a href='perfil.php?empid={$usuario['member_no']}'>Ver ID</a>";
											}
											if ($myrole == "employer") {
												print "<a href='cv.php?empid={$usuario['member_no']}'>Ver ID</a>";
											}
											if ($myrole == null) {
												print "<a href='perfil.php?empid={$usuario['member_no']}'>Ver ID</a>";
											}	
										?>
									</div>
								</div>
							</li>

					<?php
						}} catch (PDOException $e) {
						}
					?>

				</ul>
			</div>
		</div>
	</div>
	</div>
	</div>
	<!-- js -->
	<script src="v1/vendors/scripts/core.js"></script>
	<script src="v1/vendors/scripts/script.min.js"></script>
	<script src="v1/vendors/scripts/process.js"></script>
	<script src="v1/vendors/scripts/layout-settings.js"></script>
</body>

</html>