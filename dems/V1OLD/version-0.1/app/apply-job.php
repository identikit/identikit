<?php
    require '../constants/settings.php';
    use PHPMailer\PHPMailer\PHPMailer; 

    require_once "../PHPMailer/PHPMailer.php"; 
    require_once "../PHPMailer/SMTP.php"; 
    require_once "../PHPMailer/Exception.php"; 
    
    
    require_once("../config/Db.php");
	$db = new DbPDO();


    date_default_timezone_set($default_timezone);
    $apply_date = date('m/d/Y');

    session_start();
    if (isset($_SESSION['logged']) && $_SESSION['logged'] == true) {
        $myid = $_SESSION['myid'];	
        $myrole = $_SESSION['role'];
        $opt = $_GET['opt'];
        $opt1 = $_GET['opt1'];

    if ($myrole == "employee"){
        include '../constants/db_config.php';

        try {
                
            $job_aplicattions = $db->query("SELECT * FROM tbl_job_applications WHERE member_no = '$myid' AND job_id = :jobid",array("jobid"=>$opt));
            $rec = count($job_aplicattions);

            $usuarios = $db->query("select TCuenta,member_no from tbl_users as A inner join tbl_jobs as B where B.company = A.member_no and B.job_id=:jobid",array("jobid"=>$opt));

            $TCuenta = $usuarios[0]['TCuenta'];
            $Company = $usuarios[0]['member_no'];
            $num = 0;
            $conteo = 0;

            $job_aplicattions = $db->query("SELECT COUNT(*) as number_job FROM tbl_job_applications WHERE company = :company",array(":company",$Company));
            
            if ($job_aplicattions != null){
                $conteo = $job_aplicattions[0]['number_job'];
            } 

            switch($TCuenta){
                case 1:
                    $num = 6;
                    break;
                case 2:
                    $num = 30;
                    break;
                case 3:
                    $num = $rec;
                    break;
            }
	
            if ($rec == 0) {
            
            try {
                $checkExperience = $db->query("select ability from tbl_users where member_no='".$myid."'");
                $checkExperienceStatus = 0; 

                if ($checkExperience[0]["ability"] != ""){
                    $checkExperienceStatus = 1; 
                }

                if ($checkExperienceStatus){
                    if($conteo < $num){
                        $insertJob = $db->query("INSERT INTO tbl_job_applications (member_no,  company, job_id, application_date)
                        VALUES (:memberno, :company, :jobid, :appdate)",array("memberno"=>$myid,"company"=>$Company,"jobid"=>$opt,"appdate"=>$apply_date));

                        $jobs = $db->query("SELECT email FROM `tbl_jobs` as A inner join tbl_users as B where A.company = B.member_no and A.job_id=:jobid",array("jobid"=>$opt));
        
                        $email = "";
                        foreach($jobs as $job) {
                            $email = $job['email'];
                        }
                
                        $user = $_SESSION['myfname'] . " " .$_SESSION['mylname'];
                        $link = "https://idkit.com.ar/employer/postulados.php?jobid=" . $opt;
                
                        SendMail($email,$user, $opt1, $link);
                        
                        print '<br>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <strong>Felicidades!</strong> Aplicaste con exito.
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>';

                    } else {
                        print '<br>
                                <div class="alert alert-info alert-dismissible fade show" role="alert">
                                    La empresa no puede recibir mas candidatos
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>'; 
                    }
                } else {
                    print '<br> 
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                ¡Completa tu perfil antes de postular!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>'; 
                }
        }catch(PDOException $e){

    }

	}else{
                print '<br>
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <strong>Hey!</strong> Ya aplicaste a este empleo!
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                </div>';

        }}catch(PDOException $e){

        }
    }
}

function SendMail($email, $user, $vacante, $link){

    $mail = new PHPMailer(); 

    $mail->isSMTP(); 
    $mail->Host = 'smtp.hostinger.com';
    $mail->SMTPAuth = true; 
    $mail->Username = "hola@idkit.com.ar";  
    $mail->Password = "bB*TX27gR#Fs"; 
    $mail->Port = 465; 
    $mail->SMTPSecure = "ssl"; 
    $mail->CharSet = 'UTF-8';

    $mail->isHTML(true); 
    $mail->setFrom("hola@idkit.com.ar", "<no reply>"); 
    $mail->addAddress($email); 
    $mail->Subject = ("IDentiKIT"); 
    $mail->AddEmbeddedImage('../images/IFelicidades.png', 'IFelicidades', 'attachment', 'base64', 'image/png');

    $mail->Body = "    
        <center>
            <img src='cid:IFelicidades' class='img-logo' /> <br>
            ¡Hola! estamos muy felices de 
            contarte que " . $user ." se postulo para la vacante: \"".$vacante."\"  
            con el link: \" ". $link ." \"
        </center>
    ";

      
    if (preg_match('/(.*)@(hotmail)\.(.*)/', $email) != false) { 
        $mail->addCustomHeader('Mime-Version','1.0');
        $mail->addCustomHeader('Content-Type: text/html; charset=ISO-8859-1');
    } 

    if($mail->send()){
        return;
    } else {
        $status = "failed";
        $response = "Something is wrong: <br>";
        $email = $email;
        $user = $user;
    }

    exit(json_encode(array("status" => $status, "response" => $response,"email" => $email, "user" => $user)));
}

?>
