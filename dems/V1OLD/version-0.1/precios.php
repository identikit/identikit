<!DOCTYPE html>

<?php 
require 'constants/settings.php'; 
require 'constants/check-login.php';


if ($myrole == 'employee'){
	require 'employee/constants/check-login.php';
}else {
	require 'employee/constants2/check-login1.php';
	
}

?>
<html>
<head>
	<!-- Basic Page Info -->
	<meta charset="utf-8">
	<title>IDentiKIT - Explorar IDentis</title>

	
	<link rel="apple-touch-icon" sizes="180x180" href="logov3.png">
	<link rel="icon" type="image/png" sizes="32x32" href="logov3.png">
	<link rel="icon" type="image/png" sizes="16x16" href="logov3.png">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="v1/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="v1/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="v1/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="v1/src/plugins/datatables/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="v1/vendors/styles/style.css">

	<meta property="og:description" content="Tabla de precios de IDentiKIT para empresas" />
	<meta name="keywords" content="identikit, IDentiKIT, idkit, IDKIT, primera experiencia laboral, trabajos jr, junior, trabajos junior, empresas que contratan juniors, talento joven, talento tech, jovenes, empresas sin experiencia" />
	<meta name="author" content="IDentiKIT">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta property="og:image" content="https://identikit.app/logowebog.png" />
  <meta property="og:image:secure_url" content="https://identikit.app/logowebog.png" />
  <meta property="og:image:type" content="image/png" />
  <meta property="og:image:width" content="300" />
  <meta property="og:image:height" content="300" />
  <meta property="og:image:alt" content="IDentiKIT - Tu primera experiencia laboral" />
  <meta property="og:url" content="https://identikit.app/" />
  <meta property="og:type" content="website" />

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-K2NHRZT4R7"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-K2NHRZT4R7');
</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5QGSH5J');</script>
<!-- End Google Tag Manager -->





</head>
<body>

	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QGSH5J"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

	
	<!--<div class="pre-loader">
		<div class="pre-loader-box">
			<div class="loader-logo"><img src="v1/logov3.png" alt="" ></div>
			<div class='loader-progress' id="progress_div">
				<div class='bar' id='bar1' ></div>
			</div>
			<div class='percent' id='percent1'>0%</div>
			<div class="loading-text">
				Cargando...
			</div>
		</div>
	</div>-->
	<div class="header">
		<div class="header-left">
			<div class="menu-icon icon-copy ti-menu" style="color: white;"></div>
			
			<div class="search-toggle-icon dw dw-search2" style="color: #ffde00;" data-toggle="header_search"></div>
			<div class="header-search">
				<form method="post" action="busqueda.php" >
					<div class="form-group mb-0" >
						
					<i class="dw dw-search2 search-icon" style="color: #ffde00;"></i>
					
						<input type="text" class="form-control search-input" placeholder="Buscar en IDentiKIT" required name="PalabraClave">
						<input name="buscar" type="hidden" class="form-control mb-2" id="inlineFormInput" value="v">


					</div>
				</form>
			</div>
		</div>
		<div class="header-right">
			
			<!--<div class="user-notification">
				<div class="dropdown">
					<a class="dropdown-toggle no-arrow" href="#" role="button" data-toggle="dropdown">
						<i class="icon-copy dw dw-notification"></i>
						<span class="badge notification-active"></span>
					</a>
					<div class="dropdown-menu dropdown-menu-right">
						<div class="notification-list mx-h-350 customscroll">
							<ul>
								<li>
									<a href="#">
										<img src="vendors/images/img.jpg" alt="">
										<h3>John Doe</h3>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed...</p>
									</a>
								</li>
								
							</ul>
						</div>
					</div>
				</div>
			</div>-->
			<div class="user-info-dropdown">
				<div class="dropdown">
					<a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" style="color: #ffde00;">
					
						<span class="user-icon">
						<?php 
						if ($myavatar == null) {
							
							print '<center><img  src="images/default.png" title="" alt="image"  /></center>';
						}else{
							echo '<center><img alt="image" title=""  src="data:image/png;base64,'.base64_encode($myavatar).'"/></center>';	
						}
						?>	
																	
						</span>
					</a>
					<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
					<?php
						if ($user_online == true) {
						print '
							<a class="dropdown-item" href="'.$myrole.'"><i class="dw dw-user1"></i> Perfil</a>
							<!--<a class="dropdown-item" href="#"><i class="dw dw-settings2"></i> Ajustes</a>
							<a class="dropdown-item" href="#"><i class="dw dw-help"></i> Ayuda</a>-->
							<a class="dropdown-item" href="logout.php"><i class="dw dw-logout"></i> Salir</a>
';
						}else{
						print '
							<a class="dropdown-item" href="login.php"><i class="dw dw-lock"></i> Ingresar</a>
							<a class="dropdown-item"  href="registro.php"><i class="dw dw-user1"></i> Registrate</a>';						
						}
						
						?>
						
					</div>
				</div>
			</div>
			
		</div>
	</div>

	

	<div class="left-side-bar">
		<div class="brand-logo">
			<a href="#">
				<img src="logov3.png" alt="Perfil verificado por IDentiKIT" class="light-logo" width="60"> <span class="mtext"> IDentiKIT</span>
			</a>
			<div class="close-sidebar" data-toggle="left-sidebar-close">
				<i class="ion-close-round"></i>
			</div>
		</div>
		<div class="menu-block customscroll">
			<div class="sidebar-menu">
				<ul id="accordion-menu">
					<li>
					
					<?php
					if ($myrole == "employee") {
						print '<a href="index.php" class="dropdown-toggle no-arrow ">
						<span class="micon icon-copy fa fa-home"></span><span class="mtext">Inicio</span></a>

						<li class="dropdown">
						<a href="javascript:;" class="dropdown-toggle">
							<span class="micon icon-copy fa fa-bandcamp"></span><span class="mtext">Explorar</span>
						</a>
						<ul class="submenu">
							<li><a href="identis.php" class="active"> IDentis</a></li>
							<li><a href="works.php"> Trabajos</a></li>
							<li><a href="empresas.php"> Empresas</a></li>
						</ul>
						</li>

							<li>
						<a href="aplicados.php" class="dropdown-toggle no-arrow">
							<span class="micon ion-android-done-all"></span><span class="mtext">Aplicados</span>

						</a>
					</li>
						';
					} if ($myrole == "employer") {
						print '<a href="'.$myrole.'/index.php" class="dropdown-toggle no-arrow ">
						<span class="micon icon-copy fa fa-home"></span><span class="mtext">Panel</span></a>
						<a href="identis.php" class="dropdown-toggle no-arrow"><span class="micon icon-copy fa fa-bandcamp"></span><span class="mtext">identis</span></a>
						
						
						
						';
					}

					if ($myrole == null) {
						print '<a href="login.php" class="dropdown-toggle no-arrow ">
						<span class="micon icon-copy fa fa-lock"></span><span class="mtext">Iniciar sesion</span></a>

						<a href="registro.php" class="dropdown-toggle no-arrow ">
						<span class="micon icon-copy fa fa-user"></span><span class="mtext">Registrarme</span></a>

						
					
						
						
						
						';
					
					}

						
						?>
							

						
					</li>
									


				
					
				</ul>
			</div>
		</div>
	</div>
	<div class="mobile-menu-overlay"></div>

	<div class="main-container">
		<div class="pd-ltr-20 xs-pd-20">
				
					


					
											
														



					


						<div class="contact-directory-list" >
										
										<!--<div class="col-lg-3 col-md-6 col-sm-12 mb-15">
											<span>Filtrar por:</span>
											<div class="btn-group-vertical">
												
												<button type="button" class="btn btn-light dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false">  <span class="caret"></span> </button>
												<div class="dropdown-menu">
													<a class="dropdown-item" href="#">Dropdown link</a>
													<a class="dropdown-item" href="#">Dropdown link</a>
												</div>
											</div>
										</div>-->
										<div class="row">
																<div class="col-md-4 mb-30">
																	<div class="card-box pricing-card-style2">
																		<div class="pricing-card-header">
																			<div class="left">
																				<h5>Free</h5>
																			</div>
																			<div class="right">
																				<div class="pricing-price">
																					U$D 0<span>/month</span>
																				</div>
																			</div>
																		</div>
																		<div class="pricing-card-body">
																			<div class="pricing-points">
																				<ul>
																					<li>3 Job Notices</li>
																					<li>6 Candidates</li>
																					<li>Live Chat Support</li>
																				</ul>
																			</div>
																		</div>
																		<div class="cta">
																			<a href="#" class="btn btn-primary btn-rounded btn-lg">Get Started</a>
																		</div>
																	</div>
																</div>
																<div class="col-md-4 mb-30">
																	<div class="card-box pricing-card-style2">
																		<div class="pricing-card-header">
																			<div class="left">
																				<h5>Prime</h5>
																			</div>
																			<div class="right">
																				<div class="pricing-price">
																					U$D 29,99<span>/month</span>
																				</div>
																			</div>
																		</div>
																		<div class="pricing-card-body">
																			<div class="pricing-points">
																				<ul>
																					<li>∞ Job Notices</li>
																					<li>∞ Candidates</li>
																					<li>Access ∞ to database</li>
																					<li>Notifications </li>
																					<li>Advanced user management tools</li>
																					<li>Live chat support</li>

																				</ul>
																			</div>
																		</div>
																		<div class="cta">
																			<a href="#" class="btn btn-primary btn-rounded btn-lg">Get Started</a>
																		</div>
																	</div>
																</div>
																<div class="col-md-4 mb-30">
																	<div class="card-box pricing-card-style2">
																		<div class="pricing-card-header">
																			<div class="left">
																				<h5>Basic</h5>
																			</div>
																			<div class="right">
																				<div class="pricing-price">
																					U$D 19,99<span>/month</span>
																				</div>
																			</div>
																		</div>
																		<div class="pricing-card-body">
																			<div class="pricing-points">
																				<ul>
																					<li>10 Job Notices</li>
																					<li>30 Candidates</li>
																					<li>Access Limited to database</li>
																					<li>Notifications Limited </li>
																					<li>Live chat support</li>
																				</ul>
																			</div>
																		</div>
																		<div class="cta">
																			<a href="#" class="btn btn-primary btn-rounded btn-lg">Get Started</a>
																		</div>
																	</div>
																</div>
															</div>
									</div>
								</div>


			</div>
			
		</div>
	</div>
	<!-- js -->
	<script src="v1/vendors/scripts/core.js"></script>
	<script src="v1/vendors/scripts/script.min.js"></script>
	<script src="v1/vendors/scripts/process.js"></script>
	<script src="v1/vendors/scripts/layout-settings.js"></script>
</body>
</html>