<?php 

	include '../constants/settings.php'; 
	include 'constants/check-login.php';

	require_once("../config/Db.php");
	$db = new DbPDO();
	session_start();

	$TipoCuenta   			= $_SESSION['TCuenta'];
	$myid         			= $_SESSION['myid'];

	$candidatos            	= $db->query("SELECT member_no FROM `tbl_job_applications` where company=:company and not EVacante = 5",array("company"=>$myid));
	$avisos                 = $db->query("SELECT job_Id FROM `tbl_jobs` where company=:company",array("company"=>$myid));
	$mensajesEnviados       = $db->query("SELECT message_id from tbl_messages where message_sender_id=:company",array("company"=>$myid));
	$likes                  = $db->query("SELECT like_id FROM `tbl_likes`where like_member_no=:company",array("company"=>$myid));

	$Jobs                  	= $db->query("SELECT COUNT(*) as JobsSaved FROM tbl_jobs where company = :company",array("company"=> $myid));
	$JobsSaved             	= $Jobs[0]['JobsSaved'];
	$Jobs_Applications     	= $db->query("SELECT COUNT(*) as JobsApplied FROM tbl_job_applications where company = :company",array("company" => $myid));
	
	$rec = $Jobs_Applications[0]['JobsApplied']; 
	$num = 0;
	$numjobs = 0;
		
	switch($TipoCuenta){
		case 1:
			$num = 6;
			$numjobs = 3;
			break;
		case 2:
			$num = 30;
			$numjobs = 10;
			break;
		case 3:
			$num = 99999;
			$numjobs = 99999;
			break;
	}

	$conteoMensajesEnviados =  count($mensajesEnviados);
	$conteoLikes            =  count($likes);

	if ($TipoCuenta != 3){
		$candidatosdisponibles 	= $num - count($candidatos);
	} else {
		$candidatosdisponibles  = "Ilimitado";
	}
	if ($TipoCuenta != 3){
		$avisosdisponibles   	= $numjobs - count($avisos);
	} else {
		$avisosdisponibles  = "Ilimitado";
	}

	if ($user_online == "true") {
		if ($myrole == "employer") {
		} else {
			header("location:../");		
		}
	} else {
		header("location:../");	
	}

	if ($logo == null) {
		header("Location: micuenta.php");
	}

	if ($city == null) {
		header("Location: micuenta.php");
	}

	if ($logo == null) {
		header("Location: micuenta.php");
	}

?>

<!DOCTYPE html>
<html>

<head>
	<!-- Basic Page Info -->
	<meta charset="utf-8">
	<title>IDentiKIT - Panel empresa</title>

	
	<link rel="apple-touch-icon" sizes="180x180" href="logov3.png">
	<link rel="icon" type="image/png" sizes="32x32" href="../logov3.png">
	<link rel="icon" type="image/png" sizes="16x16" href="../logov3.png">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="../v1/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="../v1/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="../v1/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../v1/src/plugins/datatables/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../v1/vendors/styles/style.css">


	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-PHBQLSG');</script>
	<!-- End Google Tag Manager -->


</head>
<body>

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PHBQLSG"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<?php include 'layouts/Header.php';?>

	<?php include 'layouts/Sidebar-menu.php';?>

			
	<div class="mobile-menu-overlay"></div>

    <div class="main-container">
        <div class="pd-ltr-20 xs-pd-20">
            <div class="min-height-200px">
            	<?php require 'constants/check_reply.php'; ?>

            	<div class="row">
            		<div class="col-xl-3 mb-30">
            			<div class="card-box height-100-p widget-style1">
            				<div class="d-flex flex-wrap align-items-center">
            					<div class="progress-data">
            						<div id="chart"><img src="img/avisos.png" style="width: 70%; height: 0%;"></div>
            					</div>
            					<div class="widget-data">
            						<div class="h4 mb-0"><?= $avisosdisponibles  ?></div>
            						<div class="weight-600 font-14">Avisos disponibles</div>
            					</div>
            				</div>
            			</div>
            		</div>
            		<div class="col-xl-3 mb-30">
            			<div class="card-box height-100-p widget-style1">
            				<div class="d-flex flex-wrap align-items-center">
            					<div class="progress-data">
            						<div id="chart2"><img src="img/JOVENES.png" style="width: 70%; height: 0%;"></div>
            					</div>
            					<div class="widget-data">
            						<div class="h4 mb-0"><?= $candidatosdisponibles ?></div>
            						<div class="weight-600 font-14">Candidatos disponibles</div>
            					</div>
            				</div>
            			</div>
            		</div>
            		<div class="col-xl-3 mb-30">
            			<div class="card-box height-100-p widget-style1">
            				<div class="d-flex flex-wrap align-items-center">
            					<div class="progress-data">
            						<div id="chart3"><img src="img/mensajes.png" style="width: 70%; height: 0%;"></div>
            					</div>
            					<div class="widget-data">
            						<div class="h4 mb-0"><?= $conteoMensajesEnviados ?></div>
            						<div class="weight-600 font-14">Mensajes enviados</div>
            					</div>
            				</div>
            			</div>
            		</div>

            		<div class="col-xl-3 mb-30">
            			<div class="card-box height-100-p widget-style1">
            				<div class="d-flex flex-wrap align-items-center">
            					<div class="progress-data">
            						<div id="chart3"><img src="img/likes.png" style="width: 70%; height: 0%;"></div>
            					</div>
            					<div class="widget-data">
            						<div class="h4 mb-0"><?= $conteoLikes ?></div>
            						<div class="weight-600 font-14">Likes</div>
            					</div>
            				</div>
            			</div>
            		</div>
            		
            	</div>

				
				<?php 

					try {

                         if(($rec < $num) && ($JobsSaved <= $numjobs - 1)){

                        	echo '<center><a href="postjob.php" class="btn btn-primary btn-lg col-md-12">Publicar aviso</a></center>';
						} else {
							echo '<a class="btn btn-warning text-white btn-lg col-md-12" href="../precios.php#">Actualizar cuenta</a>';
						}}catch(PDOException $e){
							echo 'Excepción capturada: ',  $e->getMessage(), "\n";
						}
								 
				?>
										
				
				<div class="card-box mb-30">
					<div class="pd-20">
						<h4 class="h4">Mis avisos</h4>
					</div>
					<div class="pb-20">
						<table class="data-table table stripe hover nowrap">
							<thead>
								<tr>
									<th class="table-plus datatable-nosort">Titulo</th>
									<th>Ciudad</th>
									<th>Finaliza</th>
									<th class="datatable-nosort">Acciones</th>
								</tr>
							</thead>
							<tbody>
							<?php

								try {
                                    $tbl_jobs = $db->query("SELECT * FROM tbl_jobs WHERE company = :myid ORDER BY enc_id DESC",array("myid"=>$myid));
                                    
                                    foreach($tbl_jobs as $job){
										   $jobcity  = $job['city'];
										   $title    = $job['title'];
										   $deadline = $job['closing_date'];
							?>

							<tr>
								<td class="table-plus"><?php echo "$title" ?></td>
								<td><?php echo "$jobcity" ?></td>
								<td><?php echo "$deadline" ?></td>
								<td>
									<div class="dropdown">
										<a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
											<i class="dw dw-more"></i>
										</a>
										<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
											<a class="dropdown-item" href="postulados.php?jobid=<?=$job['job_id']; ?>"><i class="dw dw-eye"></i> Postulantes</a>
											<a class="dropdown-item" href="editar.php?jobid=<?=$job['job_id']; ?>"><i class="dw dw-edit2"></i> Editar</a>
											<a class="dropdown-item" onclick = "return confirm('�Deseas eliminar este aviso?')" href="app/drop-job.php?id=<?=$job['job_id'];?>"><i class="dw dw-delete-3"></i> Borrar</a>
										</div>
									</div>
								</td>
							</tr>
								
							<?php
								}}catch(PDOException $e){
									echo 'Excepción capturada: ',  $e->getMessage(), "\n";
                            	}
                            ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
</body>
  <!-- js -->
    <script src="../v1/vendors/scripts/core.js"></script>
    <script src="../v1/vendors/scripts/script.min.js"></script>
    <script src="../v1/vendors/scripts/process.js"></script>
    <script src="../v1/vendors/scripts/layout-settings.js"></script>
	<script src="../v1/src/plugins/datatables/js/jquery.dataTables.min.js"></script>
	<script src="../v1/src/plugins/datatables/js/dataTables.bootstrap4.min.js"></script>
	<script src="../v1/src/plugins/datatables/js/dataTables.responsive.min.js"></script>
	<script src="../v1/src/plugins/datatables/js/responsive.bootstrap4.min.js"></script>
	<!-- buttons for Export datatable -->
	<script src="../v1/src/plugins/datatables/js/dataTables.buttons.min.js"></script>
	<script src="../v1/src/plugins/datatables/js/buttons.bootstrap4.min.js"></script>
	<script src="../v1/src/plugins/datatables/js/buttons.print.min.js"></script>
	<script src="../v1/src/plugins/datatables/js/buttons.html5.min.js"></script>
	<script src="../v1/src/plugins/datatables/js/buttons.flash.min.js"></script>
	<script src="../v1/src/plugins/datatables/js/pdfmake.min.js"></script>
	<script src="../v1/src/plugins/datatables/js/vfs_fonts.js"></script>
	<!-- Datatable Setting js -->
	<script src="../v1/vendors/scripts/datatable-setting.js"></script></body>
	<script src="../v1/src/plugins/sweetalert2/sweetalert2.all.js"></script>
	<script src="../v1/src/plugins/sweetalert2/sweet-alert.init.js"></script>

</html>








