<div class="header">
		<div class="header-left">
			<div class="menu-icon icon-copy ti-menu" style="color: white;"></div>
			<div class="search-toggle-icon dw dw-search2" style="color: #ffde00;" data-toggle="header_search"></div>
			<div class="header-search">
				<form method="post" action="../busqueda.php" >
					<div class="form-group mb-0" >
					<i class="dw dw-search2 search-icon" style="color: #ffde00;"></i>
						<input type="text" class="form-control search-input" placeholder="Buscar en IDentiKIT" required name="PalabraClave">
						<input name="buscar" type="hidden" class="form-control mb-2" id="inlineFormInput" value="v">
					</div>
				</form>
			</div>
		</div>
		<div class="header-right">
			<div class="user-info-dropdown">
				<div class="dropdown">
					<a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" style="color: #ffde00;">
						<span class="user-icon">
                        <?php 
                            if ($logo == null) {
                                print '<img  src="../images/default.png" title="'.$compname.'" alt="image"  /></center>';
                            } else {
                                echo '<center><img alt="image" title="'.$compname.'" width="180" height="100" src="data:image/jpeg;base64,'.base64_encode($logo).'"/></center>';
                            }
						?>
						</span>
					</a>
					<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
						<?php
                            if ($user_online == true) {
                            print '
                                <a class="dropdown-item" href="micuenta.php"><i class="dw dw-user1"></i> Perfil</a>
                                <!--<a class="dropdown-item" href="#"><i class="dw dw-settings2"></i> Ajustes</a>
                                <a class="dropdown-item" href="#"><i class="dw dw-help"></i> Ayuda</a>-->
                                <a class="dropdown-item" href="logout.php"><i class="dw dw-logout"></i> Salir</a>';
                            } else {
                            print '
                                <li><a href="login.php">ingresar</a></li>
                                <li><a data-toggle="modal" href="#registerModal">registrate</a></li>';						
                            }					
						?>
					</div>
				</div>
			</div>
			
		</div>
	</div>
