<?php 
include 'constants/settings.php'; 
include 'constants/check-login.php';
?>
<!DOCTYPE html>
<html>
<head>
  <!-- Basic Page Info -->
  <meta charset="utf-8">
  <title>IDentiKIT - Registro IDentis</title>

  <!-- Site favicon -->
  <link href="logov3.png" rel="icon" type="image/png">
  <!-- Mobile Specific Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

  <!-- Google Font -->
  <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
  <!-- CSS -->
  <link rel="stylesheet" type="text/css" href="v1/vendors/styles/core.css">
  <link rel="stylesheet" type="text/css" href="v1/vendors/styles/icon-font.min.css">
  <link rel="stylesheet" type="text/css" href="v1/vendors/styles/style.css">

  <meta name="description" content="Registrate en IDentiKIT como empresa y consigue el mejor talento tech joven en un solo lugar." />
  <meta name="keywords" content="identikit, IDentiKIT, idkit, IDKIT, primera experiencia laboral, trabajos jr, junior, trabajos junior, empresas que contratan juniors, talento joven, talento tech, jovenes, empresas sin experiencia" />
  <meta name="author" content="IDentiKIT">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta property="og:image" content="https://identikit.app/logowebog.png" />
  <meta property="og:image:secure_url" content="https://identikit.app/logowebog.png" />
  <meta property="og:image:type" content="image/png" />
  <meta property="og:image:width" content="300" />
  <meta property="og:image:height" content="300" />
  <meta property="og:image:alt" content="IDentiKIT - Tu primera experiencia laboral" />
  <meta property="og:url" content="https://identikit.app/" />
  <meta property="og:type" content="website" />
    

  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-K2NHRZT4R7"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-K2NHRZT4R7');
</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5QGSH5J');</script>
<!-- End Google Tag Manager -->


  
</head>
<body class="login-page">

  <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QGSH5J"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

  <div class="login-header box-shadow">
    <div class="container-fluid d-flex justify-content-between align-items-center">
      <div class="brand-logo">
        <a href="principal.php">
          <img src="logo.png" alt="" width="50">
        </a>
      </div>
      
    </div>
  </div>
  <div class="login-wrap d-flex align-items-center flex-wrap justify-content-center">
    <div class="container">
      <div class="row align-items-center">
        
        <div class="col-md-6 col-lg-12">
          <div class="login-box bg-white box-shadow border-radius-10">
            <div class="login-title">
              <center><img src="logo.png" class="text-center text-primary" style="width: 30%; height: 30%;"></center>
            </div> 
            <?php include 'constants/check_reply.php';?>
            <form name="frm" action="app/create-account.php" method="POST" autocomplete="off" role="form">
              
              <div class="input-group custom">
                <input type="text" class="form-control form-control-lg" placeholder="Nombre de la empresa" name="company">
                <div class="input-group-append custom">
                  <span class="input-group-text"><i class="icon-copy dw dw-coding-1"></i></span>
                </div>
              </div>

              

              <div class="input-group custom">
                <input type="text" class="form-control form-control-lg" placeholder="Rubro" name="type">
                <div class="input-group-append custom">
                  <span class="input-group-text"><i class="icon-copy dw dw-meeting"></i></span>
                </div>
              </div>

              <div class="input-group custom">
                <input type="email" class="form-control form-control-lg" placeholder="Email" name="email">
                <div class="input-group-append custom">
                  <span class="input-group-text"><i class="icon-copy dw dw-email"></i></span>
                </div>
              </div>

              <div class="input-group custom">
                <input type="password" class="form-control form-control-lg" placeholder="Contraseña" name="password">
                <div class="input-group-append custom">
                  <span class="input-group-text"><i class="dw dw-padlock1"></i></span>
                </div>
              </div>

               <div class="input-group custom">
                <input type="password" class="form-control form-control-lg" placeholder="Confirmar contraseña" name="confirmpassword">
                <div class="input-group-append custom">
                  <span class="input-group-text"><i class="dw dw-padlock1"></i></span>
                </div>
              </div>
              <input type="hidden" name="acctype" value="102">
              <div class="row">
                <div class="col-sm-12">
                  <div class="input-group mb-0">
                    <p class="text-muted font-14" style="font-size: 10px;">Al hacer clic en "Registrarte", aceptas nuestras Condiciones, la Política de datos y la Política de cookies. Es posible que te enviemos notificaciones por SMS o Email, que puedes desactivar cuando quieras.</p>
                    

                    <button onclick="return val();" type="submit" name="reg_mode" class="btn btn-primary btn-sm btn-block">Registrarte</button>
                  </div>


                  
                  
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- js -->
  <script src="v1/vendors/scripts/core.js"></script>
  <script src="v1/vendors/scripts/script.min.js"></script>
  <script src="v1/vendors/scripts/process.js"></script>
  <script src="v1/vendors/scripts/layout-settings.js"></script>
</body>
</html>