<!DOCTYPE html>
<html>
<head>
	<!-- Basic Page Info -->
	<meta charset="utf-8">
	<title>IDentiKIT - ¡Crea tu cv y encontra trabajo rapido!</title>

	<!-- Site favicon 
	<link rel="apple-touch-icon" sizes="180x180" href="vendors/images/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="vendors/images/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="vendors/images/favicon-16x16.png">-->
	
	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="src/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="src/plugins/datatables/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="vendors/styles/style.css">

</head>
<body>
	

	<div class="header">
		<div class="header-left">
			<div class="menu-icon icon-copy ti-menu" style="color: white;"></div>
			
			<div class="search-toggle-icon dw dw-search2" style="color: #ffde00;" data-toggle="header_search"></div>
			<div class="header-search">
				<form>
					<div class="form-group mb-0" >
						<i class="dw dw-search2 search-icon" style="color: #ffde00;"></i>
						<input type="text" class="form-control search-input" placeholder="Buscar en IDentiKIT">
					</div>
				</form>
			</div>
		</div>
		<div class="header-right">
			
			<!--<div class="user-notification">
				<div class="dropdown">
					<a class="dropdown-toggle no-arrow" href="#" role="button" data-toggle="dropdown">
						<i class="icon-copy dw dw-notification"></i>
						<span class="badge notification-active"></span>
					</a>
					<div class="dropdown-menu dropdown-menu-right">
						<div class="notification-list mx-h-350 customscroll">
							<ul>
								<li>
									<a href="#">
										<img src="vendors/images/img.jpg" alt="">
										<h3>John Doe</h3>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed...</p>
									</a>
								</li>
								
							</ul>
						</div>
					</div>
				</div>
			</div>-->
			<div class="user-info-dropdown">
				<div class="dropdown">
					<a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" style="color: #ffde00;">
						<span class="user-icon">
							<img src="vendors/images/photo1.jpg" alt="">
						</span>
						<span class="user-name" style="color: white;">Apodo</span>
					</a>
					<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
						<a class="dropdown-item" href="miperfil.php"><i class="dw dw-user1"></i> Pefil</a>
						<a class="dropdown-item" href="#"><i class="dw dw-settings2"></i> Ajustes</a>
						<a class="dropdown-item" href="#"><i class="dw dw-help"></i> Ayuda</a>
						<a class="dropdown-item" href="#"><i class="dw dw-logout"></i> Salir</a>
					</div>
				</div>
			</div>
			
		</div>
	</div>

	

	<div class="left-side-bar">
		<div class="brand-logo">
			<a href="#">
				<img src="logov3.png" alt="" class="light-logo" width="60"> <span class="mtext"> IDentiKIT</span>
			</a>
			<div class="close-sidebar" data-toggle="left-sidebar-close">
				<i class="ion-close-round"></i>
			</div>
		</div>
		<div class="menu-block customscroll">
			<div class="sidebar-menu">
				<ul id="accordion-menu">
					<li>
						<a href="index.php" class="dropdown-toggle no-arrow active">
							<span class="micon icon-copy fa fa-home"></span><span class="mtext">Incio</span>

						</a>
					</li>
					
					<li class="dropdown">
						<a href="javascript:;" class="dropdown-toggle">
							<span class="micon icon-copy fa fa-bandcamp"></span><span class="mtext">Explorar</span>
						</a>
						<ul class="submenu" style="text-decoration: none !important;">
							<li><a href="identis.php">
								<span class="micon icon-copy fa fa-bandcamp"></span><span class="mtext"> IDentis</span>
							</a>
						</li>
							<li><a href="works.php">Trabajos</a></li>
						</ul>
					</li>
					
				</ul>
			</div>
		</div>
	</div>
	<div class="mobile-menu-overlay"></div>

<style type="text/css">
  
  .btn-idkit {
    display:block;
    width:70px;
    height:70px;
    color: #fff;
    position: fixed;
    right:20px;
    bottom:20px;
    border-radius:60%;
    line-height:80px;
    text-align:center;
    z-index:999;
  }

  .rainbow-button {

  background-image: linear-gradient(90deg, #01c0fe 0%, #1bff00 49%, #fd0098 80%, #5600ff 100%);
  
  display:flex;
  align-items:center;
  justify-content:center;
  text-transform:uppercase;
  font-size:3vw;
  font-weight:bold;
  border-radius: 100px;

  content:attr(alt);
  
  background-color:#191919;
  display:flex;
  align-items:center;
  justify-content:center;

  animation:slidebg 2s linear infinite;


}


@keyframes slidebg {
  to {
    background-position:20vw;
  }
}
</style>

<div class="btn-idkit center wow animated bounceInRight box1 " data-wow-delay="0.2s" data-toggle="tooltip" title="Contactar"  >
    <a href="chat.php" >
    <img src="logov3.png" alt="Contactar" class="rainbow-button">
    </a>
    </div>

	<div class="main-container">
			<div class="pd-ltr-20 xs-pd-20-10">
				<div class="min-height-200px">
					<div class="row">
						<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 mb-30">
							<div class="pd-20 card-box height-100-p">
								<div class="profile-photo">
									
									<img src="vendors/images/photo1.jpg" alt="" class="avatar-photo">
									<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
										<div class="modal-dialog modal-dialog-centered" role="document">
											<div class="modal-content">
												<div class="modal-body pd-5">
													<div class="img-container">
														<img id="image" src="vendors/images/photo2.jpg" alt="Picture" >
													</div>
												</div>
												<div class="modal-footer">
													<input type="submit" value="Update" class="btn btn-primary">
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>
								</div>
								<h5 class="text-center h5 mb-0 " >Nombre y apellido</h5>
								<div class="work text-success text-center pd-10"><i class="ion-android-person"></i> Puesto/carrera</div>
								<p class="text-center text-muted font-14">Descripcion Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim ven</p>

								<div class="contact-directory-box pd-0">
									<div class="contact-dire-info text-center pd-0">
										
										
										<div class="contact-skill pd-0">
											<span class="badge badge-pill">TAG 1</span>
											<span class="badge badge-pill">TAG 2</span>
											<span class="badge badge-pill">TAG 3</span>
											
										</div>

										<div class="timeline mb-30">
									<h5 class="text-center mb-20 h5 mb-0 pd-10">Experiencia laboral</h5>
									<ul>
										<li>
											<div class="timeline-date">
												2015-2017
											</div>
											<div class="timeline-desc card-box">
												<div class="pd-20">
													<h4 class="mb-10 h4">Titulo</h4>
													<p class="text-center text-muted font-14">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
												</div>
											</div>
										</li>

										<li>
											<div class="timeline-date">
												2015-2017
											</div>
											<div class="timeline-desc card-box">
												<div class="pd-20">
													<h4 class="mb-10 h4">Titulo</h4>
													<p class="text-center text-muted font-14">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
												</div>
											</div>
										</li>


										<li>
											<div class="timeline-date">
												2015-2017
											</div>
											<div class="timeline-desc card-box">
												<div class="pd-20">
													<h4 class="mb-10 h4">Titulo</h4>
													<p class="text-center text-muted font-14">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
												</div>
											</div>
										</li>

									</ul>
								</div>
						</div>
					</div>


					


							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	<!-- js -->
	<script src="vendors/scripts/core.js"></script>
	<script src="vendors/scripts/script.min.js"></script>
	<script src="vendors/scripts/process.js"></script>
	<script src="vendors/scripts/layout-settings.js"></script>
	<script src="src/plugins/apexcharts/apexcharts.min.js"></script>
	<script src="src/plugins/datatables/js/jquery.dataTables.min.js"></script>
	<script src="src/plugins/datatables/js/dataTables.bootstrap4.min.js"></script>
	<script src="src/plugins/datatables/js/dataTables.responsive.min.js"></script>
	<script src="src/plugins/datatables/js/responsive.bootstrap4.min.js"></script>
	<script src="vendors/scripts/dashboard.js"></script>
</body>
</html>