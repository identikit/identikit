<!DOCTYPE html>

<?php 

	require 'employee/constants/check-login.php';

	require_once("./config/Db.php");
	$db = new DbPDO();

	if ($user_online == "true") {
	if ($myrole == "employee") {
	}else{
	header("location:employer/index.php");	}
	}else{
	header("location:principal.php");	
	}
?>

<html>
<head>
	<meta charset="utf-8">

	<title>IDentiKIT - Inicio </title>

	
	<link rel="apple-touch-icon" sizes="180x180" href="logov3.png">
	<link rel="icon" type="image/png" sizes="32x32" href="logov3.png">
	<link rel="icon" type="image/png" sizes="16x16" href="logov3.png">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="public/css/v1/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="public/css/v1/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="public/css/v1/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="public/css/v1/src/plugins/datatables/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="public/css/v1/vendors/styles/style.css">


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-K2NHRZT4R7"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-K2NHRZT4R7');
</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5QGSH5J');</script>
<!-- End Google Tag Manager -->

</head>
<body>
	

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QGSH5J"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<div class="header">
		<div class="header-left">
			<div class="menu-icon icon-copy ti-menu" style="color: white;"></div>
			<div class="search-toggle-icon dw dw-search2" style="color: #ffde00;" data-toggle="header_search"></div>
			<div class="header-search">
			<?php include 'conexion.php';?>
				<form method="post" action="busqueda.php" >
					<div class="form-group mb-0" >
					<i class="dw dw-search2 search-icon" style="color: #ffde00;"></i>
						<input type="text" class="form-control search-input" placeholder="Buscar en IDentiKIT" required name="PalabraClave">
						<input name="buscar" type="hidden" class="form-control mb-2" id="inlineFormInput" value="v">
					</div>
				</form>
			</div>
		</div>
		<div class="header-right">
			<div class="user-info-dropdown">
				<div class="dropdown">
					<a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" style="color: #ffde00;">
						<span class="user-icon">
						<?php 
							if ($myavatar == null) {
								print '<center><img  src="images/default.png" title="'.strip_tags($myfname).'" alt="image"  /></center>';
							}else{
								echo '<center><img alt="image" title="'.strip_tags($myfname).'"  src="data:image/png;base64,'.base64_encode($myavatar).'"/></center>';	
							}
						?>											
						</span>
					</a>
					<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
						<?php
							if ($user_online == true) {
							print '
								<a class="dropdown-item" href="'.$myrole.'"><i class="dw dw-user1"></i> Perfil</a>
								<!--<a class="dropdown-item" href="#"><i class="dw dw-settings2"></i> Ajustes</a>
								<a class="dropdown-item" href="#"><i class="dw dw-help"></i> Ayuda</a>-->
								<a class="dropdown-item" href="logout.php"><i class="dw dw-logout"></i> Salir</a>';
							}else{
							print '
								<li><a href="login.php">ingresar</a></li>
								<li><a data-toggle="modal" href="#registerModal">registrate</a></li>';						
							}
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="left-side-bar">
		<div class="brand-logo">
			<a href="#">
				<img src="logov3.png" alt="" class="light-logo" width="60"><span class="mtext" style="margin:10px;">IDentiKIT</span>
			</a>
			<div class="close-sidebar" data-toggle="left-sidebar-close">
				<i class="ion-close-round"></i>
			</div>
		</div>
		<div class="menu-block customscroll">
			<div class="sidebar-menu">
				<ul id="accordion-menu">
					<?php
						if ($myrole == "employee") {
							print '<a href="index.php" class="dropdown-toggle no-arrow active">
							<span class="micon icon-copy fa fa-home"></span><span class="mtext">Inicio</span></a>

							<li class="dropdown">
								<a href="javascript:;" class="dropdown-toggle">
									<span class="micon icon-copy fa fa-bandcamp"></span><span class="mtext">Explorar</span>
								</a>
								<ul class="submenu">
									<li><a href="identis.php" > IDentis</a></li>
									<li><a href="works.php"> Trabajos</a></li>
									<li><a href="empresas.php"> Empresas</a></li>
								</ul>
							</li>

							<li>
								<a href="aplicados.php" class="dropdown-toggle no-arrow">
									<span class="micon ion-android-done-all"></span><span class="mtext">Aplicados</span>
								</a>
							</li>
							<li>
								<a href="employee/chat.php" class="dropdown-toggle no-arrow"><span class="micon icon-copy fa fa-comment"></span><span class="mtext">Chat</span></a>
							</li>';
						} else {
							print '<a href="dashboard.php" class="dropdown-toggle no-arrow ">
							<span class="micon icon-copy fa fa-home"></span><span class="mtext">Panel</span></a>
							<a href="../identis.php" class="dropdown-toggle no-arrow"><span class="micon icon-copy fa fa-bandcamp"></span><span class="mtext">identis</span></a>';
						}	
					?>						
					<li>
						<a href="academy.php" class="dropdown-toggle no-arrow ">
							<span class="micon icon-copy fa fa-free-code-camp"></span><span class="mtext">Academia</span>
						</a>
					</li>	
				</ul>
			</div>
		</div>
	</div>
	<div class="mobile-menu-overlay"></div>

	<div class="main-container">
		<div class="pd-ltr-20 xs-pd-20">
			<div class="container pd-0">							
				<div class="container pd-0">
					<div class="page-header">
						<div class="row">
							<div class="col-md-12 col-sm-12">
								<div class="title">
									<h4>Recomendado para ti!</h4>
								</div>
								
							</div>
						</div>
					</div>
					<div class="product-wrap">
						<div class="product-list">
							<ul class="row">
							<?php
							try {
								$jobs = $db->query("SELECT * FROM tbl_jobs ORDER BY RAND() LIMIT 6");
								
								foreach($jobs as $job) {
									$jobcity = $job['city'];
									$jobcountry = $job['country'];
									$type = $job['type'];
									$title = $job['title'];
									$closingdate = $job['closing_date'];
									$company_id = $job['company'];
									
									$usuarios = $db->query("SELECT * FROM tbl_users WHERE member_no = '".$company_id."' and role = 'employer'");
									
									foreach($usuarios as $usuario) {
										$complogo = $usuario['avatar'];
										$thecompname = $usuario['first_name'];	
									}
								
									if ($type == "Freelance") {
										$sta = '<div class="job-label label label-success">
												Freelance
												</div>';
													
									}
									if ($type == "Part-time") {
										$sta = '<div class="job-label label label-danger">
												Part-time
												</div>';			
									}
									if ($type == "Full-time") {
										$sta = '<div class="job-label label label-warning">
												Full-time
												</div>';
									}
							?>
								<li class="col-lg-4 col-md-6 col-sm-12">
									<div class="product-box">
										<div class="product-caption">
											<h4><a href="#"><?php echo strip_tags($title); ?></a></h4>
											<div class="price">
												
												<ins><span>📙 <?php echo $job['type']?></span> 📍 <?php echo strip_tags($job['city']); ?></ins>
												<p class="card-text"><?php echo strip_tags(substr($job['description'],0,210)); ?></p>

												<a href="work-detail.php?identiwork=<?php echo $job['job_id']; ?>" class="btn btn-primary col-md-12" style="background-color: #01c0fe; border: none;">Postularme</a>
											</div>
											
										</div>
									</div>
								</li>
								<?php
									}}catch(PDOException $e){ 
						
									}
                             	?>
								
							</ul>
								<div class="row">
									<div class="col-md-12 col-sm-12">
										<div class="title">
											<a href="works.php" class="btn btn-danger col-md-12" type="button">Explorar</a>
										</div>
										
									</div>
								</div>
							</div><br><br>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	<!-- js -->
	<script src="public/css/v1/vendors/scripts/core.js"></script>
	<script src="public/css/v1/vendors/scripts/script.min.js"></script>
	<script src="public/css/v1/vendors/scripts/process.js"></script>
	<script src="public/css/v1/vendors/scripts/layout-settings.js"></script>
	<script src="public/css/v1/src/plugins/apexcharts/apexcharts.min.js"></script>
	<script src="public/css/v1/src/plugins/datatables/js/jquery.dataTables.min.js"></script>
	<script src="public/css/v1/src/plugins/datatables/js/dataTables.bootstrap4.min.js"></script>
	<script src="public/css/v1/src/plugins/datatables/js/dataTables.responsive.min.js"></script>
	<script src="public/css/v1/src/plugins/datatables/js/responsive.bootstrap4.min.js"></script>
	<script src="public/css/v1/vendors/scripts/dashboard.js"></script>
</body>
</html>