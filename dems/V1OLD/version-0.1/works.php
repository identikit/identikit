<!DOCTYPE html>
<html>

<?php
	require 'constants/settings.php';
	require 'constants/check-login.php';

	require_once("./config/Db.php");
	$db = new DbPDO();

	$fromsearch = false;

	if (isset($_GET['search']) && $_GET['search'] == "✓") {
	} else {
	}
	if (isset($_GET['country']) && ($_GET['category'])) {
		$cate = $_GET['category'];
		$country = $_GET['country'];
		$query1 = "SELECT * FROM tbl_jobs WHERE category = :cate AND city = :country ORDER BY enc_id DESC";
		$query2 = "SELECT * FROM tbl_jobs WHERE category = :cate AND city = :country ORDER BY enc_id DESC";
		$fromsearch = true;

		$slc_country = "$country";
		$slc_category = "$cate";
		$title = "$slc_category empleos en $slc_country";
	} else {
		$query1 = "SELECT * FROM tbl_jobs ORDER BY enc_id DESC";
		$query2 = "SELECT * FROM tbl_jobs ORDER BY enc_id DESC";
		$slc_country = "NULL";
		$slc_category = "NULL";
	}

	if ($myrole == 'employee') {
		require 'employee/constants/check-login.php';
	} else {
		require 'employee/constants2/check-login1.php';
	}
?>

<style type="text/css">
	.horizontal-scroll-contenedor div {
		width: 100px;
		height: 50px;
		margin: 0 10px 0 0;
		padding: 0;
		display: inline-block;
		border: 1px red solid;
	}

	.horizontal-scroll-contenedor {
		width: auto;
		height: 70px;
		padding: 10px;
		white-space: nowrap;
		overflow-y: hidden;
		overflow-x: auto;
		text-decoration: none;
	}
</style>



<head>
	<!-- Basic Page Info -->
	<meta charset="utf-8">
	<title>IDentiKIT - Explorar trabajos</title>

	<link rel="apple-touch-icon" sizes="180x180" href="logov3.png">
	<link rel="icon" type="image/png" sizes="32x32" href="logov3.png">
	<link rel="icon" type="image/png" sizes="16x16" href="logov3.png">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="v1/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="v1/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="v1/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="v1/src/plugins/datatables/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="v1/vendors/styles/style.css">
	<link rel="stylesheet" type="text/css" href="v1/src/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">

	<meta property="og:image" content="https://identikit.app/logowebog.png" />
	<meta property="og:image:secure_url" content="https://identikit.app/logowebog.png" />
	<meta property="og:image:type" content="image/png" />
	<meta property="og:image:width" content="300" />
	<meta property="og:image:height" content="300" />
	<meta property="og:image:alt" content="IDentiKIT - Tu primera experiencia laboral" />
	<meta property="og:description" content="😎Tu primer trabajo <TECH> en empresas de alto rendimiento a un click de distancia en un solo lugar.📣 Fácil, rápido y seguro" />

	<script type="text/javascript">
		function update(str) {
			var txt;
			var r = confirm("Are you sure you want to apply this job , you can not UNDO");
			if (r == true) {
				document.getElementById("data").innerHTML = "Please wait...";
				var xmlhttp;
				if (window.XMLHttpRequest) {
					xmlhttp = new XMLHttpRequest();
				} else {
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}

				xmlhttp.onreadystatechange = function() {
					if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
						document.getElementById("data").innerHTML = xmlhttp.responseText;
					}
				}

				xmlhttp.open("GET", "app/apply-job.php?opt=" + str, true);
				xmlhttp.send();
			} else {

			}
		}
	</script>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-K2NHRZT4R7"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'G-K2NHRZT4R7');
	</script>

	<!-- Google Tag Manager -->
	<script>
		(function(w, d, s, l, i) {
			w[l] = w[l] || [];
			w[l].push({
				'gtm.start': new Date().getTime(),
				event: 'gtm.js'
			});
			var f = d.getElementsByTagName(s)[0],
				j = d.createElement(s),
				dl = l != 'dataLayer' ? '&l=' + l : '';
			j.async = true;
			j.src =
				'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
			f.parentNode.insertBefore(j, f);
		})(window, document, 'script', 'dataLayer', 'GTM-5QGSH5J');
	</script>
	<!-- End Google Tag Manager -->
</head>

<body>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QGSH5J" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<div class="header">
		<div class="header-left">
			<div class="menu-icon icon-copy ti-menu" style="color: white;"></div>

			<div class="search-toggle-icon dw dw-search2" style="color: #ffde00;" data-toggle="header_search"></div>
			<div class="header-search">
				<form method="post" action="busqueda.php">
					<div class="form-group mb-0">
						<i class="dw dw-search2 search-icon" style="color: #ffde00;"></i>
						<input type="text" class="form-control search-input" placeholder="Buscar en IDentiKIT" required name="PalabraClave">
						<input name="buscar" type="hidden" class="form-control mb-2" id="inlineFormInput" value="v">
					</div>
				</form>
			</div>
		</div>
		<div class="header-right">
			<div class="user-info-dropdown">
				<div class="dropdown">
					<a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" style="color: #ffde00;">
						<span class="user-icon">
							<?php
								if ($myavatar == null) {
									print '<center><img  src="images/default.png" title="" alt="image"  /></center>';
								} else {
									echo '<center><img alt="image" title=""  src="data:image/png;base64,' . base64_encode($myavatar) . '"/></center>';
								}
							?>
						</span>
					</a>
					<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
						<?php
							if ($user_online == true) {
								print '<a class="dropdown-item" href="' . $myrole . '"><i class="dw dw-user1"></i> Perfil</a>
								<!--<a class="dropdown-item" href="#"><i class="dw dw-settings2"></i> Ajustes</a>
								<a class="dropdown-item" href="#"><i class="dw dw-help"></i> Ayuda</a>-->
								<a class="dropdown-item" href="logout.php"><i class="dw dw-logout"></i> Salir</a>';
							} else {
								print '
								<a class="dropdown-item" href="login.php"><i class="dw dw-lock"></i> Ingresar</a>
								<a class="dropdown-item"  href="registro.php"><i class="dw dw-user1"></i> Registrate</a>';
							}
						?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="left-side-bar">
		<div class="brand-logo">
			<a href="#">
				<img src="logov3.png" alt="" class="light-logo" width="60"> <span class="mtext"> IDentiKIT</span>
			</a>
			<div class="close-sidebar" data-toggle="left-sidebar-close">
				<i class="ion-close-round"></i>
			</div>
		</div>
		<div class="menu-block customscroll">
			<div class="sidebar-menu">
				<ul id="accordion-menu">
					<?php
						if ($myrole == "employee") {
							print '<a href="index.php" class="dropdown-toggle no-arrow ">
							<span class="micon icon-copy fa fa-home"></span><span class="mtext">Inicio</span></a>

							<li class="dropdown">
								<a href="javascript:;" class="dropdown-toggle">
									<span class="micon icon-copy fa fa-bandcamp"></span><span class="mtext">Explorar</span>
								</a>
								<ul class="submenu">
									<li><a href="identis.php" > IDentis</a></li>
									<li><a href="works.php" class="active"> Trabajos</a></li>
									<li><a href="empresas.php"> Empresas</a></li>
								</ul>
							</li>
								<li>
							<a href="employee/chat.php" class="dropdown-toggle no-arrow"><span class="micon icon-copy fa fa-comment"></span><span class="mtext">Chat</span></a>
							<a href="aplicados.php" class="dropdown-toggle no-arrow">
								<span class="micon ion-android-done-all"></span><span class="mtext">Aplicados</span>
							</a>
						</li>';
						}
						
						if ($myrole == "employer") {
							print '<a href="' . $myrole . '/index.php" class="dropdown-toggle no-arrow ">
							<span class="micon icon-copy fa fa-home"></span><span class="mtext">Panel</span></a>
							<a href="identis.php" class="dropdown-toggle no-arrow"><span class="micon icon-copy fa fa-bandcamp"></span><span class="mtext">identis</span></a>';
						}

						if ($myrole == null) {
							print '<a href="login.php" class="dropdown-toggle no-arrow ">
							<span class="micon icon-copy fa fa-lock"></span><span class="mtext">Iniciar sesion</span></a>
							<a href="registro.php" class="dropdown-toggle no-arrow ">
							<span class="micon icon-copy fa fa-user"></span><span class="mtext">Registrarme</span></a>';
						}
					?>
				</ul>
			</div>
		</div>
	</div>
	<div class="mobile-menu-overlay"></div>
	<div class="main-container">
		<div class="pd-ltr-20 xs-pd-20-10">
			<div class="min-height-200px">
				
			
			
			
					<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
						


						
						<div class="horizontal-scroll-contenedor">
							<?php
							require 'constants/db_config.php';
							try {
								$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
								$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
								$stmt = $conn->prepare("SELECT * FROM tbl_categories ORDER BY category");
								$stmt->execute();
								$result = $stmt->fetchAll();
								foreach($result as $row)
								{
									$cat = $row['category'];
									?>						
									<a href="works.php?category=<?php echo $row['category']; ?>search=✓" class="badge badge-primary"  type="submit">🔥 <?php echo $row['category']; ?> </a>

											<?php
		                                     }
	                                         $stmt->execute();
						  
		                                     }catch(PDOException $e)
	                                         {
	                                    
	                                         }
		
											?>
						
						 
						</div>

						<style type="text/css">

						   .horizontal-scroll-contenedor div {
						    width: 100px;
						    height: 50px;
						    margin: 0 10px 0 0;
						    padding: 0;
						    display: inline-block;
						    border: 1px red solid;

						  }

						  .horizontal-scroll-contenedor {
						    width: auto;
						    height: 70px;
						    padding: 10px;
						    white-space: nowrap;
						    overflow-y: hidden; 
						    overflow-x: auto;
						    text-decoration: none;
						   

						  }

						</style>

				
						
	</div>
	</form>

	<div class="product-wrap">
		<div class="product-list">
			<ul class="row">
				<?php
					try {
						$jobs = $db->query($query1);

						if ($fromsearch == true) {
							$jobs->bindParam(':cate', $slc_category);
							$jobs->bindParam(':country', $slc_country);
						}
						
						foreach ($jobs as $job) {
							$type   = $job['type'];
							$compid = $job['company'];
							$tags   = $job['tech'];

							$usuarios = $db->query("SELECT * FROM tbl_users WHERE member_no = :compid and role = 'employer'",array("compid"=>$compid));

							foreach ($usuarios as $usuario) {
								$complogo    = $usuario['avatar'];
								$TCuenta     = $usuario['TCuenta'];
								$thecompname = $usuario['first_name'];
							

							if ($type == "Freelance") {
								$sta = '<span class="job-label label label-success">Freelance</span>';
							}
							if ($type == "Part-time") {
								$sta = '<span class="job-label label label-danger">Part-time</span>';
							}
							if ($type == "Full-time") {
								$sta = '<span class="job-label label label-warning">Full-time</span>';
							}
					?>
						<li class="col-lg-12 col-md-12 col-sm-12">
							<div class="product-box">
								<div class="product-caption">
									<h4><a href="#"><?php echo strip_tags($job['title']); ?></a></h4>
									<?php
										try {
											$jobsApplications = $db->query("SELECT COUNT(*) as jobs FROM tbl_job_applications WHERE company = :company",array("company"=>$compid));
											
											$conteo = $jobsApplications[0]["jobs"];
											$num = 0;

											switch ($TCuenta) {
												case 1:
													$num = 6;
													break;
												case 2:
													$num = 30;
													break;
												case 3:
													$num = $conteo;
													break;
											}
										} catch (PDOException $e) {
										}
									?>
									<div class="text-success" style="margin-top: 6px;">📍 <b><?php echo strip_tags($usuario['city']); ?></b></i> 📙 <b><?php echo strip_tags($job['type']) ?></b></div>

									<div class="text-success" style="margin: 15px 0px 15px 0px;">
										<?php
											$tags = explode(",", $job['tech']);

											foreach($tags as $i =>$key) {
												print '<span class="badge badge-pill" style="color: #01c0fe; background-color: whitesmoke; margin: 5px;">' . " $key " . '</span>';
											}
										?>
									</div>

									<div class="price">

										<p class="card-text" style="margin-top: 6px;"><?php echo html_entity_decode(strip_tags(substr($job['description'], 0, 310))); ?>...</p>

										<a href="work-detail.php?identiwork=<?php echo $job['job_id']; ?>" class="btn btn-primary col-md-12" style="background-color: #01c0fe; border: none;">Postularme</a>

									</div>

								</div>
							</div>
						</li>
				<?php

					}}
				} catch (PDOException $e) {
				}
				?>

			</ul>
		</div>


	</div>
	</div>
	</div>
	</div>

	</div>
	</div>
	<!-- js -->
	<script src="v1/vendors/scripts/core.js"></script>
	<script src="v1/vendors/scripts/script.min.js"></script>
	<script src="v1/vendors/scripts/process.js"></script>
	<script src="v1/vendors/scripts/layout-settings.js"></script>
	<script src="v1/src/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

</body>

</html>