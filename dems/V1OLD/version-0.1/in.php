<!DOCTYPE html>

<?php 
include 'constants/settings.php'; 
include 'constants/check-login.php';

if ($myrole == "employer") {
	require 'employee/constants2/check-login1.php';
}else{
	require 'employee/constants/check-login.php';
}

?>
<html>
<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>IDentiKIT tu primera experiencia laboral tech</title>
		
		<meta property="og:description" content="😎Tu primer trabajo <TECH> en empresas de alto rendimiento a un click de distancia en un solo lugar.
			📣 Fácil, rápido y seguro" />
		<meta name="keywords" content="identikit, IDentiKIT, idkit, IDKIT, primera experiencia laboral, trabajos jr, junior, trabajos junior, empresas que contratan juniors, talento joven, talento tech, jovenes, empresas sin experiencia" />
		<meta name="author" content="IDentiKIT">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta property="og:image" content="https://identikit.app/logowebog.png" />
	    <meta property="og:image:secure_url" content="https://identikit.app/logowebog.png" />
	    <meta property="og:image:type" content="image/png" />
	    <meta property="og:image:width" content="300" />
	    <meta property="og:image:height" content="300" />
	    <meta property="og:image:alt" content="IDentiKIT - Tu primera experiencia laboral" />
	    
	    <meta property="og:url" content="https://identikit.app/" />
	    <meta property="og:type" content="website" />



	   


		<link rel="apple-touch-icon" sizes="180x180" href="logov3.png">
		<link rel="icon" type="image/png" sizes="32x32" href="logov3.png">
		<link rel="icon" type="image/png" sizes="16x16" href="logov3.png">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="v1/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="v1/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="v1/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="v1/src/plugins/datatables/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="v1/vendors/styles/style.css">

		
	  <link href="https://fonts.googleapis.com/css?family=Poppins:400,800" rel="stylesheet" />
	  <link href="index/css/main.css" rel="stylesheet" />


	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-K2NHRZT4R7"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-K2NHRZT4R7');
</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5QGSH5J');</script>
<!-- End Google Tag Manager -->







</head>
<body>

	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QGSH5J"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

	
	
	<div class="header">
		<div class="header-left">
			<div class="menu-icon icon-copy ti-menu" style="color: white;"></div>
			
			<div class="header-search">
				
			</div>
		</div>


		<div class="header-right">
			

			<div class="user-info-dropdown">
				<div class="dropdown">
					<a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" style="color: #ffde00;">
					
						<span class="user-icon">
						<?php 
						if ($myavatar == null) {
							
							print '<center><img  src="images/default.png" title="" alt="image"  /></center>';
						}else{
							echo '<center><img alt="image" title=""  src="data:image/png;base64,'.base64_encode($myavatar).'"/></center>';	
						}
						?>	
																	
						</span>
					</a>
					<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
					<?php
						if ($user_online == true) {
						print '
							<a class="dropdown-item" href="'.$myrole.'"><i class="dw dw-user1"></i> Perfil</a>
							<!--<a class="dropdown-item" href="#"><i class="dw dw-settings2"></i> Ajustes</a>
							<a class="dropdown-item" href="#"><i class="dw dw-help"></i> Ayuda</a>-->
							<a class="dropdown-item" href="logout.php"><i class="dw dw-logout"></i> Salir</a>
';
						}else{
						print '
							<a class="dropdown-item" href="login.php"><i class="dw dw-lock"></i> Ingresar</a>
							<a class="dropdown-item"  href="registro.php"><i class="dw dw-user1"></i> Registrate</a>';						
						}
						
						?>
						
					</div>
				</div>
			</div>
			
		</div>
	</div>

	

	
	<div class="left-side-bar">
		<div class="brand-logo">
			<a href="#">
				<img src="logov3.png" class="light-logo" width="60"> <span class="mtext"> IDentiKIT</span>
			</a>
			<div class="close-sidebar" data-toggle="left-sidebar-close">
				<i class="ion-close-round"></i>
			</div>
		</div>
		<div class="menu-block customscroll">
			<div class="sidebar-menu">
				<ul id="accordion-menu">
					<li>
					
					<?php
					if ($myrole == "employee") {
						print '<a href="index.php" class="dropdown-toggle no-arrow ">
						<span class="micon icon-copy fa fa-home"></span><span class="mtext">Inicio</span></a>

						<li class="dropdown">
						<a href="javascript:;" class="dropdown-toggle">
							<span class="micon icon-copy fa fa-bandcamp"></span><span class="mtext">Explorar</span>
						</a>
						<ul class="submenu">
							<li><a href="identis.php" class="active"> IDentis</a></li>
							<li><a href="works.php"> Trabajos</a></li>
							<li><a href="empresas.php"> Empresas</a></li>
						</ul>
						</li>

							<li>
						<a href="aplicados.php" class="dropdown-toggle no-arrow">
							<span class="micon ion-android-done-all"></span><span class="mtext">Aplicados</span>

						</a>
					</li>

					<li>
						<a href="academy.php" class="dropdown-toggle no-arrow ">
							<span class="micon icon-copy fa fa-free-code-camp"></span><span class="mtext">Academia</span>

						</a>
					</li>
						';
					} if ($myrole == "employer") {
						print '<a href="'.$myrole.'/index.php" class="dropdown-toggle no-arrow ">
						<span class="micon icon-copy fa fa-home"></span><span class="mtext">Panel</span></a>
						<a href="identis.php" class="dropdown-toggle no-arrow"><span class="micon icon-copy fa fa-bandcamp"></span><span class="mtext">identis</span></a>
						
						
						
						';
					}

					if ($myrole == null) {
						print '<a href="login.php" class="dropdown-toggle no-arrow ">
						<span class="micon icon-copy fa fa-lock"></span><span class="mtext">Iniciar sesion</span></a>

						<a href="registro.php" class="dropdown-toggle no-arrow ">
						<span class="micon icon-copy fa fa-user"></span><span class="mtext">Registrarme</span></a>

						
					
						
						
						
						';
					
					}

						
						?>
							

						
					</li>
									


				
					
				</ul>
			</div>
		</div>
	</div>
	<div class="mobile-menu-overlay"></div>

	
					
	<style type="text/css">
		
		@font-face {
		  font-family: "moderna";
		  src: url("fonts/moderna.TTF");
		}

		@font-face {
			font-family: "MYRIADPRO-REGULAR";
			src: url("fonts/MYRIADPRO-REGULAR.otf") format("opentype");
		}

		#titlePrincipal {
		  font-family: "moderna";
		  color: white;
		  font-size: 60px !important;
		 
		}

	</style>
		 <div class="s006">

<form method="post" action="busqueda.php" >      	

	<center><img src="index/images/logo.png"></center>		<br>
        
        <fieldset>

          <legend>Tu primera experiencia laboral tech</legend>

          <div class="inner-form">
            <div class="input-field">
            	

              <button class="btn-search" type="submit">

                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                  <path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"></path>
                </svg>
              </button>

             <?php include 'conexion.php';?>
            	
            
            		<input type="text" class="form-control search-input" placeholder="Buscar en IDentiKIT" required name="PalabraClave">

					
			
            </div>
          </div>

          	<div class="horizontal-scroll-contenedor">
						<?php
						require 'constants/db_config.php';
						try {
							$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
							$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
							$stmt = $conn->prepare("SELECT * FROM tbl_categories ORDER BY rand()");
							$stmt->execute();
							$result = $stmt->fetchAll();
							foreach($result as $row)
							{
								$cat = $row['category'];
								?>						
								<a href="works.php?category=<?php echo $row['category']; ?>search=✓" class="badge badge-primary"  type="submit">🔥 <?php echo $row['category']; ?> </a>

										<?php
	                                     }
                                         $stmt->execute();
					  
	                                     }catch(PDOException $e)
                                         {
                                    
                                         }
	
										?>
					
					 
					</div>

					<style type="text/css">

					   .horizontal-scroll-contenedor div {
					    width: 100px;
					    height: 50px;
					    margin: 0 10px 0 0;
					    padding: 0;
					    display: inline-block;
					    border: 1px red solid;

					  }

					  .horizontal-scroll-contenedor {
					    width: auto;
					    height: 70px;
					    padding: 10px;
					    white-space: nowrap;
					    overflow-y: hidden; 
					    overflow-x: auto;
					    text-decoration: none;
					   

					  }

					</style>

					<br><br><br>




        </fieldset>
      </form>


      		
    </div>




			</div>

			
		</div>
	</div>
	<!-- js -->
	<script src="v1/vendors/scripts/core.js"></script>
	<script src="v1/vendors/scripts/script.min.js"></script>
	<script src="v1/vendors/scripts/process.js"></script>
	<script src="v1/vendors/scripts/layout-settings.js"></script>




</body>
</html>