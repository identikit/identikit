<?php
require '../../constants/db_config.php';
require '../constants/check-login.php';

$jobtitle = ucwords($_POST['jobtitle']);

if (isset($_POST['duties'])) {
$duties = $_POST['duties'];	
}else{
$duties = "";	
}

$techno = ($_POST['techno']);
$startdate = $_POST['startdate'];
$enddate = $_POST['enddate'];
$link = $_POST['link'];

if (isset($_POST['view'])) {
$view = $_POST['view'];	
}else{
$view = "";	
}

$expid = $_POST['expid'];

try {
$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	
$stmt = $conn->prepare("UPDATE tbl_experience SET title=:title, link=:link, duties = :duties, techno = :techno, start_date =  :startdate, end_date = :enddate, view = :view WHERE id=:expid AND member_no = '$myid'");
$stmt->bindParam(':title', $jobtitle);
$stmt->bindParam(':link', $link);
$stmt->bindParam(':duties', $duties);
$stmt->bindParam(':startdate', $startdate);
$stmt->bindParam(':enddate', $enddate);
$stmt->bindParam(':techno', $techno);
$stmt->bindParam(':view', $view);
$stmt->bindParam(':expid', $expid);

$stmt->execute();
header("location:../index.php?r=9215");					  
}catch(PDOException $e)
{

}


?>