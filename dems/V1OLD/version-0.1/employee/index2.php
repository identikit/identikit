<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-5F5WB8FMJC"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-5F5WB8FMJC');
</script>
<!DOCTYPE html>
<html>
<?php 

require '../constants/settings.php'; 
require 'constants/check-login.php';

if ($user_online == "true") {
if ($myrole == "employee") {
}else{
header("location:../");		
}
}else{
header("location:../");	
}


?>

<head>
	<!-- Basic Page Info -->
	<meta charset="utf-8">
	<title>IDentiKIT - Mi perfil</title>

	
	<link rel="apple-touch-icon" sizes="180x180" href="logov3.png">
	<link rel="icon" type="image/png" sizes="32x32" href="logov3.png">
	<link rel="icon" type="image/png" sizes="16x16" href="logov3.png">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="../v1/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="../v1/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="../v1/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../v1/src/plugins/datatables/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../v1/vendors/styles/style.css">
	<link rel="stylesheet" type="text/css" href="../v1/src/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">


	

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PHBQLSG');</script>
<!-- End Google Tag Manager -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 

<script type="text/javascript">
$(window).load(function(){
 $(function() {
  $('#file-input').change(function(e) {
      addImage(e); 
     });

     function addImage(e){
      var file = e.target.files[0],
      imageType = /image.*/;
    
      if (!file.type.match(imageType))
       return;
  
      var reader = new FileReader();
      reader.onload = fileOnload;
      reader.readAsDataURL(file);
     }
  
     function fileOnload(e) {
      var result=e.target.result;
      $('#imgSalida').attr("src",result);
     }
    });
  });
</script>

<script>
  function capturar()
  {
        var resultado="";
 
        var porNombre=document.getElementsByName("filter");
        for(var i=0;i<porNombre.length;i++)
        {
            if(porNombre[i].checked)
                resultado=porNombre[i].value;
        }

    var elemento = document.getElementById("resultado");
    if (elemento.className == "") {
      elemento.className = resultado;
      elemento.width = "600";
    }else {
      elemento.className = resultado;
      elemento.width = "600";
    }
}
</script>

<style>
#fotop {
display: block;
margin: 0 auto 20px;
width: 150px;
height: 150px;
-webkit-box-shadow: 0 0 10px rgba(0,0,0,.3);
box-shadow: 0 0 10px rgba(0,0,0,.3);
border-radius: 100%;
overflow: hidden;
}
</style>
			


</head>


</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PHBQLSG"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!--<div class="pre-loader">
		<div class="pre-loader-box">
			<div class="loader-logo"><img src="../logo.png" alt="" width="100"></div>
			<div class='loader-progress' id="progress_div">
				<div class='bar' id='bar1'></div>
			</div>
			<div class='percent' id='percent1'>0%</div>
			<div class="loading-text">
				Cargando...
			</div>
		</div>
	</div>-->

	<div class="header">
		<div class="header-left">
			<div class="menu-icon icon-copy ti-menu" style="color: white;"></div>
			
			<div class="search-toggle-icon dw dw-search2" style="color: #ffde00;" data-toggle="header_search"></div>
			<div class="header-search">
			<form method="post" action="../busqueda.php" >
					<div class="form-group mb-0" >
						
					<i class="dw dw-search2 search-icon" style="color: #ffde00;"></i>
					
						<input type="text" class="form-control search-input" placeholder="Buscar en IDentiKIT" required name="PalabraClave">
						<input name="buscar" type="hidden" class="form-control mb-2" id="inlineFormInput" value="v">


					</div>
				</form>
			</div>
		</div>
		<div class="header-right">
			
			<?php require_once "alerts.php";?>
			<div class="user-info-dropdown">
				<div class="dropdown">
					<a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" style="color: #ffde00;">
					
						<span class="user-icon">
						<?php 
																	if ($myavatar == null) {
																		print '<center><img  src="../images/default.png" title="'.$myfname.'" alt="image"  /></center>';
																	}else{
																		echo '<center><img alt="image" title="'.$myfname.'"  src="data:image/png;base64,'.base64_encode($myavatar).'"/></center>';	
																	}
																	?>	
																	
						</span>
						<!--<span class="user-name" style="color: white;"><?php //echo "$myfname"; ?></span>-->
					</a>
					<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
						<?php
						if ($user_online == true) {
						print '
							<a class="dropdown-item" href=""><i class="dw dw-user1"></i> Perfil</a>
							<!--<a class="dropdown-item" href="#"><i class="dw dw-settings2"></i> Ajustes</a>
							<a class="dropdown-item" href="#"><i class="dw dw-help"></i> Ayuda</a>-->
							<a class="dropdown-item" href="../logout.php"><i class="dw dw-logout"></i> Salir</a>
';
						}else{
						print '
							<li><a href="login.php">ingresar</a></li>
							<li><a data-toggle="modal" href="#registerModal">registrate</a></li>';						
						}
						
						?>
					</div>
				</div>
			</div>
			
		</div>
	</div>

	

	<div class="left-side-bar">
		<div class="brand-logo">
			<a href="#">
				<img src="../v1/logov3.png" alt="" class="light-logo" width="60"> <span class="mtext"> IDentiKIT</span>
			</a>
			<div class="close-sidebar" data-toggle="left-sidebar-close">
				<i class="ion-close-round"></i>
			</div>
		</div>
		<div class="menu-block customscroll">
			<div class="sidebar-menu">
				<ul id="accordion-menu">
					
					<li>
						<a href="../index.php" class="dropdown-toggle no-arrow ">
							<span class="micon icon-copy fa fa-home"></span><span class="mtext">Inicio</span>

						</a>
					</li>
					
					<li class="dropdown">
						<a href="javascript:;" class="dropdown-toggle">
							<span class="micon icon-copy fa fa-bandcamp"></span><span class="mtext">Explorar</span>
						</a>
						<ul class="submenu">
							<li><a href="../identis.php"> IDentis</a></li>
							<li><a href="../works.php"> Trabajos</a></li>
							<li><a href="../empresas.php"> Empresas</a></li>
						</ul>
					</li>

					<li>
						<a href="../aplicados.php" class="dropdown-toggle no-arrow">
							<span class="micon ion-android-done-all"></span><span class="mtext">Aplicados</span>

						</a>
					</li>
					
					<li>
						<a href="../academy.php" class="dropdown-toggle no-arrow ">
							<span class="micon icon-copy fa fa-free-code-camp"></span><span class="mtext">Academia</span>

						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="mobile-menu-overlay"></div>




	


		<div class="main-container">
			<div class="pd-ltr-20 ">
					<?php require 'constants/check_reply.php';?>
					<div class="row">
					    
		
							<div class="card-box col-md-12">
									
							<h5 class="h4 mb-20 pd-10">Mi perfil</h5>
							<div class="tab">
								<ul class="nav nav-tabs" role="tablist">
									<li class="nav-item">
										<a class="nav-link active " data-toggle="tab" href="#home" role="tab" aria-selected="true">Mi ID</a>
									</li>
									<li class="nav-item">
										<a class="nav-link " data-toggle="tab" href="#profile" role="tab" aria-selected="false">Info</a>
									</li>
									<li class="nav-item">
										<a class="nav-link " data-toggle="tab" href="#experiencia" role="tab" aria-selected="false">Proyectos</a>
									</li>
									
								</ul>
							
								<div class="tab-content">
									


									<div class="tab-pane fade show active" id="home" role="tabpanel">

                                        
										<div class="pd-20">
										
											<div class="profile-photo">
												<a href="modal" data-toggle="modal" data-target="#modal" class="edit-avatar" style="padding: 8px;"><i class="fa fa-pencil"></i></a>
														
														<?php
												if ($myavatar == null) {
												
													print '<center><img  src="../images/default.png" title="'.$myfname.'" alt="image" id="fotop" /></center>';
												
												}else{
													echo '<center><img alt="image" id="fotop" title="'.$myfname.'"  src="data:image/png;base64,'.base64_encode($myavatar).'"/></center>';
												}
												?>
												<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
													<div class="modal-dialog modal-dialog-centered" role="document">
														<div class="modal-content">
															



														<form action="app/new-dp.php" method="POST" enctype="multipart/form-data">
															<center>
																<br>
																<i class="icon-copy ion-speakerphone" style="color:yellow;"></i>Algunos tips:<br>
																<span><i class="icon-copy ion-ios-color-wand-outline" style="color:#01c0fe;"></i> Fotos cuadradas</span><br>
																<span><i class="icon-copy ion-ios-color-wand-outline" style="color:#01c0fe;"></i> Evita las fotos oscuras</span><br>
																<span><i class="icon-copy ion-ios-color-wand-outline" style="color:#01c0fe;"></i> Se vos mismo</span><br>
															</center>

														<center>	
															<div>
															  <div id="resultado" class=""><img id="imgSalida" style="width: 200px; height: 200px; border-radius: 200px;" /></div>
															</div>
														
														<div class="form-group col-md-12 ">
															
															<center><label>Actualizar imagen</label></center>
															<label for="file-input">
															  <img src="../images/mas.png"  title ="Seleccionar imagen" style="width: 90px; height: 90px">
															</label>
															<input accept="image/*" type="file" name="image" id="file-input" required hidden="" />
															</div>
															
															<div class="modal-footer">
																<input type="submit" value="Actualizar" class="btn btn-primary">
																<button type="button" class="btn btn-default" data-dismiss="modal">cerrar</button>
															</div>
															</center>
														</form>
														
														</div>
													</div>

												</div>

											</div>
											<h5 class="text-center h5 mb-0 " ><?php if($empujar == "1") { print '🌟'; }?> <?php echo "$myfname"; ?> <?php echo "$mylname"; ?></h5>
											<div class="work text-success text-center pd-10"><i class="ion-android-person"></i> <?php echo "$mytitle"; ?></div>

											<div class="btn-list text-center pd-10">
											<?php

											if ($github) {
												print '<a href="https://github.com/'."$github".'" type="button" class="btn" style="border-radius: 10px;" data-bgcolor="black" data-color="#ffffff" target="_blank"><i class="fa fa-github"></i></a>';												
											}

											if ($twitter) {
												print '<a href="https://twitter.com/'."$twitter".'" type="button" class="btn" style="border-radius: 10px;" data-bgcolor="#1da1f2" data-color="#ffffff" target="_blank"><i class="fa fa-twitter"></i></a>';												
											}

											if ($instagram) {
												print '<a href="https://instagram.com/'."$instagram".'" type="button" class="btn" style="border-radius: 10px;" data-bgcolor="#f46f30" data-color="#ffffff" target="_blank"><i class="fa fa-instagram"></i></a>';												
											}


											?>
												
												
												
												
												
											
											</div>

											<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

											  <script src="chili-1.8.js"></script>
											  <script src="jquery.expander.js"></script>

											<script>
											$(document).ready(function() {
											  var opts = {
											    collapseTimer: 4000,
											    expandEffect: 'fadeIn',
											    collapseEffect: 'fadeOut',
											  };

											  $.each(['beforeExpand', 'afterExpand', 'onCollapse'], function(i, callback) {
											    opts[callback] = function(byUser) {
											      var msg = '<div class="success">' + callback;

											      if (callback === 'onCollapse') {
											        msg += ' (' + (byUser ? 'user' : 'timer') + ')';
											      }
											      msg += '</div>';

											      $(this).parent().parent().append(msg);
											    };
											  });

											  $('dl.expander dd').eq(0).expander();
											  $('dl.expander dd').slice(1).expander(opts);

											  $('ul.expander li').expander({
											    slicePoint: 50,
											    widow: 2,
											    expandSpeed: 0,
											    userCollapseText: '[^]'
											  });

											  $('div.expander').expander();
											});
											</script>

											<div class="expander">
												<p class="text-center text-muted font-14 "><?php echo "$mydesc"; ?></p>
											</div>


											<div class="contact-directory-box pd-0">
												<div class="contact-dire-info text-center pd-0">
													
													
													<div class="contact-skill pd-0">
														
														<?php 

														$tags = explode(",", $ability);

														

												
														

														for ($i=0; $i <= count($tags) - 1  ;  $i++ ) { 
 
															$array = $tags[$i];
															print '
															<span class="badge badge-pill">'." $array".'</a></span>


															';
														}



														

													

														?>
														

														
														
													</div>
											</div>
								
<div class="timeline mb-30">
	<h5 class="text-center mb-20 h5 mb-0 pd-10">Proyectos </h5>
	<ul>
		<?php
		require '../constants/db_config.php';
		try {
			$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stmt = $conn->prepare("SELECT * FROM tbl_experience WHERE member_no = '$myid' ORDER BY id desc");
            $stmt->execute();
            $result = $stmt->fetchAll();
            foreach($result as $row) {
            	$title = $row['title'];
				$supervisor = $row['supervisor'];
				$phone = $row['supervisor_phone'];
				$start_date = $row['start_date'];
				$end_date = $row['end_date'];
				$duties = $row['duties'];
				$view = $row['view'];
				$expid = $row['id'];
				$techno = $row['techno'];
				$link = $row['link'];

				



																		


				?>

				<li>
					
					<div class="timeline-date">
						<?php echo "$start_date"; ?> <b> al </b> <?php echo "$end_date"; ?>
					</div>

					<div class="timeline-desc card-box">
						<div class="pd-20">
							<h4 class="mb-10 h4"><?php echo "$title"; ?> </h4>
							<?php
							$tags = explode(",", $techno);

							for ($i=0; $i <= count($tags) - 1  ;  $i++ ) {
								$array = $tags[$i];
								print '<span class="badge badge-pill" style="background-color: whitesmoke; margin: 5px; color: #01c0fe;">'." $array".'</a></span>';
							}
							?>	<br><br>
							
							<p class="text-center text-muted font-14"><?php echo $row['duties']; ?></p>
							<p class="text-center text-muted font-14" ><a href="<?php echo $row['link'];?>"><?php echo $row['link'];?></a></p>



							<a href="app/drop-experience.php?id=<?php echo $row['id']; ?>" onclick = "return confirm('Seguro que quieres borrar esto?')" class="btn btn-danger btn-sm btn-inverse ">Borrar</a>
							
							<a data-toggle="modal" href="#edit<?php echo $row['id']; ?>" class="btn btn-primary btn-sm btn-inverse ">Editar</a>
							
							<div id="edit<?php echo $row['id']; ?>" class="modal fade bs-example-modal-lg" tabindex="-1" data-width="550" style="display: none;" data-backdrop="static" data-keyboard="false" data-replace="true" role="dialog"aria-hidden="true">

								
								<div class="modal-dialog modal-lg modal-dialog-centered">
									<div class="modal-content">
										<div class="modal-header">
											<h4 class="modal-title" id="myLargeModalLabel">Editar <?php echo "$title"; ?></h4>
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
										</div>
										<div class="modal-body">
											
									<form action="app/update-experience.php" method="POST" autocomplete="off" enctype="multipart/form-data">
					                <div class="row gap-20">

					                <div class="col-sm-12 col-md-12">
				
							        <div class="form-group"> 
								    
								    <label>Titulo del proyecto  </label>
								    
								    <input  value="<?php echo "$title"; ?>" class="form-control " placeholder="" type="text" name="jobtitle" required> 
							        
							        </div>
						
						             </div>
								
						            
									 
									
						
								   	<div class="col-sm-6 col-md-6">
						
							        <div class="form-group"> 
								    
								    <label>Fecha inicio </label>
								    
								    <input class="form-control" placeholder="Ej: 01 Marzo 2019" type="date" name="startdate" value="<?php echo "$start_date"?>" > 
							        
							        </div>
						
						           </div>
								   
								   	<div class="col-sm-6 col-md-6">
						
							        <div class="form-group"> 
								    
								    <label>Fecha finalizacion  </label>
								    
								    <input class="form-control" placeholder="Ej: 01 Junio 2019" type="date" name="enddate" value="<?php echo "$end_date"?>" > 
							        
							        </div>
						
						           </div>
                                   
                                   <input type="hidden" name="expid" value="<?php echo "$expid"; ?>">
								   

                                   <div class="col-sm-6 col-md-12">
						           		
						           		 <div class="form-group">
                                        Opciones de visibilidad:
                                          <select class="custom-select"  name="view">

                                              <option value="<?php echo $view;?>"><?php echo $view;?></option>
                                              <option value="mostrar">Mostrar</option>
                                              <option value="ocultar">Ocultar</option>
                                          </select>
						           	</div>
						           </div>

						           <div class="col-sm-12 col-md-12">
						           	<div class="form-group ">
						           		<label>Habilidades / tecnologia aplicadas (5 máximo)</label>
						           		<input type="text" class="form-control form-control-lg" data-role="tagsinput" name="techno" placeholder="Presiona ENTER para agregar" value="<?php echo $techno;?>">

						           	</div>
						           </div>

						           <div class="col-sm-12 col-md-12">
						           	<div class="form-group ">
						           		<label>Link</label>
						           		<input type="text" class="form-control form-control-lg" name="link" placeholder="Ingresa link del proyecto" value="<?php echo $link;?>">

						           	</div>
						           </div>

								   <div class="col-sm-12 col-md-12">
						
							       <div class="form-group"> 
								   
								   <label>Descripcion del proyecto</label>
								   
								   <textarea class="form-control" name="duties"><?php echo "$duties"; ?> </textarea>
							       
							       </div>
						
						           </div>
						
					               </div>
				                  
				                   </div>
				
				                   <div class="modal-footer text-center">
				 	               
				 	               <button type="submit" class="btn btn-primary">Actualizar</button>
					               
					               <button type="button" data-dismiss="modal" class="btn btn-primary btn-inverse">Cerrar</button>
				                   
				                   </div>
				                   
				                   </form>
										</div>
										
									</div>
								</div>

								
								
									
									<?php

                                    if ($view == 'mostrar') {

                                    	$idv = $row["id"];

                                        print '
                                            <a href="#edit'.$idv.'	" data-toggle="modal" ><i class="icon-copy fa fa-eye" aria-hidden="true" name="view"></i></a>
                                           

                                        ';
                                    } else {
                                        print '
                                            <i class="icon-copy fa fa-eye-slash" aria-hidden="true"></i>
                                        ';      
                                    }

                                    ?>
			                       

			                       </div>
			                       
			                       </div>
			                   </li>
										<?php
										}
									}catch(PDOException $e)
									{

									}
									?>


										

									</ul>
								</div>

										</div>





										</div>

									</div>






									<div class="tab-pane fade" id="profile" role="tabpanel">
									<div class="">
											<div class="profile-setting">
											

											<form class="post-form-wrapper" action="app/update-profile.php" method="POST" autocomplete="off">
												
													<ul class="profile-edit-list row">
													
														<li class="weight-500 col-md-12">
															<h4 class=" h5 mb-20">Informacion</h4>
															<div class="form-group">
																<label>Nombre</label>
																<input name="fname" class="form-control form-control-lg" type="text" value="<?php echo "$myfname"; ?>">
															</div>
															<div class="form-group">
																<label>Apellido</label>
																<input name="lname" class="form-control form-control-lg" type="text" value="<?php echo "$mylname"; ?>">
															</div>

															<div class="form-group">
														<label>Ocupacion</label>
												

														<select name="title" required class="selectpicker show-tick form-control" data-live-search="true">
														
														<option disabled value="">Seleccionar</option>
															<option <?php if ($mytitle == "Front-End Developer") { print ' selected '; } ?> value="Front-End Developer">Front-End Developer</option>

															<option <?php if ($mytitle == "Back-End Developer") { print ' selected '; } ?> value="Back-End Developer">Back-End Developer</option>

															<option <?php if ($mytitle == "Full Stack Web Developer") { print ' selected '; } ?> value="Full Web Stack Developer">Full Stack Web Developer</option>

															<option <?php if ($mytitle == "Mobile Developer") { print ' selected '; } ?> value="Mobile Developer">Mobile Developer</option>

															<option <?php if ($mytitle == "SalesForce Developer") { print ' selected '; } ?> value="SalesForce Developer">SalesForce Developer</option>

															<option <?php if ($mytitle == "DevOps Engineer") { print ' selected '; } ?> value="DevOps Engineer">DevOps Engineer</option>

															<option <?php if ($mytitle == "QA Engineer") { print ' selected '; } ?> value="QA Engineer">QA Engineerr</option>

															<option <?php if ($mytitle == "IOT Developer") { print ' selected '; } ?> value="IOT Developer">IOT Developer</option>

															<option <?php if ($mytitle == "Sysadmin") { print ' selected '; } ?> value="Sysadmin">Sysadmin</option>

															<option <?php if ($mytitle == "Desktop Developer") { print ' selected '; } ?> value="Desktop Developer">Desktop Developer</option>

															<option <?php if ($mytitle == "Technical Support") { print ' selected '; } ?> value="Technical Support">Technical Support</option>

															<option <?php if ($mytitle == "BI Developer") { print ' selected '; } ?> value="BI Developer">BI Developer</option>


															<option <?php if ($mytitle == "Data Analyst") { print ' selected '; } ?> value="Data Analyst">Data Analyst</option>


															<option <?php if ($mytitle == "Database Administrator (DBA)") { print ' selected '; } ?> value="Database Administrator (DBA)">Database Administrator (DBA)</option>


															<option <?php if ($mytitle == "Architect Developer") { print ' selected '; } ?> value="Architect Developer">Architect Developer</option>

															<option <?php if ($mytitle == "Cyber Security") { print ' selected '; } ?> value="Cyber Security">Cyber Security</option>


															<option <?php if ($mytitle == "Devops Analyst") { print ' selected '; } ?> value="Devops Analyst">Devops Analyst</option>


															<option <?php if ($mytitle == "Functional Analyst	") { print ' selected '; } ?> value="Functional Analyst	">Functional Analyst	</option>

															<option <?php if ($mytitle == "Middleware Administrator") { print ' selected '; } ?> value="Middleware Administrator">Middleware Administrator</option>


															<option <?php if ($mytitle == "Project Manager") { print ' selected '; } ?> value="Project Manager">Project Manager</option>


															<option <?php if ($mytitle == "Web Designer	") { print ' selected '; } ?> value="Web Designer">Web Designer</option>


															<option <?php if ($mytitle == "UX Designer	") { print ' selected '; } ?> value="UX Designer">UX Designer</option>

															<option <?php if ($mytitle == "UI Designer	") { print ' selected '; } ?> value="UI Designer">UI Designer</option>

															<option <?php if ($mytitle == "UI & UX Designer") { print ' selected '; } ?> value="UI & UX Designer">UI & UX Designer</option>


															
														</select>
													</div>


											


													<div class="form-group">
														<label>Tecnologias/habilidades</label>
														<input type="text" value="<?php echo "$ability";?>" class="form-control form-control-lg" data-role="tagsinput" name="ability" placeholder="Presiona ENTER para agregar">
														<small class="form-text text-muted">
														  Recuerda presionar enter para agregar una tecnologia/habilidad nueva.
														</small>
													</div>
														
														<li class="weight-500 col-md-12">
														<div class="form-group">
															<label>Contanos un poco sobre vos </label>
															<textarea class="form-control" name="about" maxlength="500" ><?php echo "$mydesc"; ?></textarea>
														</div>															
														</li>
													</ul>
													

													<div class="col-lg-12 col-md-12 col-sm-12 mb-30">
															<div class="list-group">
																<a data-toggle="modal" href="#edit_contact" class="list-group-item list-group-item-action">Informacion de contacto</a>

																<div id="edit_contact" class="modal fade bs-example-modal-lg" tabindex="-1" data-width="550" style="display: none;" data-backdrop="static" data-keyboard="false" data-replace="true" role="dialog"aria-hidden="true">
																	<div class="modal-dialog modal-lg modal-dialog-centered">
																		<div class="modal-content">
																			<div class="modal-header">
																				<h4 class="modal-title" id="myLargeModalLabel">Informacion de contacto</h4>
																				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
																			</div>

																			<div class="modal-body">

																					<div class="row gap-20">
																						<div class="col-sm-12 col-md-12">
																							<div class="form-group"> 
																								<label></label>		

																								<div class="form-group">
																										<label>Email</label>
																										<input name="email"  class="form-control form-control-lg" type="email" value="<?php echo "$myemail"; ?>">
																									</div>

																									<div class="form-group">
																										<label>Telefono</label>
																										<input name="phone" class="form-control form-control-lg" type="text"  value="<?php echo "$myphone"; ?>">
																									</div>

																									<label>Usuario Github</label>
																									<div class="input-group custom">
																										<div class="input-group-prepend custom">
																											<div class="input-group-text" id="btnGroupAddon">@</div>
																										</div>

																										<input type="text" class="form-control" placeholder="Ingresa tu usuario de Github" aria-label="Input group example" aria-describedby="btnGroupAddon" name="github" value="<?php echo $github?>">
																									</div>

																									<label>Usuario Twitter</label>
																									<div class="input-group custom">
																										<div class="input-group-prepend custom">
																											<div class="input-group-text" id="btnGroupAddon">@</div>
																										</div>

																										<input type="text" class="form-control" placeholder="Ingresa tu usuario de Twitter" aria-label="Input group example" aria-describedby="btnGroupAddon" name="twitter" value="<?php echo $twitter?>">
																									</div>

																									<label>Usuario Instagram</label>
																									<div class="input-group custom">
																										<div class="input-group-prepend custom">
																											<div class="input-group-text" id="btnGroupAddon">@</div>
																										</div>

																										<input type="text" class="form-control" placeholder="Ingresa tu usuario de Instagram" aria-label="Input group example" aria-describedby="btnGroupAddon" name="instagram" value="<?php echo $instagram?>">
																									</div>

																									
								   
																							</div>
																						</div>
																						<div class="col-md-12 modal-footer text-center">
																							<button type="button" class="col-md-12 btn btn-primary" data-dismiss="modal">Continuar</button>
																						</div>
																				</div>
																			</div>	
																			</div>
																		</div>
																	</div>
																									


																<br>
																<a data-toggle="modal" href="#edit_data" class="list-group-item list-group-item-action">Datos personales</a>

																<div id="edit_data" class="modal fade bs-example-modal-lg" tabindex="-1" data-width="550" style="display: none;" data-backdrop="static" data-keyboard="false" data-replace="true" role="dialog"aria-hidden="true">
																	<div class="modal-dialog modal-lg modal-dialog-centered">
																		<div class="modal-content">
																			<div class="modal-header">
																				<h4 class="modal-title" id="myLargeModalLabel">Datos personales</h4>
																				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
																			</div>

																			<div class="modal-body">

																					<div class="row gap-20">
																						<div class="col-sm-12 col-md-12">
																							<div class="form-group"> 
																								<label></label>		

																								<div class="form-group">
														<label>Fecha de nacimiento</label>
														<div class="row gap-5">
															<div class="col-xs-3 col-sm-3">
																<select name="date" required class="selectpicker form-control" data-live-search="false">
																	<option disabled value="">Dia</option>
                                                                     <?php 
                                                                      $x = 1; 

                                                                      while($x <= 31) {
                                         
												                      if ($x < 10) {
														              $x = "0$x";
													                  print '<option '; if ($mydate == $x ) { print ' selected '; } print ' value="'.$x.'">'.$x.'</option>';
													                  }else{
													                  print '<option '; if ($mydate == $x ) { print ' selected '; } print ' value="'.$x.'">'.$x.'</option>';
													                  }
                                                                      $x++;
                                                                       } 
                                                                     ?>
																</select>
															</div><br><br><br>
															<div class="col-xs-5 col-sm-5">
																<select name="month" required class="selectpicker form-control" data-live-search="false">
																	<option disabled value="">Mes</option>
                                                                     <?php 
                                                                      $x = 1; 

                                                                      while($x <= 12) {
                                         
												                      if ($x < 10) {
														              $x = "0$x";
													                  print '<option '; if ($mymonth == $x ) { print ' selected '; } print ' value="'.$x.'">'.$x.'</option>';
													                  }else{
													                  print '<option '; if ($mymonth == $x ) { print ' selected '; } print ' value="'.$x.'">'.$x.'</option>';
													                  }
                                                                      $x++;
                                                                       } 
                                                                     ?>
																</select>
															</div><br><br><br>
															
															<div class="col-xs-4 col-sm-4">
																<select name="year" class="selectpicker form-control" data-live-search="false">
																<option disabled value="">Año</option>
													            <?php 
                                                                 $x = date('Y'); 
                                                                 $yr = 60;
													             $y2 = $x - $yr;
                                                                 while($x > $y2) {
                                         
													             print '<option '; if ($myyear == $x ) { print ' selected '; } print ' value="'.$x.'">'.$x.'</option>';
                                                                 $x = $x - 1;
                                                                  } 
                                                                  ?>
																</select>
															</div>
														</div>
													</div>

																								<div class="form-group">
																									<label>Genero</label>
																									<form>
																										<div class="row">
																											<div class="col-md-12">
																												<div class="form-group">
																													<select name="gender" required class="selectpicker show-tick form-control" data-live-search="false">
																														<option disabled value="">Seleccionar</option>
																														<option <?php if ($mygender == "Male") { print ' selected '; } ?> value="Male">Masculino</option>
																														<option <?php if ($mygender == "Female") { print ' selected '; } ?>value="Female">Femenino</option>
																														<option <?php if ($mygender == "Other") { print ' selected '; } ?>value="Other">Otro</option>
																													</select>
																												</div>
																											</div>
																										</div>
																									</div>
																									
																									<div class="form-group">
																										<label>DNI</label>
																										<input name="dni"  class="form-control form-control-lg" type="text" value="<?php  echo "$dni"; ?>">
																									</div>

																									<div class="form-group">
																										<label>Pais</label>
																										<select name="country" required class="selectpicker show-tick form-control" data-live-search="true">
																											<option disabled value="">Seleccionar</option>
																											<?php
																											require '../constants/db_config.php';
																											try {
                                                           $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
                                                           $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	
                                                           $stmt = $conn->prepare("SELECT * FROM tbl_countries ORDER BY country_name");
                                                           $stmt->execute();
                                                           $result = $stmt->fetchAll();
  
                                                           foreach($result as $row)
                                                           {
		                                                    ?> <option <?php if ($mycountry == $row['country_name']) { print ' selected '; } ?> value="<?php echo $row['country_name']; ?>"><?php echo $row['country_name']; ?></option> <?php
	 
	                                                        }

					  
	                                                       }catch(PDOException $e)
                                                           {

                                                           }
                                                           ?>
                                                         </select>
                                                       </div>

                                                       <div class="form-group">
                                                       	<label>Calle</label>
                                                       	<input name="street" class="form-control form-control-lg" type="text" value="<?php echo "$street";?>">
                                                       </div>

                                                       <div class="form-group">
                                                       	<label>Ciudad</label>
                                                       	<input name="city" class="form-control form-control-lg" type="text" value="<?php echo "$mycity"; ?>"></div>

                                                       	<div class="form-group">
                                                       		<label>Codigo postal</label>
                                                       		<input name="zip" class="form-control form-control-lg" type="text" value="<?php echo "$myzip";?>">
                                                       	</div>


																								
								   
																							</div>
																						</div>
																						<div class="col-md-12 modal-footer text-center">
																							<button type="button" class="col-md-12 btn btn-primary" data-dismiss="modal">Continuar</button>
																						</div>
																				</div>
																			</div>	
																			</div>
																		</div>
																	</div>
																

														</div>
													</div>
													<div class="form-group mb-0">
													<button type="submit" class="btn btn-primary col-md-12" >Actualizar</button>
													</div>
												</form>


										
											</div>
										</div>

									</div>




									<div class="tab-pane fade" id="experiencia" role="tabpanel">
									

										
										<div class="timeline mb-30">
									<h5 class="text-center mb-20 h5 mb-0 pd-10">Proyectos</h5>
									<center><b style="color:#990000">En esta sección podrás incluir los proyectos en los que has trabajado, recuerda que los proyectos propios tambien cuentan, no olvides cargarlos! (Además esto te posiciona mucho mejor con las empresas)</b></center>
									<form action="app/add-experience.php" method="POST" autocomplete="off" enctype="multipart/form-data">
					                <div class="row gap-20">
									

						            <div class="col-sm-12 col-md-12">
				
							        <div class="form-group"> 
								    <label>Titulo del proyecto </label>
								    <input class="form-control" placeholder="Ingresa el titulo del proyecto" type="text" name="jobtitle" required> 
							        </div>
						
						             </div>
									 
					

						             	<div class="col-sm-12 col-md-12">
						
							        <div class="form-group"> 
								    <label>Descripción</label>
								    <p class="text-muted">Este campo será visible en la vista detallada del proyecto. Describe los detalles principales del proyecto de forma clara y concisa.</p>
								    <textarea class="form-control"  name="duties"> </textarea>
							        </div>
						
						           </div>

						           <div class="col-sm-12 col-md-12">
						           	<div class="form-group ">
						           		<label>Habilidades/tecnologias aplicadas (5 máximo)</label>
						           		<input 	type="text" class="form-control form-control-lg" data-role="tagsinput" name="techno" placeholder="Presiona ENTER para agregar">

						           	</div>
						           </div>

						           <div class="col-sm-12 col-md-12">
				
							        <div class="form-group"> 
								    <label>Link del proyecto (opcional)</label>
								    <input class="form-control" placeholder="Ingresa link del proyecto" type="text" name="link" value="https://"> 
							        </div>
						
						             </div>
						
								   
								   	<div class="col-sm-6 col-md-6">
									
							        <div class="form-group"> 
							        
								    <label>Fecha Inicio (opcional)</label> 
								    <input class="form-control" placeholder="Ej: 01 Marzo 2019" type="date" name="startdate" > 
							        </div>
						
						           </div>
								   
								   	<div class="col-sm-6 col-md-6">
						
							        <div class="form-group"> 
								    <label>Fecha Finalizacion (opcional)</label>
								    <input class="form-control" placeholder="Ej: 01 Junio 2019" type="date" name="enddate" > 
							        </div>
						
						           </div>

						           <div class="col-sm-6 col-md-12">
						           		
						           		 <div class="form-group">
                                          <select class="custom-select"  name="view" required="">
                                              <option value="">Opciones de visibilidad</option>
                                              <option value="mostrar">Mostrar</option>
                                              <option value="ocultar">Ocultar</option>
                                          </select>
						           	</div>
						           </div>
								   
						
					               </div>
					               <button type="submit" class="btn btn-primary col-md-12">Añadir</button>
				                   </div>
				
				 	               
				                   </div>
				                 
				                   </form>
									
									</div>
										</div>

									</div>
									
								</div>
							</div>
						</div>
						</div>
						</div>
						</div>
						</div>
					



	</div>
	<!-- js -->
	<script src="../v1/vendors/scripts/core.js"></script>
	<script src="../v1/vendors/scripts/script.min.js"></script>
	<script src="../v1/vendors/scripts/process.js"></script>
	<script src="../v1/vendors/scripts/layout-settings.js"></script>
	<script src="../v1/src/plugins/datatables/js/jquery.dataTables.min.js"></script>
	<script src="../v1/src/plugins/datatables/js/dataTables.bootstrap4.min.js"></script>
	<script src="../v1/src/plugins/datatables/js/dataTables.responsive.min.js"></script>
	<script src="../v1/src/plugins/datatables/js/responsive.bootstrap4.min.js"></script>
	<script src="../v1/vendors/scripts/dashboard.js"></script>
	<script src="../v1vendors/scripts/core.js"></script>
	<script src="../v1vendors/scripts/script.min.js"></script>
	<script src="../v1vendors/scripts/process.js"></script>
	<script src="../v1vendors/scripts/layout-settings.js"></script>
	<script src="../v1/src/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
	<script src="../v1/src/plugins/switchery/switchery.min.js"></script>
	<!-- bootstrap-touchspin js -->
	<script src="../v1/src/plugins/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
	<script src="../v1/vendors/scripts/advanced-components.js"></script>
	<!-- add sweet alert js & css in footer -->
	<script src="../v1/src/plugins/sweetalert2/sweetalert2.all.js"></script>
	<script src="../v1/src/plugins/sweetalert2/sweet-alert.init.js"></script>
</body>
</html>