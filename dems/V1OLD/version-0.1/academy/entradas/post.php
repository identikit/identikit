<?php require_once '../model.php'; ?>

<?php $post = getPostById($_GET['slug']); ?>


<?php

?>
<!DOCTYPE html>

<html lang="es">

<title><?php echo $post['titulo'] ?></title>
<!--<meta property="og:image" content="../images/<?php//echo ($row['imagen']);?>" />
<meta content="<?php //echo strip_tags(substr($row['contenido'], 0, 30))?>" name="description" />-->



<!-- Twitter Card data -->
<meta name="twitter:card" content="IDentiKIT - Tu primera experiencia laboral.">
<meta name="twitter:site" content="@publisher_handle">
<meta name="twitter:title" content="<?php echo $post['titulo'] ?>">
<meta name="twitter:description" content="<?php echo strip_tags(substr($post['contenido'], 0, 30))?>">
<meta name="twitter:creator" content="@author_handle">
<meta name="twitter:image" content="../images/<?php echo ($post['imagen']);?>">


<!-- Open Graph data -->
<meta property="og:title" content="<?php echo $post['titulo'] ?>" />
<meta property="og:type" content="article" />
<meta property="og:url" content="post.php?slug=<?php echo ($post['slug']);?>" />
<meta property="og:image" content="../images/<?php echo $post['imagen'];?>" />
<meta property="og:image:width" content="1080" />
<meta property="og:image:height" content="1080" />
<meta property="og:description" content="<?php echo strip_tags(substr($post['contenido'], 0, 30))?>" />
<meta property="og:site_name" content="IDentiKIT - Tu primera experiencia laboral." />
<meta property="fb:app_id" content="2933918626820954" />


<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '{2933918626820954}',
      cookie     : true,
      xfbml      : true,
      version    : '{v10.0}'
    });
      
    FB.AppEvents.logPageView();   
      
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>


<meta charset="UTF-8">

<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="style2.css">

<link href="../../assets/css/style.css" rel="stylesheet">

<link href='https://fonts.googleapis.com/css?family=Rubik' rel='stylesheet' type='text/css' />

<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel='stylesheet' type='text/css' />

<link href="https://fonts.googleapis.com/css?family=Ubuntu" rel='stylesheet' type='text/css'/>

<link href="../../assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<link href="../../assets/vendor/icofont/icofont.min.css" rel="stylesheet">

<link href="../../assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">

<link href="../../assets/vendor/venobox/venobox.css" rel="stylesheet">

<link href="../../assets/vendor/aos/aos.css" rel="stylesheet">

<style type="text/css">
  a :hover{
    text-decoration: none!important;
    color: #fa8100 ;

  }

  a {
    text-decoration: none !important;
  }
  
  .articulo {
    margin: 50px;
  }

  @media screen and (max-width: 900px) {

  }
</style>

<body>

<!-- ======= Menu ======= -->
  <header id="header">
    
    <div class="container d-flex" style="margin-top: -20px;">
      <div class="logo mr-auto" >
        <span><a href="../" style="color: black; font-size: 30px; font-family: Ubuntu; max-width: 45px;"><img src="../logo.png" alt="" class="img-fluid" ></span></a>
      </div>

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li><a href="../../">Inicio</a></li>
          <li><a href="../../empresas/">Contrata talento</a></li>
          <li><a href="../../recruiters/">Recluta freelance</a></li>
          <li class="active"><a href="../">Blog</a></li>
          <li class="get-started"><a href="https://app.duhire.com" style="right: -50px;">Ingresar</a></li>
        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- Menu -->

<div class="w3-content" style="max-width:1400px">

<br><br><br>

<div class="w3-row">

<div class="w3-col l8 s12">

  <br><br>
  
  <div class="articulo">

      <h2><b><?php echo $post['titulo'] ?></b></h2>
      
<br><br>

    <div class="">

      <span>
        <?php echo $post['contenido'] ?>
      </span>
      
      <div class="w3-row"></div>
  
    </div>
  
  </div>
  
  <hr>

</div>

<div class="w3-col l4">
  
  <hr>
  
  <div class="w3-card w3-margin" style="margin-top: 15px !important;">
    <div class="w3-container w3-padding">
      <h4>Especialmente para vos</h4>
    </div>
    
    <?php

    require ("../admin/config/config.php");

    $entradas = $conexion->query("SELECT * FROM academy WHERE fecha < now() AND active = 1 ORDER BY fecha ASC LIMIT 0,3");
    while ($row = $entradas->fetch_assoc()) {
    ?>
  
    <ul class="w3-ul w3-hoverable w3-white">
    
      <a href="post.php?slug=<?php echo ($row['slug']);?>"><li class="w3-padding-16">

        <img src="../images/<?php echo ($row['imagen']);?>" alt="DuHimage" class="w3-left w3-margin-right" style="width:50px">
        
        <span class="w3-large"><?php echo ($row['titulo']);?></span><br>
    
        
    
      </li></a>

    </ul>

<?php } ?>

  </div>

  <hr> 
 
  <!-- Labels / tags 
  <div class="w3-card w3-margin">
    <div class="w3-container w3-padding">
      <h4>Tags</h4>
    </div>
    <div class="w3-container w3-white">
    <p><span class="w3-tag w3-black w3-margin-bottom">Travel</span> <span class="w3-tag w3-light-grey w3-small w3-margin-bottom">New York</span> <span class="w3-tag w3-light-grey w3-small w3-margin-bottom">London</span>
      <span class="w3-tag w3-light-grey w3-small w3-margin-bottom">IKEA</span> <span class="w3-tag w3-light-grey w3-small w3-margin-bottom">NORWAY</span> <span class="w3-tag w3-light-grey w3-small w3-margin-bottom">DIY</span>
      <span class="w3-tag w3-light-grey w3-small w3-margin-bottom">Ideas</span> <span class="w3-tag w3-light-grey w3-small w3-margin-bottom">Baby</span> <span class="w3-tag w3-light-grey w3-small w3-margin-bottom">Family</span>
      <span class="w3-tag w3-light-grey w3-small w3-margin-bottom">News</span> <span class="w3-tag w3-light-grey w3-small w3-margin-bottom">Clothing</span> <span class="w3-tag w3-light-grey w3-small w3-margin-bottom">Shopping</span>
      <span class="w3-tag w3-light-grey w3-small w3-margin-bottom">Sports</span> <span class="w3-tag w3-light-grey w3-small w3-margin-bottom">Games</span>
    </p>
    </div>
  </div>
 -->
</div>
<!-- END GRID -->
</div><br>

<!-- END w3-content -->
</div>

<?php require_once "../../vistas/chat.php"; ?>
<?php require "../../vistas/footer.php";?> 

</body>

<script src="../../assets/vendor/jquery/jquery.min.js"></script>
<script src="../../assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../../assets/vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="../../assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
<script src="../../assets/vendor/venobox/venobox.min.js"></script>
<script src="../../assets/vendor/aos/aos.js"></script>
<script src="../../assets/js/main.js"></script>
</html>
<?php ?>