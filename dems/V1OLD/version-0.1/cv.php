<?php 
require 'constants/settings.php'; 
require 'constants/check-login.php';
require 'constants/db_config.php';

if ($myrole == 'employer') {
	require 'employee/constants/check-login.php';
}else{
	header("location: employer/index.php");	
}




if (isset($_GET['empid'])) {
$empid = $_GET['empid'];

try {
	$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	
	$stmt = $conn->prepare("SELECT * FROM tbl_users WHERE role = 'employee' AND member_no = :empid");
	$stmt->bindParam(':empid', $empid);
	$stmt->execute();
	$result = $stmt->fetchAll();
	$rec = count($result);
	if ($rec == "0") {
	header("location:./");	
	}else{

	foreach($result as $row)
	{
	$myfname = $row['first_name'];
	$mylname = $row['last_name'];
	$bdate = $row['bdate'];
	$bmonth = $row['bmonth'];
	$byear = $row['byear'];
	$mycountry = $row['country'];
	$mycity = $row['city'];
	$myphone = $row['phone'];
	$about = $row['about'];
	$empavatar = $row['avatar'];
	$current_year = date('Y');
	$myedu = $row['education'];
	$mytitle = $row['title'];
	$mymail = $row['email'];
	$dni = $row['dni'];
	$ability = $row['ability'];
	$techno = $row['techno'];
	$link = $row['link'];
	$github = $row['github'];
	$twitter = $row['twitter'];
	$instagram = $row['instagram'];
	$empujar = $row['empujar'];
	}
	
	}

					  
	}catch(PDOException $e)
	{

	}


	
}else{
header("location:./");	
}

?>

<!DOCTYPE html>
<html>
<head>
	<!-- Basic Page Info -->
	<meta charset="utf-8">
	<title><?php echo "$myfname"; ?> <?php echo "$mylname"; ?> en IDentiKIT </title>

	<link rel="apple-touch-icon" sizes="180x180" href="logov3.png">
	<link rel="icon" type="image/png" sizes="32x32" href="logov3.png">
	<link rel="icon" type="image/png" sizes="16x16" href="logov3.png">
	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="v1/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="v1/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="v1/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="v1/src/plugins/datatables/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="v1/vendors/styles/style.css">

	 <meta property="og:description" content="<?php echo strip_tags(substr($about, 0, 200)); ?>" />
		<meta name="keywords" content="identikit, IDentiKIT, idkit, IDKIT, primera experiencia laboral, trabajos jr, junior, trabajos junior, empresas que contratan juniors, talento joven, talento tech, jovenes, empresas sin experiencia" />
		<meta name="author" content="IDentiKIT">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta property="og:image" content="https://identikit.app/logowebog.png" />
	  <meta property="og:image:secure_url" content="https://identikit.app/logowebog.png" />
	  <meta property="og:image:type" content="image/png" />
	  <meta property="og:image:width" content="300" />
	  <meta property="og:image:height" content="300" />




  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

  <script src="js/jquery.expander.js"></script>

<script>
$(document).ready(function() {
  var opts = {
    collapseTimer: 4000,
    expandEffect: 'fadeIn',
    collapseEffect: 'fadeOut',
  };

  $.each(['beforeExpand', 'afterExpand', 'onCollapse'], function(i, callback) {
    opts[callback] = function(byUser) {
      var msg = '<div class="success">' + callback;

      if (callback === 'onCollapse') {
        msg += ' (' + (byUser ? 'user' : 'timer') + ')';
      }
      msg += '</div>';

      $(this).parent().parent().append(msg);
    };
  });

  $('dl.expander dd').eq(0).expander();
  $('dl.expander dd').slice(1).expander(opts);

  $('ul.expander li').expander({
    slicePoint: 50,
    widow: 2,
    expandSpeed: 0,
    userCollapseText: '[^]'
  });

  $('div.expander').expander();
});
</script>


</head>
<body>
	

	<div class="header">
		<div class="header-left">
			<div class="menu-icon icon-copy ti-menu" style="color: white;"></div>
			
			<div class="search-toggle-icon dw dw-search2" style="color: #ffde00;" data-toggle="header_search"></div>
			<div class="header-search">
				<form method="post" action="busqueda.php" >
					<div class="form-group mb-0" >
						
					<i class="dw dw-search2 search-icon" style="color: #ffde00;"></i>
					
						<input type="text" class="form-control search-input" placeholder="Buscar en IDentiKIT" required name="PalabraClave">
						<input name="buscar" type="hidden" class="form-control mb-2" id="inlineFormInput" value="v">


					</div>
				</form>
			</div>
		</div>
		<div class="header-right">
			
			<!--<div class="user-notification">
				<div class="dropdown">
					<a class="dropdown-toggle no-arrow" href="#" role="button" data-toggle="dropdown">
						<i class="icon-copy dw dw-notification"></i>
						<span class="badge notification-active"></span>
					</a>
					<div class="dropdown-menu dropdown-menu-right">
						<div class="notification-list mx-h-350 customscroll">
							<ul>
								<li>
									<a href="#">
										<img src="vendors/images/img.jpg" alt="">
										<h3>John Doe</h3>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed...</p>
									</a>
								</li>
								
							</ul>
						</div>
					</div>
				</div>
			</div>-->
			<div class="user-info-dropdown">
				<div class="dropdown">
					<a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" style="color: #ffde00;">
					
						<span class="user-icon">
						<?php 
						if ($myavatar == null) {
							print '<center><img  src="images/default.png" title="" alt="image"  /></center>';
						}else{
							echo '<center><img alt="image" title=""  src="data:image/png;base64,'.base64_encode($myavatar).'"/></center>';	
						}
						?>	
																	
						</span>
						<!--<span class="user-name" style="color: white;"><?php //echo "$myfname"; ?></span>-->
					</a>
					<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
						<?php
						if ($user_online == true) {
						print '
							<a class="dropdown-item" href="'.$myrole.'"><i class="dw dw-user1"></i> Perfil</a>
							<!--<a class="dropdown-item" href="#"><i class="dw dw-settings2"></i> Ajustes</a>
							<a class="dropdown-item" href="#"><i class="dw dw-help"></i> Ayuda</a>-->
							<a class="dropdown-item" href="logout.php"><i class="dw dw-logout"></i> Salir</a>
';
						}else{
						print '
							<li><a href="login.php">ingresar</a></li>
							<li><a data-toggle="modal" href="#registerModal">registrate</a></li>';						
						}
						
						?>
					</div>
				</div>
			</div>
			
		</div>
	</div>

	

	<div class="left-side-bar">
		<div class="brand-logo">
			<a href="#">
				<img src="v1/logov3.png" alt="" class="light-logo" width="60"> <span class="mtext"> IDentiKIT</span>
			</a>
			<div class="close-sidebar" data-toggle="left-sidebar-close">
				<i class="ion-close-round"></i>
			</div>
		</div>
		<div class="menu-block customscroll">
			<div class="sidebar-menu">
				<ul id="accordion-menu">
					<?php
					if ($myrole == "employee") {
						print '<a href="index.php" class="dropdown-toggle no-arrow ">
						<span class="micon icon-copy fa fa-home"></span><span class="mtext">Inicio</span></a>

						<li class="dropdown">
						<a href="javascript:;" class="dropdown-toggle">
							<span class="micon icon-copy fa fa-bandcamp"></span><span class="mtext">Explorar</span>
						</a>
						<ul class="submenu">
							<li><a href="identis.php" class="active"> IDentis</a></li>
							<li><a href="works.php"> Trabajos</a></li>
							<li><a href="empresas.php"> Empresas</a></li>
						</ul>
						</li>

							<li>
						<a href="aplicados.php" class="dropdown-toggle no-arrow">
							<span class="micon ion-android-done-all"></span><span class="mtext">Aplicados</span>

						</a>
					</li>
						';
					} if ($myrole == "employer") {
						print '<a href="index.php" class="dropdown-toggle no-arrow ">
						<span class="micon icon-copy fa fa-home"></span><span class="mtext">Panel</span></a>
						<a href="../identis.php" class="dropdown-toggle no-arrow"><span class="micon icon-copy fa fa-bandcamp"></span><span class="mtext">identis</span></a>
						
						
						
						';
					}

					else {
						print '<a href="login.php" class="dropdown-toggle no-arrow ">
						<span class="micon icon-copy fa fa-lock"></span><span class="mtext">Iniciar sesion</span></a>

						<a href="registro.php" class="dropdown-toggle no-arrow ">
						<span class="micon icon-copy fa fa-user"></span><span class="mtext">Registrarme</span></a>

						
					
						
						
						
						';
					
					}

						
						?>
							

						
					</li>
									

				
				</ul>
			</div>
		</div>
	</div>
	<div class="mobile-menu-overlay"></div>

<style type="text/css">
  
  .btn-idkit {
	display:block;
	width:70px;
	height:70px;
	color: #fff;
	position: fixed;
	right:20px;
	bottom:20px;
	border-radius:60%;
	line-height:80px;
	text-align:center;
	z-index:999;
  }

  .rainbow-button {

  background-image: linear-gradient(90deg, #01c0fe 0%, #1bff00 49%, #fd0098 80%, #5600ff 100%);
  
  display:flex;
  align-items:center;
  justify-content:center;
  text-transform:uppercase;
  font-size:3vw;
  font-weight:bold;
  border-radius: 100px;

  content:attr(alt);
  
  background-color:#191919;
  display:flex;
  align-items:center;
  justify-content:center;

  animation:slidebg 2s linear infinite;
  height: auto !important;


}


@keyframes slidebg {
  to {
	background-position:20vw;
  }
}
</style>

<div class="btn-idkit center wow animated bounceInRight box1 " data-wow-delay="0.2s" data-toggle="tooltip" title="Perfil Verificado"  >
	<a href="#" >
	<img src="logov3.png" alt="Contactar" class="rainbow-button">
	</a>
	</div>

	<div class="main-container">
			<div class="pd-ltr-20 xs-pd-20">
				<div class="min-height-200px">
					<div class="row">
						<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 mb-30">
							<div class="pd-20 card-box height-100-p">
								<div class="profile-photo">
								<style>
								#fotop {
								display: block;
								margin: 0 auto 20px;
								width: 150px;
								height: 150px;
								-webkit-box-shadow: 0 0 10px rgba(0,0,0,.3);
								box-shadow: 0 0 10px rgba(0,0,0,.3);
								border-radius: 100%;
								overflow: hidden;
								}
								</style>
									
<?php 
										if ($empavatar == null) {
										print '<center><img class="img-circle autofit2" src="images/default.png"  alt="image" id="fotop" /></center>';
										}else{
										echo '<center><img class="img-circle autofit2" alt="image"id="fotop" src="data:image/jpeg;base64,'.base64_encode($empavatar).'"/></center>';	
										}
										?>
										<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
										<div class="modal-dialog modal-dialog-centered" role="document">
											<div class="modal-content">
												<div class="modal-body pd-5">
													<div class="img-container">
<?php 
										if ($empavatar == null) {
										print '<center><img class="img-circle autofit2" src="images/default.jpg"  alt="image"  /></center>';
										}else{
										echo '<center><img class="img-circle autofit2" alt="image" src="data:image/jpeg;base64,'.base64_encode($empavatar).'"/></center>';	
										}
										?>
										</div>
												</div>
												<div class="modal-footer">
													<input type="submit" value="Update" class="btn btn-primary">
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>
								</div>
								<h5 class="text-center h5 mb-0 " ><?php if($row['empujar'] == "1") { print '🌟'; }?><?php echo strip_tags("$myfname"); ?> <?php echo strip_tags("$mylname"); ?></h5>
								

								<?php

								$title = $row['mytitle'];

								?>
								<div class="work text-success text-center pd-10"> <?php if ($row['title']) { echo '💻 '.strip_tags($mytitle).' '; } ?></div>	
								
								<div class="col-md-4 col-sm-12 mb-30">
							
					</div>

					<div class="btn-list text-center pd-10">
					<?php

					if ($github) {
						print '<a href="https://github.com/'."$github".'" type="button" class="btn" style="border-radius: 10px;" data-bgcolor="black" data-color="#ffffff" target="_blank"><i class="fa fa-github"></i></a>';												
					}

					if ($twitter) {
						print '<a href="https://twitter.com/'."$twitter".'" type="button" class="btn" style="border-radius: 10px;" data-bgcolor="#1da1f2" data-color="#ffffff" target="_blank"><i class="fa fa-twitter"></i></a>';												
					}

					if ($instagram) {
						print '<a href="https://instagram.com/'."$instagram".'" type="button" class="btn" style="border-radius: 10px;" data-bgcolor="#f46f30" data-color="#ffffff" target="_blank"><i class="fa fa-instagram"></i></a>';												
					}


					?>
						
						
						
						
						
					
					</div>

								<div class="expander">

								<p class="text-center text-muted font-14"><?php echo "$about"; ?></p>
							</div>

								<center><a href="#" class="btn btn-primary col-sm-4 col-md-4 mb-30" data-toggle="modal" data-target="#bd-example-modal-lg" type="button">Contacto</a></center>
							<div class="modal fade bs-example-modal-lg" id="bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
								<div class="modal-dialog modal-lg modal-dialog-centered">
									<div class="modal-content">
										<div class="modal-header">
											<h4 class="modal-title" id="myLargeModalLabel">Datos de contacto</h4>
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
										</div>
										<div class="modal-body">
											<div class="work text-success text-center pd-10"> <i class="icon-copy ion-at"></i> Email: <a href="mailto:<?php echo "$mymail";?>"><?php echo "$mymail";?></a></div>
											<div class="work text-success text-center pd-10"> <i class="icon-copy ion-iphone"></i> Telefono: <a href="tel:<?php echo "$myphone";?>"><?php echo "$myphone";?> </a></div>
								<div class="work text-success text-center pd-10"> <i class="icon-copy ion-location"></i> Ciudad: <?php echo "$mycountry";?>, <?php echo "$mycity";?></div>
								<div class="work text-success text-center pd-10"> <i class="icon-copy ion-qr-scanner"></i> DNI: <?php echo "$dni";?></div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
										</div>
									</div>
								</div>
							</div>

								

								<div class="contact-directory-box pd-0">
									<div class="contact-dire-info text-center pd-0">
										
										
										<div class="contact-skill pd-0">
														<?php 

														$tags = explode(",", $ability);

														for ($i=0; $i <= 4 ; $i++) { 
															print '
															<span class="badge badge-pill">'."$tags[$i]".'</span>


															';
														}



														

													

														?>
														
														
													</div>

										<div class="timeline mb-30">
									<h5 class="text-center mb-20 h5 mb-0 pd-10">Proyectos </h5>
									<ul>
									<?php
												require 'constants/db_config.php';
												try {
												$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
												$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
												$stmt = $conn->prepare("SELECT * FROM tbl_experience WHERE member_no = :empid ORDER BY id");
												$stmt->bindParam(':empid', $empid);
												$stmt->execute();
												$result = $stmt->fetchAll();
												$rec = count($result);
												if ($rec == "0") {
 
												}else{

												foreach($result as $row)
												{
												?>
										<li>
											<div class="timeline-date">
												<?php echo $row['start_date']; ?> <b>al</b> <?php echo $row['end_date']; ?>
											</div>
											<div class="timeline-desc card-box">
												<div class="pd-20">
													<h4 class="mb-10 h4"><?php echo $row['title']; ?></h4>
													<?php
													$tags = explode(",", $row['techno']);

													for ($i=0; $i <= count($tags) - 1  ;  $i++ ) {
														$array = $tags[$i];
														print '<span class="badge badge-pill" style="background-color: whitesmoke; margin: 5px; color: #01c0fe;">'." $array".'</a></span>';
													}
													?>	<br><br>
													<p class="text-center text-muted font-14"><?php echo $row['duties']; ?></p>
													<p class="text-center text-muted font-14" ><a href="<?php echo $row['link'];?>" target="_blank"><?php echo $row['link'];?></a></p>
												</div>
											</div>
										</li>
																						<?php
												}
	
												}

					  
												}catch(PDOException $e)
												{

												 } ?>
									

									</ul>
								</div>
						</div>
					</div>


					


							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	<!-- js -->
	<script src="v1/vendors/scripts/core.js"></script>
	<script src="v1/vendors/scripts/script.min.js"></script>
	<script src="v1/vendors/scripts/process.js"></script>
	<script src="v1/vendors/scripts/layout-settings.js"></script>
	
	<script src="v1/vendors/scripts/dashboard.js"></script>
</body>
</html>