<?php
if(isset($_GET['delete_id']))
{
 // select image from db to delete
 $stmt_select = $DB_con->prepare('SELECT imagen FROM academy WHERE id =:uid');
 $stmt_select->execute(array(':uid'=>$_GET['delete_id']));
 $imgRow=$stmt_select->fetch(PDO::FETCH_ASSOC);
 unlink("../images/".$imgRow['imagen']);
 
 // it will delete an actual record from db
 $stmt_delete = $DB_con->prepare('DELETE FROM academy WHERE id =:uid');
 $stmt_delete->bindParam(':uid',$_GET['delete_id']);
 $stmt_delete->execute();
 
 header("Location: index.php");
}

?>
<!DOCTYPE html>
<html lang="es">

<head>
    <title>IDentiKIT app</title>
    <!-- HTML5 Shim and Respond.js IE11 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 11]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="" />
    <meta name="keywords" content="">
    <meta name="author" content="Phoenixcoded" />
    <!-- Favicon icon -->
    <link rel="icon" href="views/dist/assets/images/favicon.ico" type="image/x-icon">

    <!-- vendor css -->
    <link rel="stylesheet" href="views/dist/assets/css/style.css">
    
    <script src="https://cdn.tiny.cloud/1/0yb7elgidejk329quamk21ozwpex0990qg48k8b1n22eyja8/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

    <script>
      tinymce.init({
        selector: '#mytextarea'
      });
    </script>
    

</head>
<body class="">
  <!-- [ Pre-loader ] start -->
  <div class="loader-bg">
    <div class="loader-track">
      <div class="loader-fill"></div>
    </div>
  </div>


  <nav class="pcoded-navbar menu-light ">
          <div class="navbar-wrapper  ">
              <div class="navbar-content scroll-div " >
                  
                  <div class="">
                      <div class="main-menu-header">
                  
                          <img class="img-radius" src="views/profileimages/<?php echo $user['imagen'];?>" alt="User-Profile-Image">
                          <div class="user-details">
                              <div id="more-details"><?php echo $user['usuario']; ?> </div>
                          </div>
                      </div>
                      <!--<div class="collapse" id="nav-user-link">
                          <ul class="list-unstyled">
                              <li class="list-group-item"><a href="user-profile.html"><i class="feather icon-user m-r-5"></i>Ver perfil</a></li>
                              <li class="list-group-item"><a href=""><i class="feather icon-settings m-r-5"></i>Configuracion</a></li>
                              <li class="list-group-item"><a href="auth-normal-sign-in.html"><i class="feather icon-log-out m-r-5"></i>Cerrar sesion</a></li>
                          </ul>
                      </div>-->
                  </div>
                  
                  <ul class="nav pcoded-inner-navbar ">
                      <li class="nav-item pcoded-menu-caption">
                          <label>Menu</label>
                      </li>
                  
                      <li class="nav-item active">
                          <a href="usuario.php" class="nav-link "><span class="pcoded-micon"><i class="feather icon-home"></i></span><span class="pcoded-mtext">Dashboard</span></a>
                      </li>


                      <li class="nav-item ">
                          <a href="#" class="nav-link "><span class="pcoded-micon"><i class="feather icon-file-text f-28 "></i></span><span class="pcoded-mtext">Entradas</span></a>
                      </li>
                      
                      
                      
                       
                      
                      

                  </ul>
                  
                 
                  
              </div>
          </div>
      </nav>


<header class="navbar pcoded-header navbar-expand-lg navbar-light header-blue">
        
            
                <div class="m-header">
                    <a class="mobile-menu" id="mobile-collapse" href="#!"><span></span></a>
                    <a href="#!" class="b-brand">
                        <img src="views/dist/assets/images/logo.png" alt="" class="logo" width="45">
                        
                    </a>
                    <a href="#!" class="mob-toggler">
                        <i class="feather icon-more-vertical"></i>
                    </a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a href="#!" class="pop-search"><i class="feather icon-search"></i></a>
                            <div class="search-bar">
                                <input type="text" class="form-control border-0 shadow-none" placeholder="Search hear">
                                <button type="button" class="close" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </li>
                    </ul>
                    <ul class="navbar-nav ml-auto">
                        <li>
                            <div class="dropdown">
                                <a class="dropdown-toggle" href="#" data-toggle="dropdown"><i class="icon feather icon-bell"></i></a>
                                <div class="dropdown-menu dropdown-menu-right notification">
                                    <div class="noti-head">
                                        <h6 class="d-inline-block m-b-0">Notificaciones</h6>
                                        <div class="float-right">
                                            <a href="#!" class="m-r-10">Marcar como  leido</a>
                                            <a href="#!">Limpiar todo</a>
                                        </div>
                                    </div>
                                    <ul class="noti-body">
                                        <li class="n-title">
                                            <p class="m-b-0"></p>
                                        </li>
                                        <li class="notification">
                                            <div class="media">
                                                <img class="img-radius" src="views/dist/assets/images/user/avatar-1.jpg" alt="Generic placeholder image">
                                                <div class="media-body">
                                                    <p><strong>Lucho</strong><span class="n-time text-muted"><i class="icon feather icon-clock m-r-10"></i>5 min</span></p>
                                                    <p>Mensaje demo</p>
                                                </div>
                                            </div>
                                        </li>
                                        
                                        
                                    </ul>
                                    <div class="noti-footer">
                                        <a href="#!">Ver todo</a>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="dropdown drp-user">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="feather icon-user"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right profile-notification">
                                    <div class="pro-head">
                                        <img src="views/profileimages/<?php echo $user['imagen'];?>" class="img-radius" alt="User-Profile-Image">
                                        <span><?php echo $user['usuario']; ?></span>
                                        <a href="close.php" class="dud-logout" title="Logout">
                                            <i class="feather icon-log-out"></i>
                                        </a>
                                    </div>
                                    <ul class="pro-body">
                                        <li><a href="#!" class="dropdown-item"><i class="feather icon-user"></i> Perfil</a></li>
                                        <li><a href="#!" class="dropdown-item"><i class="feather icon-mail"></i> Mensajes</a></li>
                                        <li><a href="close.php" class="dropdown-item"><i class="feather icon-lock"></i> Cerrar Sesion</a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                
            
    </header>


<div class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10">Inicio </h5>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#"><i class="feather icon-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="#!">Entradas </a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
      
        <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-12">
                <div class="card">
                    <div class="col-xl-12">
                <h5 class="mt-4"> Usuarios</h5>

                <hr>
                

                 <div class="col-md-12">
                   
                        <div class="table-responsive">
                            <table class="table table-dark">
                                     


                                <thead>
                                
                                    <tr>
                                        <th>#</th>
                                        <th>Nombre y apellido</th>
                                        <th>Email</th>
                                        <th>Usename</th>
                                    </tr>
                                </thead>
                                <?php

                              require ("config/config.php");

                              //$hoy = $conexion->query("SELECT CURDATE()");
                              $user = $conexion->query("SELECT * FROM tbl_users ORDER BY member_no ASC");
                              while ($rower = $user->fetch_assoc()) { 
                              ?>
                                <tbody>
                                    <tr>
                                        <td>#</td>
                                        <td><?php echo $rower['first_name'];?> <?php echo $rower['last_name'];?></td>
                                        <td><?php echo $rower['email'];?></td>
                                        <td><a href="../perfil.php?empid=<?php echo $rower['member_no'];?>" target="_blank"><?php echo $rower['member_no'];?></a></td>
                                    </tr>
                                   
                                    
                                </tbody><?php } ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


               
            </div>
            </div>
            </div>
            <!-- [ sample-page ] end -->
        </div>
        <!-- [ Main Content ] end -->
    </div>
</div>
<!-- [ Main Content ] end -->
    <!-- Warning Section start -->
    <!-- Older IE warning message -->
    <!--[if lt IE 11]>
        <div class="ie-warning">
            <h1>Warning!!</h1>
            <p>You are using an outdated version of Internet Explorer, please upgrade
               <br/>to any of the following web browsers to access this website.
            </p>
            <div class="iew-container">
                <ul class="iew-download">
                    <li>
                        <a href="http://www.google.com/chrome/">
                            <img src="dist/assets/images/browser/chrome.png" alt="Chrome">
                            <div>Chrome</div>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.mozilla.org/en-US/firefox/new/">
                            <img src="dist/assets/images/browser/firefox.png" alt="Firefox">
                            <div>Firefox</div>
                        </a>
                    </li>
                    <li>
                        <a href="http://www.opera.com">
                            <img src="dist/assets/images/browser/opera.png" alt="Opera">
                            <div>Opera</div>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.apple.com/safari/">
                            <img src="dist/assets/images/browser/safari.png" alt="Safari">
                            <div>Safari</div>
                        </a>
                    </li>
                    <li>
                        <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                            <img src="dist/assets/images/browser/ie.png" alt="">
                            <div>IE (11 & above)</div>
                        </a>
                    </li>
                </ul>
            </div>
            <p>Sorry for the inconvenience!</p>
        </div>
    <![endif]-->
    <!-- Warning Section Ends -->

    <!-- Required Js -->
    <script src="views/dist/assets/js/vendor-all.min.js"></script>
    <script src="views/dist/assets/js/plugins/bootstrap.min.js"></script>
    <script src="views/dist/assets/js/ripple.js"></script>
    <script src="views/dist/assets/js/pcoded.min.js"></script>



</body>

</html>