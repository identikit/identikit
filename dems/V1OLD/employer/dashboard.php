<?php 
include '../constants/settings.php'; 
include 'constants/check-login.php';

if ($user_online == "true") {
if ($myrole == "employer") {
}else{
header("location:../");		
}
}else{
header("location:../");	
}


?>

<!DOCTYPE html>
<html>

<head>
	<!-- Basic Page Info -->
	<meta charset="utf-8">
	<title>IDentiKIT - �Crea tu cv y encontra trabajo rapido!</title>

	
	<!-- Site favicon 
	<link rel="apple-touch-icon" sizes="180x180" href="vendors/images/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="vendors/images/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="vendors/images/favicon-16x16.png">-->

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="../v1/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="../v1/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="../v1/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../v1/src/plugins/datatables/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../v1/vendors/styles/style.css">
	<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '4027353733953861');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=4027353733953861&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

</head>
<body>

<!--<div class="pre-loader">
		<div class="pre-loader-box">
			<div class="loader-logo"><img src="../logo.png" alt="" width="100"></div>
			<div class='loader-progress' id="progress_div">
				<div class='bar' id='bar1'></div>
			</div>
			<div class='percent' id='percent1'>0%</div>
			<div class="loading-text">
				Cargando...
			</div>
		</div>

	</div>-->

	<div class="header">
		<div class="header-left">
			<div class="menu-icon icon-copy ti-menu" style="color: white;"></div>
			
			<div class="search-toggle-icon dw dw-search2" style="color: #ffde00;" data-toggle="header_search"></div>
			<div class="header-search">
				<form>
					<div class="form-group mb-0" >
						<i class="dw dw-search2 search-icon" style="color: #ffde00;"></i>
						<input type="text" class="form-control search-input" placeholder="Buscar en IDentiKIT">
					</div>
				</form>
			</div>
		</div>
		<div class="header-right">
			
			<!--<div class="user-notification">
				<div class="dropdown">
					<a class="dropdown-toggle no-arrow" href="#" role="button" data-toggle="dropdown">
						<i class="icon-copy dw dw-notification"></i>
						<span class="badge notification-active"></span>
					</a>
					<div class="dropdown-menu dropdown-menu-right">
						<div class="notification-list mx-h-350 customscroll">
							<ul>
								<li>
									<a href="#">
										<img src="vendors/images/img.jpg" alt="">
										<h3>John Doe</h3>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed...</p>
									</a>
								</li>
								
							</ul>
						</div>
					</div>
				</div>
			</div>-->
			<div class="user-info-dropdown">
				<div class="dropdown">
					<a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" style="color: #ffde00;">
						<span class="user-icon">
							<?php 
							if ($logo == null) {
								print '<img  src="../images/default.png" title="'.$compname.'" alt="image"  /></center>';
							}else{
								echo '<center><img alt="image" title="'.$compname.'" width="180" height="100" src="data:image/jpeg;base64,'.base64_encode($logo).'"/></center>';
							}
							?>
								
						</span>
						<!--<span class="user-name" style="color: white;"><?php //echo "$myfname"; ?></span>-->
					</a>
					<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
						<?php
						if ($user_online == true) {
						print '
							<a class="dropdown-item" href="index.php"><i class="dw dw-user1"></i> Pefil</a>
							<!--<a class="dropdown-item" href="#"><i class="dw dw-settings2"></i> Ajustes</a>
							<a class="dropdown-item" href="#"><i class="dw dw-help"></i> Ayuda</a>-->
							<a class="dropdown-item" href="logout.php"><i class="dw dw-logout"></i> Salir</a>
';
						}else{
						print '
							<li><a href="login.php">ingresar</a></li>
							<li><a data-toggle="modal" href="#registerModal">registrate</a></li>';						
						}
						
						?>
					</div>
				</div>
			</div>
			
		</div>
	</div>

	

	<div class="left-side-bar">
		<div class="brand-logo">
			<a href="#">
				<img src="../v1/logov3.png" alt="" class="light-logo" width="60"> <span class="mtext"> IDentiKIT</span>
			</a>
			<div class="close-sidebar" data-toggle="left-sidebar-close">
				<i class="ion-close-round"></i>
			</div>
		</div>
		<div class="menu-block customscroll">
			<div class="sidebar-menu">
				<ul id="accordion-menu">
					<?php
					if ($myrole == "employee") {
						print '<a href="index.php" class="dropdown-toggle no-arrow ">
						<span class="micon icon-copy fa fa-home"></span><span class="mtext">Inicio</span></a>

						<li class="dropdown">
						<a href="javascript:;" class="dropdown-toggle">
							<span class="micon icon-copy fa fa-bandcamp"></span><span class="mtext">Explorar</span>
						</a>
						<ul class="submenu">
							<li><a href="identis.php" class="active"> IDentis</a></li>
							<li><a href="works.php"> Trabajos</a></li>
							<li><a href="empresas.php"> Empresas</a></li>
						</ul>
						</li>

							<li>
						<a href="aplicados.php" class="dropdown-toggle no-arrow">
							<span class="micon ion-android-done-all"></span><span class="mtext">Aplicados</span>

						</a>
					</li>
						';
					}else {
						print '<a href="dashboard.php" class="dropdown-toggle no-arrow ">
						<span class="micon icon-copy fa fa-home"></span><span class="mtext">Panel</span></a>
						<a href="../identis.php" class="dropdown-toggle no-arrow"><span class="micon icon-copy fa fa-bandcamp"></span><span class="mtext">identis</span></a>
						
						
						
						';
					
					}

						
						?>
							

						
					</li>
									

					<li>
						<a href="../index.php" class="dropdown-toggle no-arrow ">
							<span class="micon icon-copy fa fa-free-code-camp"></span><span class="mtext">Academia</span>

						</a>
					</li>
					
				</ul>
			</div>
		</div>
	</div>
	<div class="mobile-menu-overlay"></div>

    <div class="main-container">
        <div class="pd-ltr-20 xs-pd-20-10">








            <div class="min-height-200px">
                
				<center><a href="postjob.php" class="btn btn-primary col-sm-12 col-md-6 mb-30 ">Publicar aviso</a></center>
				<?php require 'constants/check_reply.php'; ?>
				<div class="card-box mb-30">
				

					<div class="pd-20">
						<h4 class="h4">Mis avisos</h4>
					
					</div>
					<div class="pb-20">
						<table class="data-table table stripe hover nowrap">
							<thead>
								<tr>
									<th class="table-plus datatable-nosort">Titulo</th>
									<th>Pa�s</th>
									<th>Ciudad</th>
									<th>Finaliza</th>
									<th>Tipo</th>
									<th class="datatable-nosort">Acciones</th>
								</tr>
							</thead>
							<tbody>
							<?php
										require '../constants/db_config.php';
										try {
                                        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
                                        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                                        $stmt = $conn->prepare("SELECT * FROM tbl_jobs WHERE company = '$myid' ORDER BY enc_id DESC ");
                                        $stmt->execute();
                                        $result = $stmt->fetchAll();
   
                                        foreach($result as $row)
                                        {
										   $jobcity = $row['city'];
										   $jobcountry = $row['country'];
										   $type = $row['type'];
										   $title = $row['title'];
										   $deadline = $row['closing_date'];
										 
										   
										   ?>
								<tr>
									<td class="table-plus"><?php echo "$title" ?></td>
									<td><?php echo "$jobcountry" ?></td>
									<td><?php echo "$jobcity" ?></td>
									<td><?php echo "$deadline" ?></td>
									<td><?php echo "$type" ?> </td>
									<td>
										<div class="dropdown">
											<a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
												<i class="dw dw-more"></i>
											</a>
												<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
										<a class="dropdown-item" href="postulados.php?jobid=<?php echo $row['job_id']; ?>"><i class="dw dw-eye"></i> Postulantes</a>
										<a class="dropdown-item" href="editar.php?jobid=<?php echo $row['job_id']; ?>"><i class="dw dw-edit2"></i> Editar</a>
										<a class="dropdown-item" onclick = "return confirm('�Deseas eliminar este aviso?')" href="app/drop-job.php?id=<?php echo $row['job_id'];?>"><i class="dw dw-delete-3"></i> Borrar</a>
									</div>
										</div>
									</td>
								</tr>
								
												<?php
		
 
	                                    }

					  
	                                    }catch(PDOException $e)
                                        {
                         
                                        }
                                             ?>
							</tbody>
						</table>
					</div>
				</div>
			
				



				

			<div class="row clearfix">

					<div class="col-sm-12 col-md-4 mb-30">
						<div class="card card-box text-center">
							<div class="card-body">
								<h5 class="card-title">Special title treatment</h5>
								<p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
								<a href="#" class="btn btn-primary">Go somewhere</a>
							</div>
						</div>
					</div>

					<div class="col-sm-12 col-md-4 mb-30">
						<div class="card card-box text-center">
							<div class="card-body">
								<h5 class="card-title">Special title treatment</h5>
								<p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
								<a href="#" class="btn btn-primary">Go somewhere</a>
							</div>
						</div>
					</div>

					<div class="col-sm-12 col-md-4 mb-30">
						<div class="card card-box text-center">
							<div class="card-body">
								<h5 class="card-title">Special title treatment</h5>
								<p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
								<a href="#" class="btn btn-primary">Go somewhere</a>
							</div>
						</div>
					</div>
			</div>
				
				
				
				
				
				
				<!--<div class="card-box mb-30">
					<div class="pd-20">
						<h4 class="text-blue h4">Data Table Simple</h4>
						<p class="mb-0">you can find more options <a class="text-primary" href="https://datatables.net/" target="_blank">Click Here</a></p>
					</div>
					<div class="pb-20">
						<table class="data-table table stripe hover nowrap">
							<thead>
								<tr>
									<th class="table-plus datatable-nosort">Name</th>
									<th>Age</th>
									<th>Office</th>
									<th>Address</th>
									<th>Start Date</th>
									<th class="datatable-nosort">Action</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="table-plus">Gloria F. Mead</td>
									<td>25</td>
									<td>Sagittarius</td>
									<td>2829 Trainer Avenue Peoria, IL 61602 </td>
									<td>29-03-2018</td>
									<td>
										<div class="dropdown">
											<a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
												<i class="dw dw-more"></i>
											</a>
											<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
												<a class="dropdown-item" href="#"><i class="dw dw-eye"></i> View</a>
												<a class="dropdown-item" href="#"><i class="dw dw-edit2"></i> Edit</a>
												<a class="dropdown-item" href="#"><i class="dw dw-delete-3"></i> Delete</a>
											</div>
										</div>
									</td>
								</tr>
								
								
							</tbody>
						</table>
					</div>
				</div>-->

			
			</div>
		</div>
	</div>
</div>
</body>
  <!-- js -->
    <script src="../v1/vendors/scripts/core.js"></script>
    <script src="../v1/vendors/scripts/script.min.js"></script>
    <script src="../v1/vendors/scripts/process.js"></script>
    <script src="../v1/vendors/scripts/layout-settings.js"></script>
	<script src="../v1/src/plugins/datatables/js/jquery.dataTables.min.js"></script>
	<script src="../v1/src/plugins/datatables/js/dataTables.bootstrap4.min.js"></script>
	<script src="../v1/src/plugins/datatables/js/dataTables.responsive.min.js"></script>
	<script src="../v1/src/plugins/datatables/js/responsive.bootstrap4.min.js"></script>
	<!-- buttons for Export datatable -->
	<script src="../v1/src/plugins/datatables/js/dataTables.buttons.min.js"></script>
	<script src="../v1/src/plugins/datatables/js/buttons.bootstrap4.min.js"></script>
	<script src="../v1/src/plugins/datatables/js/buttons.print.min.js"></script>
	<script src="../v1/src/plugins/datatables/js/buttons.html5.min.js"></script>
	<script src="../v1/src/plugins/datatables/js/buttons.flash.min.js"></script>
	<script src="../v1/src/plugins/datatables/js/pdfmake.min.js"></script>
	<script src="../v1/src/plugins/datatables/js/vfs_fonts.js"></script>
	<!-- Datatable Setting js -->
	<script src="../v1/vendors/scripts/datatable-setting.js"></script></body>
	
	<script src="../v1/src/plugins/sweetalert2/sweetalert2.all.js"></script>
	<script src="../v1/src/plugins/sweetalert2/sweet-alert.init.js"></script>


</html>








