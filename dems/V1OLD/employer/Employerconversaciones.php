<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-5F5WB8FMJC"></script>
<script>
	window.dataLayer = window.dataLayer || [];

	function gtag() {
		dataLayer.push(arguments);
	}
	gtag('js', new Date());

	gtag('config', 'G-5F5WB8FMJC');
</script>
<!DOCTYPE html>
<html>
<?php
	require_once("../config/Db.php");
    require '../constants/check-login.php';
	$db = new DbPDO();

	if(isset($_GET['receiverId'])){
		$senderId          = $_SESSION['myid'];
		$receiverId        = $_GET['receiverId'];
		$mensajes          = $db->query("SELECT * FROM tbl_messages a WHERE (a.message_sender_id = '".$senderId."' AND a.message_receiver_id = '".$receiverId."') OR (a.message_sender_id = '".$receiverId."' AND a.message_receiver_id = '".$senderId."') 
		order by message_creation_date, message_creation_hour ASC");
	}

	if ($user_online == "true") {
		if ($myrole == "employer") {
		} else {
			header("location:../");		
		}
	} else {
		header("location:../");	
	}
?>

<head>
	<!-- Basic Page Info -->
	<meta charset="utf-8">
	<title>IDentiKIT - Mi perfil</title>

	<link rel="apple-touch-icon" sizes="180x180" href="logov3.png">
	<link rel="icon" type="image/png" sizes="32x32" href="logov3.png">
	<link rel="icon" type="image/png" sizes="16x16" href="logov3.png">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="../v1/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="../v1/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="../v1/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../v1/src/plugins/datatables/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../v1/vendors/styles/style.css">
	<link rel="stylesheet" type="text/css" href="../v1/src/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdn.tiny.cloud/1/0yb7elgidejk329quamk21ozwpex0990qg48k8b1n22eyja8/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

</head>

<script>
	    $(document).ready(function(){	
			var $text = $('#MensajeHistorial');
            $text.scrollTop($text[0].scrollHeight);

			$('#enviarMensaje').on('click',function(){
                var mensaje    = $("#mensaje").val();
				var senderId   = '<?php echo $_SESSION['myid']; ?>'; 
                var receiverId = '<?php echo $_GET['receiverId']; ?>';

				$.ajax({
					type:'POST',
					url:'app/agregarconversacion.php',
					dataType: "json",
					data: { 
                        'mensaje':     mensaje, 
                        'senderId':    senderId,
                        'receiverId':  receiverId
					},
					success:function(response){
						if(response.status == "correcto"){
							$("#myModal").modal();
							setTimeout(function() {
								window.location.reload();
							}, 1500);
						}else{
						    alert('ERROR');
						} 
					}
				});
			});
		});
    </script>
<body>
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PHBQLSG" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	
		<div class="header">
		<div class="header-left">
			<div class="menu-icon icon-copy ti-menu" style="color: white;"></div>
			
			<div class="search-toggle-icon dw dw-search2" style="color: #ffde00;" data-toggle="header_search"></div>
			<div class="header-search">
			<form method="post" action="../busqueda.php" >
					<div class="form-group mb-0" >
					<i class="dw dw-search2 search-icon" style="color: #ffde00;"></i>
						<input type="text" class="form-control search-input" placeholder="Buscar en IDentiKIT" required name="PalabraClave">
						<input name="buscar" type="hidden" class="form-control mb-2" id="inlineFormInput" value="v">
					</div>
				</form>
			</div>
		</div>
		<div class="header-right">
			<div class="user-info-dropdown">
				<div class="dropdown">
					<a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" style="color: #ffde00;">
						<span class="user-icon">
							<?php 
								if ($logo == null) {
									print '<center><img  src="images/default.png" title="" alt="image"  /></center>';
								}else{
									echo '<center><img alt="image"  width="180" height="100" src="data:image/jpeg;base64,'.base64_encode($logo).'"/></center>';
								}
							?>								
						</span>
					</a>
					<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
						<?php
							if ($user_online == true) {
							print '
								<a class="dropdown-item" href="micuenta.php"><i class="dw dw-user1"></i> Perfil</a>
								<!--<a class="dropdown-item" href="#"><i class="dw dw-settings2"></i> Ajustes</a>
								<a class="dropdown-item" href="#"><i class="dw dw-help"></i> Ayuda</a>-->
								<a class="dropdown-item" href="logout.php"><i class="dw dw-logout"></i> Salir</a>';
							}else{
								print '
									<li><a href="login.php">ingresar</a></li>
									<li><a data-toggle="modal" href="#registerModal">registrate</a></li>';						
							}
						?>
					</div>
				</div>
			</div>
		</div>
	</div>

	
	<div class="left-side-bar">
		<div class="brand-logo">
			<a href="#">
				<img src="../v1/logov3.png" alt="" class="light-logo" width="60"> <span class="mtext"> IDentiKIT</span>
			</a>
			<div class="close-sidebar" data-toggle="left-sidebar-close">
				<i class="ion-close-round"></i>
			</div>
		</div>
		<div class="menu-block customscroll">
			<div class="sidebar-menu">
				<ul id="accordion-menu">
					<?php
					if ($myrole == "employee") {
						print '<a href="index.php" class="dropdown-toggle no-arrow ">
						<span class="micon icon-copy fa fa-home"></span><span class="mtext">Inicio</span></a>

						<li class="dropdown">
						<a href="javascript:;" class="dropdown-toggle">
							<span class="micon icon-copy fa fa-bandcamp"></span><span class="mtext">Explorar</span>
						</a>
						<ul class="submenu">
							<li><a href="identis.php" class="active"> IDentis</a></li>
							<li><a href="works.php"> Trabajos</a></li>
							<li><a href="empresas.php"> Empresas</a></li>
						</ul>
						</li>

							<li>
						<a href="aplicados.php" class="dropdown-toggle no-arrow">
							<span class="micon ion-android-done-all"></span><span class="mtext">Aplicados</span>
						</a>
					</li>';
					} if ($myrole == "employer") {
						print '
						<a href="index.php" class="dropdown-toggle no-arrow">
						<span class="micon icon-copy fa fa-home"></span><span class="mtext">Panel</span></a>
						<a href="../identis.php" class="dropdown-toggle no-arrow"><span class="micon icon-copy fa fa-bandcamp"></span><span class="mtext">identis</span></a>
						<li class="dropdown">
							<a href="javascript:;" class="dropdown-toggle">
								<span class="micon icon-copy fa fa-envelope"></span><span class="mtext">Buzon</span>
							</a>
							<ul class="submenu">';
								$contacts               = $db->query("select DISTINCT tbl_users.first_name,tbl_users.last_name, tbl_users.member_no from tbl_messages join tbl_users where message_sender_id = '".$senderId."' and message_receiver_id = tbl_users.member_no");
								$MensajesPostulados     = $db->query("select DISTINCT message_sender_id from tbl_messages where message_receiver_id = :receiverId", array("receiverId" => $_SESSION["myid"]));

								foreach($contacts as $contact){
									echo '<li><a href="../employer/Employerconversaciones.php?receiverId='.$contact['member_no'].'">' . $contact["first_name"] . " " . $contact["last_name"] . '</a></li>';
								}

						print '
							</ul>
						</li>';
					}

					else {
						print '<a href="login.php" class="dropdown-toggle no-arrow ">
						<span class="micon icon-copy fa fa-lock"></span><span class="mtext">Iniciar sion</span></a>

						<a href="registro.php" class="dropdown-toggle no-arrow ">
						<span class="micon icon-copy fa fa-user"></span><span class="mtext">Registrarme</span></a>';
					}
						?>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="mobile-menu-overlay"></div>
		<div class="main-container">
			<div class="pd-ltr-20">
				<h1>Historial de mensajes</h1>
				<br>
				<textarea rows="30" cols="220" name="comment" form="usrform" id="MensajeHistorial" disabled>
				<?php
					foreach($mensajes as $mensaje){
						$EmpresaNombre = $db->query("select first_name from tbl_users where member_no=:memberNo",array('memberNo'=>$mensaje['message_sender_id']));
						echo "\n";
						$titulo = $mensaje['message_subject'];
						$body   = $mensaje['message_body'];
						echo $EmpresaNombre[0]['first_name'] . ": ";
						echo $body . "\n";
					}
				?>
				</textarea>
			</div>
			<div class="pd-ltr-20">
				<br>
				<div class="col-sm-12 col-md-12">
					<div class="form-group bootstrap3-wysihtml5-wrapper">
						<h1>
                        	<strong>
                            	Responder
                            </strong>
						</h1>
						<br>
						<textarea class="textarea_editor form-control border-radius-0" name="mensaje" required placeholder="Mensaje a enviar..." id="mensaje"></textarea>												
					</div>
                    <center>
                        <button class="btn btn-primary" id="enviarMensaje">Enviar mensaje</button>
					</center>
					<br>
					<br>
				</div>
			</div>
		</div>
	</div>

	</div>
	</div>
	</div>
	</div>
	<!-- js -->
	
	<script src="../v1/vendors/scripts/core.js"></script>
	<script src="../v1/vendors/scripts/script.min.js"></script>
	<script src="../v1/vendors/scripts/process.js"></script>
	<script src="../v1/vendors/scripts/layout-settings.js"></script>
	<script src="../v1/src/plugins/datatables/js/jquery.dataTables.min.js"></script>
	<script src="../v1/src/plugins/datatables/js/dataTables.bootstrap4.min.js"></script>
	<script src="../v1/src/plugins/datatables/js/dataTables.responsive.min.js"></script>
	<script src="../v1/src/plugins/datatables/js/responsive.bootstrap4.min.js"></script>
	<script src="../v1/vendors/scripts/dashboard.js"></script>
	<script src="../v1vendors/scripts/core.js"></script>
	<script src="../v1vendors/scripts/script.min.js"></script>
	<script src="../v1vendors/scripts/process.js"></script>
	<script src="../v1vendors/scripts/layout-settings.js"></script>
	<script src="../v1/src/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
	<script src="../v1/src/plugins/switchery/switchery.min.js"></script>
	<!-- bootstrap-touchspin js -->
	<script src="../v1/src/plugins/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
	<script src="../v1/vendors/scripts/advanced-components.js"></script>
	<!-- add sweet alert js & css in footer -->
	<script src="../v1/src/plugins/sweetalert2/sweetalert2.all.js"></script>
	<script src="../v1/src/plugins/sweetalert2/sweet-alert.init.js"></script>

	<script type="text/javascript">
		var useDarkMode = window.matchMedia('(prefers-color-scheme: white)').matches;

		tinymce.init({
		  selector: 'textarea#full-featured-non-premium',
		  plugins: 'print preview paste searchreplace autolink autosave save directionality visualblocks visualchars fullscreen link charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textpattern noneditable help charmap emoticons',
		  imagetools_cors_hosts: ['picsum.photos'],
		  
		  toolbar_sticky: true,
		  autosave_ask_before_unload: true,
		  autosave_interval: '30s',
		  autosave_prefix: '{path}{query}-{id}-',
		  autosave_restore_when_empty: false,
		  autosave_retention: '2m',
		  image_advtab: true,
		  link_list: [
		    { title: 'My page 1', value: 'https://www.tiny.cloud' },
		    { title: 'My page 2', value: 'http://www.moxiecode.com' }
		  ],
		  image_list: [
		    { title: 'My page 1', value: 'https://www.tiny.cloud' },
		    { title: 'My page 2', value: 'http://www.moxiecode.com' }
		  ],
		  image_class_list: [
		    { title: 'None', value: '' },
		    { title: 'Some class', value: 'class-name' }
		  ],
		  importcss_append: true,
		  file_picker_callback: function (callback, value, meta) {
		    /* Provide file and text for the link dialog */
		    if (meta.filetype === 'file') {
		      callback('https://www.google.com/logos/google.jpg', { text: 'My text' });
		    }

		    /* Provide image and alt text for the image dialog */
		    if (meta.filetype === 'image') {
		      callback('https://www.google.com/logos/google.jpg', { alt: 'My alt text' });
		    }

		    /* Provide alternative source and posted for the media dialog */
		    if (meta.filetype === 'media') {
		      callback('movie.mp4', { source2: 'alt.ogg', poster: 'https://www.google.com/logos/google.jpg' });
		    }
		  },
		  templates: [
		        { title: 'New Table', description: 'creates a new table', content: '<div class="mceTmpl"><table width="98%%"  border="0" cellspacing="0" cellpadding="0"><tr><th scope="col"> </th><th scope="col"> </th></tr><tr><td> </td><td> </td></tr></table></div>' },
		    { title: 'Starting my story', description: 'A cure for writers block', content: 'Once upon a time...' },
		    { title: 'New list with dates', description: 'New List with dates', content: '<div class="mceTmpl"><span class="cdate">cdate</span><br /><span class="mdate">mdate</span><h2>My List</h2><ul><li></li><li></li></ul></div>' }
		  ],
		  template_cdate_format: '[Date Created (CDATE): %m/%d/%Y : %H:%M:%S]',
		  template_mdate_format: '[Date Modified (MDATE): %m/%d/%Y : %H:%M:%S]',
		  height: 600,
		  image_caption: true,
		  quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
		  noneditable_noneditable_class: 'mceNonEditable',
		  toolbar_mode: 'sliding',
		  contextmenu: 'link image imagetools table',
		  skin: useDarkMode ? 'oxide-dark' : 'oxide',
		  content_css: useDarkMode ? 'dark' : 'default',
		  content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
		 });
	</script>
	
	   <!-- The Modal -->
	<div class="modal" id="myModal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <!-- Modal body -->
                <div class="modal-body">
					<center>				
                    	Mensaje enviado correctamente
					</center>
                </div>
            </div>
        </div>
    </div>

</body>

</html>
