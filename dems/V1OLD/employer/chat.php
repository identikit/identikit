<!DOCTYPE html>
<html>

<head>
	<!-- Basic Page Info -->
	<meta charset="utf-8">
	<title>IDentiKIT - Chats</title>

	<link rel="apple-touch-icon" sizes="180x180" href="logov3.png">
	<link rel="icon" type="image/png" sizes="32x32" href="logov3.png">
	<link rel="icon" type="image/png" sizes="16x16" href="logov3.png">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="../v1/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="../v1/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="../v1/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../v1/src/plugins/datatables/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../v1/vendors/styles/style.css">

	<meta property="og:image" content="https://identikit.app/logowebog.png" />
	<meta property="og:image:secure_url" content="https://identikit.app/logowebog.png" />
	<meta property="og:image:type" content="image/png" />
	<meta property="og:image:width" content="300" />
	<meta property="og:image:height" content="300" />
	<meta property="og:image:alt" content="IDentiKIT - Tu primera experiencia laboral" />
	<meta property="og:description" content="😎 Encuentra el mejor talento tech joven en IDentiKIT a un click de distancia, fácil, rápido y seguro." />

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-K2NHRZT4R7"></script>

	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'G-K2NHRZT4R7');
	</script>

	<!-- Google Tag Manager -->
	<script>
		(function(w, d, s, l, i) {
			w[l] = w[l] || [];
			w[l].push({
				'gtm.start': new Date().getTime(),
				event: 'gtm.js'
			});
			var f = d.getElementsByTagName(s)[0],
				j = d.createElement(s),
				dl = l != 'dataLayer' ? '&l=' + l : '';
			j.async = true;
			j.src =
				'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
			f.parentNode.insertBefore(j, f);
		})(window, document, 'script', 'dataLayer', 'GTM-5QGSH5J');
	</script>
	<!-- End Google Tag Manager -->

	<?php
		include '../constants/settings.php';
		include 'constants/check-login.php';

		require_once("../config/Db.php");
		$db = new DbPDO();

		if (isset($_GET['receiverId'])) {
			$senderId          = $_SESSION['myid'];
			$receiverId        = $_GET['receiverId'];
			$mensajes          = $db->query("SELECT * FROM tbl_messages a WHERE (a.message_sender_id = '" . $senderId . "' AND a.message_receiver_id = '" . $receiverId . "') OR (a.message_sender_id = '" . $receiverId . "' AND a.message_receiver_id = '" . $senderId . "') 
				order by message_creation_date, message_creation_hour ASC");
		}

		$empresaId              = $_SESSION['myid'];
	?>
</head>

<body>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QGSH5J" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<?php include 'layouts/Header.php'; ?>
	<?php include 'layouts/Sidebar-menu.php'; ?>

	<div class="mobile-menu-overlay"></div>

	<div class="main-container">
		<div class="pd-ltr-20 xs-pd-20-10">
			<div class="min-height-200px">
				
				<div class="bg-white border-radius-4 box-shadow mb-30">
					<div class="row no-gutters">
						<div class="col-lg-3 col-md-4 col-sm-12">
							<div class="chat-list bg-light-gray">
								
								<div class="notification-list chat-notification-list customscroll">
									<ul>
										<?php

										$MensajesPostulados     = $db->query("select DISTINCT message_receiver_id from tbl_messages where message_sender_id = :empresaId", array("empresaId" => $empresaId));

										foreach ($MensajesPostulados as $Postulado) {
											$contacts = $db->query("select DISTINCT tbl_users.first_name,tbl_users.last_name,tbl_users.member_no from tbl_messages join tbl_users where message_receiver_id = '" . $Postulado['message_receiver_id'] . "' and message_receiver_id = tbl_users.member_no");
											foreach ($contacts as $contact) {

												$avatar          = $db->query("select avatar from tbl_users where member_no=:memberNo", array('memberNo' => $contact['member_no']));


												if ($avatar[0]['avatar'] == null) {
													$Img = '<img  src="../images/default.png" title="" alt="image"  /></center>';
												} else {
													$Img = '<img src="data:image/jpeg;base64,' . base64_encode($avatar[0]['avatar']) . '" alt="">';
												}

												echo '
														<li>
														 	<a href="chat.php?receiverId=' . $contact["member_no"] . '">
																' . $Img . '
																<h3 class="clearfix">' . $contact["first_name"] . " " . $contact["last_name"] . '</h3>
																<p><i class="fa fa-circle text-light-green"></i> online</p>
															</a>
														</li>';
											}
										}
										?>
										<!-- <li>
											<a href="#">
												<img src="v1/vendors/images/img.jpg" alt="">
												<h3 class="clearfix">John Doe</h3>
												<p><i class="fa fa-circle text-warning"></i> active 5 min</p>
											</a>
										</li>
										<li>
											<a href="#">
												<img src="v1/vendors/images/img.jpg" alt="">
												<h3 class="clearfix">John Doe</h3>
												<p><i class="fa fa-circle text-light-orange"></i> offline</p>
											</a>
										</li> -->
									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-9 col-md-8 col-sm-12">
							<div class="chat-detail">
								<div class="chat-profile-header clearfix">
									<div class="left">
										<div class="clearfix">
											<?php
                                                if(isset($_GET['receiverId'])){
                                                    $memberNo = $_GET['receiverId'];
                                                    $DetailsUser = $db->query("select first_name,last_name,avatar from tbl_users where member_no=:memberNo",array("memberNo"=>$memberNo));
                                                    $logo = $DetailsUser[0]["avatar"];
                                                    
                                                    if ($logo == null){
                                                        $imagePath = '<center><img  src="../images/default.png" title="' . $compname . '" alt="image"  /></center>';
                                                    } else {
                                                        $imagePath = '<img src="data:image/jpeg;base64,' . base64_encode($logo) . '" alt="">';
                                                    }
    
                                                    echo '
                                                        <div class="chat-profile-photo">
                                                            '.$imagePath.'
                                                        </div>
    
                                                        <div class="chat-profile-name">
                                                            <h3>' . $DetailsUser[0]["first_name"] . " " . $DetailsUser[0]["last_name"] . '</h3>
                                                        </div>';
                                                } 
											?>
										</div>
									</div>
								</div>
								<div class="chat-box">
									<div class="chat-desc customscroll">
										<ul>
											<?php
												$ShowChatFooter = 'block';
                                                if(isset($mensajes)){
                                                    foreach ($mensajes as $mensaje) {
                                                        $EmpresaNombre   = $db->query("select avatar from tbl_users where member_no=:memberNo", array('memberNo' => $mensaje['message_sender_id']));
                                                        $adminStatus     = '';
                                                        $Img             = '<img src="data:image/jpeg;base64,' . base64_encode($EmpresaNombre[0]['avatar']) . '" alt="">';

                                                        if ($EmpresaNombre[0]['avatar'] == null) {
                                                            $Img = '<img  src="../images/default.png" title="" alt="image"  /></center>';
                                                        }

                                                        if ($mensaje['message_sender_id'] == $_SESSION['myid']) {
                                                            $adminStatus = 'admin_chat';
                                                        }

                                                        echo '
                                                                    <li class="clearfix ' . $adminStatus . '">
                                                                        <span class="chat-img">
                                                                            ' . $Img . '
                                                                        </span>
                                                                        <div class="chat-body clearfix">
                                                                            <p>' . $mensaje['message_body'] . '</p>
                                                                            <div class="chat_time">' . $mensaje['message_creation_date'] . '</div>
                                                                            <div class="chat_time">' . substr($mensaje['message_creation_hour'], 0, 8) . '</div>
                                                                        </div>
                                                                    </li>';
                                                    }
                                                } else {
													$ShowChatFooter = 'none';
												}
											?>
										</ul>
									</div>
									<div class="chat-footer" style="display: <?= $ShowChatFooter ?>">
										<div class="file-upload"><!--<a href="#"><i class="fa fa-paperclip"></i></a>--></div>
										<div class="chat_text_area">
											<textarea placeholder="Escribe tu mensaje..." id="mensaje"></textarea>
										</div>
										<script>
											function mensaje() {
												var mensaje = $('#mensaje').val();
												var receiverId = '<?= $receiverId; ?>';
												var senderId = '<?= $senderId; ?>';

												$.ajax({
													type: 'POST',
													url: 'app/agregarconversacion.php',
													dataType: "json",
													data: {
														'mensaje': mensaje,
														'receiverId': receiverId,
														'senderId': senderId
													},
													success: function(response) {
														if (response.status == "correcto") {
															location.reload();
														} else {
															alert('ERROR');
														}
													}
												});
											}
										</script>
										<div class="chat_send">
											<button class="btn btn-link" onclick="mensaje()"><i class="icon-copy ion-paper-airplane"></i></button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- js -->
	



	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="../v1/vendors/scripts/core.js"></script>
	<script src="../v1/vendors/scripts/script.min.js"></script>
	<script src="../v1/vendors/scripts/process.js"></script>
	<script src="../v1/vendors/scripts/layout-settings.js"></script>
	<script>
		$(document).ready(function() {
			var $text = $('#mCSB_dragger');
			$text.scrollTop($text[0].scrollHeight);
		});
	</script>
</body>

</html>