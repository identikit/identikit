<?php 
include '../constants/settings.php'; 
include 'constants/check-login.php';


require_once("../config/Db.php");
$db = new DbPDO();

$empresaId              = $_SESSION['myid'];



if ($user_online == "true") {
if ($myrole == "employer") {
}else{
header("location:../");		
}
}else{
header("location:../");	
}


?>

<!DOCTYPE html>
<html>

<head>
	<!-- Basic Page Info -->
	<meta charset="utf-8">
	<title>IDentiKIT - Crear un aviso</title>

	<link rel="apple-touch-icon" sizes="180x180" href="logov3.png">
	<link rel="icon" type="image/png" sizes="32x32" href="logov3.png">
	<link rel="icon" type="image/png" sizes="16x16" href="logov3.png">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="../v1/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="../v1/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="../v1/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../v1/src/plugins/datatables/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../v1/vendors/styles/style.css">
	<link rel="stylesheet" type="text/css" href="../v1/src/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
	<script src="https://cdn.tiny.cloud/1/0yb7elgidejk329quamk21ozwpex0990qg48k8b1n22eyja8/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
	
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-PHBQLSG');</script>
	<!-- End Google Tag Manager -->



</head>
<body>

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PHBQLSG"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->


	<?php include 'layouts/Header.php';?>
	<?php include 'layouts/Sidebar-menu.php';?>

	<div class="mobile-menu-overlay"></div>

    <div class="main-container">
        <div class="pd-ltr-20 xs-pd-20">
            <div class="min-height-200px">
                <div class="pd-ltr-20">
				<div class="card-box pd-20 height-100-p mb-30">
						<div class="section-title-02">
																	<?php require 'constants/check_reply.php'; ?>

											<h3 class="text-left">Publicar</h3>
										</div>

										<form class="post-form-wrapper" action="app/post-job.php" method="POST" autocomplete="off">
								
											<div class="row gap-20">
										
												<div class="col-sm-8 col-md-8">
												
													<div class="form-group">
														<label>Puesto/Titulo del aviso</label>
														<input name="title" required type="text" class="form-control" >
													</div>
													
												</div>
												
												<div class="clear"></div>
												
												<div class="col-sm-4 col-md-4">
												
													<div class="form-group">
														<label>Ciudad</label>
														<input name="city" required type="text" class="form-control" >
													</div>
													
												</div>
												
												<div class="col-sm-4 col-md-4">
												
													<div class="form-group">
														<label>Pais</label>
														<select name="country" required class="selectpicker show-tick form-control" data-live-search="true">
															<option disabled value="">Seleccionar</option>
						                                   <?php
														   require '../constants/db_config.php';
														   try {
                                                           $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
                                                           $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	
                                                           $stmt = $conn->prepare("SELECT * FROM tbl_countries ORDER BY country_name");
                                                           $stmt->execute();
                                                           $result = $stmt->fetchAll();
  
                                                           foreach($result as $row)
                                                           {
		                                                    ?> <option <?php if ($country == $row['country_name']) { print ' selected '; } ?> value="<?php echo $row['country_name']; ?>"><?php echo $row['country_name']; ?></option> <?php
	 
	                                                        }

					  
	                                                       }catch(PDOException $e)
                                                           {

                                                           }
	
														   ?>
														</select>
													</div>
													
												</div>
												
												<div class="clear"></div>
												
											<div class="col-sm-4 col-md-4">
												
													<div class="form-group">
														<label>Categoria</label>
															<select name="category" required class="selectpicker show-tick form-control" data-live-search="true">
															<option disabled value="">Seleccionar</option>
						                                   <?php
														   require '../constants/db_config.php';
														   try {
                                                           $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
                                                           $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	
                                                           $stmt = $conn->prepare("SELECT * FROM tbl_categories ORDER BY category");
                                                           $stmt->execute();
                                                           $result = $stmt->fetchAll();
  
                                                           foreach($result as $row)
                                                           {
		                                                    ?> <option value="<?php echo $row['category']; ?>"><?php echo $row['category']; ?></option> <?php
	 
	                                                        }

					  
	                                                       }catch(PDOException $e)
                                                           {

                                                           }
	
														   ?>
														</select>
											
														
													</div>
													
												</div>
											    <div class="col-sm-4 col-md-4">
												
													<div class="form-group">
														<label>Tiempo de postulacion</label>
														<input name="deadline" required type="date" class="form-control" placeholder="EJ: 30-12-2031">
													</div>
													
												</div>
												
												<div class="clear"></div>
												
												<div class="col-xss-12 col-xs-6 col-sm-6 col-md-4">
												
													<div class="form-group mb-20">
														<label>Tipo de empleo</label>
														<select name="jobtype" required class="selectpicker show-tick form-control" data-live-search="false" data-selected-text-format="count > 3" data-done-button="true" data-done-button-text="OK" data-none-selected-text="All">
															<option value="" selected>Seleccionar</option>
															<option value="Primer empleo" data-content="<span class='label label-danger'>Primer empleo</span>">Primer empleo</option>
															<option value="Pasantia" data-content="<span class='label label-danger'>Pasantia</span>">Pasantia</option>
															<!--<option value="Full time" data-content="<span class='label label-warning'>Full-time</span>">Full-time</option>
															<option value="Part time" data-content="<span class='label label-danger'>Part-time</span>">Part-time</option>
															<option value="Temporario" data-content="<span class='label label-danger'>Temporario</span>">Temporario</option>
															<option value="Por contrato" data-content="<span class='label label-danger'>Por contrato</span>">Por contrato</option>-->
															<option value="Voluntario" data-content="<span class='label label-danger'>Voluntario</span>">Voluntario</option>
															<!--<option value="Por horas" data-content="<span class='label label-danger'>Por horas</span>">Por horas</option>
															<option value="Fines de semana" data-content="<span class='label label-danger'>Fines de semana</span>">Fines de semana</option>-->
															<option value="Freelance" data-content="<span class='label label-success'>Freelance</span>">Freelance</option>
														</select>
													</div>
													
												</div>
										
												<div class="col-xss-12 col-xs-6 col-sm-6 col-md-4">
												
													<div class="form-group mb-20">
														<label>Modalidad</label>
														<select name="experience" required class="selectpicker show-tick form-control" data-live-search="false" data-selected-text-format="count > 3" data-done-button="true" data-done-button-text="OK" data-none-selected-text="All">
															<option value="" selected >Seleccionar</option>
															<option value="Presencial">Presencial</option>
															<option value="Desde casa">Desde casa</option>
															<option value="Presencial y desde casa">Presencial y desde casa</option>
															
														</select>
													</div>
													
													
												</div>

												<div class="col-xss-12 col-xs-6 col-sm-6 col-md-4">
												
													<div class="form-group mb-20">
														<label>Tecnologias</label>
														<input type="text"class="form-control" data-role="tagsinput" name="tech" placeholder="Presiona enter para agregar">
													</div>
													
													
												</div>

												

												<div class="col-sm-12 col-md-12">
												
													<div class="form-group bootstrap3-wysihtml5-wrapper">
														<label>Descripcion</label>
														<textarea class="textarea_editor form-control border-radius-0" name="description" required placeholder="Ingresa descripcion, requerimientos y responsabilidades"></textarea>												
													</div>
													
												</div>
												
												<div class="clear"></div>
												
												<!--<div class="col-sm-12 col-md-12">
												
													<div class="form-group bootstrap3-wysihtml5-wrapper">
														<label>Responsabilidades</label>
														<textarea name="responsiblities" required class="form-control bootstrap3-wysihtml5" placeholder="" style="height: 200px;"></textarea>
													</div>
													
												</div>-->
												
												<div class="clear"></div>
												
											<!--	<div class="col-sm-12 col-md-12">
												
													<div class="form-group bootstrap3-wysihtml5-wrapper">
														<label>Requisitos</label>
														<textarea name="requirements" required class="form-control bootstrap3-wysihtml5" placeholder="" style="height: 200px;"></textarea>
													</div>
													
												</div> -->
												
												<div class="clear"></div>
												

												
												<div class="clear"></div>
												

												
												<div class="clear mb-10"></div>

												
												<div class="clear mb-15"></div>

												
												<div class="clear"></div>
												
												<div class="col-sm-12 mt-30">
												
												<?php 
												  require '../constants/db_config.php';
												  try {
												  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
												  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                                                  $TipoCuenta = $_SESSION['TCuenta'];

												  $stmt = $conn->prepare("SELECT COUNT(*) FROM tbl_jobs where company = :company");
												  $stmt->bindParam(':company',  $_SESSION['myid']);
												  $stmt->execute();
												  $result = $stmt->fetchAll();
												  $rec = $result[0][0]; 
                                                  $num = 0;
												  switch($TipoCuenta){
                                                    case 1:
                                                        $num = 3;
													    break;
													case 2:
														$num = 10;
														break;
													case 3:
														$num = $rec;
														break;
												  }
                                                  if( $rec <= $num){
                                                      echo ' 
												        <button type="submit"  onclick = "validate(this)" class="btn btn-primary btn-lg col-md-12">Publicar</button>
													  ';
												  } else {
													  echo ' 
													    <button class="btn btn-primary btn-lg col-md-12" disabled>Actualizar cuenta</button>
													  ';
												  }	
												}catch(PDOException $e)
												{

												}
								 
											    ?>
												
												</div>

											</div>
											
										</form>
					</div>
				<div class="row">
						<div class="card-box height-100-p overflow-hidden col-md-12">
				
						
							


						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
  <!-- js -->
	<script src="../v1/vendors/scripts/core.js"></script>
	<script src="../v1/vendors/scripts/script.min.js"></script>
	<script src="../v1/vendors/scripts/process.js"></script>
	<script src="../v1/vendors/scripts/layout-settings.js"></script>
	<script src="../v1/src/plugins/apexcharts/apexcharts.min.js"></script>
	<script src="../v1/src/plugins/datatables/js/jquery.dataTables.min.js"></script>
	<script src="../v1/src/plugins/datatables/js/dataTables.bootstrap4.min.js"></script>
	<script src="../v1/src/plugins/datatables/js/dataTables.responsive.min.js"></script>
	<script src="../v1/src/plugins/datatables/js/responsive.bootstrap4.min.js"></script>
	<script src="../v1/vendors/scripts/dashboard.js"></script>
	<script src="../v1/src/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

	<script type="text/javascript">
		
		var useDarkMode = window.matchMedia('(prefers-color-scheme: white)').matches;

		tinymce.init({
		  selector: 'textarea#full-featured-non-premium',
		  plugins: 'print preview paste searchreplace autolink autosave save directionality visualblocks visualchars fullscreen link charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textpattern noneditable help charmap emoticons',
		  imagetools_cors_hosts: ['picsum.photos'],
		  
		  toolbar_sticky: true,
		  autosave_ask_before_unload: true,
		  autosave_interval: '30s',
		  autosave_prefix: '{path}{query}-{id}-',
		  autosave_restore_when_empty: false,
		  autosave_retention: '2m',
		  image_advtab: true,
		  link_list: [
		    { title: 'My page 1', value: 'https://www.tiny.cloud' },
		    { title: 'My page 2', value: 'http://www.moxiecode.com' }
		  ],
		  image_list: [
		    { title: 'My page 1', value: 'https://www.tiny.cloud' },
		    { title: 'My page 2', value: 'http://www.moxiecode.com' }
		  ],
		  image_class_list: [
		    { title: 'None', value: '' },
		    { title: 'Some class', value: 'class-name' }
		  ],
		  importcss_append: true,
		  file_picker_callback: function (callback, value, meta) {
		    /* Provide file and text for the link dialog */
		    if (meta.filetype === 'file') {
		      callback('https://www.google.com/logos/google.jpg', { text: 'My text' });
		    }

		    /* Provide image and alt text for the image dialog */
		    if (meta.filetype === 'image') {
		      callback('https://www.google.com/logos/google.jpg', { alt: 'My alt text' });
		    }

		    /* Provide alternative source and posted for the media dialog */
		    if (meta.filetype === 'media') {
		      callback('movie.mp4', { source2: 'alt.ogg', poster: 'https://www.google.com/logos/google.jpg' });
		    }
		  },
		  templates: [
		        { title: 'New Table', description: 'creates a new table', content: '<div class="mceTmpl"><table width="98%%"  border="0" cellspacing="0" cellpadding="0"><tr><th scope="col"> </th><th scope="col"> </th></tr><tr><td> </td><td> </td></tr></table></div>' },
		    { title: 'Starting my story', description: 'A cure for writers block', content: 'Once upon a time...' },
		    { title: 'New list with dates', description: 'New List with dates', content: '<div class="mceTmpl"><span class="cdate">cdate</span><br /><span class="mdate">mdate</span><h2>My List</h2><ul><li></li><li></li></ul></div>' }
		  ],
		  template_cdate_format: '[Date Created (CDATE): %m/%d/%Y : %H:%M:%S]',
		  template_mdate_format: '[Date Modified (MDATE): %m/%d/%Y : %H:%M:%S]',
		  height: 600,
		  image_caption: true,
		  quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
		  noneditable_noneditable_class: 'mceNonEditable',
		  toolbar_mode: 'sliding',
		  contextmenu: 'link image imagetools table',
		  skin: useDarkMode ? 'oxide-dark' : 'oxide',
		  content_css: useDarkMode ? 'dark' : 'default',
		  content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
		 });



	</script>


</html>