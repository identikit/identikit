<?php
include '../constants/settings.php';
include 'constants/check-login.php';

if ($user_online == "true") {
	if ($myrole == "employer") {
	} else {
		header("location:../");
	}
} else {
	header("location:../");
}
?>

<!DOCTYPE html>
<html>

<head>
	<!-- Basic Page Info -->
	<meta charset="utf-8">
	<title>IDentiKIT - Mi perfil de empresa</title>

	<link rel="apple-touch-icon" sizes="180x180" href="logov3.png">
	<link rel="icon" type="image/png" sizes="32x32" href="logov3.png">
	<link rel="icon" type="image/png" sizes="16x16" href="logov3.png">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="../v1/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="../v1/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="../v1/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../v1/src/plugins/datatables/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../v1/vendors/styles/style.css">


	<!-- Google Tag Manager -->
	<script>
		(function(w, d, s, l, i) {
			w[l] = w[l] || [];
			w[l].push({
				'gtm.start': new Date().getTime(),
				event: 'gtm.js'
			});
			var f = d.getElementsByTagName(s)[0],
				j = d.createElement(s),
				dl = l != 'dataLayer' ? '&l=' + l : '';
			j.async = true;
			j.src =
				'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
			f.parentNode.insertBefore(j, f);
		})(window, document, 'script', 'dataLayer', 'GTM-PHBQLSG');
	</script>
	<!-- End Google Tag Manager -->


	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

	<script type="text/javascript">
		$(window).load(function() {
			$(function() {
				$('#file-input').change(function(e) {
					addImage(e);
				});

				function addImage(e) {
					var file = e.target.files[0],
						imageType = /image.*/;

					if (!file.type.match(imageType))
						return;

					var reader = new FileReader();
					reader.onload = fileOnload;
					reader.readAsDataURL(file);
				}

				function fileOnload(e) {
					var result = e.target.result;
					$('#imgSalida').attr("src", result);
				}
			});
		});
	</script>

	<script>
		function capturar() {
			var resultado = "";

			var porNombre = document.getElementsByName("filter");
			for (var i = 0; i < porNombre.length; i++) {
				if (porNombre[i].checked)
					resultado = porNombre[i].value;
			}

			var elemento = document.getElementById("resultado");
			if (elemento.className == "") {
				elemento.className = resultado;
				elemento.width = "600";
			} else {
				elemento.className = resultado;
				elemento.width = "600";
			}
		}
	</script>
	<style>
		#fotop {
			display: block;
			margin: 0 auto 20px;
			width: 150px;
			height: 150px;
			-webkit-box-shadow: 0 0 10px rgba(0, 0, 0, .3);
			box-shadow: 0 0 10px rgba(0, 0, 0, .3);
			border-radius: 100%;
			overflow: hidden;
		}

		.fotop {
			display: block;
			margin: 0 auto 20px;
			width: 150px;
			height: 150px;
			-webkit-box-shadow: 0 0 10px rgba(0, 0, 0, .3);
			box-shadow: 0 0 10px rgba(0, 0, 0, .3);
			border-radius: 100%;
			overflow: hidden;
		}
	</style>


</head>

<body>

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PHBQLSG" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<div class="header">
		<div class="header-left">
			<div class="menu-icon icon-copy ti-menu" style="color: white;"></div>
			<div class="search-toggle-icon dw dw-search2" style="color: #ffde00;" data-toggle="header_search"></div>
			<div class="header-search">
				<form method="post" action="../busqueda.php">
					<div class="form-group mb-0">
						<i class="dw dw-search2 search-icon" style="color: #ffde00;"></i>
						<input type="text" class="form-control search-input" placeholder="Buscar en IDentiKIT" required name="PalabraClave">
						<input name="buscar" type="hidden" class="form-control mb-2" id="inlineFormInput" value="v">
					</div>
				</form>
			</div>
		</div>
		<div class="header-right">
			<div class="user-info-dropdown">
				<div class="dropdown">
					<a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" style="color: #ffde00;">
						<span class="user-icon">
							<?php
							if ($logo == null) {
								print '<img  src="../images/default.png" title="' . $compname . '" alt="image"  /></center>';
							} else {
								echo '<center><img alt="image" title="' . $compname . '" width="180" height="100" src="data:image/jpeg;base64,' . base64_encode($logo) . '"/></center>';
							}
							?>

						</span>
						<!--<span class="user-name" style="color: white;"><?php //echo "$myfname"; 
																			?></span>-->
					</a>
					<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
						<?php
						if ($user_online == true) {
							print '
								<a class="dropdown-item" href="#"><i class="dw dw-user1"></i> Perfil</a>
								<!--<a class="dropdown-item" href="#"><i class="dw dw-settings2"></i> Ajustes</a>
								<a class="dropdown-item" href="#"><i class="dw dw-help"></i> Ayuda</a>-->
								<a class="dropdown-item" href="logout.php"><i class="dw dw-logout"></i> Salir</a>';
						} else {
							print '
								<li><a href="login.php">ingresar</a></li>
								<li><a data-toggle="modal" href="#registerModal">registrate</a></li>';
						}
						?>
					</div>
				</div>
			</div>

		</div>
	</div>



	<div class="left-side-bar">
		<div class="brand-logo">
			<a href="#">
				<img src="../v1/logov3.png" alt="" class="light-logo" width="60"> <span class="mtext"> IDentiKIT</span>
			</a>
			<div class="close-sidebar" data-toggle="left-sidebar-close">
				<i class="ion-close-round"></i>
			</div>
		</div>
		<div class="menu-block customscroll">
			<div class="sidebar-menu">
				<ul id="accordion-menu">
					<?php
					if ($myrole == "employee") {
						print '<a href="index.php" class="dropdown-toggle no-arrow ">
							<span class="micon icon-copy fa fa-home"></span><span class="mtext">Inicio</span></a>

							<li class="dropdown">
							<a href="javascript:;" class="dropdown-toggle">
								<span class="micon icon-copy fa fa-bandcamp"></span><span class="mtext">Explorar</span>
							</a>
							<ul class="submenu">
								<li><a href="identis.php" class="active"> IDentis</a></li>
								<li><a href="works.php"> Trabajos</a></li>
								<li><a href="empresas.php"> Empresas</a></li>
							</ul>
							</li>

								<li>
							<a href="aplicados.php" class="dropdown-toggle no-arrow">
								<span class="micon ion-android-done-all"></span><span class="mtext">Aplicados</span>

							</a>
						</li>';
					} else {
						print '<a href="index.php" class="dropdown-toggle no-arrow ">
							<span class="micon icon-copy fa fa-home"></span><span class="mtext">Panel</span></a>
							<a href="../identis.php" class="dropdown-toggle no-arrow"><span class="micon icon-copy fa fa-bandcamp"></span><span class="mtext">identis</span></a>';
					}
					?>
				</ul>
			</div>
		</div>
	</div>
	<div class="mobile-menu-overlay"></div>
	<div class="main-container">
		<div class="pd-ltr-20 xs-pd-20">
			<div class="min-height-200px">
				<div class="row">
					<div class="card-box height-100-p overflow-hidden col-md-12">
						<div class="pd-20 card-box">
							<h5 class="h4  mb-20">Mi perfil</h5>
							<div class="tab">
								<div class="tab-content">
									<div class="tab-pane fade show active" id="home" role="tabpanel">
										<div class="pd-20"><?php require 'constants/check_reply.php'; ?>
											<div class="profile-photo">
												<a href="modal" data-toggle="modal" data-target="#modal" class="edit-avatar"><i class="fa fa-pencil"></i></a>

												<?php
												if ($logo == null) {
													print '<center><img alt="image" id="fotop" width="180" height="100" src="../logov3.png"/></center>';
												} else {
													echo '<center><img alt="image" id="fotop" title="' . $compname . '" width="180" height="180" src="data:image/jpeg;base64,' . base64_encode($logo) . '"/></center>';
												}
												?>

												<?php
												if ($logo) {
													print '<a class="btn btn-primary col-md-12 col-lg-12" href="postjob.php">Publicar aviso </a>';
												}
												if (empty($logo)) {

													print '<a class="btn btn-primary disabled col-md-12 col-lg-12" href="postjob.php">Completa tu perfil  </a>';
												}
												?>
												<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
													<div class="modal-dialog modal-dialog-centered" role="document">
														<div class="modal-content">
															<div class="modal-body pd-5">
																<div class="img-container">
																	<?php
																	if ($logo == null) {
																		print '<center>Tu logo aqui</center>';
																	} else {
																		echo '<center><br><h6>Tu imagen actual</h6><br><img alt="image" id="fotop" title="' . $compname . '" width="180" height="180" src="data:image/jpeg;base64,' . base64_encode($logo) . '"/></center> ';
																	}
																	?>
																</div>
															</div>

															<form action="app/new-dp.php" method="POST" enctype="multipart/form-data">
																<center>
																	<div>
																		<div id="resultado"><img id="imgSalida" style="width: 200px; height: 200px;" /></div>
																	</div>

																	<div class="form-group">
																		<center><label>Actualizar imagen</label></center>
																		<label for="file-input">
																			<img src="../images/mas.png" title="Seleccionar imagen" style="width: 90px; height: 90px">
																		</label>
																		<input accept="image/*" type="file" name="image" id="file-input" required hidden="" />
																	</div>
																	<div class="modal-footer">
																		<input type="submit" value="Actualizar" class="btn btn-success">
																		<button type="button" class="btn btn-default" data-dismiss="modal">cerrar</button>
																	</div>
																</center>
															</form>
														</div>
													</div>
												</div>
											</div>

											<br><br><br>

											<form class="post-form-wrapper" action="app/update-profile.php" method="POST" autocomplete="off">
												<ul class="profile-edit-list row">
													<li class="weight-500 col-md-12">
														<h4 class=" h5 mb-20">Informacion</h4>
														<div class="form-group">
															<label>Nombre de Empresa/negocio</label>
															<input name="company" placeholder="Nombre de la empresa" type="text" class="form-control" value="<?php echo "$compname"; ?>" required>
														</div>
														<div class="form-group">
															<label>Rubro</label>
															<input class="form-control" placeholder="Ej. Webs, software factory" name="type" required type="text" value="<?php echo "$mytitle"; ?>" required>
														</div>
														<div class="form-group">
															<label>CUIT</label>
															<input class="form-control" placeholder="" name="cuit" required type="text" required>
														</div>
														<div class="form-group">
															<label>Fundada en</label>
															<input name="year" placeholder="Ingrese año" type="text" class="form-control" value="<?php echo "$esta"; ?>" required>
														</div>

														<div class="form-group">
															<label>Personas</label>
															<select name="people" required class="selectpicker show-tick form-control mb-15" data-live-search="false">
																<option <?php if ($mypeople == "1-10") {
																			print ' selected ';
																		} ?> value="1-10">1-10</option>
																<option <?php if ($mypeople == "11-100") {
																			print ' selected ';
																		} ?> value="11-100">11-100</option>
																<option <?php if ($mypeople == "200+") {
																			print ' selected ';
																		} ?> value="200+">200+</option>
																<option <?php if ($mypeople == "300+") {
																			print ' selected ';
																		} ?> value="300+">300+</option>
																<option <?php if ($mypeople == "1000+") {
																			print ' selected ';
																		} ?>value="1000+">1000+ </option>
															</select>

														</div>

														<div class="form-group">
															<label>País</label>
															<select name="country" required class="selectpicker show-tick form-control" data-live-search="true">
																<option disabled value="">Seleccionar</option>
																<?php
																require '../constants/db_config.php';
																try {
																	$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
																	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


																	$stmt = $conn->prepare("SELECT * FROM tbl_countries ORDER BY country_name");
																	$stmt->execute();
																	$result = $stmt->fetchAll();

																	foreach ($result as $row) {
																?> <option <?php if ($country == $row['country_name']) {
																			print ' selected ';
																		} ?> value="<?php echo $row['country_name']; ?>"><?php echo $row['country_name']; ?></option> <?php

																																																									}
																																																								} catch (PDOException $e) {
																																																								}

																																																										?>
															</select>
														</div>

														<div class="form-group">
															<label>Ciudad/Pueblo</label>
															<input name="city" required type="text" class="form-control" value="<?php echo "$city"; ?>" placeholder="Ingresa tu ciudad">
														</div>

														<div class="form-group">
															<label>Calle</label>
															<input name="street" required type="text" class="form-control" value="<?php echo "$street"; ?>" placeholder="Ingresa tu calle">
														</div>

														<div class="form-group">
															<label>Código Postal</label>
															<input name="zip" required type="text" class="form-control" value="<?php echo "$zip"; ?>" placeholder="Ingresa tu Codigo Postal">
														</div>



														<div class="form-group">
															<label>Teléfono</label>
															<input type="text" name="phone" required class="form-control" value="<?php echo "$myphone"; ?>" placeholder="Ingresa tu teléfono">
														</div>

														<div class="form-group">
															<label>Correo Electrónico</label>
															<input type="email" name="email" required class="form-control" value="<?php echo "$mymail"; ?>" placeholder="Ingresa tu email">
														</div>

														<div class="form-group">
															<label>Página Web</label>
															<input type="text" class="form-control" value="<?php echo "$myweb"; ?>" name="web" placeholder="Ingresa tu web">
														</div>

														<div class="form-group bootstrap3-wysihtml5-wrapper">
															<label>Historia/Logros/Experiencia</label>
															<textarea name="background" class="bootstrap3-wysihtml5 form-control" placeholder="Ingresa Historia/Logros/Experiencia de la empresa..." style="height: 200px;"><?php echo "$desc"; ?></textarea>
														</div>

														<div class="form-group bootstrap3-wysihtml5-wrapper">
															<label>Productos/Servicios</label>
															<textarea name="services" class="bootstrap3-wysihtml5 form-control" placeholder="Ingresa productos/servicios de la empresa ..." style="height: 200px;"><?php echo "$myserv"; ?></textarea>
														</div>


												</ul>
												<div class="form-group mb-0">
													<button type="submit" class="btn btn-primary col-md-12">Actualizar</button>

												</div>
											</form>

										</div>


									</div>


								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
</body>
<!-- js -->
<script src="../v1/vendors/scripts/core.js"></script>
<script src="../v1/vendors/scripts/script.min.js"></script>
<script src="../v1/vendors/scripts/process.js"></script>
<script src="../v1/vendors/scripts/layout-settings.js"></script>

</html>