<?php
date_default_timezone_set('America/Argentina/Buenos_Aires');


use PHPMailer\PHPMailer\PHPMailer;

require_once "../PHPMailer/PHPMailer.php"; 
require_once "../PHPMailer/SMTP.php"; 
require_once "../PHPMailer/Exception.php"; 

$last_login = date('d-m-Y h:m A [T P]');
require '../constants/db_config.php';

$jobid = $_POST['jobid'];
$status = $_POST['status'];
$member = $_POST['member'];
$email = $_POST['email'];
$name = $_POST['name'];


try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
    $stmt = $conn->prepare("UPDATE tbl_job_applications SET EVacante=:estado WHERE member_no = :member and job_id = :jobid");
    $stmt->bindParam(':member', $member);
    $stmt->bindParam(':jobid', $jobid);
    $stmt->bindParam(':estado', $status);

    $stmt->execute();


    $stmta = $conn->prepare("SELECT A.first_name, B.title FROM `tbl_users` as A inner join `tbl_jobs`as B where A.member_no = B.company and B.job_id=:jobid");
    $stmta->bindParam(':jobid', $jobid);
    $stmta->execute();
    $resulta = $stmta->fetchAll();
    $NCompany = $resulta[0][0];
    $Vacante = $resulta[0][1];
    $img = "";
    $text = "";

    switch($status){
        case 2:
            $img  = "IRevision.png";
            break;
        case 3:
            $img  = "IEntrevista.png";
            break;
        case 4:
            $img  = "IAceptado.png";
            break;
        case 5:
            $img  = "IRechazado.png";
            break;
    }

    SendMail($email, $img, $name, $NCompany, $Vacante, $status);
	
    header('Location: ' . $_SERVER['HTTP_REFERER']);
    		  
	}catch(PDOException $e)
    {
        echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    }

    function SendMail($email, $img, $name, $empresa, $vacante,$status){
        $mail = new PHPMailer(); 
    
        $mail->isSMTP(); 
        $mail->Host = 'smtp.hostinger.com';
        $mail->SMTPAuth = true; 
        $mail->Username = "hola@idkit.com.ar"; 
        $mail->Password = "bB*TX27gR#Fs"; 
        $mail->Port = 465; 
        $mail->SMTPSecure = "ssl"; 
        $mail->CharSet = 'UTF-8';
        
        $mail->isHTML(true); 
        $mail->setFrom("hola@idkit.com.ar","<no reply>");
        $mail->addAddress($email); 
        $mail->Subject = ("Actualizacion del estado"); 
        $mail->AddEmbeddedImage('../images/'.$img.'', 'ILogo', 'attachment', 'base64', 'image/png');

        switch($status){
            case 2:
                $mail->Body = "    
                <center>
                    Hola ".$name." tu solicitud para ".$vacante." en ".$empresa." esta en REVISION
                    <img src='cid:ILogo' class='img-logo'/> <br>
                </center>";                
                break;
            case 3:
                $mail->Body = "    
                <center>
                    Hola ".$name." la empresa ".$empresa." quiere organizar una entrevista contigo porque le parecio interesante tu perfil. Ingresa a la plataforma para continuar el proceso.
                    <br> <img src='cid:ILogo' class='img-logo'/> <br>
                </center>";    
                break;
            case 4:
                $mail->Body = "    
                <center>
                    Felicidades ".$name." fuiste aceptado para el puesto de ".$vacante." en ".$empresa." desde IDentiKIT queremos felicitarte y darte las gracias por utilizar nuestra plataforma, exito en esta nueva etapa! 
                    <img src='cid:ILogo' class='img-logo'/> <br>
                </center>";    
                break;
            case 5:
                $mail->Body = "    
                <center>
                    Hola nombre usuario lamentablemente no fuiste seleccionado para nombre trabajo en nombre empresa. Pero... Animos! Tenemos muchas mas ofertas de trabajos IDeales para tu puesto, ingresa a la IDentiKIT y continua la busqueda! Te dejamos este link para que mejores tus habilidades a la hora de buscar trabajo https://idkit.com.ar/academy.php
                </center>";    
                break;
        }


        if (preg_match('/(.*)@(hotmail)\.(.*)/', $email) != false) { 
            $mail->addCustomHeader('Mime-Version','1.0');
            $mail->addCustomHeader('Content-Type: text/html; charset=ISO-8859-1');
        }   
    
        if($mail->send()){
            return;
        } else {
            $status = "failed";
            $response = "Something is wrong: <br>";
        }
            
        exit(json_encode(array("status" => $status, "response" => $response)));
        
    }
    

?>