<?php

session_start();
if (isset($_SESSION['logged']) && $_SESSION['logged'] == true) {
    $myid = $_SESSION['myid'];
    $myfname = $_SESSION['myfname'];
    $mylname = $_SESSION['mylname'];
    $mygender = $_SESSION['gender'];
    $myemail = $_SESSION['myemail'];
    $mydate = $_SESSION['mydate'];
    $mymonth = $_SESSION['mymonth'];
    $myyear = $_SESSION['myyear'];
    $myphone = $_SESSION['myphone'];
    $mytitle = $_SESSION['mytitle'];
    $mycity = $_SESSION['mycity'];
    $street = $_SESSION['street'];
    $myzip = $_SESSION['myzip'];
    $mycountry = $_SESSION['mycountry'];
    $mydesc = $_SESSION['mydesc'];
    $myavatar = $_SESSION['avatar'];
    $mylogin = $_SESSION['lastlogin'];
    $myrole = $_SESSION['role'];
    $dni = $_SESSION['dni'];
    $ability = $_SESSION['ability'];
    $github = $_SESSION['github'];
    $twitter = $_SESSION['twitter'];
    $instagram = $_SESSION['instagram'];
    $empujar = $_SESSION['empujar'];
    $user_online = true;	
} else {
    $user_online = false;
}
?>