<?php

require '../../constants/db_config.php';
require '../constants/check-login.php';

$jobtitle = ucwords($_POST['jobtitle']);

if (isset($_POST['duties'])) {
$duties = $_POST['duties'];	
}else{
$duties = "";	
}

$techno = ($_POST['techno']);
$startdate = $_POST['startdate'];
$enddate = $_POST['enddate'];
$link = $_POST['link'];

if (isset($_POST['view'])) {
    $view = $_POST['view'];	    
}else{
    $view = "";	
}

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        
    $stmt = $conn->prepare("INSERT INTO tbl_experience (member_no, title, link, duties, techno, start_date, end_date, view) 
    VALUES ('$myid', :title, :link, :duties, :techno, :startdate, :enddate, :view)");
    $stmt->bindParam(':title', $jobtitle);
    $stmt->bindParam(':link', $link);
    $stmt->bindParam(':duties', $duties);
    $stmt->bindParam(':techno', $techno);
    $stmt->bindParam(':startdate', $startdate);
    $stmt->bindParam(':enddate', $enddate);
    $stmt->bindParam(':view', $view);

    $stmt->execute();
    header("location:../index.php?r=9210");					  
}catch(PDOException $e)
{
echo "Connection failed: " . $e->getMessage();
}
	

?>