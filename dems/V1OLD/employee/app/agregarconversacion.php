<?php 
    date_default_timezone_set("America/Argentina/Buenos_Aires");

    require_once("../../config/Db.php");
    $db = new DbPDO();
    
    $data = array();

    $mensaje                      = $_POST['mensaje'];
    $receiverId                   = $_POST['receiverId'];
    $senderId                     = $_POST['senderId'];
    $message_creation_date        = date("Y/m/d");
    $message_creation_hour        = date("h:i:sa");

    try {
        $insertarMensaje = $db->query("insert into tbl_messages(message_body,message_sender_id,message_receiver_id,message_creation_hour,message_creation_date,message_status) 
        VALUES (:message_body,:message_sender_id,:message_receiver_id,:message_creation_hour,:message_creation_date,:message_status)"
        ,array("message_body"=>$mensaje,"message_sender_id"=>$senderId,"message_receiver_id"=>$receiverId,"message_creation_hour"=>$message_creation_hour,"message_creation_date"=>$message_creation_date,"message_status"=>1));

    } catch(PDOException $e) {
        echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    }
    finally {
        $data['status']='correcto';
    }

    echo json_encode($data);
?>





