<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-5F5WB8FMJC"></script>
<script>
	window.dataLayer = window.dataLayer || [];

	function gtag() {
		dataLayer.push(arguments);
	}
	gtag('js', new Date());

	gtag('config', 'G-5F5WB8FMJC');
</script>
<!DOCTYPE html>
<html>
<?php
	require_once("../../config/Db.php");
    require '../constants/check-login.php';
	$db = new DbPDO();

    $EmpresaID     = "CM482056108";//$_GET[""];
    $UserId        = $_SESSION['myid'];

    $mensajes      = $db->query("select message_subject,message_body,message_sender_id from tbl_messages where message_receiver_id='".$UserId."' or message_receiver_id='".$EmpresaID."' and message_sender_id='".$UserId."' or message_sender_id='".$EmpresaID."' 
	order by message_creation_date, message_creation_hour ASC");

	if ($user_online == "true") {
		if ($myrole == "employee") {
		} else {
			header("location:../");
		}
	} else {
		header("location:../");
	}

?>

<head>
	<!-- Basic Page Info -->
	<meta charset="utf-8">
	<title>IDentiKIT - Mi perfil</title>

	<link rel="apple-touch-icon" sizes="180x180" href="logov3.png">
	<link rel="icon" type="image/png" sizes="32x32" href="logov3.png">
	<link rel="icon" type="image/png" sizes="16x16" href="logov3.png">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="../../v1/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="../../v1/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="../../v1/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../../v1/src/plugins/datatables/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../../v1/vendors/styles/style.css">
	<link rel="stylesheet" type="text/css" href="../../v1/src/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>

<script>
	    $(document).ready(function(){														
	    	$('#enviarMensaje').on('click',function(){
                var mensaje    = $("#mensaje").val();
				var senderId   = 'EM180016890';//user
				//CM482056108
                var receiverId = 'CM482056108';//enterprise
				//EM180016890

				$.ajax({
					type:'POST',
					url:'../../employer/app/agregarconversacion.php',
					dataType: "json",
					data: { 
                        'mensaje':     mensaje, 
                        'senderId':    senderId,
                        'receiverId':  receiverId
					},
					success:function(response){
						if(response.status == "correcto"){
							alert("ok");
                            //$("#myModal").modal();
						}else{
						    alert('ERROR');
						} 
					}
				});
			});
		});
    </script>
<body>
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PHBQLSG" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- <div class="header">
		<div class="header-left">
			<div class="menu-icon icon-copy ti-menu" style="color: white;"></div>

			<div class="search-toggle-icon dw dw-search2" style="color: #ffde00;" data-toggle="header_search"></div>
			
			<div class="header-search">
				<form method="post" action="../busqueda.php">
					<div class="form-group mb-0">
						<i class="dw dw-search2 search-icon" style="color: #ffde00;"></i>
						<input type="text" class="form-control search-input" placeholder="Buscar en IDentiKIT" required name="PalabraClave">
						<input name="buscar" type="hidden" class="form-control mb-2" id="inlineFormInput" value="v">
					</div>
				</form>
			</div>
		</div>
		<div class="header-right">
			<div class="user-info-dropdown">
				<div class="dropdown">
					<a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" style="color: #ffde00;">
						<span class="user-icon">
							<?php
								if ($myavatar == null) {
									print '<center><img  src="../images/default.png" title="' . $myfname . '" alt="image"  /></center>';
								} else {
									echo '<center><img alt="image" title="' . $myfname . '"  src="data:image/png;base64,' . base64_encode($myavatar) . '"/></center>';
								}
							?>
						</span>
					</a>
					<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
						<?php
							if ($user_online == true) {
								print '
									<a class="dropdown-item" href=""><i class="dw dw-user1"></i> Perfil</a>
									<!--<a class="dropdown-item" href="#"><i class="dw dw-settings2"></i> Ajustes</a>
									<a class="dropdown-item" href="#"><i class="dw dw-help"></i> Ayuda</a>-->
									<a class="dropdown-item" href="../logout.php"><i class="dw dw-logout"></i> Salir</a>';
							} else {
								print '
									<li><a href="login.php">ingresar</a></li>
									<li><a data-toggle="modal" href="#registerModal">registrate</a></li>';
							}
						?>
					</div>
				</div>
			</div>
		</div>
	</div> -->

	<?php include '../../layouts/Menu.php';?>

	<!-- <div class="left-side-bar">
		<div class="brand-logo">
			<a href="#">
				<img src="../../v1/logov3.png" alt="" class="light-logo" width="60"> <span class="mtext"> IDentiKIT</span>
			</a>
			<div class="close-sidebar" data-toggle="left-sidebar-close">
				<i class="ion-close-round"></i>
			</div>
		</div>
		<div class="menu-block customscroll">
			<div class="sidebar-menu">
				<ul id="accordion-menu">
					<li>
						<a href="../index.php" class="dropdown-toggle no-arrow ">
							<span class="micon icon-copy fa fa-home"></span><span class="mtext">Inicio</span>
						</a>
					</li>
					<li class="dropdown">
						<a href="javascript:;" class="dropdown-toggle">
							<span class="micon icon-copy fa fa-bandcamp"></span><span class="mtext">Explorar</span>
						</a>
						<ul class="submenu">
							<li><a href="../identis.php"> IDentis</a></li>
							<li><a href="../works.php"> Trabajos</a></li>
							<li><a href="../empresas.php"> Empresas</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="javascript:;" class="dropdown-toggle">
							<span class="micon icon-copy fa fa-envelope"></span><span class="mtext">Buzon</span>
						</a>
						<ul class="submenu">
							<li><a href="../app/conversaciones.php"> Empresa 1</a></li>
						</ul>
					</li>
					<li>
						<a href="../../aplicados.php" class="dropdown-toggle no-arrow">
							<span class="micon ion-android-done-all"></span><span class="mtext">Aplicados</span>
						</a>
					</li>
					<li>
						<a href="../academy.php" class="dropdown-toggle no-arrow ">
							<span class="micon icon-copy fa fa-free-code-camp"></span><span class="mtext">Academia</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div> -->

	<?php include '../../layouts/Sidebar-menu.php';?>


	<div class="mobile-menu-overlay"></div>
		<div class="main-container">
			<div class="pd-ltr-20">
				<h1>Historial de mensajes</h1>
			
				<textarea rows="30" cols="220" name="comment" form="usrform" disabled>
				<?php
					foreach($mensajes as $mensaje){
						$EmpresaNombre = $db->query("select first_name from tbl_users where member_no=:memberNo",array('memberNo'=>$mensaje['message_sender_id']));
						echo "\n";
						$titulo = $mensaje['message_subject'];
						$body   = $mensaje['message_body'];
						echo $EmpresaNombre[0]['first_name'] . ": ";
						echo $body . "\n";
					}
				?>
				</textarea>
			</div>
			<div class="pd-ltr-20">
				<br>
				<div class="row">
					<h1>Responder</h1>
					<br><br><br>
					<textarea rows="10" cols="220" name="comment" id="mensaje">
					</textarea>
					<br>
					<br>
					<br><br><br>
					<button class="btn btn-primary" id="enviarMensaje">Enviar mensaje</button>
				</div>
			</div>
		</div>
	</div>

	</div>
	</div>
	</div>
	</div>
	<!-- js -->
	
	<script src="../v1/vendors/scripts/core.js"></script>
	<script src="../v1/vendors/scripts/script.min.js"></script>
	<script src="../v1/vendors/scripts/process.js"></script>
	<script src="../v1/vendors/scripts/layout-settings.js"></script>
	<script src="../v1/src/plugins/datatables/js/jquery.dataTables.min.js"></script>
	<script src="../v1/src/plugins/datatables/js/dataTables.bootstrap4.min.js"></script>
	<script src="../v1/src/plugins/datatables/js/dataTables.responsive.min.js"></script>
	<script src="../v1/src/plugins/datatables/js/responsive.bootstrap4.min.js"></script>
	<script src="../v1/vendors/scripts/dashboard.js"></script>
	<script src="../v1vendors/scripts/core.js"></script>
	<script src="../v1vendors/scripts/script.min.js"></script>
	<script src="../v1vendors/scripts/process.js"></script>
	<script src="../v1vendors/scripts/layout-settings.js"></script>
	<script src="../v1/src/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
	<script src="../v1/src/plugins/switchery/switchery.min.js"></script>
	<!-- bootstrap-touchspin js -->
	<script src="../v1/src/plugins/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
	<script src="../v1/vendors/scripts/advanced-components.js"></script>
	<!-- add sweet alert js & css in footer -->
	<script src="../v1/src/plugins/sweetalert2/sweetalert2.all.js"></script>
	<script src="../v1/src/plugins/sweetalert2/sweet-alert.init.js"></script>

</body>

</html>
