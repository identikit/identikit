<?php
include 'constants/settings.php';
include 'constants/check-login.php';


//Include Configuration File
//include('config.php');

$login_button = '';

if (isset($_GET["code"])) {

  $token = $google_client->fetchAccessTokenWithAuthCode($_GET["code"]);
  if (!isset($token['error'])) {

    $google_client->setAccessToken($token['access_token']);

    $_SESSION['access_token'] = $token['access_token'];

    $google_service = new Google_Service_Oauth2($google_client);

    $data = $google_service->userinfo->get();

    if (!empty($data['given_name'])) {
      $_SESSION['user_first_name'] = $data['given_name'];
    }

    if (!empty($data['family_name'])) {
      $_SESSION['user_last_name'] = $data['family_name'];
    }

    if (!empty($data['email'])) {
      $_SESSION['user_email_address'] = $data['email'];
    }

    if (!empty($data['gender'])) {
      $_SESSION['user_gender'] = $data['gender'];
    }

    if (!empty($data['picture'])) {
      $_SESSION['user_image'] = $data['picture'];
    }
  }
}

?>


<!DOCTYPE html>
<html>

<head>
  <!-- Basic Page Info -->
  <meta charset="utf-8">
  <title>IDentiKIT - Login</title>

  <!-- Site favicon -->
  <link href="logo.png" rel="icon" type="image/png">
  <!-- Mobile Specific Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

  <!-- Google Font -->
  <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
  <!-- CSS -->
  <link rel="stylesheet" type="text/css" href="v1/vendors/styles/core.css">
  <link rel="stylesheet" type="text/css" href="v1/vendors/styles/icon-font.min.css">
  <link rel="stylesheet" type="text/css" href="v1/vendors/styles/style.css">

  <script type="text/javascript">
    function update(str) {

      if (document.getElementById('mymail').value == "") {
        alert("Porfavor Ingresa tu email");

      } else {
        document.getElementById("data").innerHTML = "Procesando...";
        var xmlhttp;

        if (window.XMLHttpRequest) {
          xmlhttp = new XMLHttpRequest();
        } else {
          xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }

        xmlhttp.onreadystatechange = function() {
          if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            document.getElementById("data").innerHTML = xmlhttp.responseText;
          }
        }

        xmlhttp.open("GET", "app/reset-pw.php?opt=" + str, true);
        xmlhttp.send();
      }

    }

    function reset_text() {
      document.getElementById('mymail').value = "";
      document.getElementById("data").innerHTML = "";
    }
  </script>

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-K2NHRZT4R7"></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'G-K2NHRZT4R7');
  </script>

  <!-- Google Tag Manager -->
  <script>
    (function(w, d, s, l, i) {
      w[l] = w[l] || [];
      w[l].push({
        'gtm.start': new Date().getTime(),
        event: 'gtm.js'
      });
      var f = d.getElementsByTagName(s)[0],
        j = d.createElement(s),
        dl = l != 'dataLayer' ? '&l=' + l : '';
      j.async = true;
      j.src =
        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
      f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-5QGSH5J');
  </script>
  <!-- End Google Tag Manager -->



  <meta name="description" content="Iniciar Sesión en IDentiKIT" />
  <meta name="keywords" content="identikit, IDentiKIT, idkit, IDKIT, primera experiencia laboral, trabajos jr, junior, trabajos junior, empresas que contratan juniors, talento joven, talento tech, jovenes, empresas sin experiencia" />
  <meta name="author" content="IDentiKIT">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta property="og:image" content="https://identikit.app/logowebog.png" />
  <meta property="og:image:secure_url" content="https://identikit.app/logowebog.png" />
  <meta property="og:image:type" content="image/png" />
  <meta property="og:image:width" content="300" />
  <meta property="og:image:height" content="300" />
  <meta property="og:image:alt" content="IDentiKIT - Tu primera experiencia laboral" />
  <meta property="og:url" content="https://identikit.app/" />
  <meta property="og:type" content="website" />

</head>

<body class="login-page">

  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PHBQLSG" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->

  <div class="login-header box-shadow">
    <div class="container-fluid d-flex justify-content-between align-items-center">
      <div class="brand-logo">
      </div>
    </div>
  </div>
  <div class="login-wrap d-flex align-items-center flex-wrap justify-content-center">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-6 col-lg-12">
          <div class="login-box bg-white box-shadow border-radius-10">
            <div class="login-title">
              <center><img src="logo.png" class="text-center text-primary" style="width: 30%; height: 30%;"></center>
            </div>
            <?php include 'constants/check_reply.php'; ?>
            <form name="frm" action="app/auth.php" method="POST">
              <div class="input-group custom">
                <input type="text" class="form-control form-control-lg" placeholder="Email" name="email">
                <div class="input-group-append custom">
                  <span class="input-group-text"><i class="icon-copy dw dw-user1"></i></span>
                </div>
              </div>
              <div class="input-group custom">
                <input type="password" class="form-control form-control-lg" placeholder="Contraseña" name="password">
                <div class="input-group-append custom">
                  <span class="input-group-text"><i class="dw dw-padlock1"></i></span>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="input-group mb-0">
                    <button type="submit" class="btn btn-primary btn-sm btn-block">Iniciar Sesión</button>
                  </div>
                  <div class="font-16 weight-600 pt-10 pb-10 text-center" data-color="#707373">O</div>
                  <div class="input-group mb-0">
                    <a class="btn btn-primary btn-sm btn-block" href="registro.php">Crear cuenta nueva</a>
                  </div>
                  <br>
                  <div class="col-sm-12">
                    <div class="mb-0">
                      <div class="forgot-password">
                        <a data-toggle="modal" onclick="reset_text()" href="#forgotPasswordModal" data-target="#bd-example-modal-lg" type="button">¿Olvidaste tu contraseña?</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
            <div class="col-md-4 col-sm-12 mb-30">
              <div class="modal fade bs-example-modal-lg" id="bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title" id="myLargeModalLabel">Recuperar cuenta</h4>
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                      <div class="col-sm-12 col-md-12">
                        <p class="mb-20">Ingresa Correo Electrónico asociado a tu cuenta, le enviaremos un correro con el enlace para restablecer su contraseña</p>
                      </div>
                      <div class="col-sm-12 col-md-12">
                        <div class="form-group">
                          <label>Correo Electrónico</label>
                          <input id="mymail" autocomplete="off" name="email" class="form-control" placeholder="Ingresa tu Correo Electrónico" type="email" required>
                        </div>
                      </div>
                      <div class="col-sm-12 col-md-12">
                        <div class="login-box-box-action">
                          <p id="data"></p>
                        </div>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                      <button type="button" class="btn btn-primary" onclick="update(mymail.value)" type="submit">Recuperar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>

  <!-- js -->
  <script src="v1/vendors/scripts/core.js"></script>
  <script src="v1/vendors/scripts/script.min.js"></script>
  <script src="v1/vendors/scripts/process.js"></script>
  <script src="v1/vendors/scripts/layout-settings.js"></script>
</body>

</html>