<div class="user-notification">
	<div class="dropdown">
		<a class="dropdown-toggle no-arrow" href="#" role="button" data-toggle="dropdown">
			<i class="icon-copy ion-android-notifications" style="color: white;"> </i>
			<?php
				if (empty($myavatar && $mycity && $dni && $mytitle && $myzip && $myphone && $mystreet && $mydesc )) {
					print '<span class="badge notification-active"></span>';
				}						
			?>
		</a>
		<div class="dropdown-menu dropdown-menu-right">
			<div class="notification-list mx-h-350 customscroll">
				<ul>
					<li>				
						<?php	
							if (empty($myavatar)) {
								print '
								<a href="#">
										<img src="logov3.png" alt="">
										<p>Cambia tu foto de perfil</p>
									</a>';	
								}
							####################################
							if (empty($mycity)) {
								print '<a href="#">
										<img src="logov3.png" alt="">
										<p>Agrega tu ciudad</p>
									</a>';
								}
							##################################
							if (empty($mystreet)) {
								print '<a href="#">
										<img src="logov3.png" alt="">
										<p>Agrega tu direccion</p>
									</a>';
								}
							#####################################
							if (empty($dni)) {
								print '<a href="#">
										<img src="logov3.png" alt="">
										<p>Agrega tu DNI</p>
									</a>';
								}
							#####################################
							if (empty($mytitle)) {
								print '<a href="#">
										<img src="logov3.png" alt="">
										<p>Agrega a tu ocupacion</p>
									</a>';
								}
							#####################################
							if (empty($mycity)) {
								print '<a href="#">
										<img src="logov3.png" alt="">
										<p>Agrega tu numero de telefono</p>
									</a>';
								}
							######################################
							if (empty($myphone)) {
								print '
									<a href="#">
										<img src="logov3.png" alt="">
										<p>Agrega tu telefono</p>
									</a>';
							}
							###################################
							if (empty($mydesc)) {
								print '
									<a href="#">
										<img src="logov3.png" alt="">
										<p>Agrega una descripcion genial</p>
									</a>';
							}
							###################################	
							if ($myavatar && $mycity && $dni && $mytitle && $myphone && $mydesc && $mystreet) {
								print ' <p><i class="icon-copy ion-happy-outline" style="color: yellow;"></i> Estas al dia</p> ';
							}else{
								print '';
							}				
						?>	
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>