<!DOCTYPE html>
<html>
<head>
	<!-- Basic Page Info -->
	<meta charset="utf-8">
	<title>IDentiKIT - ¡Crea tu cv y encontra trabajo rapido!</title>

	
	<!-- Site favicon 
	<link rel="apple-touch-icon" sizes="180x180" href="vendors/images/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="vendors/images/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="vendors/images/favicon-16x16.png">-->

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="src/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="src/plugins/datatables/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="vendors/styles/style.css">

</head>
<body>
	<!--<div class="pre-loader">
		<div class="pre-loader-box">
			<div class="loader-logo"><img src="vendors/images/deskapp-logo.svg" alt=""></div>
			<div class='loader-progress' id="progress_div">
				<div class='bar' id='bar1'></div>
			</div>
			<div class='percent' id='percent1'>0%</div>
			<div class="loading-text">
				Loading...
			</div>
		</div>
	</div>-->

	<div class="header">
		<div class="header-left">
			<div class="menu-icon icon-copy ti-menu" style="color: white;"></div>
			
			<div class="search-toggle-icon dw dw-search2" style="color: #ffde00;" data-toggle="header_search"></div>
			<div class="header-search">
				<form>
					<div class="form-group mb-0" >
						<i class="dw dw-search2 search-icon" style="color: #ffde00;"></i>
						<input type="text" class="form-control search-input" placeholder="Buscar en IDentiKIT">
					</div>
				</form>
			</div>
		</div>
		<div class="header-right">
			
			<!--<div class="user-notification">
				<div class="dropdown">
					<a class="dropdown-toggle no-arrow" href="#" role="button" data-toggle="dropdown">
						<i class="icon-copy dw dw-notification"></i>
						<span class="badge notification-active"></span>
					</a>
					<div class="dropdown-menu dropdown-menu-right">
						<div class="notification-list mx-h-350 customscroll">
							<ul>
								<li>
									<a href="#">
										<img src="vendors/images/img.jpg" alt="">
										<h3>John Doe</h3>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed...</p>
									</a>
								</li>
								
							</ul>
						</div>
					</div>
				</div>
			</div>-->
			<div class="user-info-dropdown">
				<div class="dropdown">
					<a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" style="color: #ffde00;">
						<span class="user-icon">
							<img src="vendors/images/photo1.jpg" alt="">
						</span>
						<span class="user-name" style="color: white;">Apodo</span>
					</a>
					<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
						<a class="dropdown-item" href="miperfil.php"><i class="dw dw-user1"></i> Pefil</a>
						<a class="dropdown-item" href="#"><i class="dw dw-settings2"></i> Ajustes</a>
						<a class="dropdown-item" href="#"><i class="dw dw-help"></i> Ayuda</a>
						<a class="dropdown-item" href="#"><i class="dw dw-logout"></i> Salir</a>
					</div>
				</div>
			</div>
			
		</div>
	</div>

	

	<div class="left-side-bar">
		<div class="brand-logo">
			<a href="#">
				<img src="logov3.png" alt="" class="light-logo" width="60"> <span class="mtext"> IDentiKIT</span>
			</a>
			<div class="close-sidebar" data-toggle="left-sidebar-close">
				<i class="ion-close-round"></i>
			</div>
		</div>
		<div class="menu-block customscroll">
			<div class="sidebar-menu">
				<ul id="accordion-menu">
					<li>
						<a href="index.php" class="dropdown-toggle no-arrow ">
							<span class="micon icon-copy fa fa-home"></span><span class="mtext">Incio</span>

						</a>
					</li>
					
					<li class="dropdown">
						<a href="javascript:;" class="dropdown-toggle ">
							<span class="micon icon-copy fa fa-bandcamp"></span><span class="mtext">Explorar</span>
						</a>
						<ul class="submenu">
							<li><a href="identis.php"> IDentis</a></li>
							<li><a href="works.php"> Trabajos</a></li>
						</ul>
					</li>
					
				</ul>
			</div>
		</div>
	</div>
	<div class="mobile-menu-overlay"></div>

	<div class="main-container">
		<div class="pd-ltr-20 xs-pd-20-10">
			<div class="min-height-200px">
				
				<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
					


					
					<div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
							<li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
							<li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
						</ol>
						<div class="carousel-inner">

							<div class="carousel-item active">
								<div class="card bg-dark card-box">
									<img class="card-img" src="vendors/images/img1.jpg" alt="Card image">
									<div class="card-img-overlay">
										<h5 class="card-title weight-500">Consejo 1</h5>
										<p class="card-text"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
										tempor incididunt ut labore et dolore magna aliqua...</p>
									</div>
								</div>
							</div>


							<div class="carousel-item ">
								<div class="card bg-dark card-box">
									<img class="card-img" src="vendors/images/img1.jpg" alt="Card image">
									<div class="card-img-overlay">
										<h5 class="card-title weight-500">Consejo 2</h5>
										<p class="card-text"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
										tempor incididunt ut labore et dolore magna aliqua...</p>
									</div>
								</div>
							</div>


							<div class="carousel-item ">
								<div class="card bg-dark card-box">
									<img class="card-img" src="vendors/images/img1.jpg" alt="Card image">
									<div class="card-img-overlay">
										<h5 class="card-title weight-500">Consejo 3</h5>
										<p class="card-text"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
										tempor incididunt ut labore et dolore magna aliqua...</p>
									</div>
								</div>
							</div>


						</div>
						
					</div>
														
														



					<div class="col-sm-12 col-md-12 col-lg-4 mb-30">
						
					</div>
					<div class="page-header">


						<div class="contact-directory-list">
										
										<!--<div class="col-lg-3 col-md-6 col-sm-12 mb-15">
											<span>Filtrar por:</span>
											<div class="btn-group-vertical">
												
												<button type="button" class="btn btn-light dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false">  <span class="caret"></span> </button>
												<div class="dropdown-menu">
													<a class="dropdown-item" href="#">Dropdown link</a>
													<a class="dropdown-item" href="#">Dropdown link</a>
												</div>
											</div>
										</div>-->



										<ul class="row">
											
											<li class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
												<div class="contact-directory-box">
													<div class="contact-dire-info text-center">
														<div class="contact-avatar">
															<span>
																<img src="vendors/images/photo2.jpg" alt="">
															</span>
														</div>
														<div class="contact-name">
															<h4>Usuario</h4>
															
															<div class="work text-success"><i class="ion-android-person"></i> Puesto/carrera</div>
														</div>
														<div class="contact-skill">
															<span class="badge badge-pill">TAG 1</span>
															<span class="badge badge-pill">TAG 2</span>
															<span class="badge badge-pill">TAG 3</span>
															
														</div>
														<div class="profile-sort-desc">
															Descripcion del perfil Lorem ipsum dolor sit amet, consectetur adipisic...
														</div>
													</div>
													<div class="view-contact">
														<a href="perfil.php">Ver ID</a>
													</div>
												</div>
											</li>
											
											<li class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
												<div class="contact-directory-box">
													<div class="contact-dire-info text-center">
														<div class="contact-avatar">
															<span>
																<img src="vendors/images/photo2.jpg" alt="">
															</span>
														</div>
														<div class="contact-name">
															<h4>Usuario</h4>
															
															<div class="work text-success"><i class="ion-android-person"></i> Puesto/carrera</div>
														</div>
														<div class="contact-skill">
															<span class="badge badge-pill">TAG 1</span>
															<span class="badge badge-pill">TAG 2</span>
															<span class="badge badge-pill">TAG 3</span>
															
														</div>
														<div class="profile-sort-desc">
															Descripcion del perfil Lorem ipsum dolor sit amet, consectetur adipisic...
														</div>
													</div>
													<div class="view-contact">
														<a href="perfil.php">Ver ID</a>
													</div>
												</div>
											</li>

											<li class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
												<div class="contact-directory-box">
													<div class="contact-dire-info text-center">
														<div class="contact-avatar">
															<span>
																<img src="vendors/images/photo2.jpg" alt="">
															</span>
														</div>
														<div class="contact-name">
															<h4>Usuario</h4>
															
															<div class="work text-success"><i class="ion-android-person"></i> Puesto/carrera</div>
														</div>
														<div class="contact-skill">
															<span class="badge badge-pill">TAG 1</span>
															<span class="badge badge-pill">TAG 2</span>
															<span class="badge badge-pill">TAG 3</span>
															
														</div>
														<div class="profile-sort-desc">
															Descripcion del perfil Lorem ipsum dolor sit amet, consectetur adipisic...
														</div>
													</div>
													<div class="view-contact">
														<a href="perfil.php">Ver ID</a>
													</div>
												</div>
											</li>

										</ul>
									</div>
								</div>


				</div>
			</div>
			
		</div>
	</div>
	<!-- js -->
	<script src="vendors/scripts/core.js"></script>
	<script src="vendors/scripts/script.min.js"></script>
	<script src="vendors/scripts/process.js"></script>
	<script src="vendors/scripts/layout-settings.js"></script>
</body>
</html>