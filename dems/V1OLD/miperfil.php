<!DOCTYPE html>
<html>
<?php 
require 'func/constants/settings.php'; 
require 'constants/check-login.php';

if ($user_online == "true") {
if ($myrole == "employee") {
}else{
header("location:../");		
}
}else{
header("location:../");	
}
?>
<head>
	<!-- Basic Page Info -->
	<meta charset="utf-8">
	<title>IDentiKIT - ¡Crea tu cv y encontra trabajo rapido!</title>

	
	<!-- Site favicon 
	<link rel="apple-touch-icon" sizes="180x180" href="vendors/images/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="vendors/images/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="vendors/images/favicon-16x16.png">-->

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="../v1/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="../v1/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="../v1/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../v1/src/plugins/datatables/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="../v1/vendors/styles/style.css">

</head>
<body>
	<!--<div class="pre-loader">
		<div class="pre-loader-box">
			<div class="loader-logo"><img src="vendors/images/deskapp-logo.svg" alt=""></div>
			<div class='loader-progress' id="progress_div">
				<div class='bar' id='bar1'></div>
			</div>
			<div class='percent' id='percent1'>0%</div>
			<div class="loading-text">
				Loading...
			</div>
		</div>
	</div>-->

	<div class="header">
		<div class="header-left">
			<div class="menu-icon icon-copy ti-menu" style="color: white;"></div>
			
			<div class="search-toggle-icon dw dw-search2" style="color: #ffde00;" data-toggle="header_search"></div>
			<div class="header-search">
				<form>
					<div class="form-group mb-0" >
						<i class="dw dw-search2 search-icon" style="color: #ffde00;"></i>
						<input type="text" class="form-control search-input" placeholder="Buscar en IDentiKIT">
					</div>
				</form>
			</div>
		</div>
		<div class="header-right">
			
			<!--<div class="user-notification">
				<div class="dropdown">
					<a class="dropdown-toggle no-arrow" href="#" role="button" data-toggle="dropdown">
						<i class="icon-copy dw dw-notification"></i>
						<span class="badge notification-active"></span>
					</a>
					<div class="dropdown-menu dropdown-menu-right">
						<div class="notification-list mx-h-350 customscroll">
							<ul>
								<li>
									<a href="#">
										<img src="vendors/images/img.jpg" alt="">
										<h3>John Doe</h3>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed...</p>
									</a>
								</li>
								
							</ul>
						</div>
					</div>
				</div>
			</div>-->
			<div class="user-info-dropdown">
				<div class="dropdown">
					<a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" style="color: #ffde00;">
						<span class="user-icon">
							
							<?php
							if ($myavatar == null) {
								print '<center><img src="logo.png" title="'.$myfname.'" alt="image"  /></center>';
							}else{
								echo '<center><img alt="image" title="'.$myfname.'"  src="data:image/jpeg;base64,'.base64_encode($myavatar).'"/></center>';	
							}
										?>
						</span>
						<span class="user-name" style="color: white;">Apodo</span>
					</a>
					<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
						<a class="dropdown-item" href="miperfil.php"><i class="dw dw-user1"></i> Pefil</a>
						<a class="dropdown-item" href="#"><i class="dw dw-settings2"></i> Ajustes</a>
						<a class="dropdown-item" href="#"><i class="dw dw-help"></i> Ayuda</a>
						<a class="dropdown-item" href="#"><i class="dw dw-logout"></i> Salir</a>
					</div>
				</div>
			</div>
			
		</div>
	</div>

	

	<div class="left-side-bar">
		<div class="brand-logo">
			<a href="#">
				<img src="v1/logov3.png" alt="" class="light-logo" width="60"> <span class="mtext"> IDentiKIT</span>
			</a>
			<div class="close-sidebar" data-toggle="left-sidebar-close">
				<i class="ion-close-round"></i>
			</div>
		</div>
		<div class="menu-block customscroll">
			<div class="sidebar-menu">
				<ul id="accordion-menu">
					<li>
						<a href="index.php" class="dropdown-toggle no-arrow active">
							<span class="micon icon-copy fa fa-home"></span><span class="mtext">Inicio</span>

						</a>
					</li>
					
					<li class="dropdown">
						<a href="javascript:;" class="dropdown-toggle">
							<span class="micon icon-copy fa fa-bandcamp"></span><span class="mtext">Explorar</span>
						</a>
						<ul class="submenu">
							<li><a href="identis.php"> IDentis</a></li>
							<li><a href="works.php"> Trabajos</a></li>
						</ul>
					</li>
					
				</ul>
			</div>
		</div>
	</div>
	<div class="mobile-menu-overlay"></div>

	<div class="main-container">
			<div class="pd-ltr-20 xs-pd-20-10">
				<div class="min-height-200px">
					
					<div class="row">
						<div class="card-box height-100-p overflow-hidden">

							<div class="pd-20 card-box">
							<h5 class="h4  mb-20">Default Tab</h5>
							<div class="tab">
								<ul class="nav nav-tabs" role="tablist">
									<li class="nav-item">
										<a class="nav-link active " data-toggle="tab" href="#home" role="tab" aria-selected="true">Mi ID</a>
									</li>
									<li class="nav-item">
										<a class="nav-link " data-toggle="tab" href="#profile" role="tab" aria-selected="false">Informacion</a>
									</li>
									<li class="nav-item">
										<a class="nav-link " data-toggle="tab" href="#experiencia" role="tab" aria-selected="false">Experiencia</a>
									</li>
									
								</ul>
								
								<div class="tab-content">
									


									<div class="tab-pane fade show active" id="home" role="tabpanel">

										<div class="pd-20">
											
											<div class="profile-photo">
												<a href="modal" data-toggle="modal" data-target="#modal" class="edit-avatar"><i class="fa fa-pencil"></i></a>

												<img src="vendors/images/photo2.jpg" alt="" class="avatar-photo">
												<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
													<div class="modal-dialog modal-dialog-centered" role="document">
														<div class="modal-content">
															<div class="modal-body pd-5">
																<div class="img-container">
																	<img id="image" src="vendors/images/photo2.jpg" alt="Picture" >
																	<?php 
																	if ($myavatar == null) {
																		print '<center><img  src="../images/default.jpg" title="'.$myfname.'" alt="image"  /></center>';
																	}else{
																		echo '<center><img alt="image" title="'.$myfname.'"  src="data:image/jpeg;base64,'.base64_encode($myavatar).'"/></center>';	
																	}
																	?>
																</div>
															</div>
															<div class="modal-footer">
																<input type="submit" value="Actualizar" class="btn btn-success">
																<button type="button" class="btn btn-default" data-dismiss="modal">cerrar</button>
															</div>
														</div>
													</div>
												</div>
											</div>

											<h5 class="text-center h5 mb-0 " > <?php echo "$myfname"; ?> <?php echo "$mylname"; ?></h5>
											<div class="work text-success text-center pd-10"><i class="ion-android-person"></i> <?php echo "$mytitle"; ?></div>
											<p class="text-center text-muted font-14">Descripcion Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim ven</p>

											<div class="contact-directory-box pd-0">
												<div class="contact-dire-info text-center pd-0">
													
													
													<div class="contact-skill pd-0">
														<span class="badge badge-pill">TAG 1</span>
														<span class="badge badge-pill">TAG 2</span>
														<span class="badge badge-pill">TAG 3</span>
														
													</div>
											</div>
									<div class="timeline mb-30">
									<h5 class="text-center mb-20 h5 mb-0 pd-10">Experiencia laboral</h5>
									<ul>
										<li>
											<div class="timeline-date">
												2015-2017
											</div>
											<div class="timeline-desc card-box">
												<div class="pd-20">
													<h4 class="mb-10 h4">Titulo</h4>
													<p class="text-center text-muted font-14">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
												</div>
											</div>
										</li>

										<li>
											<div class="timeline-date">
												2015-2017
											</div>
											<div class="timeline-desc card-box">
												<div class="pd-20">
													<h4 class="mb-10 h4">Titulo</h4>
													<p class="text-center text-muted font-14">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
												</div>
											</div>
										</li>


										<li>
											<div class="timeline-date">
												2015-2017
											</div>
											<div class="timeline-desc card-box">
												<div class="pd-20">
													<h4 class="mb-10 h4">Titulo</h4>
													<p class="text-center text-muted font-14">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
												</div>
											</div>
										</li>

									</ul>
								</div>
										</div>





										</div>

									</div>






									<div class="tab-pane fade" id="profile" role="tabpanel">
									<?php require 'constants/check_reply.php'; ?>
										<div class="pd-20">
											<div class="profile-setting">
												<form>
													<ul class="profile-edit-list row">
														<li class="weight-500 col-md-6">
															<h4 class=" h5 mb-20">Informacion</h4>
															<div class="form-group">
																<label>Nombre</label>
																<input class="form-control form-control-lg" type="text">
															</div>
															<div class="form-group">
																<label>Apellido</label>
																<input class="form-control form-control-lg" type="text">
															</div>
															<div class="form-group">
																<label>Email</label>
																<input class="form-control form-control-lg" type="email">
															</div>
															<div class="form-group">
																<label>Fecha de cumpleaños</label>
																<input class="form-control form-control-lg date-picker" type="text">
															</div>
															<div class="form-group">
																<label>Genero</label>
																<div class="d-flex">
																<div class="custom-control custom-radio mb-5 mr-20">
																	<input type="radio" id="customRadio4" name="customRadio" class="custom-control-input">
																	<label class="custom-control-label weight-400" for="customRadio4">Hombre</label>
																</div>
																<div class="custom-control custom-radio mb-5">
																	<input type="radio" id="customRadio5" name="customRadio" class="custom-control-input">
																	<label class="custom-control-label weight-400" for="customRadio5">Mujer</label>
																</div>
																<div class="custom-control custom-radio mb-5 mr-20">
																	<input type="radio" id="customRadio6" name="customRadio" class="custom-control-input">
																	<label class="custom-control-label weight-400" for="customRadio6">Otro</label>
																</div>
																
																</div>
															</div>
															<div class="form-group">
																<label>Pais</label>
																<select class="selectpicker form-control form-control-lg" data-style="btn-outline-secondary btn-lg" title="Pais">
																	<option>Argentina</option>
																	<option>Uruguay</option>
																	<option>Chile</option>
																	<option>Mexico</option>
																	<option>Brasil</option>
																	<option>Peru</option>
																</select>
															</div>
															<div class="form-group">
																<label>Provincia/Region</label>
																<input class="form-control form-control-lg" type="text">
															</div>
															<div class="form-group">
																<label>Codigo postal</label>
																<input class="form-control form-control-lg" type="text">
															</div>
															<div class="form-group">
																<label>Telefono</label>
																<input class="form-control form-control-lg" type="text">
															</div>
															<div class="form-group">
																<label>Calle</label>
																<input class="form-control form-control-lg" type="text">
															</div>							

															
														</li>
														<li class="weight-500 col-md-6">
															<h4 class=" h5 mb-20">Redes sociales</h4>
															<div class="form-group">
																<label>Facebook URL:</label>
																<input class="form-control form-control-lg" type="text" placeholder="Pega tu link aqui">
															</div>
															<div class="form-group">
																<label>Usuario de Twitter:</label>
																<input class="form-control form-control-lg" type="text" placeholder="@usuario">
															</div>
															<div class="form-group">
																<label>Linkedin URL:</label>
																<input class="form-control form-control-lg" type="text" placeholder="Pega tu link aqui">
															</div>
															<div class="form-group">
																<label>Usuario de Instagram:</label>
																<input class="form-control form-control-lg" type="text" placeholder="@usuario">
															</div>
															
															<div class="form-group mb-0">
																<input type="submit" class="btn btn-success" value="Guardar y actualizar">
															</div>
														</li>
													</ul>
												</form>
											</div>
										</div>

									</div>




									<div class="tab-pane fade" id="experiencia" role="tabpanel">

										<div class="pd-20">
										<div class="timeline mb-30">
									<h5 class="text-center mb-20 h5 mb-0 pd-10">Experiencia laboral</h5>
									<ul>
										<li>
											
											<div class="timeline-desc card-box">
												<div class="pd-20">
												<div class="col-md-12 col-sm-12">
													<div class="form-group">
														<input type="text" class="form-control">
													</div>
												</div>
													<textarea class="form-control mb-10 col-md-12"></textarea>
												</div>
											</div>
										</li>
										</ul></div>
										</div>

									</div>
									
								</div>
							</div>
						</div>
							

							<div class="pd-20 card-box height-100-p">
								
								

										
						</div>
						</div>
						<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 mb-30">
							<div class="card-box height-100-p overflow-hidden">
								<div class="profile-tab height-100-p">
									<div class="tab height-100-p">
										
										<div class="tab-content">
																						
												
											</div>
											<!-- Setting Tab End -->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
					



	</div>
	<!-- js -->
	<script src="../v1/vendors/scripts/core.js"></script>
	<script src="../v1/vendors/scripts/script.min.js"></script>
	<script src="../v1/vendors/scripts/process.js"></script>
	<script src="../v1/vendors/scripts/layout-settings.js"></script>
	<script src="../v1/src/plugins/apexcharts/apexcharts.min.js"></script>
	<script src="../v1/src/plugins/datatables/js/jquery.dataTables.min.js"></script>
	<script src="../v1/src/plugins/datatables/js/dataTables.bootstrap4.min.js"></script>
	<script src="../v1/src/plugins/datatables/js/dataTables.responsive.min.js"></script>
	<script src="../v1/src/plugins/datatables/js/responsive.bootstrap4.min.js"></script>
	<script src="../v1/vendors/scripts/dashboard.js"></script>
</body>
</html>