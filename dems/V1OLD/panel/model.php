<?php
// model.php
 
 require_once 'config.php'; 

 
function openConex(){
	$conex=new mysqli(DBHOST, DBUSER, DBPWD, DBNAME); 
	
    return $conex;
}    
 
function getPosts(){	
	$mysqli = openConex();
	
	$result = $mysqli->query('SELECT * FROM blog WHERE fecha < now() AND active = 1 ORDER BY fecha DESC');	

	return $result;	
}

function getPostById($id)
{
	$mysqli = openConex();
 
	$result = $mysqli->query('SELECT * FROM blog WHERE id ='.$id);
	$row = mysqli_fetch_assoc($result);
	
    return $row;
}

?>