<?php
$DB_HOST = 'localhost';
 $DB_USER = 'root';
 $DB_PASS = '';
 $DB_NAME = 'duhireblog';
 
 try{
  $DB_con = new PDO("mysql:host={$DB_HOST};dbname={$DB_NAME}",$DB_USER,$DB_PASS);
  $DB_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
 }
 catch(PDOException $e){
  echo $e->getMessage();
 }
?>
<?php

 if(isset($_POST['entrada']))
 {
  $titulo = $_POST['titulo'];// Titulo
  $contenido = $_POST['contenido'];// Contenido
  
  $imgFile = $_FILES['image']['name'];
  $tmp_dir = $_FILES['image']['tmp_name'];
  $imgSize = $_FILES['image']['size'];
  $active = $_POST['active']; // Activo 1-0
  $fecha = $_POST['fecha']; // Fecha

  
  if(empty($titulo)){
   $errMSG = "Ingresa un titulo.";
  }
  else if(empty($contenido)){
   $errMSG = "Ingresa una descripcion.";
  }
  else if(empty($imgFile)){
   $errMSG = "Selecciona una imagen.";
  }
  else
  {
   $upload_dir = '../../../blog/images/'; // Directorio de subida
 
   $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // Obtener extension de la imagen
  
   // valid image extensions
   $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // Extensiones permitidas
  
   // rename uploading image
   $imagen = rand(1000,1000000).".".$imgExt;
    
   // allow valid image file formats
   if(in_array($imgExt, $valid_extensions)){   
    // Check file size '5MB'
    if($imgSize < 5000000)    {
     move_uploaded_file($tmp_dir,$upload_dir.$imagen);
    }
    else{
     $errMSG = "Sorry, your file is too large.";
    }
   }
   else{
    $errMSG = "Solo podes subir archivos en formato JPG, JPEG, PNG & GIF no estan permitidos.";  
   }
  }
  
  
  // if no error occured, continue ....
  if(!isset($errMSG))
  {
   $stmt = $DB_con->prepare('INSERT INTO blog (titulo,contenido,imagen, usuario, active, fecha) VALUES(:titulo, :contenido, :imagen, "lucho", :activo, :fecha)');
   $stmt->bindParam(':titulo',$titulo);
   $stmt->bindParam(':contenido',$contenido);
   $stmt->bindParam(':imagen',$imagen);
   $stmt->bindParam(':activo',$active);
   $stmt->bindParam(':fecha',$fecha);


   if($stmt->execute())
   {
    $successMSG = "new record succesfully inserted ...";
    header("refresh:1; editor.php"); // redirects image view page after 5 seconds.
   }
   else
   {
    $errMSG = "error while inserting....";
   }
  }
 }

?>


<!DOCTYPE html>
<html lang="es">

<head>
    <title>Panel DuHire</title>
    <!-- HTML5 Shim and Respond.js IE11 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 11]>
    	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    	<![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="" />
    <meta name="keywords" content="">
    <meta name="author" content="Phoenixcoded" />
    <!-- Favicon icon -->
    <link rel="icon" href="../assets/images/favicon.ico" type="image/x-icon">

    <!-- vendor css -->
    <link rel="stylesheet" href="../assets/css/style.css">
    
    <script src="https://cdn.tiny.cloud/1/0yb7elgidejk329quamk21ozwpex0990qg48k8b1n22eyja8/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

    <script>
      tinymce.init({
        selector: '#mytextarea'
      });
    </script>
    

</head>
<body class="">
	<!-- [ Pre-loader ] start -->
	<div class="loader-bg">
		<div class="loader-track">
			<div class="loader-fill"></div>
		</div>
	</div>

	<?php require "modules/menu.php"?>
	<?php require "modules/cabecera.php"?>


<div class="pcoded-main-container">
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10">Ejemplo </h5>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#"><i class="feather icon-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="#!">Pagina </a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
      
        <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h5>Crear una entrada</h5>
                        <hr>
                        <div class="row">
                            
                            <div class="col-md-12">
                                <form method="post" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label>Titulo</label>
                                        <input type="text" class="form-control" placeholder="Ingresa tu titulo" name="titulo">
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">Publicacion</label>
                                        
                                        <textarea id="mytextarea" name="contenido" class="form-control" rows="9">Hello, World!</textarea>

                                    </div>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" required name="image">
                                        <label class="custom-file-label">Seleccionar imagen...</label>
                                        
                                    </div>
                                     <hr>
                                    <div class="form-group">
                                    <input type="date" name="fecha" required="" class="form-control" />
                                    </div>

                                    <hr>
                                    <div class="form-group">
                                        <select class="custom-select" required name="active">
                                            <option value="">Opciones de visibilidad</option>
                                            <option value="1">Activo</option>
                                            <option value="0">Oculto</option>
                                        </select>
                                    </div>                                
                                    
                                 
                                    <div class="form-control">
                                    <input type="submit" name="entrada" class="btn btn-primary btn-lg ">
                                    </div>
                                </form>
                            </div>
                        </div>
                </div>
            </div>
            </div>
            <!-- [ sample-page ] end -->
        </div>
        <!-- [ Main Content ] end -->
    </div>
</div>
<!-- [ Main Content ] end -->
    <!-- Warning Section start -->
    <!-- Older IE warning message -->
    <!--[if lt IE 11]>
        <div class="ie-warning">
            <h1>Warning!!</h1>
            <p>You are using an outdated version of Internet Explorer, please upgrade
               <br/>to any of the following web browsers to access this website.
            </p>
            <div class="iew-container">
                <ul class="iew-download">
                    <li>
                        <a href="http://www.google.com/chrome/">
                            <img src="../assets/images/browser/chrome.png" alt="Chrome">
                            <div>Chrome</div>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.mozilla.org/en-US/firefox/new/">
                            <img src="../assets/images/browser/firefox.png" alt="Firefox">
                            <div>Firefox</div>
                        </a>
                    </li>
                    <li>
                        <a href="http://www.opera.com">
                            <img src="../assets/images/browser/opera.png" alt="Opera">
                            <div>Opera</div>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.apple.com/safari/">
                            <img src="../assets/images/browser/safari.png" alt="Safari">
                            <div>Safari</div>
                        </a>
                    </li>
                    <li>
                        <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                            <img src="../assets/images/browser/ie.png" alt="">
                            <div>IE (11 & above)</div>
                        </a>
                    </li>
                </ul>
            </div>
            <p>Sorry for the inconvenience!</p>
        </div>
    <![endif]-->
    <!-- Warning Section Ends -->

    <!-- Required Js -->
    <script src="../assets/js/vendor-all.min.js"></script>
    <script src="../assets/js/plugins/bootstrap.min.js"></script>
    <script src="../assets/js/ripple.js"></script>
    <script src="../assets/js/pcoded.min.js"></script>



</body>

</html>
