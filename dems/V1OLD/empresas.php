<!DOCTYPE html>

<?php
	require 'constants/settings.php';
	require 'constants/check-login.php';

	require_once("./config/Db.php");
	$db = new DbPDO();

	if ($myrole == "employee") {
		require 'employee/constants/check-login.php';
	} else {
		require 'employee/constants2/check-login1.php';
	}

	if ($user_online == "true") {
		if ($myrole == "employee" or "employer") {
		} else {
			header("location:login.php");
		}
	} else {
		header("location:login.php");
	}
?>

<html>

<head>
	<!-- Basic Page Info -->
	<meta charset="utf-8">
	<title>IDentiKIT - Explorar empresas</title>

	<link rel="apple-touch-icon" sizes="180x180" href="logov3.png">
	<link rel="icon" type="image/png" sizes="32x32" href="logov3.png">
	<link rel="icon" type="image/png" sizes="16x16" href="logov3.png">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="v1/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="v1/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="v1/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="v1/src/plugins/datatables/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="v1/vendors/styles/style.css">

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-K2NHRZT4R7"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'G-K2NHRZT4R7');
	</script>

	<!-- Google Tag Manager -->
	<script>
		(function(w, d, s, l, i) {
			w[l] = w[l] || [];
			w[l].push({
				'gtm.start': new Date().getTime(),
				event: 'gtm.js'
			});
			var f = d.getElementsByTagName(s)[0],
				j = d.createElement(s),
				dl = l != 'dataLayer' ? '&l=' + l : '';
			j.async = true;
			j.src =
				'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
			f.parentNode.insertBefore(j, f);
		})(window, document, 'script', 'dataLayer', 'GTM-5QGSH5J');
	</script>
	<!-- End Google Tag Manager -->

</head>

<body>



	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QGSH5J" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<div class="header">
		<div class="header-left">
			<div class="menu-icon icon-copy ti-menu" style="color: white;"></div>
			<div class="search-toggle-icon dw dw-search2" style="color: #ffde00;" data-toggle="header_search"></div>
			<div class="header-search">
				<form method="post" action="busqueda.php">
					<div class="form-group mb-0">
						<i class="dw dw-search2 search-icon" style="color: #ffde00;"></i>
						<input type="text" class="form-control search-input" placeholder="Buscar en IDentiKIT" required name="PalabraClave">
						<input name="buscar" type="hidden" class="form-control mb-2" id="inlineFormInput" value="v">
					</div>
				</form>
			</div>
		</div>
		<div class="header-right">
			<div class="user-info-dropdown">
				<div class="dropdown">
					<a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" style="color: #ffde00;">
						<span class="user-icon">
							<?php
							if ($myavatar == null) {
								print '<center><img  src="images/default.png" title="" alt="image"  /></center>';
							} else {
								echo '<center><img alt="image" title=""  src="data:image/png;base64,' . base64_encode($myavatar) . '"/></center>';
							}
							?>
						</span>
					</a>
					<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
						<?php
							if ($user_online == true) {
								print '
								<a class="dropdown-item" href="' . $myrole . '"><i class="dw dw-user1"></i> Pefil</a>
								<!--<a class="dropdown-item" href="#"><i class="dw dw-settings2"></i> Ajustes</a>
								<a class="dropdown-item" href="#"><i class="dw dw-help"></i> Ayuda</a>-->
								<a class="dropdown-item" href="logout.php"><i class="dw dw-logout"></i> Salir</a>';
							} else {
								print '
								<li><a href="login.php">ingresar</a></li>
								<li><a data-toggle="modal" href="#registerModal">registrate</a></li>';
							}
						?>
					</div>
				</div>
			</div>

		</div>
	</div>
	
	<div class="left-side-bar">
		<div class="brand-logo">
			<a href="#">
				<img src="logov3.png" alt="" class="light-logo" width="60"> <span class="mtext"> IDentiKIT</span>
			</a>
			<div class="close-sidebar" data-toggle="left-sidebar-close">
				<i class="ion-close-round"></i>
			</div>
		</div>
		<div class="menu-block customscroll">
			<div class="sidebar-menu">
				<ul id="accordion-menu">
					<li>
						<a href="index.php" class="dropdown-toggle no-arrow ">
							<span class="micon icon-copy fa fa-home"></span><span class="mtext">Inicio</span>
						</a>
					</li>
					<li class="dropdown">
						<a href="javascript:;" class="dropdown-toggle">
							<span class="micon icon-copy fa fa-bandcamp"></span><span class="mtext">Explorar</span>
						</a>
						<ul class="submenu">
							<li><a href="identis.php"> IDentis</a></li>
							<li><a href="works.php"> Trabajos</a></li>
							<li><a href="empresas.php" class="active"> Empresas</a></li>
						</ul>
					</li>
					<a href="aplicados.php" class="dropdown-toggle no-arrow">
						<span class="micon ion-android-done-all"></span><span class="mtext">Aplicados</span>
					</a>
					<li>
						<a href="employee/chat.php" class="dropdown-toggle no-arrow"><span class="micon icon-copy fa fa-comment"></span><span class="mtext">Chat</span></a>
					</li>
					<li>
						<a href="academy.php" class="dropdown-toggle no-arrow ">
							<span class="micon icon-copy fa fa-free-code-camp"></span><span class="mtext">Academia</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="mobile-menu-overlay"></div>

	<div class="main-container">
		<div class="pd-ltr-20 xs-pd-20-10">
			<div class="min-height-200px">
				<div class="col-sm-12 col-md-12 col-lg-4 mb-30">
				</div>
				<div class="page-header">
					<div class="contact-directory-list">
						<ul class="row">
							<?php
								require 'constants/db_config.php';
								try {
									$usuarios = $db->query("SELECT * FROM tbl_users WHERE role = 'employer' ORDER BY RAND()");

									foreach ($usuarios as $usuario) {
										$complogo = $usuario['avatar'];
							?>
							<li class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
								<div class="contact-directory-box">
									<div class="contact-dire-info text-center">
										<div class="contact-avatar">
											<span>
												<?php
													if ($complogo == null) {
														print '<center><img class="autofit2" alt="image"  src="images/blank.png"/></center>';
													} else {
														echo '<center><img class="autofit2" alt="image"  src="data:image/jpeg;base64,' . base64_encode($complogo) . '"/></center>';
													}
												?>
											</span>
										</div>
										<div class="contact-name">
											<h4><?= strip_tags($usuario['first_name']); ?> <?= strip_tags($usuario['last_name']); ?></h4>
											<div class="work text-success">💡 <?= $usuario['title'] ?></div>
											<div class="text-success" style="margin-top: 6px;">📍 <?= $usuario['city']; ?></i></div>
										</div>
										<div class="profile-sort-desc">
											<?= strip_tags(substr($usuario['about'], 0, 90)); ?>...
										</div>
									</div>
									<div class="view-contact">
										<a href="empresa.php?ref=<?=$usuario['member_no']; ?>">Ver empresa</a>
									</div>
								</div>
							</li>
							<?php
							}} catch (PDOException $e) {
							
							}
							?>

						</ul>
					</div>
				</div>


			</div>
		</div>

	</div>
	</div>
	<!-- js -->
	<script src="v1/vendors/scripts/core.js"></script>
	<script src="v1/vendors/scripts/script.min.js"></script>
	<script src="v1/vendors/scripts/process.js"></script>
	<script src="v1/vendors/scripts/layout-settings.js"></script>
</body>

</html>