<?php 

	//require 'constants/settings.php'; 
	//require 'constants/check-login.php';

	require_once("./config/Db.php");
	$db = new DbPDO();

	//require 'employee/constants/check-login.php';
	

	// if ($myrole == "employer") {
	// 	require 'employee/constants2/check-login1.php';
	// }else{
		require 'employee/constants/check-login.php';
	// }

	//require 'constants/db_config.php'; 

	if (isset($_GET['identiwork'])) {
		$jobid = $_GET['identiwork'];
	try {
		$trabajos = $db->query("SELECT * FROM tbl_jobs WHERE job_id = :jobid",array("jobid"=>$jobid));
		$rec = count($trabajos);

		if ($rec == "0") {
			header("location:./");	
		}
	else{

    	foreach($trabajos as $trabajo)
    	{
			$jobtitle       = $trabajo['title'];
			$jobcity        = $trabajo['city'];
			$jobcountry     = $trabajo['country'];
			$jobcategory    = $trabajo['category'];
			$jobtype        = $trabajo['type'];
			$experience     = $trabajo['experience'];
			$jobdescription = $trabajo['description'];
			$jobreq         = $trabajo['requirements'];
			$closingdate    = $trabajo['closing_date'];
			$opendate       = $trabajo['date_posted'];
			$compid         = $trabajo['company'];
			$tags           = $trabajo['tech'];

			if ($jobtype == "Freelance") {
				$sta = '<span class="label label-success">Freelance</span>';								
			}
			if ($jobtype == "Part-time") {
				$sta = '<span class="label label-danger">Part-time</span>';									
			}
			if ($jobtype == "Full-time") {
				$sta = '<span class="label label-warning">Full-time</span>';								  
			}
		}
	}}catch(PDOException $e){
		echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    }

}else{
	header("location:./");	
}


try {
	$usuarios = $db->query("SELECT * FROM tbl_users WHERE member_no = :compid",array("compid"=>$compid));
    foreach($usuarios as $usuario){
		$compname = $usuario['first_name'];
		$complogo = $usuario['avatar'];
		$compbout = $usuario['about'];
	}}catch(PDOException $e){
		echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    }

	$today_date = strtotime(date('Y/m/d'));
	$last_date = date_format(date_create_from_format('Y-m-d', $closingdate), 'Y/m/d');
	$post_date = date_format(date_create_from_format('Y-m-d', $closingdate), 'd');
	$post_month = date_format(date_create_from_format('Y-m-d', $closingdate), 'F');
	$post_year = date_format(date_create_from_format('Y-m-d', $closingdate), 'Y');
	$conv_date = strtotime($last_date);

	if ($today_date > $conv_date){
		$jobexpired = true;
	} else {
		$jobexpired = false;
	}


?>

<!DOCTYPE html>
<html>
<head>
	
	<meta charset="utf-8">
	<title>Postulate para <?php echo strip_tags($jobtitle); ?> en IDentiKIT</title>

	<meta property="og:image" content="https://identikit.app/logowebog.png" />
	<meta property="og:image:secure_url" content="https://identikit.app/logowebog.png" />
	<meta property="og:image:type" content="image/png" />
	<meta property="og:image:width" content="300" />
	<meta property="og:image:height" content="300" />
	<meta property="og:image:alt" content="IDentiKIT - Tu primera experiencia laboral" />
	<meta property="og:description" content="<?php echo strip_tags(substr($jobdescription,0, 200));?>" />
	
	<link rel="apple-touch-icon" sizes="180x180" href="logov3.png">
	<link rel="icon" type="image/png" sizes="32x32" href="logov3.png">
	<link rel="icon" type="image/png" sizes="16x16" href="logov3.png">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="v1/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="v1/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="v1/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="v1/src/plugins/datatables/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="v1/vendors/styles/style.css">
	<link rel="stylesheet" type="text/css" href="v1/src/plugins/sweetalert2/sweetalert2.css">
	

	<script type="text/javascript">
		function update(str,str1){
			var txt;
			var r = confirm("Seguro que quieres aplicar a este trabajo?");
			if (r == true) {
				document.getElementById("data").innerHTML = "Aplicando...";
				var xmlhttp; 	
			if (window.XMLHttpRequest){
				xmlhttp=new XMLHttpRequest();
			}else{
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}	

      	xmlhttp.onreadystatechange = function() {
        	if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
          		document.getElementById("data").innerHTML = xmlhttp.responseText;
        	}
      	}

      	xmlhttp.open("GET","app/apply-job.php?opt="+str+"&opt1="+str1, true);
      	xmlhttp.send();
    } else {

    }

  }
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-K2NHRZT4R7"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-K2NHRZT4R7');
</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5QGSH5J');</script>
<!-- End Google Tag Manager -->

</head>
<body>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QGSH5J"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->


	<div class="header">
		<div class="header-left">
			<div class="menu-icon icon-copy ti-menu" style="color: white;"></div>
			<div class="search-toggle-icon dw dw-search2" style="color: #ffde00;" data-toggle="header_search"></div>
			<div class="header-search">
				<form method="post" action="busqueda.php" >
					<div class="form-group mb-0" >
						<i class="dw dw-search2 search-icon" style="color: #ffde00;"></i>
						<input type="text" class="form-control search-input" placeholder="Buscar en IDentiKIT" required name="PalabraClave">
						<input name="buscar" type="hidden" class="form-control mb-2" id="inlineFormInput" value="v">
					</div>
				</form>
			</div>
		</div>
		<div class="header-right">
			<div class="user-info-dropdown">
				<div class="dropdown">
					<a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" style="color: #ffde00;">
						<span class="user-icon">
						<?php 
							if ($myavatar == null) {
								print '<center><img  src="images/default.png" title="" alt="image"  /></center>';
							}else{
								echo '<center><img alt="image" title=""  src="data:image/png;base64,'.base64_encode($myavatar).'"/></center>';	
							}
						?>											
						</span>
					</a>
					<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
						<?php
						if ($user_online == true) {
						print '
							<a class="dropdown-item" href="'.$myrole.'"><i class="dw dw-user1"></i> Perfil</a>
							<!--<a class="dropdown-item" href="#"><i class="dw dw-settings2"></i> Ajustes</a>
							<a class="dropdown-item" href="#"><i class="dw dw-help"></i> Ayuda</a>-->
							<a class="dropdown-item" href="logout.php"><i class="dw dw-logout"></i> Salir</a>';
						}else{
						print '
							<a class="dropdown-item" href="login.php"><i class="dw dw-lock"></i> Ingresar</a>
							<a class="dropdown-item"  href="registro.php"><i class="dw dw-user1"></i> Registrate</a>';						
						}
						
						?>
					</div>
				</div>
			</div>
			
		</div>
	</div>

	<div class="left-side-bar">
		<div class="brand-logo">
			<a href="#">
				<img src="logov3.png" alt="" class="light-logo" width="60"> <span class="mtext"> IDentiKIT</span>
			</a>
			<div class="close-sidebar" data-toggle="left-sidebar-close">
				<i class="ion-close-round"></i>
			</div>
		</div>
		<div class="menu-block customscroll">
			<div class="sidebar-menu">
				<ul id="accordion-menu">
					<?php
						if ($myrole == "employee") {
							print '<a href="index.php" class="dropdown-toggle no-arrow ">
							<span class="micon icon-copy fa fa-home"></span><span class="mtext">Inicio</span></a>

							<li class="dropdown">
								<a href="javascript:;" class="dropdown-toggle">
									<span class="micon icon-copy fa fa-bandcamp"></span><span class="mtext">Explorar</span>
								</a>
								<ul class="submenu">
									<li><a href="identis.php" > IDentis</a></li>
									<li><a href="works.php" class="active"> Trabajos</a></li>
									<li><a href="empresas.php"> Empresas</a></li>
								</ul>
							</li>

							<li>
								<a href="aplicados.php" class="dropdown-toggle no-arrow">
									<span class="micon ion-android-done-all"></span><span class="mtext">Aplicados</span>
								</a>
							</li>

							<li>
								<a href="academy.php" class="dropdown-toggle no-arrow ">
									<span class="micon icon-copy fa fa-free-code-camp"></span><span class="mtext">Academia</span>
								</a>
							</li>';
							} if ($myrole == "employer") {
								print '<a href="'.$myrole.'/index.php" class="dropdown-toggle no-arrow ">
								<span class="micon icon-copy fa fa-home"></span><span class="mtext">Panel</span></a>
								<a href="identis.php" class="dropdown-toggle no-arrow"><span class="micon icon-copy fa fa-bandcamp"></span><span class="mtext">identis</span></a>';
							}
							if ($myrole == null)  {
								print '<a href="login.php" class="dropdown-toggle no-arrow ">
								<span class="micon icon-copy fa fa-lock"></span><span class="mtext">Iniciar sesion</span></a>
								<a href="registro.php" class="dropdown-toggle no-arrow ">
								<span class="micon icon-copy fa fa-user"></span><span class="mtext">Registrarme</span></a>';
							}
						?>
				</ul>
			</div>
		</div>
	</div>

	<div class="mobile-menu-overlay"></div>

	<div class="main-container">
		<div class="pd-ltr-20 xs-pd-20">
				<div class="page-header">
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="title">
								<h4><?php echo strip_tags($jobtitle); ?></h4>
							</div>
							<nav aria-label="breadcrumb" role="navigation">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="works.php">Trabajos</a></li>
									<li class="breadcrumb-item active" aria-current="page"><?php echo strip_tags($compname); ?></li>
								</ol>
							</nav>
						</div>
					</div>
				</div>
				<style>
					a.heart_mark {
						width: 40px;
						height: 40px;
						-webkit-border-radius: 5px;
						-moz-border-radius: 5px;
						border-radius: 5px;
						color: #00c1ff;
						font-size: 14px;
						line-height: 40px;
						text-align: center;
						display: inline-block;
						background: #EFFDF5;
						margin-right: 15px;
						
					}

					a.heart_mark:hover {
						background: #ff0047;
					}

					i.ti-heart {
						color: #00c1ff;
					}

					a.heart_mark:hover>i.ti-heart {
						color: white;
					}
				</style>
				<div class="product-wrap">
					<div class="product-detail-wrap mb-30">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<div class="product-detail-desc pd-20 card-box height-100-p">
									<div class="pull-right">
										<?php
											if ($user_online == true) {
												$background  = '';
												$color       = '';
												
												$isCheckedHeart = $db->query("select like_id from tbl_likes where like_user_id=:like_user_id and like_post_job=:like_post_job", array("like_user_id" => $_SESSION["myid"], "like_post_job" => $jobid));
												
												if ($isCheckedHeart != null) {
													$background = "#ff0047";
													$color      = "white";
												}
												
												
												echo '<a class="heart_mark" value=' . $jobid . ' style="background:' . $background . '; cursor:pointer">';
												echo '<i class="ti-heart" id="heart" style="color:'.$color.'"></i></a>';
												
											} else {
												echo '<a class="heart_mark" style="cursor:pointer">';
												echo '<i class="ti-heart" id="heart"></i></a>';
											}		
										?>
									</div>

									<h4 class="mb-20 pt-20"><?php echo strip_tags($jobtitle); ?> en <a href="empresa.php?ref=<?php echo "$compid"; ?>"><?php echo strip_tags($compname); ?></a> </h4>
									<div class="text-success" style="margin-top: 6px; ">📍 <?=$jobcity ?> </i>📙 <?php echo $experience;?> </div>
									<div class="text-success" style="margin-top: 6px; ">🕐 Este puesto expira el <?php echo $closingdate?></div>
									<div class="text-success" style="margin: 15px 0px 15px 0px;">
										<?php 
											$tags = explode(",", $tags);

											foreach($tags as $i =>$key) {
												print '<span class="badge badge-pill" style="color: #01c0fe; background-color: whitesmoke; margin: 5px;">'."  $key ".'</span>';
											}
										?>
									</div>
									
									<h5 class="mb-20 pt-20">Descripcion</h5>
									<p><?php echo $jobdescription; ?></p>
									<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-8712287039560329"
     crossorigin="anonymous"></script>
<ins class="adsbygoogle"
     style="display:block"
     data-ad-format="fluid"
     data-ad-layout-key="-gw-3+1f-3d+2z"
     data-ad-client="ca-pub-8712287039560329"
     data-ad-slot="1371025174"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>
									
									
									<div class="row">
										<div class="col-md-12 col-12">
										<p id="data"></p>
											<?php
												if ($user_online == true) {
													if ($jobexpired == true) {
														print '<button class="btn btn-primary disabled btn-hidden btn-lg collapsed"><i class="flaticon-line-icon-set-calendar"></i>	Este empleo ya expiro</button>';
													} else {
													if ($myrole == "employee") {
														print '<button';?> onclick="update(this.value, <?php echo "'$jobtitle'"; ?>)" <?php print ' value="'.$jobid.'" class="btn btn-primary btn-hidden btn-lg collapsed col-md-12"><i class="flaticon-line-icon-set-pencil"></i>Aplicar</button>';
													} else {
														print '<button class="btn btn-primary btn-block disabled btn-hidden btn-lg collapsed"><i class="flaticon-line-icon-set-padlock"></i> Solo los IDentis pueden aplicar!</button>';
													}	
												}
												}else{										
													print '<button class="btn btn-primary btn-block disabled btn-hidden btn-lg collapsed"><i class="flaticon-line-icon-set-padlock"></i> <a href="login.php" style="color:black;">Iniciar sesion</a> o <a href="registro.php" style="color:black;">registrarte</a> </button>';	
												}
											?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<h4 class="mb-20">Más trabajos</h4>
					<div class="product-list">
						<ul class="row">
						<?php
							try {
                            	$trabajos = $db->query("SELECT * FROM tbl_jobs ORDER BY RAND() limit 3");
                            	
                            	foreach($trabajos as $trabajo) {
									$jobcity     = $trabajo['city'];
									$jobcountry  = $trabajo['country'];
									$type        = $trabajo['type'];
									$title       = $trabajo['title'];
									$closingdate = $trabajo['closing_date'];
									$company_id  = $trabajo['company'];
						
									$usuarios = $db->query("SELECT * FROM tbl_users WHERE member_no = :companyId and role = 'employer'",array(":companyId"=>$company_id));

									foreach($usuarios as $usuario) {
										$complogo = $usuario['avatar'];	
									}
							?>
							<li class="col-lg-4 col-md-6 col-sm-12">
								<div class="product-box">
									<div class="product-caption">
										<h4><a href="#"><?php echo strip_tags($trabajo['title']); ?></a></h4>
										<div class="text-success" style="margin-top: 6px;">💡 <?php echo strip_tags($trabajo['city']);?>
										📍 <?php echo strip_tags($trabajo['city']); ?></i></div>
											
											<div class="text-success"></div>
										<div class="price">
										<p><?php echo strip_tags(substr($trabajo['description'], 0, 200)); ?>...</p>
										
										</div>
										<a href="work-detail.php?identiwork=<?=$trabajo['job_id']; ?>" class="btn btn-primary col-md-12">Postularme</a>
									</div>
									
							
							</li>
						
							<?php
								}}catch(PDOException $e){ 
					
								}
                            ?>
						</ul>
					</div>
				</div>
			</div>
			
		</div>
	</div>

	<!-- js -->
	<script src="v1/vendors/scripts/core.js"></script>
	<script src="v1/vendors/scripts/script.min.js"></script>
	<script src="v1/vendors/scripts/process.js"></script>
	<script src="v1/vendors/scripts/layout-settings.js"></script>
	<!-- Slick Slider js -->
	<script src="v1/src/plugins/slick/slick.min.js"></script>
	<!-- bootstrap-touchspin js -->
	<script src="v1/src/plugins/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
	<script src="v1/src/plugins/sweetalert2/sweetalert2.all.js"></script>
	<script src="v1/src/plugins/sweetalert2/sweet-alert.init.js"></script>
	
	<script type="text/javascript">
		$(".heart_mark").click(function() {
			var valor = $(this)[0].getAttribute("value");
			if (valor != null){
				$.ajax({
					type: 'GET',
					url: 'app/add_like.php',
					data: {'job_id': valor}
				}).done(function(){
					window.location.reload();
				});
			} else {
				$("#myModal").modal("show");
			}
		});
	</script>

	<div class="modal" tabindex="-1" role="dialog"  id="myModal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			<div class="modal-header">
				<center>
					<h5 class="modal-title">No has iniciado sesión</h5>
				</center>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<center>
					<style>
						#logoModal{
							max-width: 20%;
    						height: 20%;
							padding-bottom: 7%;
						}
					</style>
				<img  src="images/default.png" id="logoModal" alt="logo" width="30%" height="30%" />
			
				</center>
				<div class="row">

					<div class="col-6">
						<a type="submit" class="btn btn-primary btn-sm btn-block"  href="login.php">Iniciar Sesión</a>
					</div>
					<div class="col-6">
						<a class="btn btn-primary btn-sm btn-block" href="registro.php">Crear cuenta nueva</a>
					</div>
				</div>				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
			</div>
			</div>
		</div>
	</div>

</body>
</html>