	<!DOCTYPE html>
	<html>

	<?php

		require 'app/views/employee/constants/check-login.php';

		require 'config/settings.php';
		require 'config/check-login.php';

		require_once("db/db.php");
		$db = new DbPDO();

		$fromsearch = false;

		if (isset($_GET['search']) && $_GET['search'] == "✓") {
		} else {
		}
		if (isset($_GET['country']) && ($_GET['category'])) {
			$cate = $_GET['category'];
			$country = $_GET['country'];
			$query1 = "SELECT * FROM tbl_jobs WHERE category = :cate AND city = :country ORDER BY enc_id DESC";
			$query2 = "SELECT * FROM tbl_jobs WHERE category = :cate AND city = :country ORDER BY enc_id DESC";
			$fromsearch = true;

			$slc_country = "$country";
			$slc_category = "$cate";
			$title = "$slc_category empleos en $slc_country";
		} else {
			$query1 = "SELECT * FROM tbl_jobs ORDER BY enc_id DESC";
			$query2 = "SELECT * FROM tbl_jobs ORDER BY enc_id DESC";
			$slc_country = "NULL";
			$slc_category = "NULL";
		}
		if (isset($myrole)) {
			if ($myrole == 'employee') {
				require 'config/check-login.php';
			} else {
				require 'app/views/employee/constants2/check-login1.php';
			}
		}
	?>




	<head>
		<!-- Basic Page Info -->
		<meta charset="utf-8">
		<title>IDentiKIT - Explorar </title>

		<link rel="apple-touch-icon" sizes="180x180"    href="public/img/identikit/logo.png">
		<link rel="icon" type="image/png" sizes="32x32" href="public/img/identikit/logo.png">
		<link rel="icon" type="image/png" sizes="16x16" href="public/img/identikit/logo.png">

		<!-- Mobile Specific Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

		<!-- Google Font -->
		<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
		<!-- CSS -->
		<link rel="stylesheet" type="text/css" href="public/complements/v1/vendors/styles/core.css">
		<link rel="stylesheet" type="text/css" href="public/complements/v1/vendors/styles/icon-font.min.css">
		<link rel="stylesheet" type="text/css" href="public/complements/v1/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
		<link rel="stylesheet" type="text/css" href="public/complements/v1/src/plugins/datatables/css/responsive.bootstrap4.min.css">
		<link rel="stylesheet" type="text/css" href="public/complements/v1/vendors/styles/style.css">
		<link rel="stylesheet" type="text/css" href="public/complements/v1/src/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">

		<meta property="og:image" content="https://identikit.app/logowebog.png" />
		<meta property="og:image:secure_url" content="https://identikit.app/logowebog.png" />
		<meta property="og:image:type" content="image/png" />
		<meta property="og:image:width" content="300" />
		<meta property="og:image:height" content="300" />
		<meta property="og:image:alt" content="IDentiKIT - Tu primera experiencia laboral" />
		<meta property="og:description" content="😎Tu primer trabajo <TECH> en empresas de alto rendimiento a un click de distancia en un solo lugar.📣 Fácil, rápido y seguro" />

		

		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=G-K2NHRZT4R7"></script>
		<script>
			window.dataLayer = window.dataLayer || [];

			function gtag() {
				dataLayer.push(arguments);
			}
			gtag('js', new Date());

			gtag('config', 'G-K2NHRZT4R7');
		</script>

		<!-- Google Tag Manager -->
		<script>
			(function(w, d, s, l, i) {
				w[l] = w[l] || [];
				w[l].push({
					'gtm.start': new Date().getTime(),
					event: 'gtm.js'
				});
				var f = d.getElementsByTagName(s)[0],
					j = d.createElement(s),
					dl = l != 'dataLayer' ? '&l=' + l : '';
				j.async = true;
				j.src =
					'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
				f.parentNode.insertBefore(j, f);
			})(window, document, 'script', 'dataLayer', 'GTM-5QGSH5J');
		</script>
		<!-- End Google Tag Manager -->
		
		

	</head>

	<body>
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QGSH5J" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->

		<?php include 'app/views/employee/layouts/Header.php';?>
		<?php include 'app/views/employee/layouts/Sidebar-menu.php';?>

		<div class="mobile-menu-overlay"></div>

		<div class="main-container">

			

		</div>
	</div>

	</div>


		<div class="col-lg-12 col-md-12 col-sm-12 mb-30">

			<div class="pd-20 card-box height-100-p">
				
			<div class="row">
				
				<div class="col-lg-12 col-md-12 col-sm-12 mb-30">
					<div class="pd-20 card-box height-100-p">

							<?php
						try {
							$categories = $db->query("SELECT * FROM tbl_categories ORDER BY category");
							foreach ($categories as $category) {

								$cat = $category['category'];
								?>
						<div class="list-group">
							<a href="#" class="list-group-item list-group-item-action"><?= $category['category']; ?></a>
							<br>
						</div>
							<?php
							}
						} catch (PDOException $e) {

						} ?>
					</div>
				</div>

			</div>



			</div>

		</div>

	</div>





		<!-- js -->
		<script src="public/complements/v1/vendors/scripts/core.js"></script>
		<script src="public/complements/v1/vendors/scripts/script.min.js"></script>
		<script src="public/complements/v1/vendors/scripts/process.js"></script>
		<script src="public/complements/v1/vendors/scripts/layout-settings.js"></script>
		<script src="public/complements/v1/src/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
		
		

		


	</body>

	</html>