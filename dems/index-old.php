<!DOCTYPE html>
<html>

<?php 
	include 'config/settings.php'; 
	include 'config/check-login.php';

	require_once("db/db.php");
	$db = new DbPDO();

	// if ($myrole == "employer") {
	// 	require 'config/check-login.php';
	// }else{
	// 	require 'app/views/employee/constants/check-login.php';
	// }
?>


<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Bienvenid@ a IDentiKIT </title>
	<meta name="description" content="Conectamos talento joven con empresas de una forma fácil, rápida y segura." />
	<meta name="keywords" content="trabajo, empleos, cv, curriculum, empresas, carreras, primera experiencia, bolsa de trabajo, trabajo joven, adolescentes, primer trabajo" />
	<meta name="author" content="IDentiKIT">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta property="og:image" content="http://<?php echo "$actual_link"; ?>/landingv1/v3/images/logo-og.png" />
    <meta property="og:image:secure_url" content="https://<?php echo "$actual_link"; ?>/landingv1/v3/images/logo-og.png" />
    <meta property="og:image:type" content="image/png" />
    <meta property="og:image:width" content="300" />
    <meta property="og:image:height" cosntent="300" />
    <meta property="og:image:alt" content="IDentiKIT - Tu primera experiencia laboral" />
    <meta property="og:description" content="Tu primera experiencia laboral, facil, rapido y seguro." />

	<link rel="apple-touch-icon" sizes="180x180" href="logo.png">
	<link rel="icon" type="image/png" sizes="32x32" href="logo.png">
	<link rel="icon" type="image/png" sizes="16x16" href="logo.png">

	<link href="public/complements/func/css/animate.css" rel="stylesheet">
	<link href="public/complements/func/css/main.css" rel="stylesheet">
	<link href="public/complements/func/css/component.css" rel="stylesheet">
	
	<link rel="stylesheet" href="public/complements/func/icons/linearicons/style.css">
	<link rel="stylesheet" href="public/complements/func/icons/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="public/complements/func/icons/simple-line-icons/css/simple-line-icons.css">
	<link rel="stylesheet" href="public/complements/func/icons/ionicons/css/ionicons.css">
	<link rel="stylesheet" href="public/complements/func/icons/pe-icon-7-stroke/css/pe-icon-7-stroke.css">
	<link rel="stylesheet" href="public/complements/func/icons/rivolicons/style.css">
	<link rel="stylesheet" href="public/complements/func/icons/flaticon-line-icon-set/flaticon-line-icon-set.css">
	<link rel="stylesheet" href="public/complements/func/icons/flaticon-streamline-outline/flaticon-streamline-outline.css">
	<link rel="stylesheet" href="public/complements/func/icons/flaticon-thick-icons/flaticon-thick.css">
	<link rel="stylesheet" href="public/complements/func/icons/flaticon-ventures/flaticon-ventures.css">

	<link href="public/complements/func/css/style.css" rel="stylesheet">

	<link rel="stylesheet" href="public/complements/web/css/bootstrap.min.css">
	<link rel="stylesheet" href="public/complements/web/css/owl.carousel.min.css">
	<link rel="stylesheet" href="public/complements/web/css/magnific-popup.css">
	<link rel="stylesheet" href="public/complements/web/css/font-awesome.min.css">
	<link rel="stylesheet" href="public/complements/web/css/themify-icons.css">
	<link rel="stylesheet" href="public/complements/web/css/nice-select.css">
	<link rel="stylesheet" href="public/complements/web/css/gijgo.css">
	<link rel="stylesheet" href="public/complements/web/css/animate.min.css">
	<link rel="stylesheet" href="public/complements/web/css/slicknav.css">

	<link rel="stylesheet" href="public/complements/web/css/style.css">

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-PHBQLSG');</script>
	<!-- End Google Tag Manager -->
	

	<style type="text/css">
		/* fuentes en nuestro disco duro */
		@font-face {
			font-family: "moderna";
			src: url("moderna.TTF") format("truetype");
		}

		@font-face {
			font-family: "MYRIADPRO-REGULAR";
			src: url("fonts/MYRIADPRO-REGULAR.otf") format("opentype");
		}

		.autofit2 {
			width: 100px;
			border-radius: 50%;
		}

		.autofit3 {
			height: 80px;
			width: 100px;
			object-fit: cover;
		}
	</style>

</head>

<body class="home">

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PHBQLSG"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<div class="container-wrapper">
		<header>
        	<div class="header-area ">
				<div id="sticky-header" class="main-header-area">
					<div class="container-fluid ">
						<div class="header_bottom_border">
							<div class="row align-items-center">
								<div class="col-xl-3 col-lg-2">
									<div class="logo">
										<a href="">
											<img src="public/img/identikit/logo.png" alt="" width="50">
										</a>
									</div>
								</div>
								<div class="col-xl-6 col-lg-7">
									<div class="main-menu  d-none d-lg-block">
										<nav>
											<ul id="navigation">
												<li><a href="#">Inicio</a></li>
												<li><a href="app/views/employee/works.php">Trabajos</a></li>
												<li><a href="app/views/employee/academy.php">Academia</a></li>
											</ul>
										</nav>
									</div>
								</div>
								<div class="col-xl-3 col-lg-3 d-none d-lg-block">
									<div class="Appointment ">
										<ul class="nav-mini sign-in">
											<?php
													if ($myrole == "employer") {
														if ($user_online == true) {

															print '
																<li><a href="app/controllers/authcontroller.php?logout">Cerrar Sesión</a></li>
																<li><a href="app/views/'.$myrole.'/micuenta.php">Perfil</a></li>';
															}else{
																print '
																	<li><a href="app/views/auth/login.php">Ingresar</a></li>
																	<li><a href="app/views/auth/registro.php">Registrate</a></li>';                        
															}  
														} else { 
															if ($user_online == true) {

																print '
																	<li><a href="app/controllers/authcontroller.php?logout">Cerrar Sesión</a></li>
																	<li><a href="app/views/'.$myrole.'/miperfil.php">Perfil</a></li>';
																}else{
																	print '
																		<li><a href="app/views/auth/login.php">Ingresar</a></li>
																		<li><a href="app/views/auth/registro.php">Registrate</a></li>';                        
																}  
													}                   

														
											?>
										</ul>
									</div>
								</div>
								<div class="col-12">
									<div class="mobile_menu d-block d-lg-none">
										<ul class="nav-mini sign-in">
											<?php
												if ($user_online == true) {
												print '
													<li><a href="app/controllers/authcontroller.php?logout">Cerrar Sesión</a></li>
													<li><a href="app/views/'.$myrole.'/miperfil.php">Perfil</a></li>';
												}else{
												print '
													<li><a href="app/views/auth/login.php">Ingresar</a></li>
													<li><a href="app/views/auth/registro.php">Registrate</a></li>';                        
												}
											?>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
        	</div>
    	</header>

		<div class="main-wrapper">
			<div class="hero" style="background-image:url('https://identikit.app/landing/video.gif');">
				<div class="container" >
					<h1 style="font-family: 'moderna' !important;">IDentiKIT</h1>
					<p>Tu primera experiencia laboral</p>
					<div class="main-search-form-wrapper">
						<form method="post" action="app/views/busqueda.php" >
							<div class="form-group mb-0" >
								<input type="text" class="form-control search-input" placeholder="Buscar en IDentiKIT" required name="PalabraClave" autocomplete="off">
								<input name="buscar" type="hidden" class="form-control mb-2" id="inlineFormInput" value="v">
							</div>	
							<div class="btn-holder">
								<button name="search" value="✓" type="submit" class="btn"><i class="ion-android-search" style="color: #ffde00;" data-toggle="header_search"></i></button>
							</div>
						</form>	
					</div>
				</div>
			</div>
			
			<br><br>

			<!-- <div class="owl-carousel owl-theme">
    <div class="item"><h4>1</h4></div>
    <div class="item"><h4>2</h4></div>
    <div class="item"><h4>3</h4></div>
    <div class="item"><h4>4</h4></div>
    <div class="item"><h4>5</h4></div>
    <div class="item"><h4>6</h4></div>
    <div class="item"><h4>7</h4></div>
    <div class="item"><h4>8</h4></div>
    <div class="item"><h4>9</h4></div>
    <div class="item"><h4>10</h4></div>
    <div class="item"><h4>11</h4></div>
    <div class="item"><h4>12</h4></div>
</div> -->

			<!-- job_listing_area_start  -->
			<div class="job_listing_area">   	
			    <!--/ catagory_area -->
			    <div class="container">
			        <div class="row align-items-center">
			            <div class="col-lg-6">
			                <div class="section_title">
			                    <h3>Ultimos empleos</h3>
			                </div>
			            </div>

			            <div class="col-lg-6">
			                <div class="brouse_job text-right">
			                    <a href="app/views/employee/works.php" class="boxed-btn4">Explorar</a>
			                </div>
			            </div>
			        </div>	

					<div class="job_lists">
						<div class="row">
							<div class="col-lg-12 col-md-12">
								<?php

								try {
									$trabajos = $db->query("SELECT * FROM tbl_jobs ORDER BY enc_id DESC LIMIT 8");

									foreach ($trabajos as $trabajo) {
										$jobcity     = $trabajo['city'];
										$jobcountry  = $trabajo['country'];
										$type        = $trabajo['type'];
										$title       = $trabajo['title'];
										$closingdate = $trabajo['closing_date'];
										$company_id  = $trabajo['company'];
										$job_id      = $trabajo['job_id'];

										$last_date   = date_format(date_create_from_format('Y-m-d', $closingdate), 'Y/m/d');
										$post_date   = date_format(date_create_from_format('Y-m-d', $closingdate), 'd');
										$post_month  = date_format(date_create_from_format('Y-m-d', $closingdate), 'F');
										$post_year   = date_format(date_create_from_format('Y-m-d', $closingdate), 'Y');
										$usuarios    = $db->query("SELECT * FROM tbl_users WHERE member_no = :company and role = 'employer'", array("company" => $company_id));

										foreach ($usuarios as $usuario) {
											$complogo    = $usuario['avatar'];
											$thecompname = $usuario['first_name'];
										}
								?>
										<div class="single_jobs white-bg d-flex justify-content-between">
											<div class="jobs_left d-flex align-items-center">
											
												<div class="jobs_conetent">
													<a href="app/views/employee/work-detail.php?identiwork=<?= $job_id ?>">
														<h4><?= $title ?></h4>
													</a>
													<div class="links_locat d-flex align-items-center">
														<div class="location">
															<p>📍 <?php echo strip_tags($jobcity) ?>, <?php echo strip_tags($jobcountry) ?></p>
														</div>
														<div class="location">
															<p>📙 <?php echo strip_tags($type); ?></p>
														</div>
													</div>
												</div>
											</div>
											<div class="jobs_right">
												<div class="apply_now">
													<?php
														if ($user_online == true) {
															$isCheckedHeart = $db->query("select like_id from tbl_likes where like_user_id=:like_user_id and like_post_job=:like_post_job", array("like_user_id" => $_SESSION["myid"], "like_post_job" => $job_id));
															$background = '';
															$color       = '';
															if ($isCheckedHeart != null) {
																$background = "#ff0047";
																$color      = "white";
															}
															echo '<a class="heart_mark" value=' . $job_id . ' style="background:' . $background . '; color:' . $color . '; cursor:pointer">';
															echo '<i class="ti-heart" id="heart"></i></a>';
														} else {
															echo '<a class="heart_mark" style="cursor:pointer">';
															echo '<i class="ti-heart" id="heart"></i></a>';
														}
													?>

													<a href="app/views/employee/work-detail.php?identiwork=<?= $job_id ?>" class="boxed-btn3">Postularme</a>
												</div>

												<div class="date">
													<p>Finaliza: <?= $post_date; ?> <?= $post_month ?>, <?= $post_year ?></p>
												</div>
											</div>
											
											
										</div>

<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-8712287039560329"
     crossorigin="anonymous"></script>
<ins class="adsbygoogle"
     style="display:block"
     data-ad-format="fluid"
     data-ad-layout-key="-gw-3+1f-3d+2z"
     data-ad-client="ca-pub-8712287039560329"
     data-ad-slot="6618293056"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>
								<?php
									}
								} catch (PDOException $e) {
									echo 'Excepción capturada: ',  $e->getMessage(), "\n";
								}
								?>
							</div>
						</div>
					</div>
			        </div>
			    </div>

			
			
			<!-- popular_catagory_area_start  -->
			<div class="popular_catagory_area">
			    <div class="container">
			        <div class="row">
			            <div class="col-lg-12">
			                <div class="section_title mb-40">
			                    <h3>Categorias populares</h3>
			                </div>
			            </div>
			        </div>
			        <div class="row">
			            <?php
							$Categorias = $db->query("SELECT id,category FROM tbl_categories ORDER BY RAND() DESC LIMIT 8");

							try{
                            	foreach($Categorias as $Categoria) {
									$id = $Categoria['id'];
									$category = $Categoria['category'];
						?>

			            <div class="col-lg-4 col-xl-3 col-md-6">
			                <div class="single_catagory">
			                    <a href="app/views/employee/works.php?category=<?=$category ?>&search=✓"><h4><?=$category?></h4></a>
			                    <p> <span>Disponible</span> </p>
			                </div>
			            </div>

			            <?php
                            }}catch(PDOException $e){ 
								echo 'Excepción capturada: ',  $e->getMessage(), "\n";
                            }
                        ?>

			            
			        </div>
			    </div>
			</div>
			<!-- popular_catagory_area_end  -->
			
			<footer class="footer-wrapper">
				<div class="bottom-footer">	
					<div class="container">
						<div class="row">						
							<div class="col-sm-4 col-md-4">
								<p class="copy-right" style="color: white;">&#169; Copyright <?= date('Y'); ?> IDentiKIT</p>
							</div>
							
							<div class="col-sm-4 col-md-4">
								<ul class="bottom-footer-menu">
									<li><a ></a></li>
								</ul>
							</div>
							
							<div class="col-sm-4 col-md-4">
								<ul class="bottom-footer-menu for-social">
									<li><a href="https://twitter.com/identikitA" target="_blank"><i class="ri ri-twitter" data-toggle="tooltip" data-placement="top" title="twitter"></i></a></li>
									<li><a href="https://www.facebook.com/Identikit-111822167615589" target="_blank"><i class="ri ri-facebook" data-toggle="tooltip" data-placement="top" title="facebook"></i></a></li>
									<li><a href="https://instagram.com/identikit_app" target="_blank"><i class="ri ri-instagram" data-toggle="tooltip" data-placement="top" title="instagram"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</footer>

		</div>
	</div>
</body>


<script src="public/complements/web/js/vendor/modernizr-3.5.0.min.js"></script>
<script src="public/complements/web/js/vendor/jquery-1.12.4.min.js"></script>
<script src="public/complements/web/js/popper.min.js"></script>
<script src="public/complements/web/js/bootstrap.min.js"></script>
<!-- <script src="public/complements/web/js/owl.carousel.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<script src="public/complements/web/js/isotope.pkgd.min.js"></script>
<script src="public/complements/web/js/ajax-form.js"></script>
<script src="public/complements/web/js/waypoints.min.js"></script>
<script src="public/complements/web/js/jquery.counterup.min.js"></script>
<script src="public/complements/web/js/imagesloaded.pkgd.min.js"></script>
<script src="public/complements/web/js/scrollIt.js"></script>
<script src="public/complements/web/js/jquery.scrollUp.min.js"></script>
<script src="public/complements/web/js/wow.min.js"></script>
<script src="public/complements/web/js/nice-select.min.js"></script>
<script src="public/complements/web/js/jquery.slicknav.min.js"></script>
<script src="public/complements/web/js/jquery.magnific-popup.min.js"></script>
<script src="public/complements/web/js/plugins.js"></script>
<script src="public/complements/web/js/gijgo.min.js"></script>




<!--contact js-->
<script src="public/complements/web/js/contact.js"></script>
<script src="public/complements/web/js/jquery.ajaxchimp.min.js"></script>
<script src="public/complements/web/js/jquery.form.js"></script>
<script src="public/complements/web/js/jquery.validate.min.js"></script>
<script src="public/complements/web/js/mail-script.js"></script>
<script src="public/complements/web/js/main.js"></script>

<!-- <link rel="stylesheet" type="text/css" href="v1/vendors/styles/core.css">
<script src="v1/vendors/scripts/core.js"></script> -->



<script type="text/javascript">
	$(".heart_mark").click(function() {
		var valor = $(this)[0].getAttribute("value");
		if (valor != null){
			$.ajax({
				type: 'GET',
				url: 'app/controllers/helpercontroller.php?AddLike',
				data: {'job_id': valor}
			}).done(function(){
				window.location.reload();
			});
		} else {
			// $("#myModal").modal("show");
		}
	});
</script>

<script>
	$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
})
</script>



<div class="modal" tabindex="-1" role="dialog"  id="myModal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			<div class="modal-header">
				<center>
					<h5 class="modal-title">No has iniciado sesión</h5>
				</center>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<center>
					<style>
						#logoModal{
							max-width: 20%;
    						height: 20%;
							padding-bottom: 7%;
						}
					</style>
				<img  src="images/default.png" id="logoModal" alt="logo" width="30%" height="30%" />
			
				</center>
				<div class="row">

					<div class="col-6">
						<a type="submit" class="btn btn-primary btn-sm btn-block"  href="login.php">Iniciar Sesión</a>
					</div>
					<div class="col-6">
						<a type="submit" class="btn btn-primary btn-sm btn-block" href="registro.php">Crear cuenta nueva</a>
					</div>
				</div>				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
			</div>
			</div>
		</div>
	</div>

	</html>