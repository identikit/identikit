<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  
  <!-- SEO Meta Tags -->
    <meta name="description" content="Somos la primera academia diseñada para resaltar la empleabilidad joven. Aplica ahora y mejora tu futuro.">
    <meta name="author" content="IDentiKIT.app ">

    <!-- OG Meta Tags to improve the way the post looks when you share the page on LinkedIn, Facebook, Google+ -->
    <meta property="og:site_name" content="IDentiKIT - Tu primera experiencia laboral" /> <!-- website name -->
    <meta property="og:site" content="https://identikit.app/" /> <!-- website link -->
    <meta property="og:title" content="¡Consigue tu primera experiencia laboral! - IDentiKIT.app"/> <!-- title shown in the actual shared post -->
    <meta property="og:description" content="Somos la primera academia diseñada para resaltar la empleabilidad joven. Aplica ahora y mejora tu futuro." />

     <!-- description shown in the actual shared post -->
    
    <meta property="og:url" content="https://identikit.app/" /> <!-- where do you want your post to link to -->
    <meta property="og:type" content="article" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   
    <meta property="og:image" content="http://identikit.app/landingv1/v3/images/logo-og.png" />
    <meta property="og:image:secure_url" content="https://identikit.app/landingv1/v3/images/logo-og.png" />
    <meta property="og:image:type" content="image/png" />
    <meta property="og:image:width" content="300" />
    <meta property="og:image:height" content="300" />

    <!-- Website Title -->
    <title>Términos y Condiciones - IDentiKIT.app</title>
    
    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700&display=swap&subset=latin-ext" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/fontawesome-all.css" rel="stylesheet">
    <link href="css/swiper.css" rel="stylesheet">
	<link href="css/magnific-popup.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">
	
	<!-- Favicon  -->
    <link rel="icon" href="images/logo.png">

    	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-K2NHRZT4R7"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'G-K2NHRZT4R7');
	</script>

	<!-- Google Tag Manager -->
	<script>
		(function(w, d, s, l, i) {
			w[l] = w[l] || [];
			w[l].push({
				'gtm.start': new Date().getTime(),
				event: 'gtm.js'
			});
			var f = d.getElementsByTagName(s)[0],
				j = d.createElement(s),
				dl = l != 'dataLayer' ? '&l=' + l : '';
			j.async = true;
			j.src =
				'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
			f.parentNode.insertBefore(j, f);
		})(window, document, 'script', 'dataLayer', 'GTM-5QGSH5J');
	</script>
	<!-- End Google Tag Manager -->



</head>
<body data-spy="scroll" data-target=".fixed-top">
  <!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QGSH5J" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
  
    <?php require_once "wsp.php";?>
        <!-- Preloader -->
        <div class="spinner-wrapper">
            <div class="spinner">
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>
        </div>
        <!-- end of preloader -->
        

        <!-- Navigation -->
       <?php include_once "nav.php";?>

    <!-- Header -->
    <header id="header" class="ex-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Términos y Condiciones</h1>
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </header> <!-- end of ex-header -->
    <!-- end of header -->


    <!-- Breadcrumbs -->
    <div class="ex-basic-1">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumbs">
                        <a href="index.php">Home</a><i class="fa fa-angle-double-right"></i><span>Términos y Condiciones</span>
                    </div> <!-- end of breadcrumbs -->
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of ex-basic-1 -->
    <!-- end of breadcrumbs -->


    <!-- Terms Content -->
    <div class="ex-basic-2">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-container">
                        <h3>SECCIÓN I. INFORMACIÓN GENERAL</h3>
                        <p>El portal IDentiKIT.app.com es propiedad de IDentiJobs S.A.S (En adelante IDentiKIT.app). El acceso, participación y uso del Portal está regido por los términos y condiciones que se incluyen a continuación, los cuales se entienden conocidos y aceptados por los usuarios del Portal (en adelante, el «Usuario») al acceder y usar el mismo. Este contrato aplica a cualquier aplicación, herramienta y servicios disponibles para ti, en esta página web.

                        El registro en nuestra página web implica la aceptación de los Términos y Condiciones, la política de privacidad, política de cookies y demás políticas aquí establecidas. Por favor lee esta página cuidadosamente. Si no estás de acuerdo con todos los términos y condiciones de este acuerdo, no deberías acceder a la página web o usar cualquiera de los Servicios.

                        Los Términos y Condiciones contenidos en esta página, así como la política de privacidad, política de cookies y demás políticas pueden ser modificadas periódicamente. Si se realiza algún cambio importante a los Términos y Condiciones o a la Política de Privacidad, te informaremos a través de nuestra página web. Las modificaciones en ningún momento serán retro activas. Si no estas de acuerdo con las modificaciones introducidas, puedes eliminar tu cuenta con IDentiKIT.app, así como puedes abstenerte de hacer uso de cualquiera de nuestras herramientas, aplicaciones, servicios.

                        IDentiKIT.app se reserva el derecho de expulsar Candidatos y Empresas o de prohibir su acceso futuro a los Servicios, herramientas y aplicaciones que se encuentran en la página web de IDentiKIT.app, por violación de estos Términos y Condiciones o de la ley aplicable.</p>

                        
                        
                    </div> <!-- end of text-container -->
                    
                    <div class="text-container">
                        <h3>SECCIÓN II. OBLIGACIONES</h3>
                        <p>Para utilizar los Servicios provistos por IDentiKIT.app en su página, web te comprometes a cumplir con las siguientes obligaciones:</p>

                        <ul class="list-unstyled li-space-lg indent">
                            <li class="media">
                                <i class="fas fa-square"></i>
                                <div class="media-body">Tener al menos 18 años</div>
                            </li>
                            <li class="media">
                                <i class="fas fa-square"></i>
                                <div class="media-body">Proveer información verídica a IDentiKIT.app</div>
                            </li>
                            <li class="media">
                                <i class="fas fa-square"></i>
                                <div class="media-body">Mantener tu contraseña de ingreso en secreto</div>
                            </li>
                            <li class="media">
                                <i class="fas fa-square"></i>
                                <div class="media-body">No compartir tu cuenta con ninguna otra persona</div>
                            </li>
                            <li class="media">
                                <i class="fas fa-square"></i>
                                <div class="media-body">Usar los servicios de forma profesional</div>
                            </li>
                            <li class="media">
                                <i class="fas fa-square"></i>
                                <div class="media-body">Realizar un uso de la información, herramientas, aplicativos que se encuentran en la página web y que son de propiedad intelectual de IDentiKIT.app, de manera que no se infrinja estos derechos de propiedad intelectual</div>
                            </li>
                            <li class="media">
                                <i class="fas fa-square"></i>
                                <div class="media-body">No incluir datos personales de carácter sensible en las hojas de vida, tales como aquellos datos relacionados con el origen étnico, la orientación política, convicciones religiosas, pertenencia a sindicatos, organizaciones sociales, datos relativos a la salud, la vida sexual, datos biométricos (dentro de los que se incluyen las fotos de los Candidatos), entre otros.</div>
                            </li>
                            <li class="media">
                                <i class="fas fa-square"></i>
                                <div class="media-body">No incluir en la página web de IDentiKIT.app material que vulnere derechos de propiedad intelectual de terceros.</div>
                            </li>
                            <li class="media">
                                <i class="fas fa-square"></i>
                                <div class="media-body">No incluir en la página web de IDentiKIT.app material que revele secretos industriales o comerciales, a menos que usted sea el propietario de los mismos o haya obtenido autorización del propietario.</div>
                                <li class="media">
                                    <i class="fas fa-square"></i>
                                    <div class="media-body">No incluir en la página web de IDentiKIT.app material que sea obsceno, difamatorio, abusivo, amenazante u ofensivo para cualquier otro usuario o cualquier otra persona o entidad.</div>
                                </li>
                                <li class="media">
                                    <i class="fas fa-square"></i>
                                    <div class="media-body">No incluir en la página web imágenes o material pornográfico, que incluyan sexo explícito o que sea considerada como pornografía infantil.</div>
                                </li>
                                <li class="media">
                                    <i class="fas fa-square"></i>
                                    <div class="media-body">No incluir en la página web publicidad o anuncios publicitarios sin la debida autorización de IDentiKIT.app.</div>
                                </li>
                                

                            </li>
                        </ul>

                    </div> <!-- end of text-container -->

                    <div class="text-container">
                        <h3>SECCIÓN III. OBLIGACIONES ADICIONALES PARA LAS EMPRESAS</h3>
                        <p>IDentiKIT.app permite que las Empresas interesadas registren sus vacantes de empleo en la página web de IDentiKIT.app. Una vez registras las vacantes, IDentiKIT.app recomienda los Candidatos que cumplan con el perfil requerido por la Empresa, para lo cual enviará la información de contacto del Candidato, así como la hoja de vida registrada por el Candidato. Cuando las Empresas registran sus vacantes en nuestra página web se comprometen a cumplir con las siguientes obligaciones:</p>
                        <p>Incluir información verídica respecto de la vacante</p>
                        <ul class="list-unstyled li-space-lg indent">
                            <li class="media">
                                <i class="fas fa-square"></i>
                                <div class="media-body">Cumplir con la Política de Privacidad de IDentiKIT.app</div>
                            </li>
                            <li class="media">
                                <i class="fas fa-square"></i>
                                <div class="media-body">Garantizar la seguridad de la información de los Candidatos que sea compartida por IDentiKIT.app a las Empresas cuando registren sus vacantes</div>
                            </li>
                            <li class="media">
                                <i class="fas fa-square"></i>
                                <div class="media-body">Utilizar la información de los Candidatos única y exclusivamente para efectos de contratar a la persona idónea para la vacante registrada en la página web de IDentiKIT.app</div>
                            </li>
                            <li class="media">
                                <i class="fas fa-square"></i>
                                <div class="media-body">No utilizar la información de los Candidatos para fines de mercadeo, realizar promociones, publicidad, venta de productos o servicios</div>
                            </li>
                            <li class="media">
                                <i class="fas fa-square"></i>
                                <div class="media-body">No almacenar información de los Candidatos, la Empresa se compromete a eliminar la información de los Candidatos una vez, la Empresa haya contratado a alguien para llenar la vacante publicada o en el momento en que el Candidato sea eliminado del proceso de selección</div>
                            </li>
                            <li class="media">
                                <i class="fas fa-square"></i>
                                <div class="media-body">Solicitar autorización expresa a los Candidatos para almacenar sus Datos cuando la Empresa pretenda mantener los datos de contacto del Candidato para futuras convocatorias de la Empresa</div>
                            </li>
                            <li class="media">
                                <i class="fas fa-square"></i>
                                <div class="media-body">Las Empresas actuarán como responsables de los Datos Personales que reciban por parte de IDentiKIT.app, de los Candidatos. Por lo cual, las Empresas están obligadas a dar cumplimiento a la normativa aplicable sobre Datos Personales, así como a la Política de Privacidad tanto de IDentiKIT.app, como de la Empresa.</div>
                            </li>
                        
                                

                            </li>
                        </ul>
                    </div> <!-- end of text-container -->

                    <div class="text-container">
                        <h3>SECCIÓN IV. USOS PROHIBIDOS DE LA PÁGINA WEB</h3>
                        <p>En adición a otras prohibiciones que puedan estar contenidas en estos Términos y Condiciones, se prohíbe el uso del sitio o su contenido para:</p>

                        <ul class="list-unstyled li-space-lg indent">
                            <li class="media">
                                <i class="fas fa-square"></i>
                                <div class="media-body">Suplantar a cualquier persona</div>
                            </li>
                            <li class="media">
                                <i class="fas fa-square"></i>
                                <div class="media-body">Cualquier propósito ilegal o ilícito</div>
                            </li>
                            <li class="media">
                                <i class="fas fa-square"></i>
                                <div class="media-body">Pedirle a otros que realicen o participen en actos ilícitos</div>
                            </li>
                            <li class="media">
                                <i class="fas fa-square"></i>
                                <div class="media-body">Violar cualquier regulación, reglas, leyes internacionales, federales, provinciales o estatales, u ordenanzas locales</div>
                            </li>
                            <li class="media">
                                <i class="fas fa-square"></i>
                                <div class="media-body">Utilizar cualquiera de los Servicios provistos en la página web para fines ilícitos, o para cualquier propósito que pueda violar cualquier ley en la jurisdicción que rigen estos términos o cualquier ley en tu jurisdicción.</div>
                            </li>
                            <li class="media">
                                <i class="fas fa-square"></i>
                                <div class="media-body">Utilizar la página web o cualquiera de los servicios para transmitir virus o cualquier código de naturaleza destructiva.</div>
                            </li>
                            <li class="media">
                                <i class="fas fa-square"></i>
                                <div class="media-body">Reproducir, duplicar, copiar, vender, revender o explotar cualquier parte del Servicio, uso del Servicio, o acceso al Servicio o cualquier contacto en el sitio web a través del cual se presta el servicio, sin el expreso permiso por escrito de nuestra parte.</div>
                            </li>
                            <li class="media">
                                <i class="fas fa-square"></i>
                                <div class="media-body">Infringir o violar el derecho de propiedad intelectual nuestro o de terceras partes.    </div>
                            </li>
                            <li class="media">
                                <i class="fas fa-square"></i>
                                <div class="media-body">Acosar, abusar, insultar, dañar, difamar, calumniar, desprestigiar, intimidar o discriminar por razones de género, orientación sexual, religión, etnia, raza, edad, nacionalidad o discapacidad.</div>
                            </li>
                            <li class="media">
                                <i class="fas fa-square"></i>
                                <div class="media-body">Presentar información falsa o engañosa.</div>
                            </li>
                            <li class="media">
                                <i class="fas fa-square"></i>
                                <div class="media-body">Cargar o transmitir virus o cualquier otro tipo de código malicioso que sea o pueda ser utilizado en cualquier forma que pueda comprometer la funcionalidad o el funcionamiento del Servicio o de cualquier sitio web relacionado, otros sitios o Internet</div>
                            </li>
                            <li class="media">
                                <i class="fas fa-square"></i>
                                <div class="media-body">Recopilar o rastrear información personal de otros de forma indebida</div>
                            </li>
                            <li class="media">
                                <i class="fas fa-square"></i>
                                <div class="media-body">Generar spam, pharm, pretext, spider, crawl, or scrape</div>
                            </li>
                            <li class="media">
                                <i class="fas fa-square"></i>
                                <div class="media-body">Interferir con o burlar los elementos de seguridad de nuestra página web o cualquier sitio web</div>
                            </li>
                            <li class="media">
                                <i class="fas fa-square"></i>
                                <div class="media-body">En general, para cualquier propósito obsceno o inmoral</div>
                            </li>


                        
                                

                            </li>
                        </ul>
                    </div> <!-- end of text-container -->
                    
                    <div class="text-container ">
                        <h3>SECCIÓN V. MODIFICACIONES AL SERVICIO Y PRECIO</h3>
                        <p>La prestación del Servicio para los usuarios se rige por el Contrato de Prestación de Servicios celebrado entre el Candidato y IDentiKIT.app, así como por las condiciones establecidas en estos Términos y Condiciones. Los precios de nuestros servicios están sujetos a cambio, sin previo aviso. No obstante, las modificaciones en los precios no serán retroactivas en ningún evento.</p>

                        <p>Nos reservamos el derecho de modificar o discontinuar el Servicio (o cualquier parte del contenido) en cualquier momento sin aviso previo. No seremos responsables ante ti o alguna tercera parte por cualquier modificación, cambio de precio, suspensión o discontinuidad del Servicio.</p>


                    </div> <!-- end of text-container -->

                    <div class="text-container ">
                        <h3>SECCIÓN VI. INFORMACIÓN DE LOS CANDIDATOS</h3>

                        <p>Cuando te registras en esta página web deberás suministrar a IDentiKIT.app cierta información, incluyendo, pero sin limitarse a: (i) una dirección válida de correo electrónico; (ii) número de celular; (iii) perfil de LinkedIn; (iv) nivel de educación; (v) nivel de estudios; (vi) hoja de vida.</p>

                        <p>IDentiKIT.app puede revelar a terceras personas cierta información contenida en su solicitud de registro. IDentiKIT.app no revelará a terceras personas su nombre, dirección, dirección de correo electrónico o número telefónico sin su consentimiento expresado a través de las diferentes herramientas o sistemas previstos en la página web, salvo en la medida en que sea necesario o apropiado para cumplir con las leyes aplicables o con procesos o procedimientos legales en los que tal información sea pertinente. IDentiKIT.app se reserva el derecho de ofrecer servicios o productos de IDentiKIT.app o de terceras personas, basados en tus preferencias. Tales ofertas podrán ser hechas por IDentiKIT.app o por terceras personas.</p>


                    </div> <!-- end of text-container -->


                    <div class="text-container ">
                        <h3>SECCIÓN VII. REGISTRO DE VACANTES</h3>

                        <p>Cualquier Empresa puede registrar sus vacantes en la página web de IDentiKIT.app para lo cual deberá ingresar en <a href="https://IDentiKIT.app/vacantes">https://IDentiKIT.app/vacantes</a> y diligenciar la siguiente información: (i) número de celular; (ii) nombre; (iii) correo electrónico; (iv) información de la vacante.</p>

                        <p>La información registrada debe ser verídica, es decir que debe corresponder a una vacante real de empleo de la Empresa.

                        Las Empresas se comprometen a verificar la veracidad de la información de los Candidatos que se hayan registrado en la página web de IDentiKIT.app. Las Empresas aceptan y entienden con el registro de su vacante en nuestra página web que IDentiKIT.app no tiene la obligación de verificar la información reportada por los Candidatos, así como su idoneidad para la vacante publicada.

                        Las Empresas deben realizar las verificaciones que consideren necesarias antes de contratar a un Candidato para llenar una de las Vacantes publicadas</p>


                    </div> <!-- end of text-container -->


                    <div class="text-container ">
                        <h3>SECCIÓN VIII. CUENTA Y CONTRASEÑA (PASSWORD)</h3>

                        <p>Los Candidatos son responsables por mantener la confidencialidad de la contraseña o password. El Candidato como usuario es responsable por todos los usos de su cuenta en nuestra página web, sean o no autorizados por el Candidato. En el evento en que se presente cualquier uso no autorizado de la cuenta de un Candidato, se debe notificar inmediatamente a IDentiKIT.app.</p>

                        <p>Los Candidatos podrán reestablecer su contraseña con el correo electrónico asociado con su cuenta de IDentiKIT.app.</p>


                    </div> <!-- end of text-container -->


                    <div class="text-container ">
                        <h3>SECCIÓN IX. DERECHOS DE PROPIEDAD INTELECTUAL</h3>

                        <p>IDentiKIT.app se reserva todos sus derechos de propiedad intelectual. Las marcas y los logotipos utilizados en relación con los Servicios y que se encuentran en la página web son marcas comerciales registradas.</p>

                       


                    </div> <!-- end of text-container -->



                    <div class="text-container ">
                        <h3>SECCIÓN X. RESPONSABILIDAD DE IDentiKIT.app</h3>

                        <p>IDentiKIT.app presta a los candidatos el servicio de asesoría y acompañamiento en los procesos de selección que adelante el Candidato en lo relacionado con el fortalecimiento de aptitudes necesarias para la búsqueda de trabajo. Adicionalmente, nuestra página web sirve como un lugar o escenario para que las Empresas interesadas registren sus Vacantes.

                        IDentiKIT.app no revisa ni censura las ofertas, bienes o servicios publicados. IDentiKIT.app no está involucrada ni se involucra en las contrataciones que realice las Empresas a Candidatos a través de nuestra página web. La selección de los Candidatos y Empresas es responsabilidad exclusiva de los usuarios.

                        IDentiKIT.app no tiene ni ejerce control alguno sobre la calidad, idoneidad, aptitudes de los Candidatos, ni de las vacantes publicadas en nuestra página web. IDentiKIT.app no tiene ni ejerce control alguno sobre la veracidad o exactitud de la información publicada por los usuarios en nuestra página web.

                        Ten en cuenta que existen riesgos, que incluyen, pero no se limitan al daño o lesiones físicas, al tratar con extraños, menores de edad o personas que actúan bajo falsas pretensiones o identidades. Tu asumes todos los riesgos asociados con tratar con otros usuarios con los cuales puedes entrar en contacto a través de nuestra página web.

                        El cumplimiento de los términos y condiciones de los contratos o acuerdos que los Usuarios llegaren a celebrar entre sí, es responsabilidad exclusiva de estos y no de IDentiKIT.app. Debido a que la verificación de la identidad de los usuarios de internet es difícil, IDentiKIT.app no puede confirmar y no confirma que cada usuario es quien dice ser. Debido a que IDentiKIT.app no se involucra en las relaciones o tratos entre sus usuarios ni controla el comportamiento de los usuarios o participantes en la página web, en el evento en que se genere una disputa con uno o más usuarios de la página web, los usuarios se comprometen a liberar a IDentiKIT.app, así como a sus empleados y agentes de cualquier reclamación, demanda o daño de cualquier naturaleza, que surja de o de cualquier otra forma se relacione con dicha disputa.

                        IDentiKIT.app no garantiza que la página web opere libre de errores o que la página web y su servidor se encuentre libre de virus de computadores u otros mecanismos dañinos. Si el uso de la página web, aplicaciones, herramientas o en general de cualquier material resulta en la necesidad de prestar servicio de reparación o mantenimiento a sus equipos o información o de reemplazar sus equipos o información, IDentiKIT.app no es responsable por los costos que ello implique. La página web, herramientas, aplicaciones y el material se ponen a disposición en el estado en que se encuentren. IDentiKIT.app no otorga garantía alguna sobre la exactitud, confiabilidad u oportunidad del material, los servicios, los textos, el software, las gráficas y los links o vínculos.

                        En ningún caso, IDentiKIT.app, sus proveedores o cualquier persona mencionada en la página web serán responsables por daños de cualquier naturaleza, resultantes del uso o la imposibilidad de usar la página web, aplicaciones, herramientas o el material.</p>


                    </div> <!-- end of text-container -->



                    <div class="text-container ">
                        <h3>SECCIÓN XI. EXENCIÓN Y LIMITACIÓN DE LA RESPONSABILIDAD</h3>

                        <p>IDentiKIT.app pone a disposición de los Candidatos una serie de servicios, herramientas y aplicaciones para el acompañamiento en los procesos de selección en los que participe en Candidato. De igual forma, IDentiKIT.app sugiere los Candidatos que mas se ajustan a las vacantes registradas por las Empresas. IDentiKIT.app no garantiza que las Vacantes registradas por las Empresas sean reales y correspondan a información verídica, así como tampoco puede garantizar que la información publicada en nuestra página web por parte de los Candidatos sea verídica.

                        No garantizamos ni aseguramos que el uso de nuestro servicio será ininterrumpido, puntual, seguro o libre de errores. No garantizamos que los resultados que se puedan obtener del uso del servicio serán exactos o confiables. Aceptas que el servicio puede presentar intermitencias por períodos de tiempo indefinidos o que el servicio puede ser cancelado en cualquier momento sin previo aviso.

                        Aceptas expresamente que el uso de el servicio es bajo tu propio riesgo. El servicio es proporcionado tal y como este disponible para su uso, sin ningún tipo de garantías expresas o implícitas.

                        Cuando adquieres el servicio con nosotros, entiendes que la obligación de IDentiKIT.app es únicamente de medio y no de resultado, por lo cual, no existe ninguna garantía de resultado por parte de IDentiKIT.app. En consecuencia, en ningún caso nuestros directores, funcionarios, empleados, afiliados, agentes, contratistas, internos, proveedores, prestadores de servicios o licenciantes serán responsables por cualquier tipo de daño o pérdida derivado de cualquier tipo de responsabilidad, como consecuencia del uso de cualquiera de los servicios o productos adquiridos mediante el servicio, o por cualquier otro reclamo relacionado de alguna manera con el uso del servicio o cualquier producto, incluyendo pero no limitado, a cualquier error u omisión en cualquier contenido, o cualquier pérdida o daño de cualquier tipo incurridos como resultados de la utilización del servicio o cualquier contenido (o producto) publicado, transmitido, o que se pongan a disposición a través del servicio, incluso si se avisa de su posibilidad. </p>

                        

                    </div> <!-- end of text-container -->


                    <div class="text-container ">
                        <h3>SECCIÓN XII. VERACIDAD DE LA INFORMACIÓN PUBLICADA POR LOS CANDIDATOS O LAS EMPRESAS</h3>
                        <p>Los Candidatos tienen la obligación de verificar la información de las Empresas que publican sus vacantes en IDentiKIT.app, así como de cualquier empresa en la que participen en un proceso de selección. De igual forma, las Empresas que publiquen sus vacantes en IDentiKIT.app deben verificar que la información publicada por los Candidatos sea veraz.

                        Tanto los Candidatos como las Empresas deben ser diligentes en la verificación de la información anteriormente mencionada. IDentiKIT.app no será responsable por cualquier daño que se derive de la falta de veracidad de la información suministrada por los Candidatos o las Empresas, así como por la falta de diligencia de los Candidatos o las Empresas al momento de verificar la información.

                        IDentiKIT.app no otorga garantía alguna, expresa o implícita, acerca de la veracidad, exactitud o confiabilidad de la información incluida en la página web por los Candidatos o Empresas ni apoya o respalda las opiniones expresadas por los usuarios.

                        IDentiKIT.app actúa como un medio para la distribución y publicación en Internet de información presentada por los Candidatos o Empresas y no tiene la obligación de revisar anticipadamente la veracidad de tal información, ni es responsable de revisar o monitorear la información incluida en la pápor los Candidatos o Empresas.

                        En el evento en IDentiKIT.app sea notificada por un Candidato o Empresa acerca de la existencia de información que no cumpla con estos Términos y Condiciones, IDentiKIT.app puede investigar tal información y determinar de buena fe y a su exclusiva discreción si remueve o elimina tal información. IDentiKIT.app se reserva el derecho de expulsar Candidatos y Empresas o de prohibir su acceso futuro a la página web por violación de estos Términos y Condiciones o de la ley aplicable.

                        Igualmente, IDentiKIT.app se reserva el derecho de eliminar de la página web información presentada o incluida por un Candidato o Empresa cuando lo considere apropiado o necesario a su exclusiva discreción.</p>

                    </div> <!-- end of text-container -->

                    <div class="text-container ">
                        <h3>SECCIÓN XIII. POLÍTICA Y AVISO DE PRIVACIDAD</h3>
                        <p>En el enlace <a href="https://IDentiKIT.app/privacy-policy.html"> https://IDentiKIT.app/privacy-policy.html</a> se encuentra nuestra política de privacidad y en el enlace (https://IDentiKIT.app/privacy-policy.html) se encuentra nuestro aviso de privacidad para el tratamiento de Datos Personales que se acepta previa y expresamente por los Usuarios de la página web, al momento del Registro para las finalidades que allí se informan de manera previa, expresa e informada.

                        En adición a la Política de Privacidad, tu aceptas que IDentiKIT.app puede revelar a terceras personas, cierta información contenida en tu solicitud de registro, con el fin conectar Candidatos con las Empresas que publican sus vacantes en IDentiKIT.app. Nosotros no revelaremos a terceras personas tu nombre, dirección, correo electrónico, número telefónico, sin tu consentimiento previo que puede ser otorgado a través de las diferentes herramientas, aplicaciones, sistemas de nuestra página web, excepto la entrega de los datos se ajuste a la normativa de datos personales o se realiza para cumplir con una obligación legal.

</p>

                    </div> <!-- end of text-container -->

                    <div class="text-container ">
                        <h3>SECCIÓN XIV. CONFIDENCIALIDAD</h3>
                        <p>Los usuarios de esta página web pueden recibir información confidencial por lo que se obligan a mantener tal información privada en absoluta confidencialidad y deberán realizar todos los esfuerzos que sean razonables para proteger la privacidad de esta información; so pena de las acciones legales que se puedan adelantar en contra de la Parte que incumpla.

                        La información que reciban las Empresas respecto de los Candidatos es de carácter confidencial por contener datos personales, por lo cual su tratamiento deberá realizarse de conformidad con nuestra Política de Privacidad.</p>

                    </div> <!-- end of text-container -->

                    <div class="text-container ">
                        <h3>SECCIÓN XV. PROHIBICIÓN DE REVENTA, CESIÓN O USO COMERCIAL NO AUTORIZADO</h3>
                        <p>Tu como usuario de esta página web acuerdas no revender o ceder tus derechos y obligaciones bajo estos Términos y Condiciones. Los usuarios no pueden hacer un uso comercial no autorizado de la página web de IDentiKIT.app.</p>

                    </div> <!-- end of text-container -->

                    <div class="text-container ">
                        <h3>SECCIÓN XVI. ENLACES A OTRAS PÁGINAS WEBS</h3>
                        <p>Esta página web contiene enlaces a páginas web de terceros. Estos enlaces se suministran para tu conveniencia únicamente y IDentiKIT.app no respalda, recomienda o asume responsabilidad alguna sobre el contenido de estas páginas webs de terceros. Cuando decides acceder a estas páginas webs, lo haces bajo tu propio riesgo y responsabilidad. De igual forma, debes realizar el acceso a estas páginas webs respetando los términos y condiciones de cada una de estas páginas.</p>

                    </div> <!-- end of text-container -->

                    

                    <div class="text-container ">
                        <h3>SECCIÓN XVII. INTEGRIDAD Y DIVISIBILIDAD DE LOS TÉRMINOS Y CONDICIONES</h3>
                        <p>En el caso de que se determine que cualquier disposición de estos Términos y Condiciones sea ilegal, nula o inejecutable, dicha disposición se considerará separada de estos Términos de Servicio, dicha determinación no afectará la validez de aplicabilidad de las demás disposiciones restantes.</p>

                    </div> <!-- end of text-container -->

                    <div class="text-container ">
                        <h3>SECCIÓN XVIII. CLÁUSULA DE INDEMNIDAD</h3>
                        <p>Aceptas mantener indemne IDentiKIT.app así como a nuestras matrices, subsidiarias, afiliados, socios, funcionarios, directores, agentes, contratistas, concesionarios, proveedores de servicios, subcontratistas, proveedores, internos y empleados, de cualquier reclamo o demanda, incluyendo honorarios razonables de abogados, hechos por cualquier tercero a causa o como resultado de tu incumplimiento de los Términos y Condiciones o de los documentos que incorporan como referencia, o la violación de cualquier ley o de los derechos de un tercero.
</p>

                    </div> <!-- end of text-container -->

                    <div class="text-container ">
                        <h3>SECCIÓN XIX. FINALIZACIÓN DEL CONTRATO</h3>
                        <p>Cualquiera de las partes de este Contrato podrá darlo por terminado en cualquier momento, tras comunicarlo a la otra parte. Una vez terminado el Contrato perderás el derecho a acceder a nuestra página web y utilizar nuestros servicios.

                        IDentiKIT.app se reserva el derecho, a su exclusiva discreción, de borrar toda la información que hayas incluido en nuestra página web y de terminar inmediatamente tu cuenta y acceso a la página web o a determinados servicios proveídos por IDentiKIT.app, ante el incumplimiento por su parte de estos Términos y Condiciones o ante la imposibilidad de verificar o autenticar cualquier información que hayas presentado en tu formulario de registro para acceder a la página web.
</p>

                    </div> <!-- end of text-container -->

                    <div class="text-container last">
                        <h3>SECCIÓN XX. LEGISLACIÓN APLICABLE Y RESOLUCIÓN DE CONFLICTOS</h3>
                        <p>Estos Términos y Condiciones están regidos por las leyes de la República de Colombia, sin dar aplicación a las normas o principios sobre conflicto de leyes. La jurisdicción para cualquier reclamación que surja de estos Términos y Condiciones será exclusivamente la de los tribunales y jueces de la República de Colombia. Si alguna previsión de estos Términos y Condiciones es declarada nula, inválida o ineficaz, ello no afectará la validez de las restantes previsiones de estos Términos y Condiciones.</p>
                        <a class="btn-outline-reg" href="index.php">BACK</a>

                    </div> <!-- end of text-container -->

                   
                </div>
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of ex-basic -->
    <!-- end of terms content -->


   <?php include_once "footer.php";?>

    <!-- Scripts -->
    <script src="js/jquery.min.js"></script> <!-- jQuery for Bootstrap's JavaScript plugins -->
    <script src="js/popper.min.js"></script> <!-- Popper tooltip library for Bootstrap -->
    <script src="js/bootstrap.min.js"></script> <!-- Bootstrap framework -->
    <script src="js/jquery.easing.min.js"></script> <!-- jQuery Easing for smooth scrolling between anchors -->
    <script src="js/swiper.min.js"></script> <!-- Swiper for image and text sliders -->
    <script src="js/jquery.magnific-popup.js"></script> <!-- Magnific Popup for lightboxes -->
    <script src="js/validator.min.js"></script> <!-- Validator.js - Bootstrap plugin that validates forms -->
    <script src="js/scripts.js"></script> <!-- Custom scripts -->
</body>
</html>