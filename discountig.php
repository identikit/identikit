 <!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    

     <!-- description shown in the actual shared post -->
	
    <meta property="og:url" content="https://identikit.app/" /> <!-- where do you want your post to link to -->
	<meta property="og:type" content="article" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   
    


    <!-- Website Title -->
    <title>Descuento en IDentiKIT.app 🔥 - ¡Consigue tu primera experiencia laboral!</title>
    
    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700&display=swap&subset=latin-ext" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/fontawesome-all.css" rel="stylesheet">
    <link href="css/swiper.css" rel="stylesheet">
	<link href="css/magnific-popup.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">
	
	<!-- Favicon  -->
    <link rel="icon" href="images/logo.png">

   <!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-K2NHRZT4R7"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'G-K2NHRZT4R7');
	</script>

	<!-- Google Tag Manager -->
	<script>
		(function(w, d, s, l, i) {
			w[l] = w[l] || [];
			w[l].push({
				'gtm.start': new Date().getTime(),
				event: 'gtm.js'
			});
			var f = d.getElementsByTagName(s)[0],
				j = d.createElement(s),
				dl = l != 'dataLayer' ? '&l=' + l : '';
			j.async = true;
			j.src =
				'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
			f.parentNode.insertBefore(j, f);
		})(window, document, 'script', 'dataLayer', 'GTM-5QGSH5J');
	</script>
	<!-- End Google Tag Manager -->


</head>
<body data-spy="scroll" data-target=".fixed-top">

        <!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QGSH5J" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

     <?php require_once "wsp.php";?>
     <!-- Preloader -->
     <div class="spinner-wrapper">
         <div class="spinner">
             <div class="bounce1"></div>
             <div class="bounce2"></div>
             <div class="bounce3"></div>
         </div>
     </div>
     <!-- end of preloader -->
     

     <!-- Navigation -->
    <?php include_once "nav.php";?>

    <!-- Header -->


   <header id="header" class="ex-2-header">


       <div class="container">
           <div class="row g-2">
               <div class="col-lg-12">
                  
                   <div class="col-md-12 form-container2">
                       
                       

                    <form class="row g-3" action="checkout-discountig.php" method="POST">
                      <div class="col-md-6">
                        <label class="form-label">Nombre</label>
                        <input type="text" class="form-control"  name="name" required>
                      </div>
                      <div class="col-md-6">
                        <label class="form-label">Apellido</label>
                        <input type="text" class="form-control"  name="surname" required>
                      </div>
                      <div class="col-md-6">
                        <label class="form-label">Email</label>
                        <input type="email" class="form-control" name="email" required>
                        <small>(Lo vamos a usar para darte acceso)</small>
                      </div>
                      <div class="col-md-6">
                        <label class="form-label">Teléfono </label>
                        <input type="text" class="form-control" name="cellphone" required>
                        <small>(Te vamos a contactar solo si es necesario)</small>
                      </div>
                      <div class="col-md-6">
                        <label class="form-label">DNI</label>
                        <input type="text" class="form-control" name="identification" required>
                      </div>

                      <div class="col-md-6">
                        <label class="form-label">Provincia</label>
                        <select required class="selectpicker show-tick form-control" name="province">
                                                            
                                <option value="Buenos Aires">Buenos Aires</option>
                                <option value="Buenos Aires Capital">Buenos Aires Capital</option>
                                <option value="Catamarca">Catamarca</option>
                                <option value="Chaco">Chaco</option>
                                <option value="Chubut">Chubut</option>
                                <option value="Cordoba">Cordoba</option>
                                <option value="Corrientes">Corrientes</option>
                                <option value="Entre Rios">Entre Rios</option>
                                <option value="Formosa">Formosa</option>
                                <option value="Jujuy">Jujuy</option>
                                <option value="La Pampa">La Pampa</option>
                                <option value="La Rioja">La Rioja</option>
                                <option value="Mendoza">Mendoza</option>
                                <option value="Misiones">Misiones</option>
                                <option value="Neuquen">Neuquen</option>
                                <option value="Rio Negro">Rio Negro</option>
                                <option value="Salta">Salta</option>
                                <option value="San Juan">San Juan</option>
                                <option value="San Luis">San Luis</option>
                                <option value="Santa Cruz">Santa Cruz</option>
                                <option value="Santa Fe">Santa Fe</option>
                                <option value="Santiago del Estero">Santiago del Estero</option>
                                <option value="Tierra del Fuego">Tierra del Fuego</option>
                                <option value="Tucuman">Tucuman</option>

                               </select>
                      </div>

                      <div class="col-md-12">
                        <label class="form-label">¿Tu nivel de estudios?</label>
                        <select  required class="selectpicker show-tick form-control" name="study" >
                                
                                
                                <option value="A">Ninguno</option>
                                <option value="B">Secundaria en curso</option>
                                <option value="C">Secundaria completa</option>
                                <option value="D">Secundaria incompleta</option>
                                <option value="E">Cursando universidad/terciario</option>

                               </select>
                      </div>

                      
                    
                      <div class="col-12">
                        <br>
                        <button type="submit" class="form-control-submit-button">Continuar</button>
                      </div>
                      <div class="form-message">
                          <div id="lmsgSubmit" class="h3 text-center hidden"></div>
                      </div>

                    </form>
                       
                   </div> <!-- end of form container -->
                   <!-- end of sign up form -->

               </div> <!-- end of col -->
           </div> <!-- end of row -->
       </div> <!-- end of container -->
   </header> <!-- end of ex-header -->


    
   <div class="footer">
       <div class="container">
           <div class="row">
               <div class="col-md-4">
                   <div class="footer-col first">
                       <h4>About IDentiKIT.app</h4>
                       <p class="p-small">IDentiKIT es una plataforma que busca resaltar la empleabilidad joven de una forma fácil, rápida y segura.</p>
                   </div>
               </div> <!-- end of col -->
               <div class="col-md-4">
                   <div class="footer-col middle">
                       <h4>Links</h4>
                       <ul class="list-unstyled li-space-lg p-small">
                           <li class="media">
                               <i class="fas fa-square"></i>
                               <div class="media-body"><a class="white" href="empresas.php">Empresas</a></div>
                           </li>
                           <li class="media">
                               <i class="fas fa-square"></i>
                               <div class="media-body"><a class="white" href="vacantes.php">Vacantes</a></div>
                           </li>
                            <li class="media">
                               <i class="fas fa-square"></i>
                               <div class="media-body"><a class="white" href="terms-conditions.php">Términos y Condiciones</a></div>
                           </li>
                            <li class="media">
                               <i class="fas fa-square"></i>
                               <div class="media-body"><a class="white" href="privacy-policy.php">Políticas de privacidad</a></div>
                           </li>
                       </ul>
                   </div>
               </div> <!-- end of col -->
               <div class="col-md-4">
                   <div class="footer-col last">
                       <h4>Contact</h4>
                       <ul class="list-unstyled li-space-lg p-small">
                           <li class="media">
                               <i class="fas fa-map-marker-alt"></i>
                               <div class="media-body">Argentina</div>
                           </li>
                           <li class="media">
                               <i class="fas fa-envelope"></i>
                               <div class="media-body"><a class="white" href="mailto:contacto@IDentiKIT.app">contacto@IDentiKIT.app</a> <i class="fas fa-globe"></i><a class="white" href="#home">IDentiKIT.app</a></div>
                           </li>
                       </ul>
                   </div> 
               </div> <!-- end of col -->
           </div> <!-- end of row -->
       </div> <!-- end of container -->
   </div> <!-- end of footer -->  
   <!-- end of footer -->
    
    	
    <!-- Scripts -->
    <script src="js/jquery.min.js"></script> <!-- jQuery for Bootstrap's JavaScript plugins -->
    <script src="js/popper.min.js"></script> <!-- Popper tooltip library for Bootstrap -->
    <script src="js/bootstrap.min.js"></script> <!-- Bootstrap framework -->
    <script src="js/jquery.easing.min.js"></script> <!-- jQuery Easing for smooth scrolling between anchors -->
    <script src="js/swiper.min.js"></script> <!-- Swiper for image and text sliders -->
    <script src="js/jquery.magnific-popup.js"></script> <!-- Magnific Popup for lightboxes -->
    <script src="js/validator.min.js"></script> <!-- Validator.js - Bootstrap plugin that validates forms -->
    <script src="js/scripts.js"></script> <!-- Custom scripts -->
    <script src="js_whatsapp.js" type="text/javascript"></script>
    
</body>

</html>