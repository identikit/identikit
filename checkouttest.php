 <?php
     require_once 'extensiones/vendor/autoload.php';

     MercadoPago\SDK::setAccessToken("TEST-6892168716582318-040702-213401f90493fbe976374f2891310b10-1100345710");
     
     $preference = new MercadoPago\Preference();

     $item              = new MercadoPago\Item(); 
     $item->id          = '01';
     $item->title       = 'IDentiKIT.app';
     $item->quantity    = 1;
     $item->unit_price  = 50;
     $item->currency_id = "ARS";

     $preference->items = array($item); 

     $name = $_POST['name'];
     $surname = $_POST['surname'];
     $email = $_POST['email'];
     $cellphone = $_POST['cellphone'];
     $identification = $_POST['identification'];
     $province = $_POST['province'];
     $study = $_POST['study'];

     $preference->items = array($item); 
     $preference->back_urls = array(
        "success" => "https://identikit.app/success.php?name="
                    .$name."&surname=".$surname."&email=".$email."&cellphone="
                    .$cellphone."&identification=".$identification."&province="
                    .$province."&study=".$study."&ItemId=".$item->id."&currency="
                    .$item->currency_id."&price=".$item->unit_price."&quantity="
                    .$item->quantity,

        "failure" => "https://identikit.app/fail.php"
    );

    $preference->save();


 ?>

 <!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <!-- SEO Meta Tags -->
    <meta name="description" content="">


   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta http-equiv="Content-Security-Policy" content="default-src *; style-src 'self' 'unsafe-inline'; script-src 'self' 'unsafe-inline' 'unsafe-eval' https://sdk.mercadopago.com/js/v2 https://http2.mlstatic.com/storage/event-metrics-sdk/js https://www.mercadopago.com.ar/">


    
    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700&display=swap&subset=latin-ext" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/fontawesome-all.css" rel="stylesheet">
    <link href="css/swiper.css" rel="stylesheet">
	<link href="css/magnific-popup.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

	
	<!-- Favicon  -->
    <link rel="icon" href="images/logo.png">



</head>
<body data-spy="scroll" data-target=".fixed-top">

    <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PHBQLSG"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->

    <!-- Start of HubSpot Embed Code -->
      <script type="text/javascript" id="hs-script-loader" async defer src="//js-na1.hs-scripts.com/21241785.js"></script>
    <!-- End of HubSpot Embed Code -->
    
    <!-- Preloader -
	<div class="spinner-wrapper">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>
   +-- end of preloader -->
    

    <?php include_once "nav.php";?>

    <?php include_once "wsp.php";?>


    <!-- Header -->


   <header id="header" class="ex-2-header">


       <div class="container">


           <div class="row">
               <div class="col-lg-12">
                  
                   <div class="col-md-12 form-container2">
                       <form>

                          
                           
                           <div class="text-container">
                               <div class="row">
                                   <div class="col-md-6">
                                       <h3>Resumen de compra</h3>
                                       <p style="color:black;">Hola <b><?php echo $name?></b> estas a punto dar un salto en tu futuro
                                        laboral y personal.</p>
                                        <p style="color: black;">
                                            Fecha: <b>Del 3 de Mayo al 20 de Mayo</b><br>
                                            Fecha: <b>Lunes y Viernes 19:30 a 21:30hs</b><br>
                                            Modalidad: <b>online en vivo</b><br>
                                            <b>¡Te enviaremos toda la info por email!</b>

                                       </p>

                                      
                                           
                                           Curso (IDentiLaunch) <small><del>$20.000 ARS</del></small> $2.599 ARS<br>
                                           Total $2.599 ARS

                                       </p>
                                      
                                   </div> <!-- end of col -->
                                   <div class="col-md-6">
                                       <div class="image-container-small">
                                           <img class="img-fluid" src="images/checkout.gif" alt="alternative" style="border-radius: 0.5rem;">
                                       </div> <!-- end of image-container-small -->
                                   </div> <!-- end of col -->
                               </div> <!-- end of row -->
                           </div> <!-- end of text-container -->
                           
                          

                          <br>

                        

                           
<br>

                           <div class="form-message">
                               <div id="lmsgSubmit" class="h3 text-center hidden"></div>
                           </div>
                       </form>

                       <div class="cho-container " style="width: 64.5rem;"></div>

                           <script src="https://sdk.mercadopago.com/js/v2"></script>
                           <script>
                               const mp = new MercadoPago('TEST-4d81a92c-97c8-4f48-a608-00caa4765327',  {
                                   locale: "es-AR",
                               });
                             
                               mp.checkout({
                                   preference: {
                                       id: "<?php echo $preference->id; ?>",
                                   },
                                   render: {
                                       container: ".cho-container", // Indica el nombre de la clase donde se mostrará el botón de pago
                                       label: "Continuar con el pago", // Cambia el texto del botón de pago (opcional)
                                   },
                               });   
                           </script>

                            <style>
                                .mercadopago-button {
                                    background-color: #0070ff;
                                    height: 3.125rem;
                                    font: 700 0.875rem/0 "Open Sans", sans-serif;
                                    width: 100%;
                                }
                            </style>
                   </div> <!-- end of form container -->
                   <!-- end of sign up form -->

               </div> <!-- end of col -->
           </div> <!-- end of row -->
       </div> <!-- end of container -->
   </header> <!-- end of ex-header -->


    


  <div class="footer">
      <div class="container">
          <div class="row">
              <div class="col-md-4">
                  <div class="footer-col first">
                      <h4>About IDentiKIT.app</h4>
                      <p class="p-small">IDentiKIT es una plataforma que busca resaltar la empleabilidad joven de una forma fácil, rápida y segura. </p>
                  </div>
              </div> <!-- end of col -->
              <div class="col-md-4">
                  <div class="footer-col middle">
                      <h4>Links</h4>
                      <ul class="list-unstyled li-space-lg p-small">
                          <li class="media">
                              <i class="fas fa-square"></i>
                              <div class="media-body"><a class="white" href="empresas.php">Empresas</a></div>
                          </li>
                          <li class="media">
                              <i class="fas fa-square"></i>
                              <div class="media-body"><a class="white" href="vacantes.php">Vacantes</a></div>
                          </li>
                           <li class="media">
                              <i class="fas fa-square"></i>
                              <div class="media-body"><a class="white" href="terms-conditions.php">Términos y Condiciones</a></div>
                          </li>
                           <li class="media">
                              <i class="fas fa-square"></i>
                              <div class="media-body"><a class="white" href="privacy-policy.php">Políticas de privacidad</a></div>
                          </li>
                      </ul>
                  </div>
              </div> <!-- end of col -->
              <div class="col-md-4">
                  <div class="footer-col last">
                      <h4>Contact</h4>
                      <ul class="list-unstyled li-space-lg p-small">
                          <li class="media">
                              <i class="fas fa-map-marker-alt"></i>
                              <div class="media-body">Argentina</div>
                          </li>
                          <li class="media">
                              <i class="fas fa-envelope"></i>
                              <div class="media-body"><a class="white" href="mailto:contacto@IDentiKIT.app">contacto@IDentiKIT.app</a> <i class="fas fa-globe"></i><a class="white" href="#home">IDentiKIT.app</a></div>
                          </li>
                      </ul>
                  </div> 
              </div> <!-- end of col -->
          </div> <!-- end of row -->
      </div> <!-- end of container -->
  </div> <!-- end of footer -->  
  <!-- end of footer -->


    <!-- Copyright -->
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <p class="p-small">Copyright © 2022 <a href="https://IDentiKIT.app/">IDentiKIT.app</a> Todos los derechos reservados<br>
                    </p>
                </div> <!-- end of col -->
            </div> <!-- enf of row -->
        </div> <!-- end of container -->
    </div> <!-- end of copyright --> 
    <!-- end of copyright -->
    
    	
    <!-- Scripts -->
    <script src="js/jquery.min.js"></script> <!-- jQuery for Bootstrap's JavaScript plugins -->
    <script src="js/popper.min.js"></script> <!-- Popper tooltip library for Bootstrap -->
    <script src="js/bootstrap.min.js"></script> <!-- Bootstrap framework -->
    <script src="js/jquery.easing.min.js"></script> <!-- jQuery Easing for smooth scrolling between anchors -->
    <script src="js/swiper.min.js"></script> <!-- Swiper for image and text sliders -->
    <script src="js/jquery.magnific-popup.js"></script> <!-- Magnific Popup for lightboxes -->
    <script src="js/validator.min.js"></script> <!-- Validator.js - Bootstrap plugin that validates forms -->
    <script src="js/scripts.js"></script> <!-- Custom scripts -->
    <script src="js_whatsapp.js" type="text/javascript"></script>

    <script src="https://sdk.mercadopago.com/js/v2"></script>

</body>
</html>